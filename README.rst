..
  SPDX-License-Identifier: CC-BY-SA-4.0+

  SPDX-FileCopyrightText: 2019 CERN

===========
Mock Turtle
===========

Mock Turtle is a framework to develop embedded systems on FPGA.
For more information, please read the documentation in ``doc/``.

Documentation Build Instructions
================================

This project uses `Sphinx <http://www.sphinx-doc.org/en/master/>`_ to generate
documentation from `reStructuredText <http://docutils.sourceforge.net/rst.html>`_
and `CommonMark <https://commonmark.org/>`_ (Markdown) files under ``doc/``.

To build the documentation, it is highly recommended to setup a
`Python virtual environment <https://virtualenv.pypa.io/en/stable/>`_ where
the necessary packages (docuilts, sphinx, etc.) can be installed via
`pip <https://pypi.org/project/pip/>`_ and be kept at a specific version.

The following steps illustrate how to do this on a Debian/Ubuntu Linux box,
with the virtual environment placed inside the ``doc/`` folder of the project
itself:::

  sudo apt install virtualenv
  cd doc
  virtualenv build_env
  . build_env/bin/activate
  pip install -r requirements.txt
  deactivate

**Note:** If you use the same folder name and location (``doc/build_env``) for
the virtual environment as in the above example, there is already a gitignore
rule in place that will not track any auto-generated files within that folder.

Once the environment is installed, you can (re)build the documentation by
doing:::

  cd doc
  . build_env/bin/activate
  make html
  deactivate

The generated documentation can be accessed by opening
``doc/_build/html/index.html`` in your browser.

Alternatively, if you have `LaTeX <https://www.latex-project.org/>`_ installed,
you can produce a PDF by doing:::

  cd doc
  . build_env/bin/activate
  make latexpdf
  deactivate

The generated documentation can be accessed by opening the PDF found under
``doc/_build/latex/``.

**Note:** Only HTML and PDF outputs from Sphinx are supported and tested.
