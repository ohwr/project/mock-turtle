# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

files = [
    "svec_mt_demo.vhd",
]

modules = {
    "local" : [
        "../../rtl",
    ],
}
