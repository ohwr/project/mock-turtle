# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

board  = "spec"
action = "synthesis"
target = "xilinx"

syn_device  = "xc6slx45t"
syn_grade   = "-3"
syn_package = "fgg484"
syn_top     = "spec_mt_demo"
syn_project = "spec_mt_demo.xise"
syn_tool    = "ise"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"

files = [
    "spec_mt_demo.ucf",
    "buildinfo_pkg.vhd",
    "sourceid_{}_pkg.vhd".format(syn_top),
]

modules = {
    "local" : [
        "../../top/spec_mt_demo",
    ],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/gn4124-core.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
        "https://ohwr.org/project/spec.git",
    ],
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass


try:
    exec(open(fetchto + "/general-cores/tools/gen_sourceid.py").read(),
         None, {'project': syn_top})
except Exception as e:
    print("Error: cannot generate source id file")
    raise

syn_post_project_cmd = "$(TCL_INTERPRETER) syn_extra_steps.tcl $(PROJECT_FILE)"

# only use the common ucf
spec_base_ucf = []

# needs to be defined, even if not used
ctrls = ["bank3_64b_32b"]
