# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

files = [
    "mt_ethernet_tx_framer.vhd",
    "mt_rmq_packet_output.vhd",
    "mt_rmq_rx_deframer.vhd",
    "mt_rmq_rx_path.vhd",
    "mt_rmq_tx_path.vhd",
    "mt_rmq_stream_register.vhd",
    "mt_udp_tx_framer.vhd",
    "mt_wr_sink.vhd",
    "mt_wr_source.vhd",
    "mt_rmq_endpoint_tx.vhd",
    "mt_rmq_endpoint_rx.vhd",
    "mt_rmq_ethernet_endpoint.vhd",
    "mt_ep_ethernet_single.vhd",
    "mt_endpoint_pkg.vhd"
];
