-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_endpoint_pkg
--
-- description: Global package for the UDP/Ethernet RMQ endpoint.

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;

package mt_endpoint_pkg is

  constant c_rmq_data_start_offset : integer := 32;

  type t_rmq_ep_tx_config is record
    dst_mac      : std_logic_vector(47 downto 0);
    src_ip       : std_logic_vector(31 downto 0);
    dst_ip       : std_logic_vector(31 downto 0);
    src_port     : std_logic_vector(15 downto 0);
    dst_port     : std_logic_vector(15 downto 0);
    ethertype    : std_logic_vector(15 downto 0);
    vlan_id      : std_logic_vector(15 downto 0);
    is_udp       : std_logic;
    vlan_en      : std_logic;
    payload_size : std_logic_vector(15 downto 0);
  end record;

  type t_rmq_ep_rx_config is record
    dst_mac          : std_logic_vector(47 downto 0);
    dst_ip           : std_logic_vector(31 downto 0);
    dst_port         : std_logic_vector(15 downto 0);
    ethertype        : std_logic_vector(15 downto 0);
    vlan_id          : std_logic_vector(11 downto 0);
    filter_dst_mac   : std_logic;
    filter_dst_ip    : std_logic;
    filter_dst_port  : std_logic;
    filter_ethertype : std_logic;
    filter_udp       : std_logic;
    filter_raw       : std_logic;
    vlan_en          : std_logic;  -- Require vlan_id if set.
    vlan_discard     : std_logic;  -- accept any vlan and no vlan
    enable           : std_logic;  -- discard all packets if 0
  end record;

  type t_rmq_ep_rx_header is record
    is_udp     : std_logic;
    is_raw     : std_logic;
    src_mac    : std_logic_vector(47 downto 0);
    dst_mac    : std_logic_vector(47 downto 0);
    vlan_id    : std_logic_vector(11 downto 0);  -- 0 is none
    ethertype  : std_logic_vector(15 downto 0);
    src_port   : std_logic_vector(15 downto 0);
    dst_port   : std_logic_vector(15 downto 0);
    src_ip     : std_logic_vector(31 downto 0);
    dst_ip     : std_logic_vector(31 downto 0);
    udp_length : std_logic_vector(15 downto 0);
  end record;

  type t_rmq_ep_tx_slot_config_array is
    array(integer range<>) of t_rmq_ep_tx_config;
end mt_endpoint_pkg;
