-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_stream_register
--
-- description: Remote MQ: exchanges messages between CPU CBs in remote
-- nodes.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_rmq_stream_register is
  port (
    clk_i   : in  std_logic;
    rst_n_i : in  std_logic;
    snk_i   : in  t_mt_stream_sink_in;    -- 32-bit data
    snk_o   : out t_mt_stream_sink_out;
    src_i   : in  t_mt_stream_source_in;  -- 16-bit data
    src_o   : out t_mt_stream_source_out);
end entity mt_rmq_stream_register;

architecture arch of mt_rmq_stream_register is

  signal tmp_src_out : t_mt_stream_source_out;

  signal src_out : t_mt_stream_source_out;

  signal src_valid_next : std_logic;
  signal tmp_valid_next : std_logic;
  signal ready_early    : std_logic;

  signal input_to_output : std_logic;
  signal input_to_temp   : std_logic;
  signal temp_to_output  : std_logic;

  signal ready_reg : std_logic;

begin

  ready_early <= src_i.ready or (not tmp_src_out.valid and (not src_out.valid or not snk_i.valid));

  p_cmb : process (src_out, tmp_src_out, ready_reg, snk_i, src_i)
  begin
    src_valid_next <= src_out.valid;
    tmp_valid_next <= tmp_src_out.valid;

    input_to_output <= '0';
    input_to_temp   <= '0';
    temp_to_output  <= '0';

    if ready_reg = '1' then
      if src_i.ready = '1' or src_out.valid = '0' then
        src_valid_next  <= snk_i.valid;
        input_to_output <= '1';
      else
        tmp_valid_next <= snk_i.valid;
        input_to_temp  <= '1';
      end if;
    elsif src_i.ready = '1' then
      src_valid_next <= tmp_src_out.valid;
      tmp_valid_next <= '0';
      temp_to_output <= '1';
    end if;
  end process p_cmb;

  p_seq : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        ready_reg         <= '0';
        src_out.valid     <= '0';
        tmp_src_out.valid <= '0';
      else
        ready_reg         <= ready_early;
        src_out.valid     <= src_valid_next;
        tmp_src_out.valid <= tmp_valid_next;
        if input_to_output = '1' then
          src_out.data  <= snk_i.data;
          src_out.last  <= snk_i.last;
          src_out.error <= snk_i.error;
          src_out.hdr <= snk_i.hdr;
        elsif temp_to_output = '1' then
          src_out.data  <= tmp_src_out.data;
          src_out.last  <= tmp_src_out.last;
          src_out.error <= tmp_src_out.error;
          src_out.hdr <= tmp_src_out.hdr;
        end if;
        if input_to_temp = '1' then
          tmp_src_out.data  <= snk_i.data;
          tmp_src_out.last  <= snk_i.last;
          tmp_src_out.error <= snk_i.error;
          tmp_src_out.hdr <= snk_i.hdr;
        end if;
      end if;
    end if;
  end process p_seq;

  src_o       <= src_out;
  snk_o.ready <= ready_reg;

  snk_o.pkt_ready <= '0';

end arch;
