-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_endpoint_rx
--
-- description: Implements RX configuration registers and packet filter for a
--   single RMQ slot.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.genram_pkg.all;
use work.mt_mqueue_pkg.all;
use work.mt_endpoint_pkg.all;

entity mt_rmq_endpoint_rx is
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    header_valid_i : in std_logic;
    header_i       : in t_rmq_ep_rx_header;

    framer_snk_i : in  t_mt_stream_sink_in;
    framer_snk_o : out t_mt_stream_sink_out;

    mq_src_i : in  t_mt_stream_source_in;
    mq_src_o : out t_mt_stream_source_out;

    snk_config_i : in  t_mt_stream_config_out;
    snk_config_o : out t_mt_stream_config_in
    );
end mt_rmq_endpoint_rx;

architecture arch of mt_rmq_endpoint_rx is

  constant c_addr_config     : integer := 0;
  constant c_addr_dst_mac_hi : integer := 1;
  constant c_addr_dst_mac_lo : integer := 2;
  constant c_addr_vlan_id    : integer := 3;
  constant c_addr_ethertype  : integer := 4;
  constant c_addr_dst_ip     : integer := 5;
  constant c_addr_dst_port   : integer := 6;

  constant c_BROADCAST_IP  : std_logic_vector(31 downto 0) := (others => '1');
  constant c_BROADCAST_MAC : std_logic_vector(47 downto 0) := (others => '1');

  signal config : t_rmq_ep_rx_config;

  signal match, drop : std_logic;
begin  -- arch


  p_write_config_regs : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        config.enable <= '0';
      else
        if snk_config_i.we = '1' then
          case to_integer(unsigned(snk_config_i.adr(9 downto 2))) is
            when c_addr_config =>
              config.filter_raw       <= snk_config_i.dat(0);
              config.filter_udp       <= snk_config_i.dat(1);
              config.filter_dst_mac   <= snk_config_i.dat(2);
              config.vlan_en          <= snk_config_i.dat(3);
              config.vlan_discard     <= snk_config_i.dat(4);
              config.filter_ethertype <= snk_config_i.dat(5);
              config.filter_dst_ip    <= snk_config_i.dat(6);
              config.filter_dst_port  <= snk_config_i.dat(7);
              config.enable           <= snk_config_i.dat(31);
            when c_addr_dst_mac_hi =>
              config.dst_mac(47 downto 32) <= snk_config_i.dat(15 downto 0);
            when c_addr_dst_mac_lo =>
              config.dst_mac(31 downto 0) <= snk_config_i.dat;
            when c_addr_vlan_id =>
              config.vlan_id <= snk_config_i.dat(11 downto 0);
            when c_addr_ethertype =>
              config.ethertype <= snk_config_i.dat(15 downto 0);
            when c_addr_dst_ip =>
              config.dst_ip <= snk_config_i.dat;
            when c_addr_dst_port =>
              config.dst_port <= snk_config_i.dat(15 downto 0);
            when others => null;
          end case;
        end if;
      end if;
    end if;
  end process p_write_config_regs;

  p_read_config_regs : process(snk_config_i, config)
  begin
    snk_config_o.dat <= (others => '0');

    case to_integer(unsigned(snk_config_i.adr(9 downto 2))) is
      when c_addr_config =>
        snk_config_o.dat(0)            <= config.filter_raw;
        snk_config_o.dat(1)            <= config.filter_udp;
        snk_config_o.dat(2)            <= config.filter_dst_mac;
        snk_config_o.dat(3)            <= config.vlan_en;
        snk_config_o.dat(4)            <= config.filter_ethertype;
        snk_config_o.dat(5)            <= config.filter_dst_ip;
        snk_config_o.dat(6)            <= config.filter_dst_port;
        snk_config_o.dat(31)           <= config.enable;
      when c_addr_dst_mac_hi =>
        snk_config_o.dat(15 downto 0)  <= config.dst_mac(47 downto 32);
      when c_addr_dst_mac_lo =>
        snk_config_o.dat <= config.dst_mac(31 downto 0);
      when c_addr_vlan_id =>
        snk_config_o.dat(11 downto 0)  <= config.vlan_id;
      when c_addr_ethertype =>
        snk_config_o.dat(15 downto 0)  <= config.ethertype;
      when c_addr_dst_ip =>
        snk_config_o.dat               <= config.dst_ip;
      when c_addr_dst_port =>
        snk_config_o.dat(15 downto 0)  <= config.dst_port;
      when others => null;
    end case;
  end process p_read_config_regs;

  p_filter_packets : process(clk_i)
    variable match_dst_mac, match_dst_ip, match_udp, match_ethertype : std_logic;
    variable match_dst_port, match_raw, match_vlan                   : std_logic;
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        match <= '0';
      else
        match_udp       := '1';
        match_raw       := '1';
        match_ethertype := '1';
        match_dst_mac   := '1';
        match_dst_ip    := '1';
        match_dst_port  := '1';
        match_vlan      := '1';

        if config.filter_udp = '1' then
          match_udp := header_i.is_udp;
        end if;

        if config.filter_raw = '1' then
          match_raw := header_i.is_raw;
        end if;

        if config.vlan_discard = '0' then
          if config.vlan_en = '1' then
            if header_i.vlan_id /= config.vlan_id then
              match_vlan := '0';
            end if;
          else
            if header_i.vlan_id /= x"000" then
              match_vlan := '0';
            end if;
          end if;
        end if;

        if config.filter_ethertype = '1' then
          if header_i.is_raw = '1' and header_i.ethertype /= config.ethertype then
            match_ethertype := '0';
          end if;
        end if;

        if config.filter_dst_mac = '1' then
          if header_i.dst_mac /= c_BROADCAST_MAC and header_i.dst_mac /= config.dst_mac then
            match_dst_mac := '0';
          end if;
        end if;

        if config.filter_dst_ip = '1' then
          if header_i.dst_ip /= c_BROADCAST_IP and config.dst_ip /= header_i.dst_ip then
            match_dst_ip := '0';
          end if;
        end if;

        if config.filter_dst_port = '1' then
          if config.dst_port /= header_i.dst_port then
            match_dst_port := '0';
          end if;
        end if;

        match <= match_dst_mac and match_dst_ip and match_vlan and match_ethertype and
                 match_dst_port and match_udp and match_raw and config.enable;

      end if;
    end if;

  end process p_filter_packets;

  drop <= header_valid_i and not match and framer_snk_i.last;

  mq_src_o.valid <= framer_snk_i.valid;
  mq_src_o.data  <= framer_snk_i.data;
  mq_src_o.hdr   <= framer_snk_i.hdr;
  mq_src_o.last  <= framer_snk_i.last;
  mq_src_o.error <= framer_snk_i.error or drop;

  framer_snk_o.ready     <= '1';
  framer_snk_o.pkt_ready <= '1';

end architecture arch;
