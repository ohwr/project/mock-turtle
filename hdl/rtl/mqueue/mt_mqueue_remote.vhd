-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_mqueue_remote
--
-- description: Remote MQ implementation. Exchanges messages between CPU CBs in
-- remote nodes.

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_mqueue_remote is
  generic (
    g_CONFIG : t_mt_mqueue_config := c_MT_DEFAULT_MQUEUE_CONFIG);
  port (
    clk_i        : in  std_logic;
    rst_n_i      : in  std_logic;
    slave_i      : in  t_wishbone_slave_in;
    slave_o      : out t_wishbone_slave_out;

    --  From OUT rmq (from CPUs to the outside world)
    src_o        : out t_mt_stream_source_out_array(0 to g_CONFIG.slot_count-1);
    src_i        : in  t_mt_stream_source_in_array(0 to g_CONFIG.slot_count-1);

    src_config_o : out t_mt_stream_config_out_array( 0 to g_CONFIG.slot_count-1);
    src_config_i : in t_mt_stream_config_in_array( 0 to g_CONFIG.slot_count-1);

    --  To IN rmq (from inside world to the CPUs)
    snk_o        : out t_mt_stream_sink_out_array(0 to g_CONFIG.slot_count-1);
    snk_i        : in  t_mt_stream_sink_in_array(0 to g_CONFIG.slot_count-1);

    snk_config_o : out t_mt_stream_config_out_array( 0 to g_CONFIG.slot_count-1);
    snk_config_i : in t_mt_stream_config_in_array( 0 to g_CONFIG.slot_count-1);
    -- RMQ status
    rmq_in_status_o  : out std_logic_vector(g_CONFIG.slot_count-1 downto 0);
    rmq_out_status_o : out std_logic_vector(g_CONFIG.slot_count-1 downto 0));
end mt_mqueue_remote;

architecture arch of mt_mqueue_remote is

  subtype t_slot_range is natural range g_CONFIG.slot_count-1 downto 0;
  signal cpu_incoming_in  : t_slot_bus_in_array(t_slot_range);
  signal cpu_incoming_out : t_slot_bus_out_array(t_slot_range);
  signal cpu_outgoing_in  : t_slot_bus_in_array(t_slot_range);
  signal cpu_outgoing_out : t_slot_bus_out_array(t_slot_range);

  signal ep_incoming_in  : t_slot_bus_in_array(t_slot_range);
  signal ep_incoming_out : t_slot_bus_out_array(t_slot_range);
  signal ep_outgoing_in  : t_slot_bus_in_array(t_slot_range);
  signal ep_outgoing_out : t_slot_bus_out_array(t_slot_range);

  signal incoming_stat : t_slot_status_out_array(t_slot_range);
  signal outgoing_stat : t_slot_status_out_array(t_slot_range);
begin  -- rtl

  U_SI_Wishbone_Slave : entity work.mt_mqueue_wishbone_slave
    generic map (
      g_CONFIG   => g_CONFIG)
    port map (
      clk_i             => clk_i,
      rst_n_i           => rst_n_i,
      incoming_status_i => incoming_stat,
      outgoing_status_i => outgoing_stat,
      incoming_o        => cpu_incoming_in,
      incoming_i        => cpu_incoming_out,
      outgoing_o        => cpu_outgoing_in,
      outgoing_i        => cpu_outgoing_out,
      slave_i           => slave_i,
      slave_o           => slave_o);

  gen_slots : for i in 0 to g_CONFIG.slot_count-1 generate

    U_Out_SlotX : entity work.mt_mqueue_slot
      generic map (
        g_CONFIG  => g_config.slot_config(i),
        g_EP_CONFIG_MAP_PORT => "inb" )
      port map (
        clk_i   => clk_i,
        rst_n_i => rst_n_i,
        stat_o  => outgoing_stat(i),
        inb_i   => cpu_outgoing_in(i),
        inb_o   => cpu_outgoing_out(i),
        outb_i  => ep_outgoing_in(i),
        outb_o  => ep_outgoing_out(i),
        ep_config_o => src_config_o(i),
        ep_config_i => src_config_i(i)
        );

    U_Out_Tx : entity work.mt_rmq_tx
      generic map (
        g_CONFIG  => g_config.slot_config(i))
      port map (
        clk_i       => clk_i,
        rst_n_i     => rst_n_i,
        inb_stat_i  => outgoing_stat(i),
        inb_i       => ep_outgoing_out(i),
        inb_o       => ep_outgoing_in(i),
        src_o       => src_o(i),
        src_i       => src_i(i));

    rmq_out_status_o (i) <= not outgoing_stat(i).full;

    U_In_SlotX : entity work.mt_mqueue_slot
      generic map (
        g_CONFIG  => g_config.slot_config(i),
        g_EP_CONFIG_MAP_PORT => "outb")
      port map (
        clk_i   => clk_i,
        rst_n_i => rst_n_i,
        stat_o  => incoming_stat(i),
        outb_i  => cpu_incoming_in(i),
        outb_o  => cpu_incoming_out(i),
        inb_i   => ep_incoming_in(i),
        inb_o   => ep_incoming_out(i),
        ep_config_o => snk_config_o(i),
        ep_config_i => snk_config_i(i)
        );

    U_In_Rx : entity work.mt_rmq_rx
      generic map (
        g_CONFIG  => g_config.slot_config(i))
      port map (
        clk_i            => clk_i,
        rst_n_i          => rst_n_i,
        outb_stat_i      => incoming_stat(i),
        outb_i           => ep_incoming_out(i),
        outb_o           => ep_incoming_in(i),
        snk_i            => snk_i(i),
        snk_o            => snk_o(i));

    rmq_in_status_o (i) <= not incoming_stat(i).empty;
  end generate gen_slots;

end architecture arch;
