-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_rmq_tx
--
-- description: Single outgoing (MT->world) slot of the Remote Message Queue.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_rmq_tx is

  generic (
    g_CONFIG : t_mt_mqueue_slot_config);
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    --  Input
    inb_i : in  t_slot_bus_out;
    inb_o : out t_slot_bus_in;

    --  Status from input.
    inb_stat_i : in t_slot_status_out;

    --  Output
    src_o : out t_mt_stream_source_out;
    src_i : in  t_mt_stream_source_in
    );

end mt_rmq_tx;

architecture rtl of mt_rmq_tx is
  type t_state_enum is (WAIT_SLOT, CLAIM, SEND_HEADER, SEND_PAYLOAD_EVEN, SEND_PAYLOAD_ODD, DISCARD);

  type t_state is record
    state         : t_state_enum;
    addr          : unsigned(13 downto 0);
    pkt_last_addr : unsigned(13 downto 0);
  end record;

  signal state, n_state : t_state;

  signal header_last, payload_last : std_logic;
begin

  payload_last <= f_to_std_logic (state.addr = state.pkt_last_addr);

  p_fsm : process (rst_n_i, state, src_i, inb_i, inb_stat_i,
                   header_last, payload_last)
  begin
    if rst_n_i = '0' then
      inb_o <= (sel   => '0',
                adr   => (others => 'X'),
                dat   => (others => 'X'),
                we    => 'X',
                wmask => "XXXX");
      src_o <= (data  => (others => 'X'),
                hdr   => 'X',
                valid => '0',
                last  => 'X',
                error => 'X');
      n_state <= (state         => WAIT_SLOT,
                  addr          => (others => 'X'),
                  pkt_last_addr => (others => 'X'));
    else
      n_state <= state;
      src_o <= (data  => (others => 'X'),
                hdr   => 'X',
                valid => '0',
                last  => 'X',
                error => '0');

      case state.state is
        when WAIT_SLOT =>
          if inb_stat_i.empty = '0' then
            --  A packet can be read and sent.  Claim it.
            inb_o <= (sel            => '1',
                      adr            => c_mqueue_addr_command,
                      dat            => (c_mqueue_command_claim => '1',
                                         others => '0'),
                      we             => '1',
                      wmask          => "1111");

            n_state <= (state         => CLAIM,
                        addr          => unsigned(c_mqueue_addr_header_size),
                        pkt_last_addr => (others => 'X'));
          else
            --  Do nothing, wait until the packet is ready.
            inb_o <= (sel   => '0',
                      adr   => (others => 'X'),
                      dat   => (others => 'X'),
                      we    => 'X',
                      wmask => "XXXX");
          end if;
        when CLAIM =>
          --  Read first header word.
          inb_o <= (sel   => '1',
                    adr   => std_logic_vector(state.addr),
                    dat   => (others => 'X'),
                    we    => '0',
                    wmask => "XXXX");
          n_state <= (state         => SEND_HEADER,
                      addr          => state.addr,
                      pkt_last_addr => (others => 'X'));
        when SEND_HEADER =>
          --if state.addr = unsigned(c_mqueue_addr_header(12 downto 2)) then
          --  Save packet length.
          if src_i.ready = '1' then
            n_state.pkt_last_addr <=
              unsigned("1" & inb_i.dat(10 downto 0) & "00" ) - 4;
          end if;

          src_o <= (data  => inb_i.dat,
                    hdr   => '1',
                    valid => '1',
                    last  => '0',
                    error => '0');

          if src_i.ready = '1' then
            n_state.state <= SEND_PAYLOAD_EVEN;
            n_state.addr  <= unsigned(c_mqueue_addr_payload);

            inb_o <= (sel   => '1',
                      adr   => c_mqueue_addr_payload,
                      dat   => (others => 'X'),
                      we    => '0',
                      wmask => "XXXX");

          else

            inb_o <= (sel   => '1',
                      adr   => std_logic_vector(c_mqueue_addr_header_size),
                      dat   => (others => 'X'),
                      we    => '0',
                      wmask => "XXXX");

            n_state.state <= SEND_HEADER;
            n_state.addr  <= unsigned(c_mqueue_addr_payload);
          end if;


        when SEND_PAYLOAD_EVEN =>

          --  Read 2nd word
          inb_o <= (sel   => '1',
                    adr   => std_logic_vector(state.addr),
                    dat   => (others => 'X'),
                    we    => '0',
                    wmask => "XXXX");

          --  Send payload word
          src_o <= (data  => x"0000" & inb_i.dat(31 downto 16),
                    hdr   => '0',
                    valid => '1',
                    last  => '0',
                    error => '0');

          if src_i.ready = '1' then
              n_state <= (state         => SEND_PAYLOAD_ODD,
                          addr          => state.addr,
                          pkt_last_addr => state.pkt_last_addr);
          end if;

        when SEND_PAYLOAD_ODD =>

          --  By default, if not overriden below, keep requesting the same payload
          inb_o <= (sel   => '1',
                    adr   => std_logic_vector(state.addr),
                    dat   => (others => 'X'),
                    we    => '0',
                    wmask => "XXXX");

          --  Send payload word
          src_o <= (data  => x"0000" & inb_i.dat(15 downto 0) ,
                    hdr   => '0',
                    valid => '1',
                    last  => payload_last,
                    error => '0');

          if src_i.ready = '1' then
            if payload_last = '1' then
              --  Packet was sent, discard it.
              inb_o <= (sel            => '1',
                        adr            => c_mqueue_addr_command,
                        dat            => (c_mqueue_command_discard => '1',
                                others => '0'),
                        we             => '1',
                        wmask          => "1111");
              n_state <= (state         => DISCARD,
                          addr          => (others => 'X'),
                          pkt_last_addr => (others => 'X'));
            else
              --  Request next payload
              inb_o <= (sel   => '1',
                        adr   => std_logic_vector(state.addr + 4),
                        dat   => (others => 'X'),
                        we    => '0',
                        wmask => "XXXX");
              n_state <= (state         => SEND_PAYLOAD_EVEN,
                          addr          => state.addr + 4,
                          pkt_last_addr => state.pkt_last_addr);
            end if;
          end if;


        when DISCARD =>
          --  Need to wait one cycle so that empty status  is updated.
          n_state <= (state         => WAIT_SLOT,
                      addr          => (others => 'X'),
                      pkt_last_addr => (others => 'X'));
          inb_o <= (sel   => '0',
                    adr   => (others => 'X'),
                    dat   => (others => 'X'),
                    we    => 'X',
                    wmask => "XXXX");
      end case;
    end if;
  end process;

  p_fsm_update : process (clk_i, rst_n_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        state <= (state         => WAIT_SLOT,
                  addr          => (others => 'X'),
                  pkt_last_addr => (others => 'X'));
      else
        state <= n_state;
      end if;
    end if;
  end process;


end rtl;
