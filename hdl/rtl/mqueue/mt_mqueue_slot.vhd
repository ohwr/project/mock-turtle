-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_mqueue_slot
--
-- description: Single slot (FIFO) of a Message Queue.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.genram_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_mqueue_slot is
  generic (
    g_CONFIG                 : t_mt_mqueue_slot_config;
    g_EP_CONFIG_MAP_PORT     : string := "inb");
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    stat_o : out t_slot_status_out;

    --  Input
    inb_i : in  t_slot_bus_in;
    inb_o : out t_slot_bus_out;

    --  Output
    outb_i : in  t_slot_bus_in;
    outb_o : out t_slot_bus_out;

    -- Aux WB master for configuring the RMQ endpoints. Maps to the header space.
    ep_config_o : out t_mt_stream_config_out;
    ep_config_i : in  t_mt_stream_config_in := c_MT_DUMMY_EP_CONFIG_IN
    );

end mt_mqueue_slot;

architecture arch of mt_mqueue_slot is

  constant c_MEMORY_ADDR_BITS : natural := g_CONFIG.width_bits + g_CONFIG.entries_bits;
  constant c_MEMORY_SIZE      : natural := 2**c_MEMORY_ADDR_BITS;
  constant c_HEADER_ADDR_BITS : natural := g_CONFIG.header_bits + g_CONFIG.entries_bits;
  constant c_HEADER_SIZE      : natural := 2**c_HEADER_ADDR_BITS;

  type t_slot_mem_array is
    array (0 to c_MEMORY_SIZE-1) of std_logic_vector(7 downto 0);

  shared variable mem0 : t_slot_mem_array := (others => (others => '0'));
  shared variable mem1 : t_slot_mem_array := (others => (others => '0'));
  shared variable mem2 : t_slot_mem_array := (others => (others => '0'));
  shared variable mem3 : t_slot_mem_array := (others => (others => '0'));

  signal mem_raddr, mem_waddr        : unsigned(c_MEMORY_ADDR_BITS-1 downto 0);
  signal mem_rdata_in, mem_rdata_out : std_logic_vector(31 downto 0);

  type t_header_mem_array is
    array(0 to c_HEADER_SIZE-1) of std_logic_vector(7 downto 0);

  signal hdr_raddr, hdr_waddr        : unsigned(c_HEADER_ADDR_BITS-1 downto 0);
  signal hdr_rdata_in, hdr_rdata_out : std_logic_vector(31 downto 0);

  shared variable hdr0 : t_header_mem_array := (others => (others => '0'));
  shared variable hdr1 : t_header_mem_array := (others => (others => '0'));
  shared variable hdr2 : t_header_mem_array := (others => (others => '0'));
  shared variable hdr3 : t_header_mem_array := (others => (others => '0'));

  signal rd_ptr, wr_ptr : unsigned(g_CONFIG.entries_bits-1 downto 0);
  signal occupied       : unsigned(7 downto 0);
  signal words_written  : unsigned(7 downto 0);

  type t_wr_state is (IDLE, ACCEPT_DATA, IGNORE_MESSAGE);
  type t_rd_state is (IDLE, WAIT_DISCARD);

  signal mem_we : std_logic_vector(3 downto 0);

  signal full, empty : std_logic;

  signal wr_state : t_wr_state;
  signal rd_state : t_rd_state;

  signal in_claim, in_purge, in_ready : std_logic;

  signal in_cmd_wr, in_stat_rd   : std_logic;
  signal out_cmd_wr, out_stat_rd : std_logic;

  signal status : std_logic_vector(31 downto 0) := (others => '0');

  signal out_discard, out_purge : std_logic;

  signal q_read, q_write : std_logic;

  signal purge_int : std_logic;

  type t_reg_rd is (REG_RD_STATUS, REG_RD_HEADER, REG_RD_PAYLOAD, REG_RD_EP_CONFIG);
  signal in_reg_rd, out_reg_rd : t_reg_rd;

begin
  --  Generate mem_we.
  p_mem_write : process(inb_i, wr_state)
  begin
    if(wr_state = IGNORE_MESSAGE) then
      --  Do not to memory when data is ignored.
      mem_we <= (others => '0');
    else
      for i in 0 to 3 loop
        mem_we(i) <= (inb_i.we and inb_i.sel and inb_i.wmask(i));
      end loop;
    end if;
  end process p_mem_write;

  mem_waddr <= wr_ptr & unsigned(inb_i.adr(g_CONFIG.width_bits+1 downto 2));
  hdr_waddr <= wr_ptr & unsigned(inb_i.adr(g_CONFIG.header_bits+1 downto 2));

  mem_raddr <= rd_ptr & unsigned(outb_i.adr(g_CONFIG.width_bits+1 downto 2));
  hdr_raddr <= rd_ptr & unsigned(outb_i.adr(g_CONFIG.header_bits+1 downto 2));

  status(0)            <= full;
  status(1)            <= empty;
  status(7 downto 2)   <= (others => '0');
  status(15 downto 8)  <= std_logic_vector(occupied);
  status(31 downto 28) <= (others => '0');

  in_cmd_wr <= '1' when (inb_i.sel = '1'
                         and inb_i.we = '1'
                         and inb_i.adr = c_mqueue_addr_command)
               else '0';
  out_cmd_wr <= '1' when (outb_i.sel = '1'
                          and outb_i.we = '1'
                          and outb_i.adr = c_mqueue_addr_command)
                else '0';

  in_claim <= in_cmd_wr and inb_i.dat(c_mqueue_command_claim);
  in_purge <= in_cmd_wr and inb_i.dat(c_mqueue_command_purge);
  in_ready <= in_cmd_wr and inb_i.dat(c_mqueue_command_ready);

  out_discard <= out_cmd_wr and outb_i.dat(c_mqueue_command_discard);
  out_purge   <= out_cmd_wr and outb_i.dat(c_mqueue_command_purge);

  p_purge_int : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        purge_int <= '0';
      else
        purge_int <= out_purge or in_purge;
      end if;
    end if;
  end process p_purge_int;

  p_read_status : process(clk_i)
    function f_to_reg_rd (bus_in : t_slot_bus_in) return t_reg_rd is
    begin
      if bus_in.adr(13) = '1' then
        return REG_RD_PAYLOAD;
      else
        if bus_in.adr(12) = '0' then
          return REG_RD_STATUS;
        else
          if bus_in.adr(11) = '1' then
            return REG_RD_EP_CONFIG;
          else
            return REG_RD_HEADER;
          end if;
        end if;
      end if;
    end f_to_reg_rd;
  begin
    if rising_edge(clk_i) then
      in_reg_rd  <= f_to_reg_rd(inb_i);
      out_reg_rd <= f_to_reg_rd(outb_i);
    end if;
  end process p_read_status;

  with in_reg_rd select inb_o.dat <=
    status          when REG_RD_STATUS,
    hdr_rdata_in    when REG_RD_HEADER,
    mem_rdata_in    when REG_RD_PAYLOAD,
    ep_config_i.dat when REG_RD_EP_CONFIG;

  with out_reg_rd select outb_o.dat <=
    status          when REG_RD_STATUS,
    hdr_rdata_out   when REG_RD_HEADER,
    mem_rdata_out   when REG_RD_PAYLOAD,
    ep_config_i.dat when REG_RD_EP_CONFIG;

  -- Note: we do four separate memories due to a bug in Xilinx ISE which
  -- does not infer properly BRAM when using one 32-bit memory
  p_mem_port1 : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if mem_we(0) = '1' and inb_i.adr(13 downto 12) = "10" then
        mem0(to_integer(mem_waddr)) := inb_i.dat(7 downto 0);
      end if;
      if mem_we(1) = '1' and inb_i.adr(13 downto 12) = "10" then
        mem1(to_integer(mem_waddr)) := inb_i.dat(15 downto 8);
      end if;
      if mem_we(2) = '1' and inb_i.adr(13 downto 12) = "10" then
        mem2(to_integer(mem_waddr)) := inb_i.dat(23 downto 16);
      end if;
      if mem_we(3) = '1' and inb_i.adr(13 downto 12) = "10" then
        mem3(to_integer(mem_waddr)) := inb_i.dat(31 downto 24);
      end if;

      mem_rdata_in(7 downto 0)   <= mem0(to_integer(unsigned(mem_waddr)));
      mem_rdata_in(15 downto 8)  <= mem1(to_integer(unsigned(mem_waddr)));
      mem_rdata_in(23 downto 16) <= mem2(to_integer(unsigned(mem_waddr)));
      mem_rdata_in(31 downto 24) <= mem3(to_integer(unsigned(mem_waddr)));
    end if;
  end process p_mem_port1;

  p_mem_port2 : process(clk_i)
  begin
    if rising_edge(clk_i) then
      mem_rdata_out(7 downto 0)   <= mem0(to_integer(mem_raddr));
      mem_rdata_out(15 downto 8)  <= mem1(to_integer(mem_raddr));
      mem_rdata_out(23 downto 16) <= mem2(to_integer(mem_raddr));
      mem_rdata_out(31 downto 24) <= mem3(to_integer(mem_raddr));
    end if;
  end process p_mem_port2;

  -- Note: we do four separate memories due to a bug in Xilinx ISE which
  -- does not infer properly BRAM when using one 32-bit memory
  p_hdr_port1 : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if mem_we(0) = '1' and inb_i.adr(13 downto 12) = "01" then
        hdr0(to_integer(hdr_waddr)) := inb_i.dat(7 downto 0);
      end if;
      if mem_we(1) = '1' and inb_i.adr(13 downto 12) = "01" then
        hdr1(to_integer(hdr_waddr)) := inb_i.dat(15 downto 8);
      end if;
      if mem_we(2) = '1' and inb_i.adr(13 downto 12) = "01" then
        hdr2(to_integer(hdr_waddr)) := inb_i.dat(23 downto 16);
      end if;
      if mem_we(3) = '1' and inb_i.adr(13 downto 12) = "01" then
        hdr3(to_integer(hdr_waddr)) := inb_i.dat(31 downto 24);
      end if;

      hdr_rdata_in(7 downto 0)   <= hdr0(to_integer(unsigned(hdr_waddr)));
      hdr_rdata_in(15 downto 8)  <= hdr1(to_integer(unsigned(hdr_waddr)));
      hdr_rdata_in(23 downto 16) <= hdr2(to_integer(unsigned(hdr_waddr)));
      hdr_rdata_in(31 downto 24) <= hdr3(to_integer(unsigned(hdr_waddr)));
    end if;
  end process p_hdr_port1;

  p_hdr_port2 : process(clk_i)
  begin
    if rising_edge(clk_i) then
      hdr_rdata_out(7 downto 0)   <= hdr0(to_integer(hdr_raddr));
      hdr_rdata_out(15 downto 8)  <= hdr1(to_integer(hdr_raddr));
      hdr_rdata_out(23 downto 16) <= hdr2(to_integer(hdr_raddr));
      hdr_rdata_out(31 downto 24) <= hdr3(to_integer(hdr_raddr));
    end if;
  end process p_hdr_port2;

  p_write_side : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' or purge_int = '1' then
        wr_state <= IDLE;
      else
        case wr_state is
          when IDLE =>
            if in_claim = '1' then
              if full = '1' then
                wr_state <= IGNORE_MESSAGE;
              else
                wr_state <= ACCEPT_DATA;
              end if;
            end if;

          when IGNORE_MESSAGE =>
            if in_ready = '1' then
              wr_state <= IDLE;
            end if;

          when ACCEPT_DATA =>
            if in_ready = '1' then
              wr_state <= IDLE;
            end if;
        end case;
      end if;
    end if;
  end process p_write_side;

  p_read_side : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' or purge_int = '1' then
        rd_state <= IDLE;
      else
        case rd_state is
          when IDLE =>
            if empty = '0' then
              rd_state <= WAIT_DISCARD;
            end if;

          when WAIT_DISCARD =>
            if out_discard = '1' then
              rd_state <= IDLE;
            end if;
        end case;
      end if;
    end if;
  end process p_read_side;

  q_write <= '1' when wr_state = ACCEPT_DATA and in_ready = '1'     else '0';
  q_read  <= '1' when rd_state = WAIT_DISCARD and out_discard = '1' else '0';

  p_counters : process(clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' or purge_int = '1' then
        rd_ptr   <= (others => '0');
        wr_ptr   <= (others => '0');
        occupied <= (others => '0');
      else
        if q_write = '1' then
          assert full = '0' severity failure;
          wr_ptr <= wr_ptr + 1;
        end if;

        if q_read = '1' then
          assert empty = '0' severity failure;
          rd_ptr <= rd_ptr + 1;
        end if;

        if q_write = '1' and q_read = '0' then
          occupied <= occupied + 1;
        elsif q_write = '0' and q_read = '1' then
          occupied <= occupied - 1;
        end if;
      end if;
    end if;
  end process p_counters;

  full  <= '1' when wr_ptr + 1 = rd_ptr else '0';
  empty <= '1' when wr_ptr = rd_ptr     else '0';

  stat_o <= (full => full, empty => empty);


  gen_ep_config_use_inb : if g_EP_CONFIG_MAP_PORT = "inb" generate
    ep_config_o.adr(10 downto 0) <= inb_i.adr(10 downto 0);
    ep_config_o.dat <= inb_i.dat;
    ep_config_o.we <= inb_i.sel and inb_i.we and inb_i.adr(12) and inb_i.adr(11);
  end generate gen_ep_config_use_inb;

  gen_ep_config_use_outb : if g_EP_CONFIG_MAP_PORT = "outb" generate
    ep_config_o.adr(10 downto 0) <= outb_i.adr(10 downto 0);
    ep_config_o.dat <= outb_i.dat;
    ep_config_o.we <= outb_i.sel and outb_i.we and outb_i.adr(12) and outb_i.adr(11);
  end generate gen_ep_config_use_outb;

end architecture arch;
