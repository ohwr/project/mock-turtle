-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_mqueue_pkg
--
-- description: Global package for the Message Queues

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;

package mt_mqueue_pkg is

  constant c_RMQ_DATA_START_OFFSET : integer := 32;

  constant c_MT_STREAM_TAG_HEADER  : std_logic_vector(1 downto 0) := "00";
  constant c_MT_STREAM_TAG_PAYLOAD : std_logic_vector(1 downto 0) := "01";

  type t_mt_stream_sink_in is record
    data     : std_logic_vector(31 downto 0);
    hdr      : std_logic;
    valid    : std_logic;
    last     : std_logic;
    error    : std_logic;
  end record t_mt_stream_sink_in;

  type t_mt_stream_sink_out is record
    ready : std_logic;

    --  Ready to accept a new packet.
    pkt_ready : std_logic;
  end record t_mt_stream_sink_out;

  type t_mt_stream_config_out is record
    adr : std_logic_vector(10 downto 0);
    dat : std_logic_vector(31 downto 0);
    we  : std_logic;
  end record t_mt_stream_config_out;

  type t_mt_stream_config_in is record
    dat : std_logic_vector(31 downto 0);
  end record t_mt_stream_config_in;

  subtype t_mt_stream_source_in is t_mt_stream_sink_out;
  subtype t_mt_stream_source_out is t_mt_stream_sink_in;

  constant c_MT_DUMMY_SOURCE_IN : t_mt_stream_sink_out :=
    ('0', '0');
  constant c_MT_DUMMY_SINK_IN : t_mt_stream_sink_in :=
    (x"00000000", '0', '0', '0', '0');

  constant c_MT_DUMMY_EP_CONFIG_OUT : t_mt_stream_config_out :=
    ("00000000000", x"00000000", '0');

  constant c_MT_DUMMY_EP_CONFIG_IN : t_mt_stream_config_in :=
    (dat => x"00000000");

  type t_mt_stream_sink_in_array is array(integer range<>) of t_mt_stream_sink_in;
  type t_mt_stream_sink_out_array is array(integer range<>) of t_mt_stream_sink_out;

  type t_mt_stream_config_out_array is array(integer range<>) of t_mt_stream_config_out;
  type t_mt_stream_config_in_array is array(integer range<>) of t_mt_stream_config_in;


  subtype t_mt_stream_source_in_array is t_mt_stream_sink_out_array;
  subtype t_mt_stream_source_out_array is t_mt_stream_sink_in_array;

  type t_mt_mqueue_slot_config is record
    entries_bits        : natural;
    width_bits          : natural;
    header_bits         : positive;
    endpoint_id         : std_logic_vector(31 downto 0);
    enable_config_space : boolean;
  end record t_mt_mqueue_slot_config;

  subtype t_maxslot_range is natural range 0 to 7;

  type t_mt_mqueue_slot_config_array is
    array(t_maxslot_range) of t_mt_mqueue_slot_config;

  type t_mt_mqueue_config is record
    --  IN and OUT slots are always peered.
    slot_count  : integer;
    slot_config : t_mt_mqueue_slot_config_array;
  end record t_mt_mqueue_config;

  constant c_DUMMY_MT_MQUEUE_SLOT : t_mt_mqueue_slot_config :=
    (0, 0, 1, x"0000_0000", false);

  constant c_DUMMY_MT_MQUEUE_CONFIG : t_mt_mqueue_config := (
    slot_count  => 0,
    slot_config => (others => (c_DUMMY_MT_MQUEUE_SLOT)));

  constant c_MT_DEFAULT_MQUEUE_CONFIG : t_mt_mqueue_config :=
    (1, ((7, 5, 6, x"0000_0000", false), others => (c_DUMMY_MT_MQUEUE_SLOT)));

  --  Bus to control an mqueue slot.
  type t_slot_bus_in is record
    --  Other bits are valid only when sel='1'.
    sel : std_logic;

    --  Byte address (16KB).
    adr : std_logic_vector(13 downto 0);

    dat   : std_logic_vector(31 downto 0);
    we    : std_logic;
    wmask : std_logic_vector(3 downto 0);
  end record t_slot_bus_in;

  type t_slot_bus_out is record
    dat : std_logic_vector(31 downto 0);
  end record t_slot_bus_out;

  constant c_DUMMY_SLOT_BUS_OUT : t_slot_bus_out := (dat => x"00000000");

  constant c_mqueue_addr_command : std_logic_vector (13 downto 0) :=
    b"00_0000_0000_0000";
  constant c_mqueue_addr_status : std_logic_vector (13 downto 0) :=
    b"00_0000_0000_0100";
  constant c_mqueue_addr_header : std_logic_vector (13 downto 0) :=
    b"01_0000_0000_0000";
  constant c_mqueue_addr_header_size : std_logic_vector (13 downto 0) :=
    b"01_0000_0000_0100";
  constant c_mqueue_addr_payload : std_logic_vector (13 downto 0) :=
    b"10_0000_0000_0000";

  constant c_mqueue_command_claim   : natural := 24;
  constant c_mqueue_command_purge   : natural := 25;
  constant c_mqueue_command_ready   : natural := 26;
  constant c_mqueue_command_discard : natural := 27;

  --  Mqueue slot status.
  type t_slot_status_out is record
    --  All entries are used.
    full : std_logic;

    --  No entry is used.
    empty : std_logic;
  end record t_slot_status_out;

  constant c_dummy_status_out : t_slot_status_out := ('0', '0');


  type t_slot_bus_in_array is array(integer range <>) of t_slot_bus_in;
  type t_slot_bus_out_array is array(integer range <>) of t_slot_bus_out;
  type t_slot_status_out_array is array(integer range <>) of t_slot_status_out;

  type t_mt_irq_config is record
    mask_in   : std_logic_vector(7 downto 0);
    mask_out  : std_logic_vector(7 downto 0);
    threshold : std_logic_vector(7 downto 0);
    timeout   : std_logic_vector(7 downto 0);
  end record t_mt_irq_config;
end mt_mqueue_pkg;
