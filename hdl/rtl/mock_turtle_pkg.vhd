-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mock_turtle_pkg
--
-- description: top level package with public types, definitions and components.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.mt_mqueue_pkg.all;

package mock_turtle_pkg is

  constant c_MT_UART_MESSAGE_FIFO_SIZE : integer := 512;

  type t_mt_timing_if is record
    link_up    : std_logic;
    time_valid : std_logic;
    tai        : std_logic_vector(39 downto 0);
    cycles     : std_logic_vector(27 downto 0);
    aux_locked : std_logic_vector(7 downto 0);
  end record t_mt_timing_if;

  constant c_DUMMY_MT_TIMING : t_mt_timing_if := (
    link_up    => '0',
    time_valid => '0',
    tai        => (others => '0'),
    cycles     => (others => '0'),
    aux_locked => (others => '0'));

  type t_int_array is array(integer range<>) of integer;

  subtype t_maxcpu_range is natural range 0 to 7;

  type t_mt_cpu_config is record
    -- CPU memory sizes, in words
    memsize    : natural;
    -- Per CPU message queue config.
    hmq_config : t_mt_mqueue_config;
    rmq_config : t_mt_mqueue_config;
  end record t_mt_cpu_config;

  type t_mt_cpu_config_array is array(t_maxcpu_range) of t_mt_cpu_config;

  type t_mt_config is record
    app_id          : std_logic_vector(31 downto 0);
    cpu_count       : natural range 1 to 8;
    cpu_config      : t_mt_cpu_config_array;
    -- shared memory size, in words
    shared_mem_size : integer range 256 to 65536;
  end record t_mt_config;

  constant c_DEFAULT_MT_CONFIG : t_mt_config :=
    (
      app_id          => x"115790de",
      cpu_count       => 2,
      cpu_config      => (others => (8192,
                                c_MT_DEFAULT_MQUEUE_CONFIG,
                                c_MT_DEFAULT_MQUEUE_CONFIG)),
      shared_mem_size => 2048
      );

  constant c_MOCK_TURTLE_SDB : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"00",
    wbd_endian    => c_SDB_ENDIAN_BIG,
    wbd_width     => x"7",                 -- 8/16/32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"000000000001ffff",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"000090de",
        version   => x"00000001",
        date      => x"20141201",
        name      => "Mock-Turtle-Core   ")));

  type t_mt_stream_sink_in_array2d is
    array(t_maxcpu_range) of t_mt_stream_sink_in_array(t_maxslot_range);
  type t_mt_stream_sink_out_array2d is
    array(t_maxcpu_range) of t_mt_stream_sink_out_array(t_maxslot_range);

  subtype t_mt_stream_source_in_array2d is t_mt_stream_sink_out_array2d;
  subtype t_mt_stream_source_out_array2d is t_mt_stream_sink_in_array2d;

  type t_mt_stream_config_in_array2d is
    array(t_maxcpu_range) of t_mt_stream_config_in_array(t_maxslot_range);
  type t_mt_stream_config_out_array2d is
    array(t_maxcpu_range) of t_mt_stream_config_out_array(t_maxslot_range);

  constant c_MT_STREAM_SINK_IN_ARRAY2D_DEFAULT_VALUE : t_mt_stream_sink_in_array2d :=
    (others => (others => c_MT_DUMMY_SINK_IN ) );

  constant c_MT_STREAM_SOURCE_IN_ARRAY2D_DEFAULT_VALUE : t_mt_stream_source_in_array2d :=
    (others => (others => c_MT_DUMMY_SOURCE_IN ) );

  constant c_MT_STREAM_CONFIG_IN_ARRAY2D_DEFAULT_VALUE : t_mt_stream_config_in_array2d :=
    (others => (others => c_MT_DUMMY_EP_CONFIG_IN ) );

  type t_mt_rmq_endpoint_iface_out is record
    src_out        : t_mt_stream_source_out_array2d;
    snk_out        : t_mt_stream_sink_out_array2d;
    src_config_out : t_mt_stream_config_out_array2d;
    snk_config_out : t_mt_stream_config_out_array2d;
  end record;

  type t_mt_rmq_endpoint_iface_in is record
    src_in        : t_mt_stream_source_in_array2d;
    snk_in        : t_mt_stream_sink_in_array2d;
    src_config_in : t_mt_stream_config_in_array2d;
    snk_config_in : t_mt_stream_config_in_array2d;
  end record;

  constant c_MT_RMQ_ENDPOINT_IFACE_IN_DEFAULT_VALUE : t_mt_rmq_endpoint_iface_in :=
    (
      c_MT_STREAM_SOURCE_IN_ARRAY2D_DEFAULT_VALUE,
      c_MT_STREAM_SINK_IN_ARRAY2D_DEFAULT_VALUE,
      c_MT_STREAM_CONFIG_IN_ARRAY2D_DEFAULT_VALUE,
      c_MT_STREAM_CONFIG_IN_ARRAY2D_DEFAULT_VALUE
      );

  component mock_turtle_core is
    generic (
      g_CONFIG            : t_mt_config := c_DEFAULT_MT_CONFIG;
      g_SYSTEM_CLOCK_FREQ : integer     := 62500000;
      g_CPU0_IRAM_INITF   : string      := "none";
      g_CPU1_IRAM_INITF   : string      := "none";
      g_CPU2_IRAM_INITF   : string      := "none";
      g_CPU3_IRAM_INITF   : string      := "none";
      g_CPU4_IRAM_INITF   : string      := "none";
      g_CPU5_IRAM_INITF   : string      := "none";
      g_CPU6_IRAM_INITF   : string      := "none";
      g_CPU7_IRAM_INITF   : string      := "none";
      g_WITH_WHITE_RABBIT : boolean     := FALSE);
    port (
      clk_i           : in  std_logic;
      rst_n_i         : in  std_logic;
      cpu_ext_rst_n_i : in  std_logic_vector(g_CONFIG.cpu_count-1 downto 0) := (others=>'1');
      sp_master_o     : out t_wishbone_master_out;
      sp_master_i     : in  t_wishbone_master_in                                  := c_DUMMY_WB_MASTER_IN;
      dp_master_o     : out t_wishbone_master_out_array(0 to g_CONFIG.cpu_count-1);
      dp_master_i     : in  t_wishbone_master_in_array(0 to g_CONFIG.cpu_count-1) := (others => c_DUMMY_WB_MASTER_IN);
      host_slave_i    : in  t_wishbone_slave_in;
      host_slave_o    : out t_wishbone_slave_out;
      rmq_endpoint_o  : out t_mt_rmq_endpoint_iface_out;
      rmq_endpoint_i  : in  t_mt_rmq_endpoint_iface_in := c_MT_RMQ_ENDPOINT_IFACE_IN_DEFAULT_VALUE;
      clk_ref_i       : in  std_logic                                             := '0';
      tm_i            : in  t_mt_timing_if                                        := c_DUMMY_MT_TIMING;
      gpio_o          : out std_logic_vector(31 downto 0);
      gpio_i          : in  std_logic_vector(31 downto 0)                         := (others => '0');
      hmq_in_irq_o    : out std_logic;
      hmq_out_irq_o   : out std_logic;
      notify_irq_o    : out std_logic;
      console_irq_o   : out std_logic);
  end component mock_turtle_core;

end mock_turtle_pkg;
