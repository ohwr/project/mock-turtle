# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

files = [
    "mock_turtle_core.vhd", 
    "mock_turtle_pkg.vhd",
    "mt_config_rom.vhd",
]

modules = { 
    "local" : [
        "cpu",
        "mqueue",
        "smem",
        "endpoint"
    ],
}
