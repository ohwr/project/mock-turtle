-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Mock Turtle
-- https://gitlab.cern.ch/coht/mockturtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_config_rom
--
-- description: A ROM to hold the various configuration values of the current
-- MockTurtle implementation.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mock_turtle_pkg.all;
use work.mt_mqueue_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;

entity mt_config_rom is
  generic(
    -- Message Queue and CPU configuration
    g_CONFIG            : t_mt_config := c_DEFAULT_MT_CONFIG;
    -- Frequency of clk_sys_i, in Hz
    g_SYSTEM_CLOCK_FREQ : integer     := 62500000;
    -- Enables/disables WR support
    g_WITH_WHITE_RABBIT : boolean     := FALSE);
  port (
    clk_i    : in  std_logic;
    slave1_i : in  t_wishbone_slave_in;
    slave1_o : out t_wishbone_slave_out;
    slave2_i : in  t_wishbone_slave_in;
    slave2_o : out t_wishbone_slave_out);
end entity mt_config_rom;

architecture arch of mt_config_rom is

  constant c_ROM_SIZE    : integer := 512;
  type t_word_array is array(natural range <>) of std_logic_vector(31 downto 0);
  subtype t_rom_array is t_word_array(0 to c_ROM_SIZE-1);
  constant c_MT_REVISION : integer := 20161208;

  function f_generate_rom return t_rom_array
  is
    function f_to_slv (n : natural; len : natural := 32)
      return std_logic_vector is
    begin
      return std_logic_vector(to_unsigned(n, len));
    end f_to_slv;

    subtype t_queue_rom_array is t_word_array(0 to 15);

    function f_generate_mqueue_rom (qcfg : t_mt_mqueue_config)
      return t_queue_rom_array
    is
      variable res : t_queue_rom_array := (others => (others => '0'));
    begin
      for i in 0 to qcfg.slot_count - 1 loop
        res(2*i + 0) := x"00"
                        & f_to_slv(qcfg.slot_config(i).entries_bits, 8)
                        & f_to_slv(qcfg.slot_config(i).width_bits, 8)
                        & f_to_slv(qcfg.slot_config(i).header_bits, 8);
        res(2*i + 1) := qcfg.slot_config(i).endpoint_id;
      end loop;
      return res;
    end f_generate_mqueue_rom;

    variable cfg : t_rom_array := (others => (others => '0'));
  begin

    cfg(0)    := f_to_std_logic_vector("TRTL");
    cfg(1)    := f_to_slv(c_MT_REVISION);
    cfg(3)    := f_to_slv(g_SYSTEM_CLOCK_FREQ);
    cfg(4)(0) := f_to_std_logic(g_WITH_WHITE_RABBIT);
    cfg(5)    := g_CONFIG.app_id;
    cfg(6)    := f_to_slv(g_CONFIG.cpu_count);
    cfg(7)    := f_to_slv(g_CONFIG.shared_mem_size);

    for i in 0 to g_CONFIG.cpu_count - 1 loop
      cfg(8+i)  := f_to_slv(g_CONFIG.cpu_config(i).memsize);
      cfg(16+i) := f_to_slv(g_CONFIG.cpu_config(i).hmq_config.slot_count);
      cfg(24+i) := f_to_slv(g_CONFIG.cpu_config(i).rmq_config.slot_count);
      cfg(128 + 16 * i to 143 + 16 * i) :=
        f_generate_mqueue_rom(g_CONFIG.cpu_config(i).hmq_config);
      cfg(256 + 16 * i to 271 + 16 * i) :=
        f_generate_mqueue_rom(g_CONFIG.cpu_config(i).rmq_config);
    end loop;

    return cfg;
  end function f_generate_rom;

  constant c_ROM : t_rom_array := f_generate_rom;

begin  -- arch

  p_wb_transaction : process(clk_i)
  begin
    if rising_edge(clk_i) then
      slave1_o.ack <= slave1_i.cyc and slave1_i.stb;
      slave2_o.ack <= slave2_i.cyc and slave2_i.stb;
      slave1_o.dat <= c_ROM(to_integer(unsigned(slave1_i.adr(10 downto 2))));
      slave2_o.dat <= c_ROM(to_integer(unsigned(slave2_i.adr(10 downto 2))));
    end if;
  end process p_wb_transaction;

  slave1_o.stall <= '0';
  slave1_o.err   <= '0';
  slave1_o.rty   <= '0';

  slave2_o.stall <= '0';
  slave2_o.err   <= '0';
  slave2_o.rty   <= '0';

end architecture arch;
