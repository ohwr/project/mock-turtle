# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

sim_tool   = "modelsim"
sim_top    = "main"
action     = "simulation"
target     = "xilinx"
syn_device = "xc6slx150t"
vcom_opt   = "-93 -mixedsvvh"

fetchto = "../../ip_cores"

include_dirs = [
    "../include/",
#    "../include/regs/",
    fetchto + "/wr-cores/sim",
    fetchto + "/general-cores/sim/",
]

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
#sim_pre_cmd = "EXTRA2_CFLAGS='-DSIMULATION' make -C ../../../tests/firmware/rmq-udp-send defconfig all"

files = [
    "main.sv",
]

modules = {
    "local" :  [
        "../../rtl",
    ],
    "git" : [
        "git://ohwr.org/hdl-core-lib/general-cores.git",
        "git://ohwr.org/hdl-core-lib/wr-cores.git",
        "git://ohwr.org/hdl-core-lib/urv-core.git",
    ],
}
