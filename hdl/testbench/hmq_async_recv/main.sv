// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   main
//
// description: A SystemVerilog testbench for the supplied SPEC MT demo, using
//              the hmq_async_recv test firmware

`timescale 1ns/1ps

`include "gn4124_bfm.svh"
`include "mock_turtle_driver.svh"

module main;

   IGN4124PCIMaster Host ();

   reg clk_125m = 0;

   always #4ns clk_125m <= ~clk_125m;

   // the Device Under Test
   spec_mt_demo
     DUT
     (
      .clk_125m_pllref_p_i (clk_125m),
      .clk_125m_pllref_n_i (~clk_125m),
      .led_act_o           (),
      .led_link_o          (),
      .button1_n_i         (1'b1),
      .aux_leds_o          (),
      .pcbrev_i            (4'b0),
      .fmc0_prsnt_m2c_n_i  (1'b1),
      .fmc0_scl_b          (),
      .fmc0_sda_b          (),
      .gn_rst_n_i          (Host.rst_n),
      .gn_p2l_clk_n_i      (Host.p2l_clk_n),
      .gn_p2l_clk_p_i      (Host.p2l_clk_p),
      .gn_p2l_rdy_o        (Host.p2l_rdy),
      .gn_p2l_dframe_i     (Host.p2l_dframe),
      .gn_p2l_valid_i      (Host.p2l_valid),
      .gn_p2l_data_i       (Host.p2l_data),
      .gn_p_wr_req_i       (Host.p_wr_req),
      .gn_p_wr_rdy_o       (Host.p_wr_rdy),
      .gn_rx_error_o       (Host.rx_error),
      .gn_l2p_clk_n_o      (Host.l2p_clk_n),
      .gn_l2p_clk_p_o      (Host.l2p_clk_p),
      .gn_l2p_dframe_o     (Host.l2p_dframe),
      .gn_l2p_valid_o      (Host.l2p_valid),
      .gn_l2p_edb_o        (Host.l2p_edb),
      .gn_l2p_data_o       (Host.l2p_data),
      .gn_l2p_rdy_i        (Host.l2p_rdy),
      .gn_l_wr_rdy_i       (Host.l_wr_rdy),
      .gn_p_rd_d_rdy_i     (Host.p_rd_d_rdy),
      .gn_tx_error_i       (Host.tx_error),
      .gn_vc_rdy_i         (Host.vc_rdy),
      .gn_gpio_b           ());

   IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT.U_Mock_Turtle));

   string fw = "../../../tests/firmware/hmq-async-recv/fw-hmq-async-recv.bin";

   const uint64_t mt_base = 'h2_0000;

   MockTurtleDriver drv;

   uint32_t core_count;

   CBusAccessor acc;

   semaphore processCounter;

   initial begin

      $timeformat (-6, 3, "us", 10);

      wait (Host.ready == 1'b1);

      acc = Host.get_accessor();

      drv = new (acc, mt_base, IrqMonitor);

      // 1us is the default update delay value, this serves as an example
      // on how to change the update loop delay
      drv.init(.update_delay(1us));

      core_count = drv.rom.getCoreCount();

      processCounter = new(core_count);

      for (uint32_t i = 0; i < core_count; i++)
        begin
           drv.enable_console_irq (i, 1);
           for (uint32_t j = 0; j < drv.rom.getHmqSlotCount(i); j++)
             begin
                drv.enable_hmqi_irq(i, j, 1);
                drv.enable_hmqo_irq(i, j, 1);
             end
           drv.load_firmware(i, fw, 1'b0);
        end

      for (uint32_t i = 0; i < core_count; i++)
        begin
           drv.reset_core(i, 0);
           fork
              // unique copy of i to be used inside the forked process
              automatic uint32_t k = i;
              begin
                 processCounter.get();
                 for (uint32_t j = 0; j < drv.rom.getHmqSlotCount(k); j++)
                   begin
                      automatic uint32_t dim = drv.rom.getHmqDimensions(k, j);
                      automatic uint32_t ne = `TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(dim);
                      automatic uint32_t np = `TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(dim);
                      automatic MQueueMsg msg = new (k, j);
                      drv.mdisplay($sformatf("receiving %0d messages from cpu %0d, hmq %0d",
                                             ne, k, j));
                      for (uint32_t m = 0; m < ne; m++)
                        begin
                           while (drv.hmq_pending_messages(k, j) == 1'b0)
                             begin
                                #1us;
                             end
                           drv.hmq_receive_message(msg);
                           assert(msg.header.seq == m);
                           assert(msg.header.len == np);
                           for (uint32_t v = 0; v < msg.header.len; v++)
                             assert (msg.data[v] == v)
                               else begin
                                  $error("Error in received message: %s", msg.tostring());
                                  $finish;
                               end
                        end
                      assert (drv.hmq_pending_messages(k, j) == 1'b0);
                      drv.mdisplay($sformatf("%0d messages received OK from cpu %0d, hmq %0d",
                                             ne, k, j));
                   end // for (uint32_t j = 0; j < drv.rom.getHmqSlotCount(k); j++)
                 processCounter.put();
              end // fork begin
           join_none
        end // for (uint32_t i = 0; i < core_count; i++)
      // will block until all forks have returned their semaphore keys
      processCounter.get(core_count);
      $display ("Simulation PASSED");
      $finish;

   end // initial begin

endmodule // main
