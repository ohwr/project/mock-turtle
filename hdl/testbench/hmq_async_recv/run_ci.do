# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

# Modelsim run script for continuous integration
# execute: vsim -c -do "run_ci.do"

vsim -quiet -L unisim work.main
set StdArithNoWarnings 1
set NumericStdNoWarnings 1

run -all

exit

