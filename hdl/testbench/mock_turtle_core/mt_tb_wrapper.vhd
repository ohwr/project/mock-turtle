-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

--------------------------------------------------------------------------------
-- CERN BE-CEM-EDL
-- Mock Turtle
-- https://ohwr.org/project/mock-turtle
--------------------------------------------------------------------------------
--
-- unit name:   mt_tb_wrapper
--
-- description: Testbench wrapper for mock-turtle core
--
-- This wrapper is needed in order to provide a custom MT configuration generic
-- to the systemverilog testbench.

library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;
use work.mock_turtle_pkg.all;
use work.mt_mqueue_pkg.all;

entity mt_tb_wrapper is

  port (
    clk_i          : in  std_logic;
    rst_n_i        : in  std_logic;
    sp_master_o    : out t_wishbone_master_out;
    sp_master_i    : in  t_wishbone_master_in;
    dp_master_o    : out t_wishbone_master_out_array(0 to 1);
    dp_master_i    : in  t_wishbone_master_in_array(0 to 1);
    rmq_endpoint_o : out t_mt_rmq_endpoint_iface_out;
    rmq_endpoint_i : in  t_mt_rmq_endpoint_iface_in    := c_MT_RMQ_ENDPOINT_IFACE_IN_DEFAULT_VALUE;
    host_slave_i   : in  t_wishbone_slave_in;
    host_slave_o   : out t_wishbone_slave_out;
    clk_ref_i      : in  std_logic;
    tm_i           : in  t_mt_timing_if;
    gpio_o         : out std_logic_vector(31 downto 0);
    gpio_i         : in  std_logic_vector(31 downto 0) := x"00000000";
    hmq_in_irq_o   : out std_logic;
    hmq_out_irq_o  : out std_logic;
    notify_irq_o   : out std_logic;
    console_irq_o  : out std_logic);

end mt_tb_wrapper;

architecture arch of mt_tb_wrapper is

  constant c_MT_CONFIG : t_mt_config := (
    app_id     => x"d330d330",
    cpu_count  => 2,
    cpu_config => (0 =>
                   (memsize => 8192,
                    hmq_config => (8, (0      => (7, 3, 2, x"0000_0000", false),
                                       others => (5, 4, 3, x"0000_0000", false))),
                    rmq_config => (8, (0      => (7, 2, 2, x"0000_0000", false),
                                       others => (5, 4, 3, x"0000_0000", false)))),
                   1 =>
                   (memsize => 8192,
                    hmq_config => (1, (0      => (7, 3, 2, x"0000_0000", false),
                                       others => (c_DUMMY_MT_MQUEUE_SLOT))),
                    rmq_config => c_MT_DEFAULT_MQUEUE_CONFIG),
                   others => (0, c_DUMMY_MT_MQUEUE_CONFIG, c_DUMMY_MT_MQUEUE_CONFIG)),
    shared_mem_size => 2048);

begin  -- architecture arch

  cmp_mock_turtle_core : mock_turtle_core
    generic map (
      g_CONFIG            => c_MT_CONFIG,
      g_WITH_WHITE_RABBIT => false)
    port map (
      clk_i          => clk_i,
      rst_n_i        => rst_n_i,
      sp_master_o    => sp_master_o,
      sp_master_i    => sp_master_i,
      dp_master_o    => dp_master_o,
      dp_master_i    => dp_master_i,
      rmq_endpoint_o => rmq_endpoint_o,
      rmq_endpoint_i => rmq_endpoint_i,
      host_slave_i   => host_slave_i,
      host_slave_o   => host_slave_o,
      clk_ref_i      => clk_ref_i,
      tm_i           => tm_i,
      gpio_o         => gpio_o,
      gpio_i         => gpio_i,
      hmq_in_irq_o   => hmq_in_irq_o,
      hmq_out_irq_o  => hmq_out_irq_o,
      notify_irq_o   => notify_irq_o,
      console_irq_o  => console_irq_o);

end architecture arch;
