# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

sim_tool   = "modelsim"
sim_top    = "main"
action     = "simulation"
target     = "xilinx"
syn_device = "xc6slx150t"
vcom_opt   = "-93 -mixedsvvh"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

include_dirs = [
    "../include/",
    "../include/regs/",
    fetchto + "/general-cores/sim/",
]

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
#sim_pre_cmd = "EXTRA2_CFLAGS='-DSIMULATION' make -C ../../../tests/firmware/sim-verif defconfig all"

files = [
    "main.sv",
    "mt_tb_wrapper.vhd",
]

modules = {
    "local" :  [
        "../../rtl",
    ],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
    ],
}
