// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   main
//
// description: A SystemVerilog testbench to exercise all the main features of
// Mock Turtle core.

`include "mock_turtle_driver.svh"
`include "vhd_wishbone_master.svh"

import mock_turtle_pkg::*;

`timescale 1ns/1ps

module main;

   reg rst_n = 0;
   reg clk_sys = 0;

   t_mt_rmq_endpoint_iface_out mt2ep;
   t_mt_rmq_endpoint_iface_in ep2mt;

   always #8ns clk_sys <= ~clk_sys;

   initial begin
      repeat(20) @(posedge clk_sys);
      rst_n = 1;
   end

   mt_tb_wrapper #
     (
      )
   DUT (
	.clk_i         (clk_sys),
	.rst_n_i       (rst_n),
	.host_slave_i  (Host.out),
	.host_slave_o  (Host.in),
	.sp_master_o   (),
	.sp_master_i   (),
	.dp_master_o   (),
	.dp_master_i   (),
	.rmq_endpoint_o(mt2ep),
	.rmq_endpoint_i(ep2mt),
	.clk_ref_i     (),
	.tm_i          (),
	.gpio_o        (),
	.gpio_i        (),
	.hmq_in_irq_o  (),
	.hmq_out_irq_o (),
	.notify_irq_o  (),
	.console_irq_o ()
	);

   generate
   for (genvar slot = 0; slot < 8; slot++) begin
	 assign ep2mt.snk_in[0][slot] = mt2ep.src_out[0][slot];
	 assign ep2mt.src_in[0][slot] = mt2ep.snk_out[0][slot];
      end
   endgenerate

   IVHDWishboneMaster Host ( clk_sys, rst_n );

   IMockTurtleIRQ IrqMonitor (`MT_ATTACH_IRQ(DUT.cmp_mock_turtle_core));

   string fw = "../../../tests/firmware/sim-verif/sim-verif.bin";

   const uint64_t mt_base = 'h0000;

   MockTurtleDriver drv;

   uint64_t count;

   task inc_counter();
      uint32_t val;

      drv.smem_read (0, val);

      if (val != count)
	$fatal (1, "incorrect count (%0d, expected %0d)", val, count);
      count++;
      drv.smem_write (0, count);
   endtask // inc_counter

   task check_cpu_notifications ();
      t_notify_queue ntf;
      automatic MQueueMsg msg;
      drv.get_cpu_notifications (0, ntf);
      while (ntf.size)
	begin
	   inc_counter();
	   ntf.delete(0);
	   msg = new (0, 0);
	   msg.header.len = 1;
	   msg.data[0] = 0;
	   drv.hmq_send_message (msg);
	end
   endtask // check_cpu_notifications

   task check_hmq_incoming (int slot);
      // check from pending messages
      while (drv.hmq_pending_messages(0, slot))
	begin
	   automatic MQueueMsg msg = new (0, slot);
	   drv.hmq_receive_message (msg);
	   inc_counter();
	   for (int i = 0; i < msg.data.size(); i++)
	     msg.data[i] = ~msg.data[i];
	   drv.hmq_send_message (msg);
	end
   endtask // check_hmq_incoming

   task check_debug_interface(int core);
      uint32_t v;

      //  Enable debug
      drv.dbg.force_core(core, 1);
      drv.reset_core(core, 0);
      drv.dbg.force_core(core, 0);

      //  Check write/read
      drv.dbg.write_word(core, 32'h4, 32'h12349876);
      drv.dbg.read_word(core, 32'h4, v);
      if (v != 32'h12349876)
	$fatal(1, "debug interface: r/w memory");
   endtask : check_debug_interface

   task check_final_count (uint32_t expected);
      uint32_t val;
      #600us;

      drv.smem_read (0, val);
      if (val != expected)
	$fatal (1, "Simulation FAILED: incorrect final count (%0d, expected %0d)", val, expected);
      else
	$display ("Simulation PASSED");
	$finish;
   endtask // check_final_result

   CWishboneAccessor acc;

   initial begin

      acc = Host.get_accessor();

      acc.set_mode (PIPELINED);

      $timeformat (-6, 3, "us", 10);

      #10us;

      drv = new (acc, mt_base, IrqMonitor);

      drv.init();

      drv.enable_console_irq (0, 1);

      for (int slot = 0; slot < 8; slot++)
	drv.enable_hmqi_irq (0, slot, 1);

      check_debug_interface(0);

      fork
	 check_final_count(240);
      join_none

      count = 0;

      drv.load_firmware (0, fw, 1'b0);

      drv.reset_core (0, 0);

      forever begin
	 check_cpu_notifications ();
	 for (int slot = 0; slot < 8; slot++)
	   check_hmq_incoming (slot);
	 # 1us;
      end

   end // initial begin

endmodule // main
