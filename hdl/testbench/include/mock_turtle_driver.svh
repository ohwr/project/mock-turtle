// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MockTurtleDriver
//
// description: A SystemVerilog Class to provide an abstraction of the complete
// MockTurtle core.

`ifndef __MOCK_TURTLE_DRIVER_INCLUDED
 `define __MOCK_TURTLE_DRIVER_INCLUDED

 `include "simdrv_defs.svh"
 `include "mt_cpu_csr_driver.svh"
 `include "mt_queue_message.svh"
 `include "mt_hmq_driver.svh"
 `include "mt_debug_driver.svh"
 `include "mt_config_rom_driver.svh"

// Simple interface to deliver the four interrupts of MT to the
// MockTurtleDriver class below
interface IMockTurtleIRQ (
			  input logic hmq_in,
			  input logic hmq_out,
			  input logic notify,
			  input logic console);
endinterface // IMockTurtleIRQ

// Macro to simplify attaching one side of the interface to signals
// in mock_turtle_core.vhd.
// (The "other" side is monitored by the MockTurtleDriver class below)
 `define MT_ATTACH_IRQ(mt)\
.hmq_in  (mt.hmq_in_irq),\
.hmq_out (mt.hmq_out_irq),\
.notify  (mt.notify_irq),\
.console (mt.console_irq)

// A class can only accept virtual interfaces, so we declare it here
typedef virtual IMockTurtleIRQ vIMockTurtleIRQ;

// The main class, to monitor and drive the complete MT
class MockTurtleDriver;
   protected string name;
   protected CBusAccessor acc;
   protected uint64_t base;
   protected uint32_t core_count;
   protected MTCPUControl csr;
   protected MQueueHost hmq[];
   protected vIMockTurtleIRQ irq;
   MTConfigRom rom;
   MDebug dbg;

   function  new ( CBusAccessor acc, uint64_t base,
		   vIMockTurtleIRQ irq, string name = "" );
      this.name = name;
      this.acc  = acc;
      this.base = base;
      this.irq  = irq;
   endfunction // new

   task mdisplay ( string str );
      string	    tmp;
      if (this.name == "")
	tmp = $sformatf("<%t> %s", $realtime, str);
      else
	tmp = $sformatf("[%s] <%t> %s", this.name, $realtime, str);
      $display (tmp);
   endtask // mdisplay

   task init (time update_delay = 1us);
      uint32_t slot_count;
      // first init the config ROM, to be able to get core count etc.
      rom = new ( acc, base + 'he000 );
      rom.init();
      core_count = rom.getCoreCount();
      mdisplay ($sformatf("App ID: 0x%x", rom.getAppID));
      mdisplay ($sformatf("Core count: %0d", core_count));
      // Next init the MT CSR
      csr = new ( acc, base + 'hc000, core_count, name );
      csr.init();
      // Debug interface
      dbg = new (acc, csr);
      // Host message queue objects are one per cpu core
      hmq = new [core_count];
      for (int i = 0; i < core_count; i++)
	begin
	   slot_count = rom.getHmqSlotCount(i);
	   // CSR is passed to each queue to be able to select slot
	   // inside the class via csr.hmq_select()
	   hmq[i] = new ( acc, base + 'h0000, i, csr, slot_count );
	   hmq[i].init();
	end
      fork
	 forever begin
	    _update();
	    #(update_delay);
	 end
      join_none
   endtask // init

   task load_firmware ( int core, string filename, bit check = 1'b1);
      csr.load_firmware ( core, filename, check );
   endtask // load_firmware

   task reset_core ( int core, int reset );
      csr.reset_core ( core, reset );
   endtask // reset_core

   task cfg_rom_display();
      rom.pretty_print();
   endtask // cfg_rom_display

   task cfg_rom_dump();
      rom.dump();
   endtask // cfg_rom_dump

   task enable_console_irq ( int core, int enable );
      csr.uart_int_enable ( core, enable );
   endtask // enable_console_irq

   task enable_hmqi_irq ( int core, int queue, int enable );
      csr.hmq_in_int_enable ( core, queue, enable );
   endtask // enable_hmqi_irq

   task enable_hmqo_irq ( int core, int queue, int enable );
      csr.hmq_out_int_enable ( core, queue, enable );
   endtask // enable_hmqo_irq

   task smem_read ( input uint32_t offset, ref uint32_t val );
      uint64_t tmp;
      acc.read (base + 32'h1_0000 + offset, tmp);
      val = tmp;
   endtask // smem_read

   task smem_write ( uint32_t offset, uint32_t val);
      acc.write (base + 32'h1_0000 + offset, val);
   endtask // smem_write

   function bit pending_cpu_notifications (uint32_t core);
      return ( csr.notify_queue[core].size() != 0 );
   endfunction // pending_cpu_notification

   task get_single_cpu_notification (int core, ref uint32_t val);
      if ( pending_cpu_notifications (core) )
	val = csr.notify_queue[core].pop_front();
   endtask // get_single_cpu_notification

   task get_cpu_notifications (int core, ref t_notify_queue ntf);
      ntf = csr.notify_queue[core];
      csr.notify_queue[core] = {};
   endtask // get_cpu_notifications

   task hmq_receive_message (ref MQueueMsg msg);
      msg = hmq[msg.core].receive_message (msg.slot);
   endtask // hmq_receive_message

   task hmq_peek_message (ref MQueueMsg msg);
      msg = hmq[msg.core].peek_message (msg.slot);
   endtask // hmq_peek_message

   task hmq_send_message (input MQueueMsg msg);
      hmq[msg.core].send_message (msg.slot, msg);
   endtask // hmq_send_message

   task hmq_purge (int core, int slot);
      hmq[core].purge (slot);
   endtask // hmq_purge

   task hmq_purge_all (int core);
      for (int slot = 0; slot < rom.getHmqSlotCount(core); slot++)
	hmq[core].purge (slot);
   endtask // hmq_purge_all

   function bit hmq_pending_messages (uint32_t core, uint32_t slot);
      return hmq[core].pending_in(slot);
   endfunction // hmq_pending_messages

   // internal task, used by init, not to be called directly
   task _update();
      uint32_t val;
      if (irq.console == 1'b1)
	begin
	   csr.check_consoles();
	end
      if (irq.notify == 1'b1)
	begin
	   csr.handle_notification_irq();
	end
      if (irq.hmq_in == 1'b1)
	begin
	   for (int i = 0; i < core_count; i++)
	     begin
		hmq[i].receive_pending();
	     end
	end
      for (int i = 0; i < core_count; i++)
	begin
	   hmq[i].send_pending();
	end
   endtask // _update

endclass // MockTurtleDriver

`endif //  `ifndef __MOCK_TURTLE_DRIVER_INCLUDED
