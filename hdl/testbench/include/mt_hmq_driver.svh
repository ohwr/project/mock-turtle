// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MQueueHost
//
// description: A SystemVerilog Class to provide an abstraction of the
// MockTurtle core's host message queue.

`ifndef __MT_MQUEUE_HOST_DRIVER_INCLUDED
 `define __MT_MQUEUE_HOST_DRIVER_INCLUDED

 `define MQUEUE_BASE_IN  ('h0000)
 `define MQUEUE_BASE_OUT ('h8000)

 `define MQUEUE_CMD_CLAIM   (1<<24)
 `define MQUEUE_CMD_PURGE   (1<<25)
 `define MQUEUE_CMD_READY   (1<<26)
 `define MQUEUE_CMD_DISCARD (1<<27)

 `define MQUEUE_SLOT_COMMAND 0
 `define MQUEUE_SLOT_STATUS  4

 `define MQUEUE_SLOT_HEADER 16'h1000
 `define MQUEUE_SLOT_DATA   16'h2000

 `include "mt_queue_message.svh"

class MQueueHost;
   protected CBusAccessor bus;
   protected uint32_t base;
   protected uint32_t core;
   protected uint32_t slot_count;
   protected MTCPUControl csr;

   typedef MQueueMsg slot_queue_t[$];

   slot_queue_t slots_in[], slots_out[];

   function new( CBusAccessor bus, uint32_t base, uint32_t core,
		 MTCPUControl csr, uint32_t slot_count);
      this.base       = base;
      this.bus        = bus;
      this.core       = core;
      this.csr        = csr;
      this.slot_count = slot_count;
   endfunction // new

   task init();
      this.slots_in  = new[slot_count];
      this.slots_out = new[slot_count];
      for (int i = 0; i < slot_count; i++)
	begin
	   this.slots_in[i]  = {};
	   this.slots_out[i] = {};
	   purge(i);
	end
   endtask // init

   task read (uint32_t offset, output uint32_t rv);
      uint64_t tmp;
      bus.read (base + offset, tmp);
      rv = tmp;
   endtask // read

   task write (uint32_t offset, uint32_t rv);
      bus.write (base + offset, rv);
   endtask // write

   task incoming_write (uint32_t r, uint32_t v);
      write (`MQUEUE_BASE_IN + r, v);
   endtask // incoming_write

   task outgoing_write (uint32_t r, uint32_t v);
      write (`MQUEUE_BASE_OUT + r, v);
   endtask // outgoing_write

   task incoming_read (uint32_t r, ref uint32_t v);
      read (`MQUEUE_BASE_IN + r, v);
   endtask // incoming_read

   task outgoing_read (uint32_t r, ref uint32_t v);
      read (`MQUEUE_BASE_OUT + r, v);
   endtask // outgoing_read

   task incoming_purge ();
      incoming_write (`MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_PURGE );
   endtask // incoming_purge

   task outgoing_purge ();
      outgoing_write (`MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_PURGE );
   endtask // outgoing_purge

   task purge (int slot);
      csr.hmq_select (this.core, slot);
      incoming_purge ();
      outgoing_purge ();
   endtask // purge

   task incoming_check_full (output int full);
      uint32_t rv;
      incoming_read (`MQUEUE_SLOT_STATUS, rv);
      full = (rv & 1) ? 1:  0;
   endtask // incoming_check_full

   task outgoing_check_full (output int full);
      uint32_t rv;
      outgoing_read (`MQUEUE_SLOT_STATUS, rv);
      full = (rv & 1) ? 1:  0;
   endtask // outgoing_check_full

   task slot_send ( int slot );

      MQueueMsg msg;

      u32_queue hdr;

      csr.hmq_select ( core, slot );

      msg = slots_out[slot].pop_back();

      hdr = msg.hdr_pack ();

      outgoing_write (`MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_CLAIM);
      for (int i = 0; i < hdr.size(); i++)
	begin
	   outgoing_write (`MQUEUE_SLOT_HEADER + i * 4, hdr[i]);
	end
      for (int i = 0; i < msg.header.len; i++)
	begin
	   outgoing_write (`MQUEUE_SLOT_DATA + i * 4, msg.data[i]);
	end
      outgoing_write (`MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_READY);

   endtask // slot_send

   task slot_recv ( int slot );

      MQueueMsg msg;

      uint32_t val;

      uint32_t hdr[$];

      csr.hmq_select ( core, slot );

      msg = new ( core, slot );

      hdr = {};
      incoming_read (`MQUEUE_SLOT_HEADER + 0, val );
      hdr[0] = val;
      incoming_read (`MQUEUE_SLOT_HEADER + 4, val );
      hdr[1] = val;
      incoming_read (`MQUEUE_SLOT_HEADER + 8, val );
      hdr[2] = val;

      msg.set_header ( hdr );

      for (int i = 0; i < msg.header.len; i++)
	begin
	   incoming_read (`MQUEUE_SLOT_DATA + 4*i, val);
	   msg.data.push_back(val);
	end

      incoming_write(`MQUEUE_SLOT_COMMAND, `MQUEUE_CMD_DISCARD );

      slots_in[slot].push_front(msg);

   endtask // slot_recv

   function MQueueMsg receive_message (int slot);
      MQueueMsg ret = slots_in[slot].pop_back();
      return ret;
   endfunction // receive_message

   function MQueueMsg peek_message (int slot);
      MQueueMsg ret = slots_in[slot][$];
      return ret;
   endfunction // peek_message

   function void send_message (int slot, MQueueMsg msg);
      slots_out[slot].push_front(msg);
   endfunction // send_message

   function bit pending_in (int slot);
      return slots_in[slot].size() != 0 ? 1'b1 : 1'b0;
   endfunction // pending_in

   task receive_pending();

      uint64_t stat, imsk;
      uint32_t core_stat, core_imsk;

      csr.get_hmq_in_status(stat);

      core_stat = (stat >> (core*8)) & 'hff;

      if (core_stat == 0)
	return;

      csr.get_hmq_in_int_en(imsk);

      core_imsk = (imsk >> (core*8)) & 'hff;

      core_stat &= core_imsk;

      if (core_stat == 0)
	return;

      for (int i = 0; i < slot_count; i++)
	begin
	   if (core_stat & (1 << i))
	     slot_recv(i);
	end
   endtask // receive_pending

   task send_pending();
      uint32_t full;

      for (int i = 0; i < slot_count ;i++)
	begin
	   while ( slots_out[i].size() )
	     begin
		csr.hmq_select (core, i);
		outgoing_check_full(full);
		if (full)
		  break;
		slot_send(i);
	     end
	end
   endtask // send_pending

endclass // MQueueHost

`endif //  `ifndef __MT_MQUEUE_HOST_DRIVER_INCLUDED
