// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MDebug
//
// description: A SystemVerilog Class to provide an abstraction of the
// MockTurtle core's debug interface.

`ifndef __MT_DEBUG_DRIVER_INCLUDED
 `define __MT_DEBUG_DRIVER_INCLUDED

/* [0x180]: REG Debug Interface Status Register */
`define MT_CPU_CSR_REG_DBG_STATUS 32'h00000180
/* [0x184]: REG Debug Interface Force Register */
`define MT_CPU_CSR_REG_DBG_FORCE 32'h00000184
/* [0x188]: REG Debug Interface Instruction Ready Register */
`define MT_CPU_CSR_REG_DBG_INSN_READY 32'h00000188
/* [0x18c]: REG Debug Interface Core[0] Instruction Register */
`define MT_CPU_CSR_REG_DBG_CORE0_INSN 32'h0000018c
/* [0x190]: REG Debug Interface Core[1] Instruction Register */
`define MT_CPU_CSR_REG_DBG_CORE1_INSN 32'h00000190
/* [0x194]: REG Debug Interface Core[0] Mailbox Data Register */
`define MT_CPU_CSR_REG_DBG_CORE0_MBX 32'h00000194
/* [0x198]: REG Debug Interface Core[1] Mailbox Data Register */
`define MT_CPU_CSR_REG_DBG_CORE1_MBX 32'h00000198

class MDebug;
   protected CBusAccessor bus;
   protected MTCPUControl csr;

   function new( CBusAccessor bus, MTCPUControl csr);
      this.bus        = bus;
      this.csr        = csr;
   endfunction // new

   local task read (uint32_t offset, ref uint32_t rv);
      csr.readl (offset, rv);
   endtask // read

   local task write (uint32_t offset, uint32_t rv);
      csr.writel (offset, rv);
   endtask // write

   task force_core(int core, uint32_t v);
      uint32_t val;
      read(`MT_CPU_CSR_REG_DBG_FORCE, val);
      val[core] = v;
      write(`MT_CPU_CSR_REG_DBG_FORCE, val);
   endtask

   local task write_mbx(int core, uint32_t v);
      write(`MT_CPU_CSR_REG_DBG_CORE0_MBX + 4 * core, v);
   endtask

   local task read_mbx(int core, output uint32_t v);
      read(`MT_CPU_CSR_REG_DBG_CORE0_MBX + 4 * core, v);
   endtask

   local task exec_insn(int core, uint32_t insn);
      write(`MT_CPU_CSR_REG_DBG_CORE0_INSN + 4 * core, insn);
   endtask

   local task exec_mbx_to_reg(int core, uint32_t r);
      exec_insn(core, 32'h7d002073 + (r << 7));
   endtask

   local task exec_reg_to_mbx(int core, uint32_t r);
      exec_insn(core, 32'h7d001073 + (r << 15));
   endtask

   local task exec_nop(int core);
      exec_insn(core, 32'h00000013);
   endtask

   task read_reg(int core, int r, output uint32_t v);
      exec_reg_to_mbx(core, r);
      exec_nop(core);
      exec_nop(core);
      exec_nop(core);
      read_mbx(core, v);
   endtask

   task write_reg(int core, int r, uint32_t v);
      write_mbx(core, v);
      exec_mbx_to_reg(core, r);
   endtask

   task read_pc_via_ra(int core, output uint32_t pc);
      // Read current pc value but destroy ra.
      exec_insn(core, 32'h000000ef); // ra = pc + 4
      exec_nop(core);
      exec_nop(core);
      exec_nop(core);
      exec_reg_to_mbx(core, 1);
      exec_nop(core);
      exec_nop(core);
      exec_nop(core);
      read_mbx(core, pc);
      pc -= 4;
   endtask

   task write_pc_via_ra(int core);
      exec_insn(core, 32'h00008067); // ret
      exec_nop(core);
      exec_nop(core);
      exec_nop(core);
   endtask

   task advance_pc_4(int core);
      exec_insn(core, 32'h00000263); // beqz zero, +4
      exec_nop(core);
      exec_nop(core);
      exec_nop(core);
   endtask

   task write_word(int core, uint32_t addr, uint32_t v);
      uint32_t a0, a1;

      read_reg(core, 10, a0);
      read_reg(core, 11, a1);
      write_reg(core, 10, addr);
      write_reg(core, 11, v);
      exec_insn(core, 32'h00b52023); // sw a1,0(a0)
      // exec_insn(core, 32'h00450513); // addi a0, a0, 4
      write_reg(core, 10, a0);
      write_reg(core, 11, a1);
   endtask

   task read_word(int core, uint32_t addr, output uint32_t v);
      uint32_t a0, a1;

      read_reg(core, 10, a0);
      read_reg(core, 11, a1);
      write_reg(core, 10, addr);
      exec_insn(core, 32'h00052583); // lw a1,0(a0)
      read_reg(core, 11, v);
      write_reg(core, 10, a0);
      write_reg(core, 11, a1);
   endtask

   /*
   task incoming_write (uint32_t r, uint32_t v);
      write (`MQUEUE_BASE_IN + r, v);
   endtask // incoming_write
*/
endclass

`endif
