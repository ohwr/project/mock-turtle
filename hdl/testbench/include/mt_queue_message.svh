// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: CERN-OHL-W-2.0+

//------------------------------------------------------------------------------
// CERN BE-CO-HT
// Mock Turtle
// https://gitlab.cern.ch/coht/mockturtle
//------------------------------------------------------------------------------
//
// unit name:   MQueueMsg
//
// description: A SystemVerilog Class to provide an abstraction of the
// MockTurtle core's queue message.

`ifndef __MT_MQUEUE_MSG_INCLUDED
 `define __MT_MQUEUE_MSG_INCLUDED

 `define TRTL_HMQ_HEADER_FLAG_SYNC (1 << 0)
 `define TRTL_HMQ_HEADER_FLAG_ACK  (1 << 1)
 `define TRTL_HMQ_HEADER_FLAG_RPC  (1 << 2)

typedef uint32_t u32_queue[$];

typedef struct {
   uint32_t app_id;
   uint32_t flags;
   uint32_t msg_id;
   uint32_t len;
   uint32_t sync_id;
   uint32_t seq;
} mqueue_msg_header;

class MQueueMsg;
   int core;
   int slot;
   mqueue_msg_header header;
   u32_queue data;

   function new ( int core = 0, int slot = 0,
		  u32_queue header = {},
		  u32_queue data = {});
      this.core   = core;
      this.slot   = slot;
      if ( header.size() )
	this.header = hdr_unpack ( header );
      else
	this.header = '{0, 0, 0, 0, 0, 0};
      this.data   = data;
   endfunction // new

   function mqueue_msg_header hdr_unpack (u32_queue header );
      mqueue_msg_header hdr;
      if ( header.size() > 2 )
	begin
	   hdr.app_id  = header[0] & 32'h0000ffff;
	   hdr.flags   = (header[0] & 32'h00ff0000) >> 16;
	   hdr.msg_id  = (header[0] & 32'hff000000) >> 24;
	   hdr.len     = header[1] & 32'h0000ffff;
	   hdr.sync_id = (header[1] & 32'hffff0000) >> 16;
	   hdr.seq     = header[2];
	end
      else
	hdr = '{0, 0, 0, 0, 0, 0};
      if ( header.size() != 3 )
	$warning ( "incorrect message header size %0d", header.size() );
      return hdr;
   endfunction // hdr_unpack

   task set_header ( u32_queue header );
      this.header = hdr_unpack ( header );
   endtask // set_header

   function u32_queue hdr_pack ( );
      u32_queue hdr;
      hdr[0]  =  header.app_id & 'hffff;
      hdr[0] |= (header.flags & 'hff) << 16;
      hdr[0] |= (header.msg_id & 'hff) << 24;
      hdr[1]  =  header.len & 'hffff;
      hdr[1] |= (header.sync_id & 'hffff) << 16;
      hdr[2]  =  header.seq;
      return hdr;
   endfunction // hdr_pack

   function string tostring ();
      string hdr, dat;

      hdr = $sformatf ( {"CPU%0d, SLOT%0d, APP_ID: 0x%4x, MSG_ID: 0x%2x, ",
			 "FLAGS: 0x%2x, LEN: %0d, SYNC_ID: 0x%4x, SEQ: %0d" },
			core, slot, header.app_id, header.msg_id, header.flags,
			header.len, header.sync_id, header.seq);

      dat = " PAYLOAD:";
      for (int i = 0; i < header.len; i++) begin
	 dat = {dat, $sformatf(" 0x%8x", this.data[i])};
      end

      return {hdr, dat};
   endfunction // tostring

endclass // MQueueMsg

`endif //  `ifndef __MT_MQUEUE_MSG_INCLUDED
