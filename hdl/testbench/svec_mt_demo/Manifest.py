# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

board      = "svec"
sim_tool   = "modelsim"
sim_top    = "main"
action     = "simulation"
target     = "xilinx"
syn_device = "xc6slx150t"
vcom_opt   = "-93 -mixedsvvh"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../ip_cores"

# Ideally this should be done by hdlmake itself, to allow downstream Manifests to be able to use the
# fetchto variable independent of where those Manifests reside in the filesystem.
import os
fetchto = os.path.abspath(fetchto)

include_dirs = [
    "../include/",
    "../include/regs/",
    fetchto + "/general-cores/sim/",
    fetchto + "/vme64x-core/hdl/sim/vme64x_bfm/",
]

# Now done via CI, otherwise it must be done manually using a RISC-V cross-compiler
#sim_pre_cmd = "EXTRA2_CFLAGS='-DSIMULATION' make -C ../../../demos/hello_world/firmware/fw-01 defconfig all"

files = [
    "main.sv",
    "buildinfo_pkg.vhd",
    "sourceid_svec_mt_demo_pkg.vhd",
]

modules = {
    "local" :  [
        "../../top/svec_mt_demo",
    ],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/vme64x-core.git",
        "https://ohwr.org/project/ddr3-sp6-core.git",
        "https://ohwr.org/project/svec.git",
    ],
}

# Do not fail during hdlmake fetch
try:
  exec(open(fetchto + "/general-cores/tools/gen_buildinfo.py").read())
except:
  pass


try:
    exec(open(fetchto + "/general-cores/tools/gen_sourceid.py").read(),
         None, {'project': 'svec_mt_demo'})
except Exception as e:
    print("Error: cannot generate source id file")
    raise

# needs to be defined, even if not used
ctrls = ["bank4_64b_32b"]
