"""
@package docstring
@author: Federico Vaga <federico.vaga@cern.ch>

SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN  (home.cern)
"""

from ctypes import *
import errno
import os

TRTL_CONFIG_ROM_MAX_CPU = 8
TRTL_CONFIG_ROM_MAX_HMQ = 8
TRTL_CONFIG_ROM_MAX_RMQ = 8

ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT = 83638

class TrtlConfigMq(Structure):
    """
    It describe the configuration information for a single message queue

    :ivar sizes:
    :ivar endpoint_id:
    """

    _fields_ = [
        ("sizes", c_uint32),
        ("endpoint_id", c_uint32),
    ]

    @property
    def entries(self):
        return 1 << ((self.sizes >> 16) & 0xFF)

    @property
    def payload_size(self):
        return 1 << ((self.sizes >> 8) & 0xFF)

    @property
    def header_size(self):
        return 1 << ((self.sizes >> 0) & 0xFF)


class TrtlConfig(Structure):
    """
    The synthesis configuration ROM descriptor shows useful configuration
    options during synthesis.
    """

    _fields_ = [
        ("signature", c_uint32),
        ("version", c_uint32),
        ("pad", c_uint32),
        ("clock_freq", c_uint32),
        ("flags", c_uint32),
        ("app_id", c_uint32),
        ("n_cpu", c_uint32),
        ("smem_size", c_uint32),
        ("mem_size", c_uint32 * TRTL_CONFIG_ROM_MAX_CPU),
        ("n_hmq", c_uint32 * TRTL_CONFIG_ROM_MAX_CPU),
        ("n_rmq", c_uint32 * TRTL_CONFIG_ROM_MAX_CPU),
        ("pad2", c_uint32 * 96),
        ("hmq", TRTL_CONFIG_ROM_MAX_CPU * (TrtlConfigMq * TRTL_CONFIG_ROM_MAX_HMQ)),
        ("rmq", TRTL_CONFIG_ROM_MAX_CPU * (TrtlConfigMq * TRTL_CONFIG_ROM_MAX_HMQ)),
    ]

    TRTL_CONFIG_ROM_SIGNATURE = 0x5452544c
    """This signature must be present on all the configuration rom"""

class TrtlHmqHeader(Structure):
    """
    It describes the HMQ protocol header

    :ivar rt_app_id:
    :ivar flags:
    :ivar msg_id:
    :ivar len:
    :ivar sync_id:
    :ivar seq:
    """

    _fields_ = [
        ("rt_app_id", c_uint16),
        ("flags",  c_uint8),
        ("msg_id", c_uint8),
        ("len", c_uint16),
        ("sync_id", c_uint16),
        ("seq",  c_uint32),
    ]

    TRTL_HMQ_HEADER_FLAG_SYNC = (1 << 0)
    """Flag for synchronous messages"""
    TRTL_HMQ_HEADER_FLAG_ACK = (1 << 1)
    """Flag for synchronous acknowledgment"""

    def __eq__(self, other):
        if self.rt_app_id != other.rt_app_id:
            return False
        if self.flags != other.flags:
            return False
        if self.msg_id != other.msg_id:
            return False
        if self.len != other.len:
            return False
        if self.sync_id != other.sync_id:
            return False
        if self.seq != other.seq:
            return False
        return True

    def __str__(self):
        return "rt_app_id: 0x{:x}, flags: 0x{:x}, msg_id: 0x{:x}, len: {:d}, sync_id: {:d}, seq: {:d}".format(self.rt_app_id,self.flags, self.msg_id, self.len, self.sync_id, self.seq)

class TrtlMessage(Structure):
    """
    It is a container for Mock Turtle messages

    :ivar header: message header
    :ivar payload: message payload
    """

    _fields_ = [
        ("header", TrtlHmqHeader),
        ("payload", (c_uint32 * (4096))),
    ]

    def __eq__(self, other):
        if self.header != other.header:
            return False
        cnt = 0
        for v1, v2 in zip(self.payload, other.payload):
            if v1 != v2:
                return False
            cnt += 1
            if cnt >= self.header.len:
                break
        return True

    def __str__(self):
        return "{:s}, payload: ##".format(str(self.header))


class TrtlFirmwareVersion(Structure):
    """
    It is a descriptor for firmware versions

    :ivar fw_id: firmware unique identifier
    :ivar fw_version: firmware version
    :ivar git_version: firmware first 32bit git SHA
    """

    _fields_ = [
        ("fw_id", c_uint32),
        ("fw_version", c_uint32),
        ("git_version", c_uint32),
    ]

    def __eq__(self, other):
        if self.fw_id != other.fw_id:
            return False
        if self.fw_version != other.fw_version:
            return False
        if self.git_version != other.git_version:
            return False
        return True


class TrtlDevice(object):
    """
    It is a Python class that represent a Mock Turtle Device

    :param devid: Mock Turtle device identifier

    :ivar device_id: device ID associated with the instance
    :ivar tkn: device token to be used with the libmockturtle library
    :ivar libtrtl: the libmockturtle library
    :ivar rom: configuration rom
    :ivar cpu: list of TrtlCpu instances
    :ivar shm: shared memory
    """

    def __init__(self, devid):
        self.libtrtl = None
        if devid is None:
            raise Exception("Invalid device ID")
        self.__init_library()

        self.device_id = devid
        self.libtrtl.trtl_init()
        set_errno(0)
        self.tkn = self.libtrtl.trtl_open_by_id(self.device_id)
        rom_ptr = self.libtrtl.trtl_config_get(self.tkn)
        self.rom = rom_ptr.contents

        self.name = "trtl-{:04x}".format(self.device_id)
        self.sysfspath = os.path.join("/sys/class/mockturtle/", self.name)

        self.cpu = []
        for i in range(self.rom.n_cpu):
            self.cpu.append(TrtlCpu(self, i))
        self.shm = TrtlSmem(self)

    def __del__(self):
        if self.libtrtl is not None:
            if hasattr(self, 'tkn'):
                self.libtrtl.trtl_close(self.tkn)
            self.libtrtl.trtl_exit()

    def __str__(self):
        return self.name

    def __repr__(self):
        return "{:s}".format(str(self))

    def __init_library(self):
        self.libtrtl = CDLL("libmockturtle.so", use_errno=True)

        self.libtrtl.trtl_open_by_id.argtypes = [c_uint]
        self.libtrtl.trtl_open_by_id.restype = c_void_p
        self.libtrtl.trtl_open_by_id.errcheck = self.__errcheck_pointer
        self.libtrtl.trtl_close.argtypes = [c_void_p]
        self.libtrtl.trtl_close.restype = None
        # ERROR
        self.libtrtl.trtl_strerror.argtypes = [c_int]
        self.libtrtl.trtl_strerror.restype = c_char_p
        self.libtrtl.trtl_open_by_id.errcheck = self.__errcheck_pointer
        # SHM READ
        self.libtrtl.trtl_smem_read.argtypes = [c_void_p, c_uint,
                                                POINTER(c_int), c_size_t,
                                                c_uint]
        self.libtrtl.trtl_smem_read.restype = c_int
        self.libtrtl.trtl_smem_read.errcheck = self.__errcheck_int
        # SHM WRITE
        self.libtrtl.trtl_smem_write.argtypes = [c_void_p, c_uint,
                                                 POINTER(c_int), c_size_t,
                                                 c_uint]
        self.libtrtl.trtl_smem_write.restype = c_int
        self.libtrtl.trtl_smem_write.errcheck = self.__errcheck_int
        # CPU Enable
        self.libtrtl.trtl_cpu_enable.argtypes = [c_void_p, c_uint]
        self.libtrtl.trtl_cpu_enable.restype = c_int
        self.libtrtl.trtl_cpu_enable.errcheck = self.__errcheck_int
        # CPU Disable
        self.libtrtl.trtl_cpu_disable.argtypes = [c_void_p, c_uint]
        self.libtrtl.trtl_cpu_disable.restype = c_int
        self.libtrtl.trtl_cpu_disable.errcheck = self.__errcheck_int
        # CPU Is Enable
        self.libtrtl.trtl_cpu_is_enable.argtypes = [c_void_p,
                                                     c_uint,
                                                     POINTER(c_uint)]
        self.libtrtl.trtl_cpu_is_enable.restype = c_int
        self.libtrtl.trtl_cpu_is_enable.errcheck = self.__errcheck_int
        # CPU Load
        self.libtrtl.trtl_cpu_load_application_file.argtypes = [c_void_p,
                                                                c_uint,
                                                                c_char_p]
        self.libtrtl.trtl_cpu_load_application_file.restype = c_int
        self.libtrtl.trtl_cpu_load_application_file.errcheck = self.__errcheck_int
        # CPU Dump
        self.libtrtl.trtl_cpu_dump_application_file.argtypes = [c_void_p,
                                                                c_uint,
                                                                c_char_p]
        self.libtrtl.trtl_cpu_dump_application_file.restype = c_int
        self.libtrtl.trtl_cpu_dump_application_file.errcheck = self.__errcheck_int
        # GET CONFIG
        self.libtrtl.trtl_config_get.argtypes = [c_void_p, ]
        self.libtrtl.trtl_config_get.restype = POINTER(TrtlConfig)
        self.libtrtl.trtl_config_get.errcheck = self.__errcheck_pointer
        # ASYNC RECEIVE
        self.libtrtl.trtl_msg_async_recv.argtypes = [c_void_p,
                                                     c_uint,
                                                     c_uint,
                                                     POINTER(TrtlMessage),
                                                     c_uint]
        self.libtrtl.trtl_msg_async_recv.restype = c_int
        self.libtrtl.trtl_msg_async_recv.errcheck = self.__errcheck_int
        # ASYNC SEND
        self.libtrtl.trtl_msg_async_send.argtypes = [c_void_p,
                                                     c_uint,
                                                     c_uint,
                                                     POINTER(TrtlMessage),
                                                     c_uint]
        self.libtrtl.trtl_msg_async_send.restype = c_int
        self.libtrtl.trtl_msg_async_send.errcheck = self.__errcheck_int
        # SYNC SEND and RECEIVE
        self.libtrtl.trtl_msg_sync.argtypes = [c_void_p,
                                               c_uint,
                                               c_uint,
                                               POINTER(TrtlMessage),
                                               POINTER(TrtlMessage),
                                               c_uint]
        self.libtrtl.trtl_msg_sync.restype = c_int
        self.libtrtl.trtl_msg_sync.errcheck = self.__errcheck_int
        # FLUSH
        self.libtrtl.trtl_hmq_flush.argtypes = [c_void_p,
                                                c_uint,
                                                c_uint]
        self.libtrtl.trtl_hmq_flush.restype = c_int
        self.libtrtl.trtl_hmq_flush.errcheck = self.__errcheck_int
        # FW PING
        self.libtrtl.trtl_fw_ping.argtypes = [c_void_p, c_uint, c_uint]
        self.libtrtl.trtl_fw_ping.restype = c_int
        self.libtrtl.trtl_fw_ping.errcheck = self.__errcheck_int
        # FW NAME
        self.libtrtl.trtl_fw_name.argtypes = [c_void_p, c_uint, c_uint,
                                              POINTER(c_char), c_uint]
        self.libtrtl.trtl_fw_name.restype = c_int
        self.libtrtl.trtl_fw_name.errcheck = self.__errcheck_int
        # FW VERSION
        self.libtrtl.trtl_fw_version.argtypes = [c_void_p, c_uint, c_uint,
                                                 POINTER(TrtlFirmwareVersion)]
        self.libtrtl.trtl_fw_version.restype = c_int
        self.libtrtl.trtl_fw_version.errcheck = self.__errcheck_int
        # FW VARIABLE
        self.libtrtl.trtl_fw_variable_get.argtypes = [c_void_p, c_uint, c_uint,
                                                      POINTER(c_uint32),
                                                      c_uint]
        self.libtrtl.trtl_fw_variable_get.restype = c_int
        self.libtrtl.trtl_fw_variable_get.errcheck = self.__errcheck_int
        self.libtrtl.trtl_fw_variable_set.argtypes = [c_void_p, c_uint, c_uint,
                                                      POINTER(c_uint32),
                                                      c_uint]
        self.libtrtl.trtl_fw_variable_set.restype = c_int
        self.libtrtl.trtl_fw_variable_set.errcheck = self.__errcheck_int

    def __errcheck_pointer(self, ret, func, args):
        """Generic error handler for functions returning pointers"""
        if ret is None:
            raise OSError(get_errno(),
                          self.libtrtl.trtl_strerror(get_errno()), "")
        else:
            return ret

    def __errcheck_int(self, ret, func, args):
        """Generic error checker for functions returning 0 as success
        and -1 as error"""
        if ret < 0:
            raise OSError(get_errno(),
                          self.libtrtl.trtl_strerror(get_errno()), "")
        else:
            return ret


class TrtlFirmwareVariable(object):
    """This class make it possible to use the firmware framework variable
    feature like an array. Note that this implementation allows to access
    elements one by one. By using the C library it is actually possible
    to get/set multiple variables
    """
    def __init__(self, trtl_hmq):
        self.trtl_hmq = trtl_hmq
        self.trtl_cpu = self.trtl_hmq.trtl_cpu
        self.trtl_dev = self.trtl_cpu.trtl_dev
        self.libtrtl = self.trtl_dev.libtrtl

    def __getitem__(self, key):
        try:
            val = (c_uint32 * 2)(key, 0)
            variables = cast(val, POINTER(c_uint32))

            self.libtrtl.trtl_fw_variable_get(self.trtl_dev.tkn,
                                              self.trtl_cpu.idx_cpu,
                                              self.trtl_hmq.idx_hmq,
                                                variables, 1)
            return val[1]
        except OSError as e:
            # raise IndexError if this is the case
            raise e

    def __setitem__(self, key, value):
        try:
            val = (c_uint32 * 2)(key, value)
            variables = cast(val, POINTER(c_uint32))

            self.libtrtl.trtl_fw_variable_set(self.trtl_dev.tkn,
                                              self.trtl_cpu.idx_cpu,
                                              self.trtl_hmq.idx_hmq,
                                              variables, 1)
        except OSError as e:
            # raise IndexError if this is the case
            raise e


class TrtlCpu(object):
    """
    It is a Python class that represents a Mock Turtle device CPU

    :param trtl_dev: the device instance to use
    :param idx_cpu: the core to access

    :ivar trtl_dev: parent device instance
    :ivar idx_cpu: the core index
    :ivar hmq: the list of TrtlHmq instances
    """

    def __init__(self, trtl_dev, idx_cpu):
        self.trtl_dev = trtl_dev
        self.idx_cpu = idx_cpu

        self.devname = "{:s}-{:02d}".format(str(self.trtl_dev), self.idx_cpu)
        self.sysfspath = os.path.join("/sys/class/mockturtle/", self.devname)

        self.hmq = []
        for i in range(self.trtl_dev.rom.n_hmq[self.idx_cpu]):
            self.hmq.append(TrtlHmq(self, i))

        self.libtrtl = self.trtl_dev.libtrtl

    def __str__(self):
        return self.devname

    def __repr__(self):
        return "{:s}".format(str(self))

    def enable(self):
        """
        It enables a CPU; in other words, it clear the reset line of a CPU.

        :raises OSError: from C library errors
        """
        self.libtrtl.trtl_cpu_enable(self.trtl_dev.tkn, self.idx_cpu)

    def disable(self):
        """
        It disables a CPU; in other words, it sets the reset line of a CPU.

        :raises OSError: from C library errors
        """
        self.libtrtl.trtl_cpu_disable(self.trtl_dev.tkn, self.idx_cpu)

    def is_enable(self):
        """
        It checks if the CPU is enabled (or not)

        :return: True when the CPU is enable, False otherwise
        :raises OSError: from C library errors
        """
        enable = c_uint(0)
        self.libtrtl.trtl_cpu_is_enable(self.trtl_dev.tkn, self.idx_cpu,
                                        byref(enable))
        return True if enable.value else False

    def load_application_file(self, file_path):
        """
        It loads a firmware from the given file

        :param file_path: path to the firmware file
        :raises OSError: from C library errors
        """
        self.libtrtl.trtl_cpu_load_application_file(self.trtl_dev.tkn,
                                                    self.idx_cpu,
                                                    file_path.encode())

    def dump_application_file(self, file_path):
        """
        It dumps the running firmware to the given file

        :param file_path: path to the firmware file
        :raises OSError: from C library errors
        """
        self.libtrtl.trtl_cpu_dump_application_file(self.trtl_dev.tkn,
                                                    self.idx_cpu,
                                                    file_path.encode())

    def ping(self, idx_hmq=0):
        """
        It pings the firmware running on the CPU

        :param idx_hmq: which HMQ to use for pinging (default: 0)
        :type idx_hmq: int
        :return: True if the firmware is alive, False otherwise
        :rtype: bool
        :raises OSError: from C library errors
        """
        val = self.libtrtl.trtl_fw_ping(self.trtl_dev.tkn,
                                        self.idx_cpu, idx_hmq)
        return True if val == 0 else False


    def name(self, idx_hmq=0):
        """
        It gets the firmware name running on the CPU.
        :param idx_hmq: which HMQ to use for pinging (default: 0)
        :type idx_hmq: int
        :return: the firmware name
        :raises OSError: from C library errors
        """
        name = create_string_buffer(16)
        version = TrtlFirmwareVersion()
        self.libtrtl.trtl_fw_name(self.trtl_dev.tkn,
                                  self.idx_cpu, idx_hmq,
                                  name, 16)

        return name.value.decode()


    def version(self, idx_hmq=0):
        """
        It pings the firmware running on the CPU.
        :param idx_hmq: which HMQ to use for pinging (default: 0)
        :type idx_hmq: int
        :return: True if the firmware is alive, False otherwise
        :raises OSError: from C library errors
        """
        version = TrtlFirmwareVersion()
        self.libtrtl.trtl_fw_version(self.trtl_dev.tkn,
                                     self.idx_cpu, idx_hmq,
                                     pointer(version))
        return version


class TrtlHmq(object):
    """
    Python wrapper for HMQ management

    :param trtl_cpu: the cpu instance to use
    :param idx_hmq: the HMQ to access
    """

    def __init__(self, trtl_cpu, idx_hmq):
        self.trtl_cpu = trtl_cpu
        self.trtl_dev = self.trtl_cpu.trtl_dev
        self.idx_cpu = self.trtl_cpu.idx_cpu
        self.idx_hmq = idx_hmq
        self.libtrtl = self.trtl_dev.libtrtl

        self.devname = "{:s}-{:02d}".format(str(self.trtl_cpu), self.idx_hmq)
        self.sysfspath = os.path.join("/sys/class/mockturtle/", self.devname)

    def __str__(self):
        return self.devname

    def __repr__(self):
        return "{:s}".format(str(self))

    def get_stats(self):
        """ It gets statistics
        :return: a dictionary with the statistics
        :rtype: dict
        """
        s = {
            "message_received": 0,
            "message_sent": 0,
        }

        for name in s:
            with open(os.path.join(self.sysfspath, "statistics", name)) as f:
                s[name] = int(f.read())
        return s

    def flush(self):
        """
        It removes enqueued messages from the queue. It does it for both
        direction: input and output.
        """
        set_errno(0)
        self.libtrtl.trtl_hmq_flush(self.trtl_dev.tkn,
                                    self.idx_cpu, self.idx_hmq)

    def send_msg(self, msg, timeout=-1):
        """
        It sends an asynchronous message.

        :param msg: the message to send
        :type msg: TrtlMessage
        :param timeout: time to wait before returning
        :type timeout: int
        :raises OSError: from C library errors
        """
        set_errno(0)
        self.libtrtl.trtl_msg_async_send(self.trtl_dev.tkn,
                                         self.idx_cpu, self.idx_hmq,
                                         pointer(msg), 1)

    def recv_msg(self, timeout=-1):
        """
        It receives an asynchronous message.

        :param timeout: time to wait before returning
        :type timeout: int
        :return: an asynchronous message or None
        :rtype: TrtlMessage
        :raises OSError: from C library errors
        """
        set_errno(0)
        msg = TrtlMessage()
        ret = self.libtrtl.trtl_msg_async_recv(self.trtl_dev.tkn,
                                               self.idx_cpu, self.idx_hmq,
                                               pointer(msg), 1)
        return None if ret == 0 else msg

    def sync_msg(self, msg_s, timeout=1000):
        """
        It sends a synchronous message.

        :param msg_s: the message to send
        :type msg_s: TrtlMessage
        :param timeout: time to wait before returning in milli-seconds
        :type timeout: int
        :return: the synchronous answer
        :rtype: TrtlMessage
        :raises OSError: from C library errors
        """
        set_errno(0)
        msg_r = TrtlMessage()
        self.libtrtl.trtl_msg_sync(self.trtl_dev.tkn,
                                   self.idx_cpu, self.idx_hmq,
                                   byref(msg_s), pointer(msg_r),
                                   timeout)
        return msg_r


class TrtlSmem(object):
    """
    It allows to read and write the Mock Turtle shared memory
    """

    MOD_DIRECT = 0
    """Normal write"""

    MOD_ADD = 1
    """Write modifier for atomic addition of the shared memory value with
       the given one"""

    MOD_SUB = 2
    """Write modifier for atomic subtraction of the shared memory value with
       the given one"""

    MOD_SET = 3
    """Write modifier for atomic OR of the shared memory value with
       the given one. In other words it sets the bit in the given value"""

    MOD_CLEAR = 4
    """Write modifier for atomic AND NOT of the shared memory value with
       the given one. In other words it clears the bit in the given value"""

    MOD_FLIP = 5
    """Write modifier for atomic XOR of the shared memory value with
       the given one. In other words it flips the bit in the given value"""

    MOD_TEST_AND_SET = 6
    """Write modifier for atomic TEST and SET. FIXME"""

    def __init__(self, trtl_dev):
        self.trtl_dev = trtl_dev

    def read(self, address, count):
        """
        It reads from the shared memory 'count' 32bit words starting
        from 'address'

        :param address: memory address where start writing
        :param count: number of 32bit consectutive words to read
        :return: the values
        :rtype: list of int
        :raises OSError: from C library errors
        """
        dataArr = (c_int * count)()
        dataCas = cast(dataArr, POINTER(c_int))
        self.libtrtl.trtl_smem_read(self.trtl_dev.tkn, address,
                                    dataCas, count, 0)
        return list(dataArr)

    def write(self, address, values, modifier):
        """
        It writes 'values' to the shared memory starting from 'address'
        using the access mode 'modifier'

        :param address: memory address where start writing
        :type address: int
        :param values: the values to write
        :type values: list of int
        :param modifier: write operation modifier. It changes the write
                         behaviour
        :type modifier: int
        :raises OSError: from C library errors
        """
        dataArr = (c_int * len(values))(*values)
        dataCas = cast(dataArr, POINTER(c_int))
        self.libtrtl.trtl_smem_write(self.trtl_dev.tkn, address, dataCas,
                                     len(values), modifier)
