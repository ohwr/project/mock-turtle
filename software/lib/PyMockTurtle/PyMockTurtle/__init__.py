"""
@package docstring
@author: Federico Vaga <federico.vaga@cern.ch>

SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN  (home.cern)
"""

from .PyMockTurtle import TrtlHmqHeader, TrtlMessage, TrtlConfig, \
                          TrtlCpu, TrtlHmq, TrtlSmem, TrtlDevice, \
                          TrtlFirmwareVersion, TrtlConfigMq, \
                          TrtlFirmwareVariable, \
                          TRTL_CONFIG_ROM_MAX_CPU, \
                          TRTL_CONFIG_ROM_MAX_HMQ, \
                          TRTL_CONFIG_ROM_MAX_RMQ, \
                          ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT

__all__ = (
    "TrtlDevice",
    "TrtlCpu",
    "TrtlHmq",
    "TrtlSmem",
    "TrtlHmqHeader",
    "TrtlMessage",
    "TrtlConfig",
    "TrtlConfigMq",
    "TrtlFirmwareVersion",
    "TrtlFirmwareVariable",
    "TRTL_CONFIG_ROM_MAX_CPU",
    "TRTL_CONFIG_ROM_MAX_HMQ",
    "TRTL_CONFIG_ROM_MAX_RMQ",
    "ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT",
)
