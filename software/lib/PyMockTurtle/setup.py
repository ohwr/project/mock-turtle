#!/usr/bin/env python

"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

from distutils.core import setup

import os

setup(name='PyMockTurtle',
      version=os.environ.get('VERSION', "0.0.0"),
      description='Python Module to handle Mock Turtle devices',
      author='Federico Vaga',
      author_email='federico.vaga@cern.ch',
      maintainer="Federico Vaga",
      maintainer_email="federico.vaga@cern.ch",
      url='http://www.ohwr.org/projects/mock-turtle',
      packages=['PyMockTurtle'],
      license='LGPL-2.1-or-later',
     )
