/*
 * SPDX-FileCopyrightText: 2017-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/bitmap.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/kallsyms.h>

#include <mockturtle/hw/mockturtle_cpu_csr.h>

#include "mockturtle-drv.h"

static inline void trtl_tty_insert_flip_char(struct trtl_cpu *cpu,
					     unsigned char ch, char flag)
{
#if KERNEL_VERSION(3, 9, 0) > LINUX_VERSION_CODE
	tty_insert_flip_char(cpu->tty_port.tty, ch, flag);
#else
	tty_insert_flip_char(&cpu->tty_port, ch, flag);
#endif
}

static inline void trtl_tty_flip_buffer_push(struct trtl_cpu *cpu)
{
#if KERNEL_VERSION(3, 9, 0) > LINUX_VERSION_CODE
	tty_schedule_flip(cpu->tty_port.tty);
#else
	tty_flip_buffer_push(&cpu->tty_port);
#endif
}

/**
 * trtl_tty_handler_getchar - Get Character From soft-CPU
 * Retrieve a character from the soft-CPU serial interface
 * and it adds the character to the TTY buffer
 * @cpu: the soft-CPU to use
 */
static void trtl_tty_handler_getchar(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	unsigned long flags;
	char c;

	/* Select the CPU and read data */
	spin_lock_irqsave(&trtl->lock_cpu_sel, flags);
	trtl_iowrite(trtl, cpu->index,
		     trtl->base_csr + MT_CPU_CSR_REG_CORE_SEL);
	c = trtl_ioread(trtl,
			trtl->base_csr + MT_CPU_CSR_REG_UART_MSG);
	spin_unlock_irqrestore(&trtl->lock_cpu_sel, flags);

	if (!cpu->tty_port.tty) /* Not open */
		return;

	trtl_tty_insert_flip_char(cpu, c, TTY_NORMAL);
}


static irqreturn_t trtl_tty_handler(int irq, void *arg)
{
	struct trtl_dev *trtl = arg;
	uint32_t status;
	int i, max_irq = 32, lock;

	status = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_UART_POLL);
do_irq:
	i = -1;
	while (status && likely(++i < trtl->cfgrom.n_cpu)) {
		lock = spin_is_locked(&trtl->lock_cpu_sel);
		if (!(status & 0x1) || lock) {
			status >>= 1;
			continue;
		}
		/* Get a character from the soft-CPU */
		trtl_tty_handler_getchar(&trtl->cpu[i]);
	}

	/* Check again the interrupt status */
	status = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_UART_POLL);
	if (max_irq > 0 && status) {
		max_irq--;
		goto do_irq;
	}

	/* Push all the collected characters */
	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		if (!trtl->cpu[i].tty_port.tty) /* Not open */
			continue;

		trtl_tty_flip_buffer_push(&trtl->cpu[i]);
	}

	return IRQ_HANDLED;
}

static int trtl_tty_open(struct tty_struct *tty, struct file *file)
{
	struct trtl_cpu *cpu = to_trtl_cpu(tty->dev->parent);
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	uint32_t irq_mask;
	int err;

	err = tty_port_open(&cpu->tty_port, tty, file);
	if (err)
		return err;

	/* Enable the interrupt on the Mock Turtle */
	irq_mask = trtl_ioread(trtl,
		    trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);
	irq_mask |= (1 << cpu->index);
	trtl_iowrite(trtl, irq_mask,
		     trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);

	return 0;
}

static void trtl_tty_close(struct tty_struct *tty, struct file *file)
{
	struct trtl_cpu *cpu = to_trtl_cpu(tty->dev->parent);
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	uint32_t irq_mask;

	/* Disable the interrupt on the Mock Turtle */
	irq_mask = trtl_ioread(trtl,
		    trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);
	irq_mask &= ~(1 << cpu->index);
	trtl_iowrite(trtl, irq_mask,
		     trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);

	tty_port_close(&cpu->tty_port, tty, file);
}


const struct tty_operations trtl_tty_ops = {
	.open = trtl_tty_open,
	.close = trtl_tty_close,
};

static const struct tty_port_operations null_ops = { };

static int trtl_tty_port_init(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);

	tty_port_init(&cpu->tty_port);
	cpu->tty_port.ops = &null_ops;
#if  KERNEL_VERSION(3, 7, 0) <= LINUX_VERSION_CODE
	cpu->tty_dev = tty_port_register_device(&cpu->tty_port,
						trtl->tty_driver,
						cpu->index,
						&cpu->dev);
#else
	cpu->tty_dev = tty_register_device(trtl->tty_driver,
					   cpu->index,
					   &cpu->dev);
#endif
	if (IS_ERR_OR_NULL(cpu->tty_dev)) {
		tty_port_put(&cpu->tty_port);
		return PTR_ERR(cpu->tty_dev);
	}

	return 0;
}


static void trtl_tty_port_exit(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);

	tty_unregister_device(trtl->tty_driver, cpu->index);
#if  KERNEL_VERSION(3, 7, 0) <= LINUX_VERSION_CODE
	tty_port_destroy(&cpu->tty_port);
#endif
}


void trtl_tty_remove(struct trtl_dev *trtl)
{
	struct platform_device *pdev = to_pdev_dev(trtl);
	int i;

	/* Disable the interrupts on the Mock Turtle */
	trtl_iowrite(trtl, 0x0, trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);

	if (!trtl->tty_driver)
		return;

	free_irq(platform_get_irq(pdev, TRTL_IRQ_CON), trtl);

	for (i = 0; i < trtl->cfgrom.n_cpu; ++i)
		trtl_tty_port_exit(&trtl->cpu[i]);

	tty_unregister_driver(trtl->tty_driver);

	kfree(trtl->tty_driver->driver_name);
	kfree(trtl->tty_driver->name);
	tty_driver_kref_put(trtl->tty_driver);
}


int trtl_tty_probe(struct trtl_dev *trtl)
{
	struct platform_device *pdev = to_pdev_dev(trtl);
	struct resource *r;
	int i, err = 0;

	r = platform_get_resource(pdev, IORESOURCE_IRQ, TRTL_IRQ_CON);
	if (!r) {
		dev_warn(&trtl->dev,
			"disable console: invalid interrupt source\n");
		goto err; /* no error code, we can live without console */
	}

	trtl->tty_driver = tty_alloc_driver(trtl->cfgrom.n_cpu, 0);
	if (IS_ERR(trtl->tty_driver))
		return PTR_ERR(trtl->tty_driver);
	trtl->tty_driver->driver_name = kasprintf(GFP_KERNEL, "%s-tty",
						  dev_name(&trtl->dev));
	trtl->tty_driver->name = kasprintf(GFP_KERNEL, "tty%s-",
					   dev_name(&trtl->dev));
	if (!trtl->tty_driver->driver_name || !trtl->tty_driver->name) {
		err = -ENOMEM;
		goto err_cfg;
	}
	trtl->tty_driver->type = TTY_DRIVER_TYPE_CONSOLE;
	trtl->tty_driver->subtype = 0;
	trtl->tty_driver->flags = TTY_DRIVER_REAL_RAW |
				  TTY_DRIVER_DYNAMIC_DEV |
				  TTY_DRIVER_RESET_TERMIOS;
	tty_set_operations(trtl->tty_driver, &trtl_tty_ops);
	trtl->tty_driver->init_termios = tty_std_termios;
	trtl->tty_driver->init_termios.c_iflag = ICRNL;
	trtl->tty_driver->init_termios.c_oflag = 0;
	trtl->tty_driver->init_termios.c_cflag = CS8 | CREAD | CLOCAL;
	trtl->tty_driver->init_termios.c_lflag = 0;

	err = tty_register_driver(trtl->tty_driver);
	if (err < 0)
		goto err_tty;

	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		err = trtl_tty_port_init(&trtl->cpu[i]);
		if (err)
			goto err_port;
	}


	/* Enable debug interrupts */
	err = request_any_context_irq(r->start, trtl_tty_handler, 0,
				      r->name, trtl);
	if (err < 0) {
		dev_err(&trtl->dev,
			"Cannot request IRQ %lld - disable console\n",
			r->start);
		goto err_irq;
	}

	return 0;

err_irq:
err_port:
	while (--i >= 0)
		trtl_tty_port_exit(&trtl->cpu[i]);
	tty_unregister_driver(trtl->tty_driver);
err_tty:
err_cfg:
	kfree(trtl->tty_driver->driver_name);
	kfree(trtl->tty_driver->name);
	tty_driver_kref_put(trtl->tty_driver);
err:
	trtl->tty_driver = NULL;
	return err;
}
