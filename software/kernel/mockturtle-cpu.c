/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/vmalloc.h>
#include <linux/uaccess.h>
#include <linux/wait.h>

#include <mockturtle/hw/mockturtle_cpu_csr.h>
#include "mockturtle-drv.h"

static const unsigned int trtl_cpu_reset_timeout_us = 1000000;

/**
 * Get the reset bit of the CPUs according to the mask
 */
static uint32_t trtl_cpu_reset_get(struct trtl_dev *trtl)
{
	return trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_RESET);
}


/**
 * Set the reset bit of the CPUs according to the mask
 */
void trtl_cpu_reset_set(struct trtl_dev *trtl, uint8_t mask)
{
	uint32_t reg_val;

	reg_val = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_RESET);
	reg_val |= (mask & 0xFF);
	trtl_iowrite(trtl, reg_val, trtl->base_csr + MT_CPU_CSR_REG_RESET);
}

/**
 * Clear the reset bit of the CPUs according to the mask
 */
static void trtl_cpu_reset_clr(struct trtl_dev *trtl, uint8_t mask)
{
	uint32_t reg_val;

	reg_val = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_RESET);
	reg_val &= (~mask & 0xFF);
	trtl_iowrite(trtl, reg_val, trtl->base_csr + MT_CPU_CSR_REG_RESET);
}


static int trtl_cpu_reset_wait(struct trtl_cpu *cpu, uint32_t state,
							   unsigned int timeout_us)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	unsigned long timeout;

	timeout = jiffies + usecs_to_jiffies(timeout_us);
	while ((trtl_cpu_reset_get(trtl) >> cpu->index) && 0x1 == state) {
		cpu_relax();

		if (time_after(jiffies, timeout))
			return -ETIMEDOUT;
	}

	return 0;
}


static void trtl_cpu_off(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	int err, i;

	trtl_cpu_reset_set(trtl, (1 << cpu->index));

	err = trtl_cpu_reset_wait(cpu, 0, trtl_cpu_reset_timeout_us);
	if (err == -ETIMEDOUT)
		dev_warn(&cpu->dev,
				 "The user asked to turn off CPU %d, but after %u us the CPU was still on\n",
				 cpu->index, trtl_cpu_reset_timeout_us);

	/* Clean up all the HMQ */
	for (i = 0; i < trtl->cfgrom.n_hmq[cpu->index]; ++i)
		trtl_hmq_purge(&cpu->hmq[i]);
}


static void trtl_cpu_on(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	int err;

	/* now the CPU can run */
	trtl_cpu_reset_clr(trtl, (1 << cpu->index));

	err = trtl_cpu_reset_wait(cpu, 1, trtl_cpu_reset_timeout_us);
	if (err == -ETIMEDOUT)
		dev_warn(&cpu->dev,
				 "The user asked to turn on CPU %d, but after %u us the CPU was still off\n",
				 cpu->index, trtl_cpu_reset_timeout_us);
}


static ssize_t last_notification_show(struct device *dev,
				      struct device_attribute *attr,
				      char *buf)
{
	struct trtl_cpu *cpu = to_trtl_cpu(dev);

	if (cpu->idx_r == cpu->idx_w)
		return sprintf(buf, "\n");

	return sprintf(buf, "0x%02x\n", cpu->notification[cpu->idx_r]);
}
DEVICE_ATTR(last_notification, 0444,
	    last_notification_show, NULL);


static ssize_t notification_history_show(struct device *dev,
					 struct device_attribute *attr,
					 char *buf)
{
	struct trtl_cpu *cpu = to_trtl_cpu(dev);
	ssize_t len;
	unsigned int i; /* Mut be unsigned for the circ-buffer */
	int idx, id;

	len = 0;
	for (i = cpu->idx_r; i < cpu->idx_w; ++i) {
		idx = i & (TRTL_CPU_NOTIFY_HISTORY_N - 1);
		id = cpu->notification[idx];
		len += snprintf(buf + len, PAGE_SIZE, "0x%02x\n", id);
	}

	return len;
}
DEVICE_ATTR(notification_history, 0444,
	    notification_history_show, NULL);


/**
 * Return the CPU reset status
 */
static ssize_t reset_show(struct device *dev,
			  struct device_attribute *attr,
			  char *buf)
{
	struct trtl_cpu *cpu = to_trtl_cpu(dev);
	struct trtl_dev *trtl = to_trtl_dev(dev->parent);
	uint32_t reg_val;

	reg_val = trtl_cpu_reset_get(trtl);

	return sprintf(buf, "%d\n", !!(reg_val & (1 << cpu->index)));
}

/**
 * Asert or de-assert the CPU reset line
 */
static ssize_t reset_store(struct device *dev,
			   struct device_attribute *attr,
			   const char *buf, size_t count)
{
	struct trtl_cpu *cpu = to_trtl_cpu(dev);
	long val;

	if (kstrtol(buf, 0, &val))
		return -EINVAL;

	if (val)
		trtl_cpu_off(cpu);
	else
		trtl_cpu_on(cpu);

	return count;
}
DEVICE_ATTR(reset, 0664, reset_show, reset_store);

static struct attribute *trtl_cpu_attr[] = {
	&dev_attr_reset.attr,
	&dev_attr_last_notification.attr,
	&dev_attr_notification_history.attr,
	NULL,
};

static const struct attribute_group trtl_cpu_group = {
	.attrs = trtl_cpu_attr,
};

const struct attribute_group *trtl_cpu_groups[] = {
	&trtl_cpu_group,
	NULL,
};


/**
 * Load a given application into the CPU memory
 */
static int trtl_cpu_firmware_load(struct trtl_cpu *cpu, void *fw_buf,
				  size_t count, loff_t off)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	uint32_t *fw = fw_buf, word, word_rb, irq_dbg_old;
	int size, offset, i, err = 0;

	/*
	 * The Debug console uses the CORE select. In order to avoid an IRQ
	 * burst due to the loading of a new firmware, we temporary disable
	 * the IRQs.
	 * The Debug IRQ handler, can be called anyway right before we disable
	 * it but the spinlock will prevent any conflict on the core selection
	 */
	irq_dbg_old = trtl_ioread(trtl,
				  trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);
	trtl_iowrite(trtl, 0, trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);

	/* Select the CPU memory to write */
	spin_lock(&trtl->lock_cpu_sel);
	trtl_iowrite(trtl, cpu->index,
		     trtl->base_csr + MT_CPU_CSR_REG_CORE_SEL);

	if (off + count > trtl->cfgrom.mem_size[cpu->index] * 4) {
		dev_err(&cpu->dev,
			"Cannot load firmware: size limit %d byte\n",
			trtl->cfgrom.mem_size[cpu->index] * 4);
		err = -ENOMEM;
		goto out;
	}

	/* Calculate code size in 32bit word*/
	size = (count + 3) / 4;
	offset = off / 4;

	/* Reset the CPU before overwrite its memory */
	trtl_cpu_off(cpu);

	/* Clean CPU memory */
	for (i = offset; i < trtl->cfgrom.mem_size[cpu->index]  * 4 / 1024; ++i) {
		trtl_iowrite(trtl, i, trtl->base_csr + MT_CPU_CSR_REG_UADDR);
		udelay(1);
		trtl_iowrite(trtl, 0, trtl->base_csr + MT_CPU_CSR_REG_UDATA);
		udelay(1);
	}

	/* Load the firmware */
	for (i = 0; i < size; ++i) {
		word = cpu_to_be32(fw[i]);
		trtl_iowrite(trtl, i + offset,
			   trtl->base_csr + MT_CPU_CSR_REG_UADDR);
		udelay(1);
		trtl_iowrite(trtl, word, trtl->base_csr + MT_CPU_CSR_REG_UDATA);
		udelay(1);
		word_rb = trtl_ioread(trtl,
				    trtl->base_csr + MT_CPU_CSR_REG_UDATA);
		udelay(1);
		if (word != word_rb) {
			dev_err(&cpu->dev,
				"failed to load firmware (byte %d | 0x%x != 0x%x)\n",
				i, word, word_rb);
			err = -EFAULT;
			goto out;
		}
	}
out:

	spin_unlock(&trtl->lock_cpu_sel);

	trtl_iowrite(trtl, irq_dbg_old,
		     trtl->base_csr + MT_CPU_CSR_REG_UART_IMSK);
	return err;
}

static int trtl_cpu_firmware_dump(struct trtl_cpu *cpu, void *fw_buf,
				  size_t count, loff_t off)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	uint32_t *fw = fw_buf, word;
	int size, offset, i, err = 0;


	/* Calculate code size in 32bit word*/
	size = (count + 3) / 4;
	offset = off / 4;

	/* Select the CPU memory to write */
	spin_lock(&trtl->lock_cpu_sel);
	trtl_iowrite(trtl, cpu->index,
		     trtl->base_csr + MT_CPU_CSR_REG_CORE_SEL);

	if (off + count > trtl->cfgrom.mem_size[cpu->index] * 4 ) {
		dev_err(&cpu->dev, "Cannot dump firmware: size limit %d byte\n",
			trtl->cfgrom.mem_size[cpu->index] * 4);
		err = -ENOMEM;
		goto out;
	}

	/* Stop the CPU before dumping its memory */
	trtl_cpu_off(cpu);

	/* Dump the firmware */
	for (i = 0; i < size; ++i) {
		trtl_iowrite(trtl, i + offset,
			   trtl->base_csr + MT_CPU_CSR_REG_UADDR);
		udelay(1);
		word = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_UDATA);
		udelay(1);
		fw[i] = be32_to_cpu(word);
	}

out:
	spin_unlock(&trtl->lock_cpu_sel);
	return err;
}


static int trtl_cpu_simple_open(struct inode *inode, struct file *file)
{
	int m = iminor(inode);

	file->private_data = to_trtl_cpu(minors[m]);

	return 0;
}

/**
 * Write a given firmware into a CPU
 */
static ssize_t trtl_cpu_write(struct file *f, const char __user *buf,
			      size_t count, loff_t *offp)
{
	struct trtl_cpu *cpu = f->private_data;
	void *lbuf;
	int err;

	if (!count)
		return -EINVAL;

	lbuf = vmalloc(count);
	if (!lbuf)
		return -ENOMEM;
	if (copy_from_user(lbuf, buf, count)) {
		err = -EFAULT;
		goto out_cpy;
	}

	err = trtl_cpu_firmware_load(cpu, lbuf, count, *offp);
	if (err)
		goto out_load;

	*offp += count;

out_load:
out_cpy:
	vfree(lbuf);
	return err ? err : count;
}

/**
 * Read the firmware from a CPU
 */
static ssize_t trtl_cpu_read(struct file *f, char __user *buf,
			     size_t count, loff_t *offp)
{
	struct trtl_cpu *cpu = f->private_data;
	void *lbuf;
	int err;

	lbuf = vmalloc(count);
	if (!lbuf)
		return -ENOMEM;

	err = trtl_cpu_firmware_dump(cpu, lbuf, count, *offp);
	if (err)
		goto out_dmp;

	if (copy_to_user(buf, lbuf, count)) {
		err = -EFAULT;
		goto out_cpy;
	}

	*offp += count;

out_cpy:
out_dmp:
	vfree(lbuf);
	return err ? err : count;
}

const struct file_operations trtl_cpu_fops = {
	.owner = THIS_MODULE,
	.open  = trtl_cpu_simple_open,
	.write  = trtl_cpu_write,
	.read = trtl_cpu_read,
	.llseek = generic_file_llseek,
};


static uint32_t trtl_cpu_irq_status(struct trtl_dev *trtl)
{
	uint32_t status;

	status = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_INT);

	return status & 0xFF;
}

static void trtl_cpu_irq_clear(struct trtl_dev *trtl, uint32_t mask)
{
	trtl_iowrite(trtl, mask, trtl->base_csr + MT_CPU_CSR_REG_INT);
}

static uint8_t trtl_cpu_not_get(struct trtl_cpu *cpu)
{
	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	void *addr;

	addr = trtl->base_csr + MT_CPU_CSR_REG_INT_VAL_LO;
	addr += (cpu->index & ~0x3);

	return (uint8_t)(trtl_ioread(trtl, addr) & 0xFF);
}

irqreturn_t trtl_cpu_irq_handler_not(int irq, void *arg)
{
	struct trtl_dev *trtl = arg;
	struct trtl_cpu *cpu;
	uint32_t status;
	unsigned long flags;
	int i, idx;

	status = trtl_cpu_irq_status(trtl);
	if (!status)
		return IRQ_NONE;


	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		if (!(status & (1 << i)))
			continue;

		cpu = &trtl->cpu[i];
		spin_lock_irqsave(&cpu->lock, flags);
		idx = cpu->idx_w++ & (TRTL_CPU_NOTIFY_HISTORY_N - 1);
		cpu->notification[idx] = trtl_cpu_not_get(cpu);
		if (cpu->idx_r + TRTL_CPU_NOTIFY_HISTORY_N <= cpu->idx_w)
			cpu->idx_r++;
		spin_unlock_irqrestore(&cpu->lock, flags);

		wake_up(&cpu->q_notify);
	}

	/* what if we clear a new interrupt coming right now ? */
	trtl_cpu_irq_clear(trtl, status);

	return IRQ_HANDLED;
}
