/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/circ_buf.h>
#include <linux/uaccess.h>
#include <linux/version.h>
#include <linux/idr.h>

#include <mockturtle/hw/mockturtle_queue.h>
#include <mockturtle/hw/mockturtle_cpu_csr.h>
#include "mockturtle-drv.h"


static void trtl_message_push(struct trtl_hmq *hmq, struct trtl_msg *msg);

static bool trtl_hmq_user_is_sync(struct trtl_hmq_user *user)
{
	return (user->flags & TRTL_HMQ_USER_FLAG_SYNC);
}

static bool trtl_hmq_user_is_waiting(struct trtl_hmq_user *user)
{
	return (user->flags & TRTL_HMQ_USER_FLAG_SYNC_WAIT);
}

static bool trtl_msg_is_ack(struct trtl_msg *msg)
{
	return (msg->hdr.flags & TRTL_HMQ_HEADER_FLAG_ACK);
}

static bool trtl_hmq_user_msg_is_answer(struct trtl_hmq_user *user,
					struct trtl_msg *msg)
{
	return (user->sync_id == msg->hdr.sync_id);
}

/**
 * Set the user in waiting mode for a sync answer
 * @user user
 * @sync_id synchronous message identifier
 *
 * Return: 0 on success otherwise a negative error number
 */
static void trtl_hmq_user_sync_wait_set(struct trtl_hmq_user *user)
{
	spin_lock(&user->lock);
	user->flags |= TRTL_HMQ_USER_FLAG_SYNC_WAIT;
	spin_unlock(&user->lock);
}

/**
 * User received the messages, do not wait anymore
 * @user user
 */
static void trtl_hmq_user_sync_wait_clr(struct trtl_hmq_user *user)
{
	spin_lock(&user->lock);
	user->flags &= ~TRTL_HMQ_USER_FLAG_SYNC_WAIT;
	spin_unlock(&user->lock);
}

/**
 * An opaque type. It is used to avoid mistakes when using features
 * that requires the HMQ selection. If you do not provide this type (which is
 * provided by the selection function) you will get at least a warning from
 * the compiler
 */
struct trtl_hmq_ctx;

static uint32_t trtl_hmq_occupied_slots(struct trtl_hmq_ctx *ctx,
					unsigned int is_output)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t val;
	void *base;

	base = is_output ? hmq->base_out : hmq->base_in;
	val = trtl_ioread(trtl, base + TRTL_MQ_SLOT_STATUS);
	val &= TRTL_MQ_SLOT_STATUS_OCCUPIED_MASK;
	val >>= TRTL_MQ_SLOT_STATUS_OCCUPIED_SHIFT;

	return val;
}

static uint32_t trtl_hmq_free_slots(struct trtl_hmq_ctx *ctx,
				    unsigned int is_output)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	uint32_t entries;

	entries = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(hmq->cfg->sizes);

	return (entries - trtl_hmq_occupied_slots(ctx, is_output));
}


static void trtl_hmq_command(struct trtl_hmq_ctx *ctx,
			     unsigned int command,
			     unsigned int is_output)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	void *base;

	base = is_output ? hmq->base_out : hmq->base_in;
	trtl_iowrite(trtl, command, base + TRTL_MQ_SLOT_COMMAND);
}

static void trtl_hmq_data_write(struct trtl_hmq_ctx *ctx, void *buf,
				unsigned int n_word)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t *data = buf, *base;
	int i;

	base = hmq->base_out + TRTL_MQ_SLOT_DATA_START;
	for (i = 0; i < n_word; ++i, ++base)
		trtl_iowrite(trtl, data[i], base);
}

static void trtl_hmq_data_read(struct trtl_hmq_ctx *ctx, void *buf,
			       unsigned int n_word)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t *data = buf, *base;
	int i;

	base = hmq->base_in + TRTL_MQ_SLOT_DATA_START;
	for (i = 0; i < n_word; ++i, ++base)
		data[i] = trtl_ioread(trtl, base);
}

static void trtl_hmq_header_read(struct trtl_hmq_ctx *ctx,
				 struct trtl_hmq_header *hdr)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t *base, val;

	base = hmq->base_in + TRTL_MQ_SLOT_HEADER_START;
	if (trtl->flags & TRTL_DEV_FLAGS_BIGENDIAN) {
		/* word 0 */
		val = trtl_ioread(trtl, &base[0]);
		hdr->rt_app_id =  (val & 0x0000FFFF) >> 0;
		hdr->flags  =     (val & 0x00FF0000) >> 16;
		hdr->msg_id =     (val & 0xFF000000) >> 24;
		/* word 1*/
		val = trtl_ioread(trtl, &base[1]);
		hdr->len =      (val & 0x0000FFFF) >> 0;
		hdr->sync_id =  (val & 0xFFFF0000) >> 16;
		/* word 2*/
		hdr->seq = trtl_ioread(trtl, &base[2]);
	} else {
		uint32_t *hdr_w = (uint32_t *)hdr;
		int i;

		for (i = 0; i < sizeof(struct trtl_hmq_header) / 4; ++i)
			hdr_w[i] = trtl_ioread(trtl, &base[i]);
	}
}

static void trtl_hmq_header_write(struct trtl_hmq_ctx *ctx,
				  struct trtl_hmq_header *hdr)
{
	struct trtl_hmq *hmq = (struct trtl_hmq *)ctx;
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t *base, val;

	base = hmq->base_out + TRTL_MQ_SLOT_HEADER_START;
	if (trtl->flags & TRTL_DEV_FLAGS_BIGENDIAN) {
		/* word 1 */
		val = 0;
		val |= (hdr->rt_app_id << 0);
		val |= (hdr->msg_id << 24);
		val |= (hdr->flags << 16);
		trtl_iowrite(trtl, val, &base[0]);
		/* word 1 */
		val = 0;
		val |= (hdr->sync_id << 16);
		val |= (hdr->len << 0);
		trtl_iowrite(trtl, val, &base[1]);
		/* word 2 */
		val = 0;
		val |= (hdr->seq << 0);
		trtl_iowrite(trtl, val, &base[2]);
	} else {
		uint32_t *hdr_w = (uint32_t *)hdr;
		int i;

		for (i = 0; i < sizeof(struct trtl_hmq_header) / 4; ++i)
			trtl_iowrite(trtl, hdr_w[i], &base[i]);
	}
}


static inline void trtl_cpu_hmq_select(struct trtl_dev *trtl,
				       unsigned int cpu, unsigned int hmq)
{
	uint32_t sel;


	sel  = (cpu << MT_CPU_CSR_HMQ_SEL_CORE_SHIFT);
	sel |= (hmq << MT_CPU_CSR_HMQ_SEL_QUEUE_SHIFT);
	sel &= (MT_CPU_CSR_HMQ_SEL_CORE_MASK | MT_CPU_CSR_HMQ_SEL_QUEUE_MASK);

	trtl_iowrite(trtl, sel, trtl->base_csr + MT_CPU_CSR_REG_HMQ_SEL);
}

static struct trtl_hmq_ctx * trtl_hmq_select(struct trtl_hmq *hmq)
{
	struct trtl_cpu *cpu = to_trtl_cpu(hmq->dev.parent);
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);

	trtl_cpu_hmq_select(trtl, cpu->index, hmq->index);

	return (struct trtl_hmq_ctx *)hmq;
}

void trtl_hmq_purge(struct trtl_hmq *hmq)
{
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	struct trtl_hmq_ctx *ctx;
	unsigned long flags;

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	ctx = trtl_hmq_select(hmq);
	trtl_hmq_command(ctx, TRTL_MQ_CMD_PURGE, 1);
	trtl_hmq_command(ctx, TRTL_MQ_CMD_PURGE, 0);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);
}


/**
 * Get the bit number of a HMQ within CSR HMQ registers
 * Return: a bit number
 */
static inline uint32_t trtl_hmq_csr_bit(struct trtl_hmq *hmq)
{
	struct trtl_cpu *cpu = to_trtl_cpu(hmq->dev.parent);
	uint32_t val;

	/* set the HMQ bit */
	val = 1 << (hmq->index & (TRTL_MAX_MQ_CHAN - 1));
	/* move to the right CPU */
	val <<= ((cpu->index & 0x3) * TRTL_MAX_CPU);

	return val;
}


/**
 * Enable IRQ for HMQ
 *
 * Internally it locks the HMQ selection spinlock
 */
static void trtl_hmq_irq_enable(struct trtl_hmq *hmq, unsigned int is_output)
{
	struct trtl_cpu *cpu = to_trtl_cpu(hmq->dev.parent);
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t val;
	void *addr;

	addr = trtl->base_csr;
	if (is_output)
		addr += MT_CPU_CSR_REG_HMQO_INTEN_LO;
	else
		addr += MT_CPU_CSR_REG_HMQI_INTEN_LO;
	/* move to correct register for the given CPU */
	addr += (cpu->index & ~0x3) * 2; /* NOTE - the next register is at +8 */

	val = trtl_ioread(trtl, addr);
	val |= trtl_hmq_csr_bit(hmq);
	trtl_iowrite(trtl, val, addr);
}


/**
 * Disable IRQ for HMQ
 *
 * Internally it locks the HMQ selection spinlock
 */
static void trtl_hmq_irq_disable(struct trtl_hmq *hmq, unsigned int is_output)
{
	struct trtl_cpu *cpu = to_trtl_cpu(hmq->dev.parent);
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	uint32_t val;
	void *addr;

	addr = trtl->base_csr;
	if (is_output)
		addr += MT_CPU_CSR_REG_HMQO_INTEN_LO;
	else
		addr += MT_CPU_CSR_REG_HMQI_INTEN_LO;
	addr += (cpu->index & ~0x3) * 2; /* NOTE - the next register is at +8 */

	val = trtl_ioread(trtl, addr);
	val &= ~trtl_hmq_csr_bit(hmq);
	trtl_iowrite(trtl, val, addr);
}



/**
 * Remove all the data from local buffers and HW slots
 * @hmq: the HMQ to flush
 *
 * Do not use it in interrupt context
 */
static void trtl_hmq_flush(struct trtl_hmq *hmq)
{
	struct trtl_hmq_user *usr, *tmp;
	struct mturtle_hmq_buffer *buf;
	unsigned long flags;

	mutex_lock(&hmq->mtx); /* Block user activity while flushing */

	/* purge (wven if soft-CPU may write again) */
	trtl_hmq_purge(hmq);

	/* Resets all the indexes */
	buf = &hmq->buf_in;
	spin_lock_irqsave(&buf->lock, flags);
	buf->idx_w = 0;
	buf->idx_r = 0;
	list_for_each_entry_safe(usr, tmp, &hmq->list_usr, list) {
		usr->idx_r = 0;
	}
	spin_unlock_irqrestore(&buf->lock, flags);

	buf = &hmq->buf_out;
	spin_lock_irqsave(&buf->lock, flags);
	buf->idx_w = 0;
	buf->idx_r = 0;
	spin_unlock_irqrestore(&buf->lock, flags);

	mutex_unlock(&hmq->mtx);
}

/**
 * Tell if user accepts the given message
 * @user: HMQ user
 * @msg: message
 *
 * Return: true if the user can read the message, otherwise false
 */
static bool trtl_hmq_user_filter_one(struct trtl_hmq_user *user,
				     struct trtl_msg *msg)
{
	if (trtl_hmq_user_is_sync(user)) {
		return trtl_hmq_user_is_waiting(user) &&
		    trtl_msg_is_ack(msg) &&
			trtl_hmq_user_msg_is_answer(user, msg);
	}

	return !trtl_msg_is_ack(msg);
}

/**
 * Clear the content of the HMQ
 */
static ssize_t discard_all_store(struct device *dev,
				 struct device_attribute *attr,
				 const char *buf, size_t count)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);

	/* Block soft-CPU activity while flushing */
	trtl_hmq_irq_disable(hmq, 0);
	trtl_hmq_irq_disable(hmq, 1);

	trtl_hmq_flush(hmq);

	/* re-enable input interrupts */
	trtl_hmq_irq_enable(hmq, 0);

	return count;
}
DEVICE_ATTR(discard_all, 0220, NULL, discard_all_store);


/**
 * It return 1 if the message quque slot is full
 */
static ssize_t full_show(struct device *dev,
			 struct device_attribute *attr,
			 char *buf)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);
	struct trtl_dev *trtl = to_trtl_dev(dev->parent->parent);
	uint32_t s_in, s_out;
	unsigned long flags;

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	trtl_hmq_select(hmq);
	s_in = trtl_ioread(trtl, hmq->base_in + TRTL_MQ_SLOT_STATUS);
	s_out = trtl_ioread(trtl, hmq->base_out + TRTL_MQ_SLOT_STATUS);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);

	return sprintf(buf, "%d %d\n", !!(s_in & 0x1), !!(s_out & 0x1));
}

DEVICE_ATTR(full, 0444, full_show, NULL);


/**
 * It return 1 if the message quque slot is empty
 */
static ssize_t empty_show(struct device *dev,
			  struct device_attribute *attr,
			  char *buf)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);
	struct trtl_dev *trtl = to_trtl_dev(dev->parent->parent);
	uint32_t s_in, s_out;
	unsigned long flags;

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	trtl_hmq_select(hmq);
	s_in = trtl_ioread(trtl, hmq->base_in + TRTL_MQ_SLOT_STATUS);
	s_out = trtl_ioread(trtl, hmq->base_out + TRTL_MQ_SLOT_STATUS);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);

	return sprintf(buf, "%d %d\n", !!(s_in & 0x2), !!(s_out & 0x2));
}
DEVICE_ATTR(empty, 0444, empty_show, NULL);

/**
 * It return 1 if the message quque slot is empty
 */
static ssize_t occupied_show(struct device *dev,
			     struct device_attribute *attr,
			     char *buf)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);
	struct trtl_dev *trtl = to_trtl_dev(dev->parent->parent);
	uint32_t s_in, s_out, entries;
	unsigned long flags;

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	trtl_hmq_select(hmq);
	s_in = trtl_ioread(trtl, hmq->base_in + TRTL_MQ_SLOT_STATUS);
	s_out = trtl_ioread(trtl, hmq->base_out + TRTL_MQ_SLOT_STATUS);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);

	entries = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(hmq->cfg->sizes);
	s_in &= TRTL_MQ_SLOT_STATUS_OCCUPIED_MASK;
	s_in >>= TRTL_MQ_SLOT_STATUS_OCCUPIED_SHIFT;
	s_out &= TRTL_MQ_SLOT_STATUS_OCCUPIED_MASK;
	s_out >>= TRTL_MQ_SLOT_STATUS_OCCUPIED_SHIFT;

	return sprintf(buf, "%d/%d %d/%d\n",
		       s_in, entries, s_out, entries);
}
DEVICE_ATTR(occupied, 0444, occupied_show, NULL);

static struct attribute *trtl_hmq_attr[] = {
	&dev_attr_discard_all.attr,
	&dev_attr_full.attr,
	&dev_attr_empty.attr,
	&dev_attr_occupied.attr,
	NULL,
};

static const struct attribute_group trtl_hmq_group = {
	.attrs = trtl_hmq_attr,
};


static ssize_t message_sent_show(struct device *dev,
				 struct device_attribute *attr,
				 char *buf)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);

	return sprintf(buf, "%d\n", hmq->buf_out.count);
}
DEVICE_ATTR(message_sent, 0444, message_sent_show, NULL);

static ssize_t message_received_show(struct device *dev,
				     struct device_attribute *attr,
				     char *buf)
{
	struct trtl_hmq *hmq = to_trtl_hmq(dev);

	return sprintf(buf, "%d\n", hmq->buf_in.count);
}
DEVICE_ATTR(message_received, 0444, message_received_show, NULL);

static struct attribute *trtl_hmq_attr_stat[] = {
	&dev_attr_message_received.attr,
	&dev_attr_message_sent.attr,
	NULL,
};

static const struct attribute_group trtl_hmq_group_stat = {
	.name = "statistics",
	.attrs = trtl_hmq_attr_stat,
};


static const struct attribute_group *trtl_hmq_groups[] = {
	&trtl_hmq_group,
	&trtl_hmq_group_stat,
	NULL,
};

static unsigned int trtl_hmq_buf_idx_get(struct mturtle_hmq_buffer *buf,
					 unsigned int idx_r_absolute)
{
	return idx_r_absolute & (buf->entries - 1);
}

static DEFINE_IDA(trtl_hmq_user_ida);

/**
 * It simply opens a HMQ device
 */
static int trtl_hmq_open(struct inode *inode, struct file *file)
{
	struct trtl_hmq_user *user;
	struct trtl_hmq *hmq;
	int m = iminor(inode);
	int id;
	unsigned long flags;

	hmq = to_trtl_hmq(minors[m]);

	user = kzalloc(sizeof(struct trtl_hmq_user), GFP_KERNEL);
	if (!user)
		return -ENOMEM;

	user->hmq = hmq;
	spin_lock_init(&user->lock);

	/* Add new user to the list */
	spin_lock_irqsave(&hmq->lock, flags);
	list_add(&user->list, &hmq->list_usr);
	hmq->n_user++;
	spin_unlock_irqrestore(&hmq->lock, flags);

	/* Assign new unique 16-bit sync_id to this user */
	id = ida_simple_get(&trtl_hmq_user_ida, 0, 65536, GFP_KERNEL);
	if (id < 0)
		return id;
	user->sync_id = id;

	spin_lock_irqsave(&hmq->buf_in.lock, flags);
	/* Point to the current position in buffer */
	user->idx_r = hmq->buf_in.idx_w;
	spin_unlock_irqrestore(&hmq->buf_in.lock, flags);

	file->private_data = user;

	return 0;
}

static int trtl_hmq_release(struct inode *inode, struct file *f)
{
	struct trtl_hmq_user *user = f->private_data;
	struct trtl_hmq *hmq = user->hmq;
	unsigned long flags;


	/* Remove user from the list */
	spin_lock_irqsave(&hmq->lock, flags);
	hmq->n_user--;
	ida_simple_remove(&trtl_hmq_user_ida, user->sync_id);
	list_del(&user->list);
	kfree(user);
	spin_unlock_irqrestore(&hmq->lock, flags);

	return 0;
}


/**
 * Write a single message from a buffer
 * @user: HMQ instance destination
 * @buf: source buffer
 * Return: 0 on success, otherwise a negative error number
 */
static int trtl_hmq_write_one(struct trtl_hmq_user *user,
			      const char __user *ubuf)
{
	struct trtl_hmq *hmq = user->hmq;
	struct trtl_msg *msg;
	int err = 0, copy_size;
	size_t size;
	struct mturtle_hmq_buffer *buf = &hmq->buf_out;
	unsigned long flags;

	if (trtl_hmq_user_is_sync(user) && trtl_hmq_user_is_waiting(user))
		return -EBUSY;

	/* Here we can safely sleep */
	size = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(hmq->cfg->sizes);
	copy_size = sizeof(struct trtl_hmq_header);
	copy_size += size;
	copy_size *= 4;
	if (copy_from_user(&buf->msg_tmp, ubuf, copy_size))
		return  -EFAULT;

	if (buf->msg_tmp.hdr.len > size) {
		dev_err(&hmq->dev,
			"write: cannot send %d bytes, the maximum size is %zu bytes\n",
			buf->msg_tmp.hdr.len * 4, size * 4);
		return -EINVAL;
	}

	if (trtl_hmq_user_is_sync(user)) {
		buf->msg_tmp.hdr.flags |= TRTL_HMQ_HEADER_FLAG_SYNC;
		buf->msg_tmp.hdr.sync_id = user->sync_id;
		trtl_hmq_user_sync_wait_set(user);
	}

	/* don't sleep here */
	spin_lock_irqsave(&buf->lock, flags);
	if ((buf->idx_w - buf->idx_r) >= buf->entries) {
		err = -EAGAIN;
	} else {
		/* copy only the message */
		copy_size = sizeof(struct trtl_hmq_header)
			+ (buf->msg_tmp.hdr.len) * sizeof(uint32_t);
		msg = &buf->msg[trtl_hmq_buf_idx_get(buf, buf->idx_w++)];
		memcpy(msg, &buf->msg_tmp, copy_size);
	}
	spin_unlock_irqrestore(&buf->lock, flags);

	return err;
}

/**
 * Write message in the drive message queue. The messages will be sent on
 * IRQ signal
 */
static ssize_t trtl_hmq_write(struct file *f, const char __user *buf,
			      size_t count, loff_t *offp)
{
	struct trtl_hmq_user *user = f->private_data;
	struct trtl_hmq *hmq = user->hmq;
	unsigned int i, n_msg;
	const char __user *curbuf = buf;
	int err = 0;

	/* We handle buffer messages-size aligned */
	if (count % sizeof(struct trtl_msg)) {
		dev_err(&hmq->dev,
			"write: buffer size not aligned with message-size (message-size: %zu, buffer-size: %zu)\n",
			sizeof(struct trtl_msg), count);
		return -EINVAL;
	}
	n_msg = count / sizeof(struct trtl_msg);

	count = 0;
	mutex_lock(&hmq->mtx);
	for (i = 0; i < n_msg; i++, curbuf += sizeof(struct trtl_msg)) {
		err = trtl_hmq_write_one(user, curbuf);
		if (err)
			break;
	}
	mutex_unlock(&hmq->mtx);


	/* Enable interrupts if we succesfully wrote at least one message */
	if (i > 0)
		trtl_hmq_irq_enable(hmq, 1);

	/* Update counter */
	count = i * sizeof(struct trtl_msg);
	*offp += count;

	/*
	 * If `count` is not 0, it means that we saved at least one message,
	 * even if we got an error on the second message. So, in this case
	 * notifiy the user space about how many messages where stored and
	 * ignore errors. Return the error only when we did not save any
	 * message.
	 *
	 * I choosed this solution because we do not have any dangerous error
	 * here, and it simplify the code.
	 */
	return count ? count : err;
}

/**
 * Set of special operations that can be done on the HMQ
 */
static long trtl_hmq_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
	struct trtl_hmq_user *user = f->private_data;
	void __user *uarg = (void __user *)arg;
	int err = 0;

	/* Check type and command number */
	if (_IOC_TYPE(cmd) != TRTL_IOCTL_MAGIC)
		return -ENOTTY;

#if KERNEL_VERSION(5, 0, 0) <= LINUX_VERSION_CODE
	err = !access_ok(uarg, _IOC_SIZE(cmd));
#else
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE, uarg, _IOC_SIZE(cmd));
	if (_IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ, uarg, _IOC_SIZE(cmd));
#endif
	if (err)
		return -EFAULT;

	/* Perform commands */
	mutex_lock(&user->hmq->mtx);
	switch (cmd) {
	case TRTL_IOCTL_MSG_SYNC_ABORT:
		if (trtl_hmq_user_is_sync(user)) {
			if (trtl_hmq_user_is_waiting(user))
				trtl_hmq_user_sync_wait_clr(user);
		} else {
			err = -EPERM;
		}
		break;
	case TRTL_IOCTL_HMQ_SYNC_SET:
		spin_lock(&user->lock);
		user->flags |= TRTL_HMQ_USER_FLAG_SYNC;
		spin_unlock(&user->lock);
		break;
	default:
		pr_warn("trtl: invalid ioctl command %d\n", cmd);
		err = -EINVAL;
		break;
	}
	mutex_unlock(&user->hmq->mtx);

	return err;
}


/**
 * Return a message to user space messages from an output HMQ
 */
static ssize_t trtl_hmq_read(struct file *f, char __user *ubuf,
			     size_t count, loff_t *offp)
{
	struct trtl_hmq_user *usr = f->private_data;
	struct trtl_hmq *hmq = usr->hmq;
	struct mturtle_hmq_buffer *buf = &hmq->buf_in;
	struct trtl_msg *msg;
	unsigned int i = 0, copy_size, n_msg;
	int err = 0;
	unsigned long flags;

	/* Calculate the number of messages to read */
	if (count % sizeof(struct trtl_msg)) {
		dev_err(&hmq->dev,
			"%s: buffer size not aligned with message-size (message-size: %zu, buffer-size: %zu)\n",
			__func__, sizeof(struct trtl_msg), count);
		return -EINVAL;
	}

	n_msg = count / sizeof(struct trtl_msg);
	count = 0;

	mutex_lock(&hmq->mtx);
	while (!err && i < n_msg && buf->idx_w != usr->idx_r) {

		spin_lock_irqsave(&buf->lock, flags);
		msg = &buf->msg[trtl_hmq_buf_idx_get(buf, usr->idx_r++)];

		if (!trtl_hmq_user_filter_one(usr, msg)) {
			spin_unlock_irqrestore(&buf->lock, flags);
			continue;
		}

		copy_size = sizeof(struct trtl_hmq_header);
		copy_size += (msg->hdr.len * 4);
		memcpy(&buf->msg_tmp, msg, copy_size);
		spin_unlock_irqrestore(&buf->lock, flags);

		if (trtl_hmq_user_is_sync(usr))
			trtl_hmq_user_sync_wait_clr(usr);

		/* now we copy to user space and we can safely sleep */
		if (copy_to_user(ubuf + count, &buf->msg_tmp, copy_size)) {
			err = -EFAULT;
			break;
		}
		count = (++i) * sizeof(struct trtl_msg);
	}
	mutex_unlock(&hmq->mtx);

	*offp += count;

	return count ? count : err;
}


/**
 * Filter out messages until a valid one
 * @usr: pointer to a user HMQ instance
 */
static void trtl_hmq_user_filter(struct trtl_hmq_user *usr)
{
	struct trtl_hmq *hmq = usr->hmq;
	struct mturtle_hmq_buffer *buf = &hmq->buf_in;
	struct trtl_msg *msg;

	/* Loop until we find a valid message for the user */
	while (buf->idx_w != usr->idx_r) {
		msg = &buf->msg[trtl_hmq_buf_idx_get(buf, usr->idx_r)];
		if (trtl_hmq_user_filter_one(usr, msg))
			break;
		usr->idx_r++;
	}
}

/**
 * Look into the input and output buffer to see if the user cna take
 * any action. If the user can write 'POLLOUT', if the user can read 'POLLOUT'
 */
static unsigned int trtl_hmq_poll(struct file *f, struct poll_table_struct *w)
{
	struct trtl_hmq_user *usr = f->private_data;
	struct trtl_hmq *hmq = usr->hmq;
	unsigned int ret = 0;
	unsigned long flags = 0;

	poll_wait(f, &hmq->q_msg, w);

	if (hmq->buf_out.idx_w == hmq->buf_out.idx_r) /* FIXME */
		ret |= POLLOUT | POLLWRNORM;

	/* Check if we have something to read */
	spin_lock_irqsave(&hmq->buf_in.lock, flags);
	trtl_hmq_user_filter(usr);
	if (hmq->buf_in.idx_w != usr->idx_r)
		ret |= POLLIN | POLLRDNORM;
	spin_unlock_irqrestore(&hmq->buf_in.lock, flags);

	return ret;
}

const struct file_operations trtl_hmq_fops = {
	.owner = THIS_MODULE,
	.open  = trtl_hmq_open,
	.release = trtl_hmq_release,
	.write  = trtl_hmq_write,
	.read = trtl_hmq_read,
	.unlocked_ioctl = trtl_hmq_ioctl,
	.poll = trtl_hmq_poll,
};


/**
 * Extract a message from the given HMQ
 * @hmq: where to get the message from
 * @msg: where to store the message
 *
 * Internally this function locks the HMQ selection (spinlock)
 */
static void trtl_message_pop(struct trtl_hmq *hmq, struct trtl_msg *msg)
{
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	struct trtl_hmq_ctx *ctx;
	size_t copy_size = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(hmq->cfg->sizes);
	unsigned long flags;

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	ctx = trtl_hmq_select(hmq);
	trtl_hmq_header_read(ctx, &msg->hdr);
	if (unlikely(msg->hdr.len > copy_size)) {
		/* FIXME perhaps a flag in the header */
		dev_warn(&hmq->dev,
			 "Incoming message length (len: %d) is invalid (max: %zu)\n",
			 msg->hdr.len, copy_size);
		msg->hdr.len = copy_size;
	}
	trtl_hmq_data_read(ctx, msg->data, msg->hdr.len);
	trtl_hmq_command(ctx, TRTL_MQ_CMD_DISCARD, 0);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);
}


/**
 * Extract a message from the given HMQ and store it in the local buffer
 * @hmq: where to get the message from
 *
 * Internally this function locks the local input buffer (spinlock)
 */
static void trtl_message_pop_to_buf(struct trtl_hmq *hmq)
{
	unsigned long flags;
	struct trtl_msg *msg;
	struct mturtle_hmq_buffer *buf = &hmq->buf_in;


	spin_lock_irqsave(&buf->lock, flags);
	msg = &buf->msg[trtl_hmq_buf_idx_get(buf, buf->idx_w++)];
	trtl_message_pop(hmq, msg);
	spin_unlock_irqrestore(&buf->lock, flags);

	buf->count++;
	wake_up_poll(&hmq->q_msg, POLLIN);
}

/**
 * Handle an incoming interrupt, messages coming from the soft-CPU
 */
static void trtl_irq_handler_input(struct trtl_hmq *hmq)
{
	struct mturtle_hmq_buffer *buf = &hmq->buf_in;
	struct trtl_hmq_user *usr, *tmp;
	unsigned long flags;

	/*
	 * Update user pointer when the write pointer is overwriting data
	 * not yet read by the user. Loop over all users and check their
	 * read-pointer
	 */
	list_for_each_entry_safe(usr, tmp, &hmq->list_usr, list) {
		spin_lock_irqsave(&buf->lock, flags);
		if (usr->idx_r + buf->entries > buf->idx_w) {
			spin_unlock_irqrestore(&buf->lock, flags);
			continue;
		}
		usr->idx_r++;
		spin_unlock_irqrestore(&buf->lock, flags);
	}

	trtl_message_pop_to_buf(hmq);
}


/**
 * Get the IRQ status for the output HMQ
 * @trtl: Mock Turtle device instance
 */
static uint64_t trtl_hmq_irq_status_in(struct trtl_dev *trtl)
{
	uint64_t s;

	s = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_HMQI_STATUS_HI);
	s <<= 32;
	s |= trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_HMQI_STATUS_LO);

	return s;
}


/**
 * Handle the HMQ interrupts incoming messages from the soft-CPU
 */
irqreturn_t trtl_irq_handler_in(int irq_core_base, void *arg)
{
	struct trtl_dev *trtl = arg;
	struct trtl_hmq *hmq;
	uint64_t status;
	unsigned long *loop;
	int i, n_disp = 0;

	status = trtl_hmq_irq_status_in(trtl);
	if (!status)
		return IRQ_NONE;

dispatch_irq:
	n_disp++;
	loop = (unsigned long *)&status;
	for_each_set_bit(i, loop, TRTL_MAX_MQ_CHAN * TRTL_MAX_CPU) {
		hmq = &trtl->cpu[i / TRTL_MAX_MQ_CHAN].hmq[i % TRTL_MAX_MQ_CHAN];
		trtl_irq_handler_input(hmq);
	}

	status = trtl_hmq_irq_status_in(trtl);
	if (status && n_disp < trtl->max_irq_loop)
		goto dispatch_irq;

	return IRQ_HANDLED;
}



/**
 * Write a message to a FPGA HMQ. Note that you have to take
 * the HMQ spinlock before call this function
 * @hmq: where to send the message
 * @msg: the message to send
 *
 * Internally this function locks the HMQ selection (spinlock)
 *
 * Use this only from the output IRQ handler because it assumes that
 * the output channel has a free slot (the reason for having the IRQ)
 */
static void trtl_message_push(struct trtl_hmq *hmq, struct trtl_msg *msg)
{
	struct trtl_dev *trtl = to_trtl_dev(hmq->dev.parent->parent);
	struct trtl_hmq_ctx *ctx;
	unsigned long flags;
	size_t size;

	size = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(hmq->cfg->sizes);
	if (unlikely(msg->hdr.len > size)) {
		dev_err(&hmq->dev,
			"The message (%d bytes) does not fit in the maximum message size (%d bytes)\n",
			msg->hdr.len,
			TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(hmq->cfg->sizes));
		msg->hdr.len = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(hmq->cfg->sizes);
	}

	spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
	ctx = trtl_hmq_select(hmq);
	trtl_hmq_command(ctx, TRTL_MQ_CMD_CLAIM, 1);
	trtl_hmq_header_write(ctx, &msg->hdr);
	trtl_hmq_data_write(ctx, msg->data, msg->hdr.len);
	trtl_hmq_command(ctx, TRTL_MQ_CMD_READY, 1);
	spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);
}


/**
 * Inject a message to the given HMQ from the local buffer
 * @hmq: where to get the message from
 *
 * Internally this function locks the local output buffer (spinlock)
 *
 * Use this only from the output IRQ handler because it assumes that
 * the output channel has a free slot (the reason for having the IRQ)
 */
static void trtl_message_push_from_buf(struct trtl_hmq *hmq)
{
	unsigned long flags;
	struct mturtle_hmq_buffer *buf = &hmq->buf_out;
	struct trtl_msg *msg;

	spin_lock_irqsave(&buf->lock, flags);
	if (likely(buf->idx_w != buf->idx_r)) {
		msg = &buf->msg[trtl_hmq_buf_idx_get(buf, buf->idx_r++)];
		trtl_message_push(hmq, msg);
		buf->count++;
	}
	if (buf->idx_w == buf->idx_r)
		trtl_hmq_irq_disable(hmq, 1);
	spin_unlock_irqrestore(&buf->lock, flags);

	wake_up_poll(&hmq->q_msg, POLLOUT);
}


/**
 * Get the IRQ status for the input HMQ
 * @trtl: Mock Turtle device instance
 */
static uint64_t trtl_hmq_irq_status_out(struct trtl_dev *trtl)
{
	uint64_t s;

	s = trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_HMQO_STATUS_HI);
	s <<= 32;
	s |= trtl_ioread(trtl, trtl->base_csr + MT_CPU_CSR_REG_HMQO_STATUS_LO);

	return s;
}


/**
 * Handle the HMQ interrupts for outgoing messages to the soft-CPU
 */
irqreturn_t trtl_irq_handler_out(int irq_core_base, void *arg)
{
	struct trtl_dev *trtl = arg;
	struct trtl_hmq_ctx *ctx;
	uint64_t status;
	int i, freeslots;
	struct trtl_hmq *hmq;
	unsigned long flags;
	unsigned long *loop;

	status = trtl_hmq_irq_status_out(trtl);
	if (!status)
		return IRQ_NONE;

	loop = (unsigned long *)&status;
	for_each_set_bit(i, loop, TRTL_MAX_MQ_CHAN * TRTL_MAX_CPU) {
		hmq = &trtl->cpu[i / TRTL_MAX_MQ_CHAN].hmq[i % TRTL_MAX_MQ_CHAN];

		/*
		 * It should never happen that we get an interrupt and
		 * there are no free slots
		 */
		spin_lock_irqsave(&trtl->lock_hmq_sel, flags);
		ctx = trtl_hmq_select(hmq);
		freeslots = trtl_hmq_free_slots(ctx, 1);
		spin_unlock_irqrestore(&trtl->lock_hmq_sel, flags);
		if (unlikely(!freeslots)) {
			dev_err(&hmq->dev,
				"There must be a freeslot if we got an interrupt. Look for an HDL bug\nDisable HMQ output IRQ\n");
			trtl_hmq_irq_disable(hmq, 1);
		}
		trtl_message_push_from_buf(hmq);
	}

	return IRQ_HANDLED;
}

/**
 * Things to do after HMQ device release
 */
static void trtl_hmq_dev_release(struct device *dev)
{
	trtl_minor_put(dev);
}


static int trtl_hmq_buf_init(struct mturtle_hmq_buffer *buf)
{
	spin_lock_init(&buf->lock);

	buf->idx_r = 0;
	buf->idx_w = 0;
	buf->count = 0;

	return 0;
}

static int input_msg_buffer_size = -1;
module_param(input_msg_buffer_size, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(input_msg_buffer_size, "input buffer size");

static int output_msg_buffer_size = -1;
module_param(output_msg_buffer_size, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(output_msg_buffer_size, "output buffer size");

/**
 * Initialize and registers a HMQ device
 */
int trtl_probe_hmq(struct trtl_cpu *cpu, unsigned int hmq_idx)
{

	struct trtl_dev *trtl = to_trtl_dev(cpu->dev.parent);
	struct trtl_hmq *hmq = &cpu->hmq[hmq_idx];
	int err;

	hmq->index = hmq_idx;

	err = trtl_minor_get(&hmq->dev, TRTL_HMQ);
	if (err)
		return err;

	err = dev_set_name(&hmq->dev, "%s-%02d",
			   dev_name(&cpu->dev), hmq->index);
	if (err)
		return err;


	hmq->cfg = &trtl->cfgrom.hmq[cpu->index][hmq->index];

	hmq->base_in = trtl->base_hmq + TRTL_MQ_BASE_IN;
	err = trtl_hmq_buf_init(&hmq->buf_in);
	if (err)
		goto out_buf_in;

	hmq->base_out = trtl->base_hmq + TRTL_MQ_BASE_OUT;
	err = trtl_hmq_buf_init(&hmq->buf_out);
	if (err)
		goto out_buf_out;

	init_waitqueue_head(&hmq->q_msg);
	mutex_init(&hmq->mtx);
	INIT_LIST_HEAD(&hmq->list_msg_input);
	INIT_LIST_HEAD(&hmq->list_usr);
	spin_lock_init(&hmq->lock);

	hmq->dev.class = &trtl_cdev_class;
	hmq->dev.parent = &cpu->dev;
	hmq->dev.groups = trtl_hmq_groups;
	hmq->dev.release = trtl_hmq_dev_release;
	err = device_register(&hmq->dev);
	if (err)
		goto out_dev;

	/* Allocate buffers */
	if (input_msg_buffer_size != -1) {
		// check if power of 2
		if (input_msg_buffer_size != 0 && (input_msg_buffer_size & (input_msg_buffer_size - 1)) != 0) {
			goto out_dev;
	    }
		hmq->buf_in.entries = input_msg_buffer_size;
	} else {
		hmq->buf_in.entries = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(hmq->cfg->sizes);
	}

	hmq->buf_in.msg = kcalloc(hmq->buf_in.entries,
				  sizeof(struct trtl_msg),
				  GFP_KERNEL);
	if (!hmq->buf_in.msg)
		goto out_msg_in;

	if (output_msg_buffer_size != -1) {
		// check if power of 2
		if (output_msg_buffer_size != 0 && (output_msg_buffer_size & (output_msg_buffer_size - 1)) != 0) {
			goto out_msg_out;
	    	}
		hmq->buf_out.entries = output_msg_buffer_size;
	} else {
		hmq->buf_out.entries = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(hmq->cfg->sizes);
	}

	hmq->buf_out.msg = kcalloc(hmq->buf_out.entries,
				   sizeof(struct trtl_msg),
				   GFP_KERNEL);
	if (!hmq->buf_out.msg)
		goto out_msg_out;

	trtl_hmq_purge(hmq);
	trtl_hmq_irq_enable(hmq, 0);
	trtl_hmq_irq_disable(hmq, 1);

	return 0;

out_msg_out:
	kfree(hmq->buf_in.msg);
out_msg_in:
	device_unregister(&hmq->dev);
out_dev:
out_buf_out:
out_buf_in:
	return err;
}

void trtl_remove_hmq(struct trtl_cpu *cpu, unsigned int hmq_idx)
{
	struct trtl_hmq *hmq = &cpu->hmq[hmq_idx];

	trtl_hmq_irq_disable(hmq, 0);
	trtl_hmq_irq_disable(hmq, 1);

	trtl_hmq_flush(hmq);

	kfree(hmq->buf_in.msg);
	kfree(hmq->buf_out.msg);

	device_unregister(&hmq->dev);
}
