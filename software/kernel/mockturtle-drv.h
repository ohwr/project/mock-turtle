/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#ifndef __TRTL_H__
#define __TRTL_H__

#include <linux/platform_device.h>
#include <linux/irqreturn.h>
#include <linux/circ_buf.h>
#include <linux/tty.h>
#include <linux/version.h>

#include "mockturtle/hw/mockturtle_addresses.h"
#include "mockturtle/hw/mockturtle_queue.h"
#include "mockturtle/mockturtle.h"

#define MAX_MQUEUE_SLOTS (TRTL_MAX_HMQ_SLOT / 2)
#define TRTL_MAX_CPU_MINORS (TRTL_MAX_CPU * TRTL_MAX_CARRIER)
#define TRTL_MAX_HMQ_MINORS (TRTL_MAX_MQ_CHAN * TRTL_MAX_CARRIER)
#define TRTL_MAX_MINORS (TRTL_MAX_CARRIER + \
			 TRTL_MAX_CPU_MINORS + \
			 TRTL_MAX_HMQ_MINORS)

#define TRTL_SMEM_MAX_SIZE 65536

#define to_trtl_cpu(_dev) (container_of(_dev, struct trtl_cpu, dev))
#define to_trtl_dev(_dev) (container_of(_dev, struct trtl_dev, dev))
#define to_pdev_dev(_trtl) (container_of(_trtl->dev.parent, \
					 struct platform_device, \
					 dev))
#define to_trtl_hmq(_dev) (container_of(_dev, struct trtl_hmq, dev))

/**
 * 1 CPU input, 0 CPU output
 */
#define TRTL_FLAG_HMQ_DIR (1 << 0)

/**
 * 1 shared, means that more than 1 CPU is using it
 */
#define TRTL_FLAG_HMQ_SHR (1 << 1)

/**
 * Shared by users
 */
#define TRTL_FLAG_HMQ_SHR_USR (1 << 2)

extern struct class trtl_cdev_class;


/**
 * Enumerate the IRQ sources
 * @TRTL_IRQ_HMQ_IN: incoming messages from soft-CPUs
 * @TRTL_IRQ_HMQ_OUT: outgoing messages to soft-CPUs
 * @TRTL_IRQ_HMQ_CON: messages in the console
 * @TRTL_IRQ_HMQ_NOT: soft-CPU notifications
 */
enum mockturtle_irq_resource {
	TRTL_IRQ_HMQ_IN = 0,
	TRTL_IRQ_HMQ_OUT,
	TRTL_IRQ_CON,
	TRTL_IRQ_NOT,
};

enum mockturtle_mem_resource {
	TRTL_MEM_BASE = 0,
};


static inline uint32_t trtl_get_sequence(struct trtl_msg *msg)
{
	return msg->data[1];
}

struct trtl_memory_ops {
#if KERNEL_VERSION(5, 8, 0) <= LINUX_VERSION_CODE
	u32 (*read)(const void *addr);
#else
	u32 (*read)(void *addr);
#endif
	void (*write)(u32 value, void *addr);
};

extern struct trtl_memory_ops memops;

/**
 * Available type of devices
 * @TRTL_DEV: the whole TRTL component
 * @TRTL_CPU: CPU core of the TRTL
 * @TRTL_HMQ: HMQ slot ot the TRTL
 */
enum trtl_dev_type {
	TRTL_DEV,
	TRTL_CPU,
	TRTL_HMQ,
};

/**
 * Message structure for the driver
 * @list: to keep it in our local queue
 * @msg: the real message
 */
struct trtl_msg_element {
	struct list_head list;
	struct trtl_msg *msg;
};


/**
 * Circular buffer implementation for MockTurtle
 */
struct mturtle_hmq_buffer {
	spinlock_t lock;
	unsigned int idx_w;
	unsigned int idx_r;
	unsigned int entries;
	struct trtl_msg *msg;
	struct trtl_msg msg_tmp;
	unsigned int count;
};


/**
 * struct trtl_hmq_slot - slot description
 * @header: slot header
 * @payload:
 */
struct trtl_hmq_slot {
	struct trtl_hmq_header header;
	uint32_t *payload;
};



/**
 * Decribe the status of a HMQ slot
 * @dev:
 * @index: instance number
 * @flags: describe the status of the HMQ slot from the driver point of view
 * @status: describe the status of the HMQ slot from the cpu point of view
 * @base_in: base address for incoming HMQ
 * @base_out: base address for outgoing HMQ
 * @head list_msg_input: list of messages to input slot
 * @n_input: number of messages in the list
 * @lock: to protect the users list
 * @mtx: to protect operations on the HMQ
 * @q_msg: wait queue for messages
 * @list_usr: list of consumer of the output slot
 * @n_user: number of users in the list
 * @cfg: configuration information for the MQ
 * @max_width: maximum words number per single buffer
 * @max_depth: maximum buffer queue length (HW)
 * @buf_in: Circular buffer incoming messages
 * @buf_out: Circular buffer outgoing messages
 */
struct trtl_hmq {
	struct device dev;
	int index;
	unsigned long flags;
	uint32_t status;
	void *base_in;
	void *base_out;
	struct list_head list_msg_input;
	unsigned int n_input;
	spinlock_t lock;
	struct mutex mtx;
	wait_queue_head_t q_msg;
	struct list_head list_usr;
	unsigned int n_user;
	const struct trtl_config_rom_mq *cfg;
	unsigned int max_width;
	unsigned int max_depth;
	struct mturtle_hmq_buffer buf_in;
	struct mturtle_hmq_buffer buf_out;
};


/**
 * User do only sync messages
 */
#define TRTL_HMQ_USER_FLAG_SYNC BIT(0)

/**
 * User is waiting for a sync answer message
 */
#define TRTL_HMQ_USER_FLAG_SYNC_WAIT BIT(1)

/**
 * Describe the consumer of the output slot
 * @list: to keep it in our local queue
 * @hmq: reference to opened HMQ
 * @lock: to protect flags
 * @flags: status flags for syncrhonous messages
 * @idx_r: index read pointer for the message circular buffer. This is
 *         protected by the input buffer lock. Accessing this field means that
 *         you are accessing the buffer input, and in order to do that you need
 *         to get the input buffer spinlock. So, we are protecting this
 *         variable as well.
 * @sync_id: Unique sync id to be used when waiting for sync answer
 */
struct trtl_hmq_user {
	struct list_head list;
	struct trtl_hmq *hmq;
	spinlock_t lock;
	unsigned long flags;
	unsigned int idx_r;
	uint16_t sync_id;
};


/**
 * Describe a single instance of a CPU of the TRTL
 * @index: instance number
 * @dev: device representing a single CPU
 * @cbuf: debug circular buffer
 * @lock:
 * @hmq: list of HMQ slots used by this CPU
 * @tty_dev;
 * @tty_port:
 * @q_notify:
 * @notification:
 * @idx_r:
 * @idx_w:
 */
struct trtl_cpu {
	int index;
	struct device dev;
	struct circ_buf cbuf;
	spinlock_t lock;
	struct trtl_hmq hmq[TRTL_MAX_MQ_CHAN];
	struct device *tty_dev;
	struct tty_port tty_port;
	wait_queue_head_t q_notify;
	enum trtl_cpu_notification notification[TRTL_CPU_NOTIFY_HISTORY_N];
	unsigned int idx_r;
	unsigned int idx_w;
};


#define TRTL_DEV_FLAGS_BIGENDIAN BIT(0)
/**
 * Describe the generic instance of a TRTL
 * @
 * @dev;
 * @cpu: CPU instances
 * @flags: various options
 * @base_core: base address of the TRTL component
 * @base_csr: base address of the Shared Control Register
 * @base_hmq: base address of the HMQ
 * @base_cfg: base address for the configuration ROM
 * @base_smem: base address of the Shared Memory
 * @irq_mask: IRQ mask in use
 * @lock_cpu_sel:
 * @lock_hmq_sel: protects the HMQ usage which is based on selection. It
 *                protects also last_seq which is related to HMQ messages
 * @cfgrom: synthesis configuration ROM
 * @tty_driver:
 * @memops:
 * @dbg_dir: root debug directory
 * @dbg_info: information
 * @max_irq_loop: maximum number of messages that can be retrieved with a
 *                single IRQ. It regulates the HMQ IRQ handler
 * @debugger: debugfs interface for gdbserver
 */
struct trtl_dev {
	struct device dev;
	struct trtl_cpu cpu[TRTL_MAX_CPU];
	unsigned long flags;
	void *base_core;
	void *base_csr;
	void *base_hmq;
	void *base_cfg;
	void *base_smem;
	uint64_t irq_mask;
	spinlock_t lock_cpu_sel;
	spinlock_t lock_hmq_sel;
	const struct trtl_config_rom cfgrom;
	struct tty_driver *tty_driver;
	struct trtl_memory_ops memops;
	struct dentry *dbg_dir;
	struct dentry *dbg_info;
	unsigned int max_irq_loop;
	struct dentry *debugger;
};

static inline u32 trtl_ioread(struct trtl_dev *trtl, void *addr)
{
	return trtl->memops.read(addr);
}

static inline void trtl_iowrite(struct trtl_dev *trtl,
				u32 value, void *addr)
{
	return trtl->memops.write(value, addr);
}

/* Global data */
extern struct device *minors[TRTL_MAX_MINORS];
extern int trtl_minor_get(struct device *dev, enum trtl_dev_type type);
extern void trtl_minor_put(struct device *dev);

/* CPU data */
extern const struct file_operations trtl_cpu_fops;
extern const struct attribute_group *trtl_cpu_groups[];
extern void trtl_cpu_reset_set(struct trtl_dev *trtl, uint8_t mask);
extern irqreturn_t trtl_cpu_irq_handler_not(int irq, void *arg);
/* HMQ */
extern int hmq_shared;
extern int trtl_probe_hmq(struct trtl_cpu *cpu, unsigned int hmq_idx);
extern void trtl_remove_hmq(struct trtl_cpu *cpu, unsigned int hmq_idx);
extern const struct file_operations trtl_hmq_fops;
extern irqreturn_t trtl_irq_handler_out(int irq_core_base, void *arg);
extern irqreturn_t trtl_irq_handler_in(int irq_core_base, void *arg);
extern void trtl_hmq_purge(struct trtl_hmq *hmq);
/* TTY */
extern int trtl_tty_probe(struct trtl_dev *trtl);
extern void trtl_tty_remove(struct trtl_dev *trtl);
/* DBG */
extern void trtl_debugfs_init(struct trtl_dev *trtl);
extern void trtl_debugfs_exit(struct trtl_dev *trtl);

extern struct class trtl_cdev_class;

#endif
