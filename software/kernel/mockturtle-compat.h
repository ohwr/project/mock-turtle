// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __TRTL_COMPAT_H__
#define __TRTL_COMPAT_H__

#include <linux/version.h>
#include <linux/device.h>
#include <linux/ioport.h>

#if KERNEL_VERSION(3, 9, 0) > LINUX_VERSION_CODE
int vm_iomap_memory(struct vm_area_struct *vma, phys_addr_t start,
		    unsigned long len);
void __iomem *devm_ioremap_resource(struct device *dev, struct resource *res);
#endif

#endif
