/*
 * SPDX-FileCopyrightText: 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <linux/debugfs.h>
#include <linux/device.h>
#include <linux/seq_file.h>
#include <linux/mm.h>

#include <mockturtle/hw/mockturtle_cpu_csr.h>
#include "mockturtle-drv.h"
#include "mockturtle-compat.h"

static int trtl_dbg_info_seq_read(struct seq_file *s, void *data)
{
	struct trtl_dev *trtl = s->private;
	struct trtl_cpu *cpu;
	struct trtl_hmq *hmq;
	struct trtl_hmq_user *usr;
	uint32_t reg;
	int i, k;

	seq_printf(s, "%s\n", dev_name(&trtl->dev));
	seq_puts(s, "cpu-core:\n");

	reg = trtl_ioread(trtl,
			  trtl->base_csr + MT_CPU_CSR_REG_HMQI_STATUS_LO);
	seq_printf(s, "  HMQ input status LO: 0x%x:\n", reg);

	reg = trtl_ioread(trtl,
			  trtl->base_csr + MT_CPU_CSR_REG_HMQI_STATUS_HI);
	seq_printf(s, "  HMQ input status HI: 0x%x:\n", reg);

	reg = trtl_ioread(trtl,
			  trtl->base_csr + MT_CPU_CSR_REG_HMQO_STATUS_LO);
	seq_printf(s, "  HMQ output status LO: 0x%x:\n", reg);

	reg = trtl_ioread(trtl,
			  trtl->base_csr + MT_CPU_CSR_REG_HMQO_STATUS_HI);
	seq_printf(s, "  HMQ output status HI: 0x%x:\n", reg);

	seq_puts(s, "  cpu-core:\n");
	for (i = 0; i < trtl->cfgrom.n_cpu; ++i) {
		cpu = &trtl->cpu[i];
		seq_printf(s, "  - index: %d\n",
			   cpu->index);
		seq_printf(s, "    name: %s\n",
			   dev_name(&cpu->dev));
		seq_puts(s, "    hmq:\n");
		for (k = 0; k < trtl->cfgrom.n_hmq[i]; ++k) {
			hmq = &cpu->hmq[k];
			seq_printf(s, "    - index: %d\n",
				   hmq->index);
			seq_printf(s, "      name: %s\n",
				   dev_name(&hmq->dev));
			seq_printf(s, "      buf-in-max: %d\n",
				   hmq->buf_in.entries);
			list_for_each_entry(usr, &hmq->list_usr, list) {
				seq_printf(s, "      buf-in-r: %d (user %p)\n",
					   usr->idx_r, usr);
			}
			seq_printf(s, "      buf-in-w: %d\n",
				   hmq->buf_in.idx_w);
			seq_printf(s, "      buf-out-max: %d\n",
				   hmq->buf_out.entries);
			seq_printf(s, "      buf-out-r: %d\n",
				   hmq->buf_out.idx_r);
			seq_printf(s, "      buf-out-w: %d\n",
				   hmq->buf_out.idx_w);
		}
	}
	return 0;
}

static int trtl_dbg_info_open(struct inode *inode, struct file *file)
{
	return single_open(file, trtl_dbg_info_seq_read, inode->i_private);
}


static const struct file_operations trtl_dbg_info_ops = {
	.owner = THIS_MODULE,
	.open = trtl_dbg_info_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

#define TRTL_DBG_PORT_SIZE PAGE_SIZE /* FIXME */
static int trtl_dbg_debugger_mmap(struct file *file,
				  struct vm_area_struct *vma)
{
	struct trtl_dev *trtl = file->private_data;
	unsigned long vsize;
	int ret;
	struct platform_device *pdev = to_platform_device(trtl->dev.parent);
	struct resource *r;


	if (vma->vm_pgoff > 0) {
		dev_err(&trtl->dev,
			"Debug Port mapping does not accept offsets\n");
		return -EINVAL;
	}

	vsize = vma->vm_end - vma->vm_start;
	if (vsize > TRTL_DBG_PORT_SIZE)  {
		dev_err(&trtl->dev,
			"Debug Port mapping can't map more that %lu bytes (%lu)\n",
			TRTL_DBG_PORT_SIZE, vsize);
		return -EINVAL;
	}

	vma->vm_flags |= VM_IO | VM_DONTCOPY | VM_DONTEXPAND;
	vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);

	r = platform_get_resource(pdev, IORESOURCE_MEM, TRTL_MEM_BASE);
	return vm_iomap_memory(vma,
			       r->start + TRTL_ADDR_OFFSET_DBG,
			       TRTL_DBG_PORT_SIZE);

	return ret;
}

static const struct file_operations trtl_dbg_debugger_ops = {
	.owner = THIS_MODULE,
	.open = simple_open,
	.mmap = trtl_dbg_debugger_mmap,
};

/**
 * Create the debug info file
 * @trtl: the mock turtle device instance
 */
void trtl_debugfs_init(struct trtl_dev *trtl)
{
	char name[20];

	trtl->dbg_dir = debugfs_create_dir(dev_name(&trtl->dev), NULL);
	if (IS_ERR_OR_NULL(trtl->dbg_dir)) {
		dev_err(&trtl->dev, "Cannot create debugfs\n");
		return;
	}


	trtl->dbg_info = debugfs_create_file("info", 0444,
					     trtl->dbg_dir, trtl,
					     &trtl_dbg_info_ops);
	if (IS_ERR_OR_NULL(trtl->dbg_info)) {
		dev_err(&trtl->dev,
			"Cannot create debugfs file: %ld\n",
			PTR_ERR(trtl->dbg_info));
		trtl->dbg_info = NULL;
	}

	snprintf(name, 20, "%s-dbg", dev_name(&trtl->dev));
	trtl->debugger = debugfs_create_file(name, 0664, trtl->dbg_dir,
					     trtl, &trtl_dbg_debugger_ops);
	if (IS_ERR_OR_NULL(trtl->debugger)) {
		dev_err(&trtl->dev,
			"Cannot create debugfs file: %ld\n",
			PTR_ERR(trtl->debugger));
		trtl->debugger = NULL;
	}
}


/**
 * Remove the debug info file
 * @trtl: the mock turtle device instance
 */
void trtl_debugfs_exit(struct trtl_dev *trtl)
{
	debugfs_remove_recursive(trtl->dbg_dir);
}
