/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>
#include <getopt.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <libgen.h>

/**
 * Maximum number of possible queue selection
 */
#define MAX_SELECTION (TRTL_MAX_CPU * TRTL_MAX_MQ_CHAN)


/**
 * Necessary context for a thread
 */
struct trtl_thread {
	uint32_t devid;
	int cpu;
	int hmq;
	int share;
	int n;
};

static int timestamp = 0;
static int protocol;

static void help()
{
	fprintf(stderr, "\n");
	fprintf(stderr, "mockturtle-messages [options]\n\n");
	fprintf(stderr, "Dump all messages from a given set of Mock-Turtle slots\n\n");
	fprintf(stderr, "-q   message queue selection string\n\t0x<devid>,<cpu>,<hmq>,<n>\n");
	fprintf(stderr, "-t   print message timestamp\n");
	fprintf(stderr, "-p   parse protocol\n");
	fprintf(stderr, "-h   show this help\n");
	fprintf(stderr, "\n");
	fprintf(stderr,
		"e.g. Dumping messagges from slots 1 and 2 on CPU 1 of device 0x0402\n\n");
	fprintf(stderr,
		"        mockturtle-messages -q 0x0402,1,1 -q 0x0402,1,2\n\n");
	exit(1);
}


/**
 * Retreive a message from a given slots and it prints its content
 * @param[in] trtl device to use
 * @param[in] hmq slot to read
 */
static int dump_message(struct polltrtl *p)
{
	struct trtl_msg wmsg;
	int ret;

	if (timestamp) {
		time_t tm;
		struct tm *gm;
		char strtime[64];

		tm = time(NULL);
		gm = gmtime(&tm);
		strftime(strtime, 64,"%T", gm);
		fprintf(stdout, "[%s] ", strtime);
	}
	fprintf(stdout, "%s cpu: %u hmq: %u:",
		trtl_name_get(p->trtl), p->idx_cpu, p->idx_hmq);
	ret = trtl_msg_async_recv(p->trtl, p->idx_cpu, p->idx_hmq, &wmsg, 1);
	if (ret < 0) {
		fprintf(stdout, " error : %s\n", trtl_strerror(errno));
		return -1;
	}
	if (ret == 0)
		return 0;

	fprintf(stdout, "\n");
	if (protocol) {
		trtl_print_message(&wmsg);
	} else {
		int i;

		for (i = 0; i < wmsg.hdr.len; ++i) {
			if (i % 4 == 0)
				fprintf(stdout, "\n    %04d :", i);
			fprintf(stdout, " 0x%08x", wmsg.data[i]);
		}

	}
	fprintf(stdout, "\n");

	return 0;
}


/**
 * pthread for each device. It dumps messages from slots
 * @param[in] arg a pointer to the device index
 */
void *message_thread(void *arg)
{
	struct trtl_thread *th = arg;
	unsigned int cnt = 0;
	struct polltrtl p[1];
	struct trtl_dev *trtl;
	int err;

	/* Open the device */
	trtl = trtl_open_by_id(th->devid);
	if (!trtl) {
		fprintf(stderr, "Cannot open Mock Turtle device: %s\n", trtl_strerror(errno));
		pthread_exit(NULL);
	}

	/* trtl_hmq_share_set(trtl, 0 , th->share, 1); */

	p[0].trtl = trtl;
	p[0].idx_cpu = th->cpu;
	p[0].idx_hmq = th->hmq;
	p[0].events = POLLIN | POLLERR;

	/* Start dumping messages */
	while (th->n == 0 || th->n > cnt) {
		int ret;

		/* Polling slots */
		ret = trtl_msg_poll(p, 1, 1000);
		switch (ret) {
		default:
			/* Dump from the slot */
			if (!(p[0].revents & POLLIN))
				continue;

			err = dump_message(&p[0]);
			if (err)
				continue;
			cnt++;
			break;
		case 0:
			/* timeout */
			break;
		case -1:
			/* error - force loop to close */
			cnt = th->n;
		}
	}

	trtl_close(trtl);
	return NULL;
}


int main(int argc, char *argv[])
{
	struct trtl_thread th[MAX_SELECTION], *tmp;
	unsigned int th_idx = 0;
	unsigned long i;
	pthread_t tid_msg[MAX_SELECTION];
	int err, ret;
	char c;

	atexit(trtl_exit);

	memset(th, 0, sizeof(struct trtl_thread) * MAX_SELECTION);

	while ((c = getopt (argc, argv, "hq:tp")) != -1) {
		switch (c) {
		default:
			help();
			break;
		case 'q':
			tmp = &th[th_idx];
			ret = sscanf(optarg, "0x%x,%d,%d,%d",
				     &tmp->devid, &tmp->cpu, &tmp->hmq, &tmp->n);
			switch (ret) {
			case 0:
				fprintf(stderr, "Missing devid id\n");
			case 1:
				fprintf(stderr, "Missing cpu index\n");
			case 2:
				fprintf(stderr, "Missing hmq index\n");
				break;
			case 3:
				tmp->n = 0;
			case 4:
				break;
			}

			th_idx++;
			break;
		case 't':
			timestamp = 1;
			break;
		case 'p':
			protocol = 1;
			break;
		}
	}

	if (argc == 1) {
		help();
		exit(1);
	}

	err = trtl_init();
	if (err) {
		fprintf(stderr, "Cannot init Mock Turtle lib: %s\n",
			trtl_strerror(errno));
		exit(1);
	}

	/* Run a thread for each message queue */
	for (i = 0; i < th_idx; ++i) {
		err = pthread_create(&tid_msg[i], NULL,
				     message_thread, (void *)(&th[i]));
		if (err)
			fprintf(stderr,
				"Cannot create thread instance %s\n",
				strerror(errno));
	}

	/* Wait for the threads to finish */
	for (i = 0; i < th_idx; i++)
		pthread_join(tid_msg[i], NULL);

	exit(0);
}
