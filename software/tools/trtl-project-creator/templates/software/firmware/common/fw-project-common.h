/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 */

#ifndef __FW_{{short_name_capital}}_COMMON_H__
#define __FW_{{short_name_capital}}_COMMON_H__
#include <mockturtle-rt.h>
#include <{{short_name}}-common.h>

#endif
