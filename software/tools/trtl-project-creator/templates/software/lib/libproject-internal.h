/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 *
 * Define here the internal API
 */


#ifndef __LIB{{short_name_capital}}_INTERNAL__H__
#define __LIB{{short_name_capital}}_INTERNAL__H__

#include <stdlib.h>
#include <lib{{short_name}}.h>


/**
 * Description of a {{name}} device
 */
struct {{short_name}}_desc {
	struct trtl_dev *trtl; /**< Mock Turtle device associated */
	uint32_t dev_id; /**< device id */
};

#endif
