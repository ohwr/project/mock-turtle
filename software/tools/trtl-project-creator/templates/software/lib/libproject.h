/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 *
 * Define in this file the public API
 */


#ifndef __LIB{{short_name_capital}}_H__
#define __LIB{{short_name_capital}}_H__
/** @file lib{{short_name}}.h */

#include <{{short_name}}-common.h>

struct {{short_name}}_node;

enum {{short_name}}_error_list {
	__E{{short_name_capital}}_MIN_ERROR_NUMBER = 3200,
	/*
	 * Add new messages here.
	 * The first one must be equal to __E{{short_name_capital}}_MIN_ERROR_NUMBER
	 */
	__E{{short_name_capital}}_MAX_ERROR_NUMBER,
};

/**
 * @defgroup util Generic Functions
 * Generic funcions to handle {{name}} devices
 * @{
 */
extern const char *{{short_name}}_strerror(unsigned int error);
extern int {{short_name}}_init();
extern void {{short_name}}_exit();
extern struct {{short_name}}_node *{{short_name}}_open_by_id(uint32_t device_id);
extern void {{short_name}}_close(struct {{short_name}}_node *dev);
extern struct trtl_dev *{{short_name}}_get_trtl_dev(struct {{short_name}}_node *dev);
/**@}*/

#endif
