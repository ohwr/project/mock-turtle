/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: <year> <owner>
 *
 * FIXME choose your license and set the copyright
 *
 * Define the symbols which are shared between the user-space and
 * the firmware application
 */

#ifndef __{{short_name_capital}}_COMMON_H
#define __{{short_name_capital}}_COMMON_H
#include <mockturtle/mockturtle.h>

#endif
