..
  SPDX-License-Identifier: LGPL-2.1-or-later
  SPDX-FileCopyrightText: <year> <owner>

  FIXME choose your license and set the copyright

==========
Change Log
==========

[0.0.0] - {{date}}
==================
This start the project {{name}} from the Mock Turtle template.
It includes basic files for hdl and software development
