"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN (home.cern)
"""


#!/usr/bin/env python
import argparse
import PyUAL
import sys
import time
import socket
import select
import struct

gdb_port = 3000
flag_verbose = False
flag_keep = False
flag_cpu = 0
flag_term = True

# Mock-Turtle base address (for VME)

HMQ_IN_BASE  = 0x0000
HMQ_OUT_BASE = 0x8000
CSR_BASE     = 0xc000
TPU_BASE     = 0xd000
CROM_BASE    = 0xe000

CSR_RESET      = CSR_BASE + 0x000
CSR_INT        = CSR_BASE + 0x004
CSR_SMEM_OP    = CSR_BASE + 0x018
CSR_HMQ_SEL    = CSR_BASE + 0x01C

CSR_HMQI_STATUS_LO = CSR_BASE + 0x40
CSR_HMQI_STATUS_HI = CSR_BASE + 0x48
CSR_HMQO_STATUS_LO = CSR_BASE + 0x60
CSR_HMQO_STATUS_HI = CSR_BASE + 0x68

CSR_HMQI_INTEN_LO = CSR_BASE + 0x80
CSR_HMQI_INTEN_HI = CSR_BASE + 0x88
CSR_HMQO_INTEN_LO = CSR_BASE + 0x90
CSR_HMQO_INTEN_HI = CSR_BASE + 0x98

CSR_CORESEL    = CSR_BASE + 0xC0
CSR_UADDR      = CSR_BASE + 0xC4
CSR_UDATA      = CSR_BASE + 0xC8

CSR_UART_MSG    = CSR_BASE + 0x100
CSR_UART_POLL   = CSR_BASE + 0x104
CSR_UART_IMASK  = CSR_BASE + 0x108

CSR_DBG_STATUS     = CSR_BASE + 0x180
CSR_DBG_FORCE      = CSR_BASE + 0x184
CSR_DBG_INSN_READY = CSR_BASE + 0x188
CSR_DBG_CPU0_INSN  = CSR_BASE + 0x18C
CSR_DBG_CPU1_INSN  = CSR_BASE + 0x190
CSR_DBG_MBX0       = CSR_BASE + 0x194
CSR_DBG_MBX1       = CSR_BASE + 0x198

def read_corenbr(csr):
    return read_crom(csr, 6)

def cmd_status(csr):
    print ("Status:")
    print (" RESET:           0x{:08x}".format(csr.readl(CSR_RESET)))
    print (" INT:             0x{:08x}".format(csr.readl(CSR_INT)))
    print (" SMEM_OP:         0x{:08x}".format(csr.readl(CSR_SMEM_OP)))
    print (" HMQ_SEL:         0x{:08x}".format(csr.readl(CSR_HMQ_SEL)))
    value = [csr.readl(CSR_HMQI_STATUS_LO),
             csr.readl(CSR_HMQI_STATUS_HI)]
    print (" HMQI_STATUS:     0x{:08x} 0x{:08x}".format(*value))
    value = [csr.readl(CSR_HMQO_STATUS_HI),
             csr.readl(CSR_HMQO_STATUS_LO)]
    print (" HMQO_STATUS:     0x{:08x} 0x{:08x}".format(*value))
    value = [csr.readl(CSR_HMQI_INTEN_HI),
             csr.readl(CSR_HMQI_INTEN_LO)]
    print (" HMQI_INTEN:      0x{:08x} 0x{:08x}".format(*value))
    value = [csr.readl(CSR_HMQO_INTEN_HI),
             csr.readl(CSR_HMQO_INTEN_LO)]
    print (" HMQO_INTEN:      0x{:08x} 0x{:08x}".format(*value))
    print (" UADDR:           0x{:08x}".format(csr.readl(CSR_UADDR)))
    print (" CORESEL:         0x{:08x}".format(csr.readl(CSR_CORESEL)))
    nbr_cores = read_corenbr(csr)
    print (" UART_POLL:       0x{:08x}".format(csr.readl(CSR_UART_POLL)))
    print (" UART_IMASK:      0x{:08x}".format(csr.readl(CSR_UART_IMASK)))
    print (" DBG_STATUS:      0x{:08x}".format(csr.readl(CSR_DBG_STATUS)))
    print (" DBG_FORCE:       0x{:08x}".format(csr.readl(CSR_DBG_FORCE)))
    print (" DBG_INSN_READY:  0x{:08x}".format(csr.readl(CSR_DBG_INSN_READY)))

def disp_mq_slot(csr, base):
    print (" COMMAND: 0x{:08x}".format(csr.readl(base + 0x00)))
    status = csr.readl(base + 0x04)
    print (" STATUS:  0x{:08x} ->  OCC:{} EMPTY:{} FULL:{}".format(
               status,
               (status >> 8) & 0xff,
               (status >> 1) & 1,
               status & 1))

def cmd_hmq(csr):
    for c in range(read_corenbr(csr)):
        print("CPU #{}:".format(c))
        for i in range(read_crom(csr, 24+c)):
            csr.writel(CSR_HMQ_SEL, (c << 8 | i))
            print (" HMQ IN#{}:".format(i))
            disp_mq_slot(csr, HMQ_IN_BASE)
            print (" HMQ OUT#{}:".format(i))
            disp_mq_slot(csr, HMQ_OUT_BASE)


def cmd_whmq(csr, cpu=0, hmq=0):
    out_status = (csr.readl(CSR_HMQO_STATUS_HI) << 32) | csr.readl(CSR_HMQO_STATUS_LO)
    if ((out_status >> (cpu * 8)) & (1 << hmq)) == 0:
        print ("CPU#{} HMQ#{} is full".format(cpu, hmq))
        return

    print ("Write to CPU#{} HMQ#{}".format(cpu, hmq))
    base = HMQ_OUT_BASE
    csr.writel(CSR_HMQ_SEL, (cpu << 8 | hmq))
    # Claim
    csr.writel(base + 0, 1 << 24)
    # Header
    csr.writel(base + 0x1000, 2)
    # Data
    csr.writel(base + 0x2000, 0x0abcd000 + cpu)
    csr.writel(base + 0x2004, 0x00abcd00)
    csr.writel(base + 0x2008, 0x000abcd0)
    csr.writel(base + 0x200c, 0x0000abcd)
    # Ready
    csr.writel(base + 0, 1 << 26)

def cmd_rhmq(csr, cpu=0, hmq=0):
    in_status = (csr.readl(CSR_HMQI_STATUS_HI) << 32) | csr.readl(CSR_HMQI_STATUS_LO)
    if ((in_status >> (cpu * 8)) & (1 << hmq)) == 0:
        print ("CPU#{} HMQ#{} is empty".format(cpu, hmq))
        return

    print ("Write to CPU#{} HMQ#{}".format(cpu, hmq))
    base = HMQ_IN_BASE
    csr.writel(CSR_HMQ_SEL, i)
    for j in range(4):
        print (" H[{}]: 0x{:08x}".format(
            j, csr.readl(base + 0x1000 + 4*j)))
    for j in range(8):
        print (" D[{}]: 0x{:08x}".format(
            j, csr.readl(base + 0x2000 + 4*j)))
    # Discard
    csr.writel(base + 0, 1 << 27)


def read_crom(csr, off):
    return csr.readl(CROM_BASE + 4 * off)

def disp_mq(csr, offset):
    sizes = read_crom(csr, offset)
    ep_id = read_crom(csr, offset + 1)
    p2entries = (sizes >> 16) & 0xff
    p2width = (sizes >>  8) & 0xff
    p2header = (sizes >>  0) & 0xff
    return "entries:2^{} width:2^{} header:2^{} endpoint:{:08x}".format(
        p2entries, p2width, p2header, ep_id)


def cmd_crom(csr):
    print ("CROM:")
    print ("Signature:    {:08x}".format(read_crom(csr, 0)))
    print ("MT revision:  {:08x}".format(read_crom(csr, 1)))
    print ("Clock freq:   {}".format(read_crom(csr, 3)))
    print ("config:       {:08x}".format(read_crom(csr, 4)))
    print ("APP Id:       {:08x}".format(read_crom(csr, 5)))
    ncpus = read_crom(csr, 6)
    print ("CPU count:    {:08x}".format(ncpus))
    print ("Shared RAM:   {:08x}".format(read_crom(csr, 7)))
    for i in range(ncpus):
        print ("CPU#{}:".format(i))
        print ("  memsize:   {:08x}".format(read_crom(csr, 8+i)))
        nhmq = read_crom(csr, 16+i)
        print ("  hmq slots: {}".format(nhmq))
        nrmq = read_crom(csr, 24+i)
        print ("  rmq slots: {}".format(nrmq))
        for j in range(nhmq):
            print ("  hmq#{}: {}".format(
                j, disp_mq(csr, 128 + 16 * i + 2 * j)))
        for j in range(nrmq):
            print ("  rmq#{}: {}".format(
                j, disp_mq(csr, 256 + 16 * i + 2 * j)))


class CpuPort(object):
    def __init__(self, csr, cpu):
        self.csr = csr
        self.cpu = cpu

        m = {0 : CSR_DBG_MBX0, 1: CSR_DBG_MBX1 }
        self.mbx_reg = m.get(cpu, None)
        if self.mbx_reg is None:
            raise AssertionError

        m = {0 : CSR_DBG_CPU0_INSN, 1: CSR_DBG_CPU1_INSN }
        self.insn_reg = m.get(cpu, None)
        if self.insn_reg is None:
            raise AssertionError

    def readl(self, addr):
        return self.csr.readl(addr)

    def writel(self, addr, val):
        self.csr.writel(addr, val)

    def read_mbx(self):
        return self.readl(self.mbx_reg)

    def write_mbx(self, val):
        self.writel(self.mbx_reg, val)

    def exec_insn(self, insn):
        self.writel(self.insn_reg, insn)

    def exec_reg_to_mbx(self, reg):
        self.exec_insn(0x7d001073 + (reg << 15))

    def exec_mbx_to_reg(self, reg):
        self.exec_insn(0x7d002073 + (reg << 7))

    def exec_nop(self):
        self.exec_insn(0x00000013)

    def in_debug_mode(self):
        poll = self.readl(CSR_DBG_STATUS)
        return ((poll >> self.cpu) & 1) == 1

    def force_debug(self):
        if self.in_debug_mode():
            return
        self.writel(CSR_DBG_FORCE, 1 << self.cpu)
        while not self.in_debug_mode():
            pass
        self.writel(CSR_DBG_FORCE, 0)


def swap32(v):
    return struct.unpack("<I", struct.pack(">I", v))[0]

def cmd_mbx(csr):
    nbr_cores = read_corenbr(csr)
    for i in range(nbr_cores):
        port = CpuPort(csr, i)
        print (" MBX{}: 0x{:08x}".format(i, port.read_mbx()))

def cmd_stop(csr):
    port = CpuPort(csr, flag_cpu)
    if port.in_debug_mode():
        print ("CPU #{} already stopped".format(port.cpu))
    else:
        port.force_debug()

def cmd_start(csr):
    port = CpuPort(csr, flag_cpu)
    if port.in_debug_mode():
        port.exec_insn(0x00100073) # ebreak
    else:
        print ("CPU #{} already running".format(port.cpu))

def cmd_demo(csr):
    # Set force
    csr.writel(CSR_DBG_FORCE, 0xf)
    # Un-reset CPUs
    csr.writel(CSR_RESET, 0)
    # Disable force
    csr.writel(CSR_DBG_FORCE, 0)

    port = CpuPort(csr, flag_cpu)
    port.exec_insn(0x001005b7) # lui	a1,0x100
    port.exec_insn(0x01c5e593) # ori	a1,a1,28
    port.exec_insn(0x06800513) # li	a0,104
    port.exec_insn(0x00a58023) # sb	a0,0(a1)
    port.exec_reg_to_mbx(11)


def cmd_reset(csr):
    # Un-reset CPUs
    csr.writel(CSR_RESET, 0xff)
    csr.writel(CSR_DBG_FORCE, 0xff)
    time.sleep(0.01)
    csr.writel(CSR_RESET, 0x00)
    csr.writel(CSR_DBG_FORCE, 0x00)


def cmd_term(csr):
    cpu = 0
    csr.writel(CSR_CORESEL, cpu);
    nbr_cores = read_corenbr(csr)
    lines = [''] * nbr_cores
    timeout = 1
    try:
        while True:
            poll = csr.readl(CSR_UART_POLL)
            if poll != 0:
                prev_cpu = cpu
                while (poll & (1 << cpu)) == 0:
                    # Select a different cpu
                    cpu += 1
                    if cpu == nbr_cores:
                        cpu = 0
                if cpu != prev_cpu:
                    csr.writel(CSR_CORESEL, cpu);
                c = csr.readl(CSR_UART_MSG)
                lines[cpu] += chr(c & 0xff)
                if c == 10:
                    sys.stdout.write ('CPU#{}: '.format(cpu))
                    sys.stdout.write(lines[cpu])
                    sys.stdout.flush()
                    lines[cpu] = ''
                timeout = 1
            else:
                time.sleep(timeout / 1000.0)
                if timeout < 10:
                    timeout += 1
                elif timeout < 100:
                    timeout += 10
    except KeyboardInterrupt:
        sys.stdout.write("INT\n")
        pass
    except:
        raise


class GdbServer(object):
    def __init__(self, dap, port):
        self.dap = dap
        self.port = port
        self.conn = None
        self.packet_size = 2048
        self.term = (flag_term
                     and self.dap.csr.readl(CSR_CORESEL) == self.dap.cpu)
        if not self.term:
            print ("Terminal is disabled")

    def checksum(self, packet):
        chk = 0
        for b in packet:
            chk += ord(b)
        return chk & 0xff

    def in_debug_mode(self):
        return self.dap.in_debug_mode()

    def force_debug(self):
        self.dap.force_debug()

    def send(self, packet):
        pkt = "${}#{:02x}".format(packet, self.checksum(packet))
        if flag_verbose:
            print("> {}".format(packet))
        if sys.hexversion >= 0x3000000:
            pkt = pkt.encode('latin-1')
        self.conn.send(pkt)

    def read_reg(self, reg):
        assert reg >= 0 and reg < 32
        self.dap.exec_reg_to_mbx(reg)
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()
        return self.dap.read_mbx()

    def write_reg(self, reg, val):
        assert reg >= 0 and reg < 32
        self.dap.write_mbx(val)
        self.dap.exec_mbx_to_reg(reg)

    def read_pc_via_ra(self):
        # Read current pc value but destroy ra.
        self.dap.exec_insn(0x000000ef) # ra = pc + 4
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_reg_to_mbx(1)
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()
        return (self.dap.read_mbx() - 4) & 0xffffffff

    def write_pc_via_ra(self):
        self.dap.exec_insn(0x00008067) # ret
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()

    def advance_pc_4(self):
        self.dap.exec_insn(0x00000263) # beqz zero, +4
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()

    def handle_c(self, packet):
        if len(packet) > 1:
            addr = int(packet[1:], 16)
            self.write_pc(addr)
        self.dap.exec_insn(0x00100073) # ebreak
        # Wait until the CPU is in debug mode
        timeout = 1
        while True:
            if self.in_debug_mode():
                # Check twice due to possible race if the ebreak is not yet
                # executed
                if self.in_debug_mode():
                    break
            if self.term:
                poll = self.dap.csr.readl(CSR_UART_POLL)
                if poll & (1 << self.dap.cpu):
                    c = self.dap.csr.readl(CSR_UART_MSG)
                    sys.stdout.write(chr(c & 0xff))
                    sys.stdout.flush()
                    timeout = 1
            # FIXME: ^C handling is not reliable (can happen anywhere).
            stop = False
            try:
                (r, w, x) = select.select([self.conn], [], [], timeout / 1000.0)
            except KeyboardInterrupt:
                stop = True
            if stop or self.conn in r:
                self.force_debug()
                self.send('S02')
                return
            if timeout < 10:
                timeout += 1
            elif timeout < 100:
                timeout += 10
        self.send('S05')

    def handle_D(self, packet):
        # Detach
        self.dap.exec_insn(0x00100073) # ebreak
        self.send('OK')

    def handle_g(self, packet):
        # Serious work: read all registers.
        regs = [None] * 32
        pc = None
        res = ''
        for r in range(32):
            regs[r] = self.read_reg(r)
            res += "{:08x}".format(swap32(regs[r]))
        # Read PC
        pc = self.read_pc_via_ra()
        res += "{:08x}".format(swap32(pc))
        self.write_reg(1, regs[1])
        self.send(res)

    def handle_G(self, packet):
        if len(packet) != 1 + 33 * 8:
            self.send('')
            return
        regs = [None] * 33
        for r in range(33):
            regs[r] = swap32(int(packet[r*8 + 1: r*8 + 9], 16))
        self.write_reg(1, regs[32])
        self.write_pc_via_ra()
        for r in range(32):
            self.write_reg(r, regs[r])
        self.send('OK')

    def handle_H(self, packet):
        if packet == 'Hg0':
            self.send('OK')
        else:
            self.send('')

    def handle_k(self, packet):
        # kill
        pass

    def handle_M(self, packet):
        # Write data to memory.
        comma = packet.find(',')
        colon = packet.find(':')
        if comma == -1 or colon == -1 or colon < comma:
            self.send('')
            return
        addr = int(packet[1:comma], 16)
        nbytes = int(packet[comma + 1:colon], 16)
        data = packet[colon + 1:]
        if len(data) != nbytes * 2:
            self.send('E01')
            return
        # Save a0, a1
        a0 = self.read_reg(10)
        a1 = self.read_reg(11)
        self.write_reg(10, addr)
        if (addr % 4) == 0:
            while nbytes >= 4:
                w = swap32(int(data[:8], 16))
                data = data[8:]
                nbytes -= 4
                self.write_reg(11, w)
                self.dap.exec_insn(0x00b52023) # sw a1,0(a0)
                self.dap.exec_insn(0x00450513) # addi a0, a0, 4
        while nbytes > 0:
            b = int(data[:2], 16)
            data = data[2:]
            nbytes -= 1
            self.write_reg(11, b)
            self.dap.exec_insn(0x00b50023) # sb a1,0(a0)
            self.dap.exec_insn(0x00150513) # addi a0, a0, 1
        self.write_reg(10, a0)
        self.write_reg(11, a1)
        self.send('OK')

    def handle_m(self, packet):
        # Read data from memory.
        comma = packet.find(',')
        if comma == -1:
            self.send('')
            return
        addr = int(packet[1:comma], 16)
        nbytes = int(packet[comma + 1:], 16)
        res = ''
        # Save a0, a1
        a0 = self.read_reg(10)
        a1 = self.read_reg(11)
        self.write_reg(10, addr)
        while nbytes > 0:
            self.dap.exec_insn(0x00054583) # lbu a1,0(a0)
            self.dap.exec_insn(0x00150513) # addi a0, a0, 1
            b = self.read_reg(11)
            res += '{:02x}'.format(b)
            nbytes -= 1
        self.write_reg(10, a0)
        self.write_reg(11, a1)
        self.send(res)

    def handle_p(self, packet):
        # Read a specific register
        regnum = int(packet[1:], 16)
        if regnum == (65 + 0x301):
            # MISA CSR
            self.send('{:08x}'.format((1 << 30) | (1 << (ord('I') - 65))))
        else:
            self.send('E01')

    def handle_P(self, packet):
        # Write a specific register
        self.send('')

    def handle_q_supported(self, packet):
        self.send('PacketSize={:x}'.format(self.packet_size))

    def handle_q(self, packet):
        if packet.startswith('qSupported:'):
            self.handle_q_supported(packet)
        else:
            self.send('')

    def handle_s(self, packet):
        # Single step
        ra = self.read_reg(1)
        if len(packet) > 1:
            pc = int(packet[1:], 16)
            self.write_pc(pc)
        else:
            pc = self.read_pc_via_ra()
        # Read insn
        self.write_reg(1, pc)
        self.dap.exec_insn(0x0000a083) # lw ra,0(ra)
        self.dap.exec_nop()
        self.dap.exec_nop()
        self.dap.exec_nop()
        insn = self.read_reg(1)
        self.write_reg(1, ra)
        if flag_verbose:
            print ("execute: {:08x} at pc={:08x}".format(insn, pc))
        self.dap.exec_insn(insn)
        self.dap.exec_nop()
        self.dap.exec_nop()
        if (insn & 0x77) == 0x67:
            # JAL/JLR: pc is always updated
            pass
        elif (insn & 0x7f) == 0x63:
            # A branch insn
            ra = self.read_reg(1)
            npc = self.read_pc_via_ra()
            self.write_reg(1, ra)
            if npc == pc:
                self.advance_pc_4()
        else:
            self.advance_pc_4()
        self.send('S05')

    def handle_vCont(self, packet):
        if packet == '?':
            self.send('vCont;c;s')
        else:
            self.send('')

    def handle_v(self, packet):
        if packet.startswith('vMustReplyEmpty:'):
            self.send('')
        elif packet.startswith('vCont'):
            self.handle_vCont(packet[5:])
        else:
            self.send('')

    def handle_X(self, packet):
        # Write binary data to memory.
        self.send('')

    def handle_qm(self, packet):
        self.send('S05')

    def gdb_command(self, packet):
        if flag_verbose:
            print("< {}".format(packet))
        cmd = packet[0]
        m = getattr(self, 'handle_' + cmd, None)
        if m:
            m(packet)
        elif cmd == '?':
            self.handle_qm(packet)
        else:
            if flag_verbose:
                print ('Unknown command: {}'.format(cmd))
            self.send('');

    def run_server(self, sock):
        sys.stdout.write("Waiting for connection on port {}\n".format(gdb_port))
        self.conn, client_addr = sock.accept()
        sys.stdout.write("connection from {}\n".format(client_addr))
        self.force_debug()
        while True:
            # Wait for a packet
            packet = ""
            data = self.conn.recv(self.packet_size)
            # Exit in case of disconnection
            if len(data) == 0:
                break
            if isinstance(data, bytes):
                data = data.decode('latin-1')
            for c in data:
                if len(packet) == 0:
                    if c == '$':
                        packet = c
                    elif c == '+':
                        # Ignore ack to answers.
                        pass
                    elif c == '\x03':
                        print ("Got a break!")
                    else:
                        if flag_verbose:
                            print ("Ignore character {:x}".format(ord(c)))
                else:
                    packet += c
                    if len(packet) > 3 and packet[-3] == '#':
                        chk = self.checksum(packet[1:-3])
                        pchk = int(packet[-2:], 16)
                        if chk != pchk:
                            print ('chkerr: {:x} vs {:x}'.format(chk, pchk))
                            # print 'pkt: {}'.format(packet[1:-3])
                            raise Exception('invalid checksum')
                        self.conn.send(b'+')
                        packet = packet[1:-3]
                        if packet == '':
                            pass
                        else:
                            self.gdb_command(packet)
                            packet = ''

    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('', gdb_port))
        sock.listen(1)
        self.run_server(sock)
        while flag_keep:
            self.run_server(sock)


def cmd_gdb(csr):
    rst = csr.readl(CSR_RESET)
    cpu_bit = 1 << flag_cpu
    if (rst & cpu_bit) != 0:
        sys.stdout.write("Put cpu #{} out of reset\n".format(flag_cpu))
        # Set force
        csr.writel(CSR_DBG_FORCE, cpu_bit)
        # Clear reset
        csr.writel(CSR_RESET, rst & ~cpu_bit)
        # Disable force
        csr.writel(CSR_DBG_FORCE, 0)

    csr.writel(CSR_CORESEL, flag_cpu);

    dap = CpuPort(csr, flag_cpu)
    gdb = GdbServer(dap, gdb_port)
    gdb.run()


def auto_int(val):
    try:
        return int(val)
    except ValueError:
        return int(val, 16)

def main():
    global flag_verbose, flag_keep, flag_cpu, flag_term, gdb_port

    cmds = {'status': cmd_status,
            'demo':   cmd_demo,
            'mbx':    cmd_mbx,
            'hmq':    cmd_hmq,
            'whmq':   cmd_whmq,
            'rhmq':   cmd_rhmq,
            'crom':   cmd_crom,
            'term':   cmd_term,
            'reset':  cmd_reset,
            'stop':   cmd_stop,
            'start':  cmd_start,
            'gdb':    cmd_gdb}

    parser = argparse.ArgumentParser(description='Low-level debugging interface without driver')
    parser.add_argument("-b", "--bus", choices=["pci", "vme"],
                        required=True, dest='bus', help='to select the bus on which there is the Mock Turtle core to access.')
    parser.add_argument("-o", "--offset", type=auto_int, required=True, dest='offset', default=0,
                        help='The memory offset to apply to be able to point to the Mock Turtle core. This is strictly dependent on the bus in use.')
    parser.add_argument("-v", "--verbose", action='store_true',
                        default=False, dest='verbose', help='')
    parser.add_argument("-k", "--keep", action='store_true',
                        default=False, dest='keep', help='')
    parser.add_argument("-c", "--cpu", type=int, default=0, dest='cpu',
                        help='Select the CPU to use')
    parser.add_argument("-t", "--no-term", action='store_false',
                        default=True, dest='term',
                        help='It disable the console during GDB session')
    parser.add_argument("-d", "--device", default=None, dest='device',
                        help='PCI device id (Bus << 16) | (Dev << 8) | Func')
    parser.add_argument("cmd", default=False, nargs='+', choices=cmds.keys(),
                        help='The command to execute')
    parser.add_argument("-p", "--gdb-port", type=auto_int, required=False, dest='gdb_port', default=3000,
                        help='Define the listening port for GDB sessions.')

    args = parser.parse_args()
    flag_verbose = args.verbose
    flag_keep = args.keep
    flag_cpu = args.cpu
    flag_term = args.term
    gdb_port = args.gdb_port

    if args.bus == "pci":
        if args.device is None:
            raise Exception("--device required for PCI")
        mt_desc = PyUAL.PyUALPCI(int(args.device, 16), 4, 0, 0x10000,
                                 args.offset, 0)
    elif args.bus == "vme":
        mt_desc = PyUAL.PyUALVME(4, 0x39, 0x10000, args.offset, 1)
    else:
        raise Exception("Invalid bus")

    try:
        csr = PyUAL.PyUAL(args.bus, mt_desc)
        for cmd in args.cmd:
            cmds[cmd](csr)
    except Exception as e:
        print("Failed! {}".format(str(e)))

if __name__ == "__main__":
    main()
