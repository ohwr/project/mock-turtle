/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#include <inttypes.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>


/**
 * Print the help message
 * @param[in] name the program name
 */
static void help(char *name)
{
	fprintf(stderr, "%s [options]\n", name);
	fprintf(stderr, "  -h             print this help\n");
	fprintf(stderr, "  -D 0x<dev_id>  device id to ping\n");
	fprintf(stderr, "  -c <num>       CPU index\n");
	fprintf(stderr, "  -q <num>       HMQ index\n");
	fprintf(stderr, "  -n <num>       number of ping to perform\n");
	fprintf(stderr, "  -p <num>       ping period in micro-seconds\n");
	fprintf(stderr, "  -v             show device version\n");
}


/**
 * Print the given Mock Turtle firmware version
 * @param[in] v version to print
 */
static void trtl_ping_version_print(struct trtl_fw_version *v,
									const char *name)
{
	fprintf(stdout, "\tFirmware Version:\n");
	fprintf(stdout, "\t  application_id: 0x%08X (%s)\n",
		v->rt_id, name);
	fprintf(stdout, "\tfirmware_version: 0x%08X\n",
		v->rt_version);
	fprintf(stdout, "\t     git_version: 0x%08X\n",
		v->git_version);
}


int main(int argc, char *argv[])
{
	struct trtl_dev *trtl;
	uint32_t dev_id = 0, n = 1;
	struct trtl_fw_version version;
	uint64_t period = 0;
	int err, f_version = 0, idx_cpu = 0, idx_hmq = 0;
	char c;

	while ((c = getopt (argc, argv, "hD:n:p:tvc:q:")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			help(argv[0]);
			exit(1);
			break;
		case 'D':
			sscanf(optarg, "0x%x", &dev_id);
			break;
		case 'n':
			sscanf(optarg, "%u", &n);
			break;
		case 'p':
			sscanf(optarg, "%"SCNu64, &period);
			break;
		case 'v':
			f_version = 1;
			break;
		case 'c':
			sscanf(optarg, "%d", &idx_cpu);
			break;
		case 'q':
			sscanf(optarg, "%d", &idx_hmq);
			break;
		}
	}


	atexit(trtl_exit);
	err = trtl_init();
	if (err) {
		fprintf(stderr,
			"Cannot init Mock Turtle lib: %s\n",
			trtl_strerror(errno));
		exit(1);
	}

	trtl = trtl_open_by_id(dev_id);
	if (!trtl) {
		fprintf(stderr,
			"Cannot open Mock Turtle device 0x%x: %s\n",
			dev_id, trtl_strerror(errno));
		exit(1);
	}

	while (n--) {
		fprintf(stdout, "\n");
		usleep(period);

		/* Try to ping */
		err = trtl_fw_ping(trtl, idx_cpu, idx_hmq);
		if (err) {
			fprintf(stderr,
				"Ping failed on Device 0x%x CPU %d HMQ %d: %s\n",
				dev_id, idx_cpu, idx_hmq, trtl_strerror(errno));
			continue;
		}

		fprintf(stdout,
			"Firmware on device: 0x%x, CPU: %d, is running!\n",
			dev_id, idx_cpu);

		/* Get the version */
		if (f_version) {
			char name[TRTL_NAME_LEN];

			trtl_fw_name(trtl, idx_cpu, idx_hmq, name, sizeof(name));
			trtl_fw_version(trtl, idx_cpu, idx_hmq, &version);
			trtl_ping_version_print(&version, name);
		}

	}

	trtl_close(trtl);
	exit(0);
}
