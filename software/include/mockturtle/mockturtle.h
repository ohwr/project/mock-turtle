/*
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */


#ifndef __TRTL_USER_H__
#define  __TRTL_USER_H__
/** @file mockturtle.h */

#ifndef __KERNEL__
#include <string.h>
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(_a) (sizeof(_a) / sizeof(_a[0]))
#endif

/**
 * Maximum string size for names
 */
#define TRTL_NAME_LEN 16

/**
 * Maximum number of supported message ID
 */
#define __TRTL_MSG_ID_MAX 128

/**
 * Maximum number of supported Mock Turtle message ID
 */
#define __TRTL_MSG_ID_MAX_TRTL 32

/**
 * Maximum number of supported user message ID
 */
#define __TRTL_MSG_ID_MAX_USER (__TRTL_MSG_ID_MAX - __TRTL_MSG_ID_MAX_TRTL)


#define __TRTL_CPU_NOTIFY_APPLICATION_MAX 64

/**
 * @enum trtl_cpu_notification
 * List all Mock Turtle notification's code.
 */
enum trtl_cpu_notification {
	TRTL_CPU_NOTIFY_APPLICATION = __TRTL_CPU_NOTIFY_APPLICATION_MAX, /**< anonymous application
									    notification (user) */
	TRTL_CPU_NOTIFY_INIT, /**< the firmware is executing init phase */
	TRTL_CPU_NOTIFY_MAIN, /**< the firmware is executing main phase */
	TRTL_CPU_NOTIFY_EXIT, /**< the firmware is executing exit finishing */
	TRTL_CPU_NOTIFY_ERR, /**< general error, firmware is not running
				anymore */
	__TRTL_CPU_NOTIFY_MAX,
};


/**
 * Enumerate the Mock Turtle message ID.
 * These IDs starts after the user defined ones
 */
enum trtl_msg_id {
	TRTL_MSG_ID_UNDEFINED = __TRTL_MSG_ID_MAX_USER,
	TRTL_MSG_ID_ERROR,
	TRTL_MSG_ID_PING,
	TRTL_MSG_ID_VER,
	TRTL_MSG_ID_DBG,
	TRTL_MSG_ID_VAR_GET,
	TRTL_MSG_ID_BUF_GET,
	TRTL_MSG_ID_VAR_SET,
	TRTL_MSG_ID_BUF_SET,
	TRTL_MSG_ID_NAME,
};


/**
 * Synchronous. When set, the sync_id is valid and the receiver
 * must answer with a message with the same sync_id
 */
#define TRTL_HMQ_HEADER_FLAG_SYNC (1 << 0)

/**
 * Acknowledgment. When set, the sync_id is valid and it is the
 * same as the sync message previously sent
 */
#define TRTL_HMQ_HEADER_FLAG_ACK  (1 << 1)


/**
 * Remote Procedure Call. when set the message ID will be used
 * to execute a special function by the receiver
 */
#define TRTL_HMQ_HEADER_FLAG_RPC  (1 << 2)


/**
 * HMQ header descriptor. It helps the various software layers to process
 * messages.
 */
struct trtl_hmq_header {
	/* word 0 */
	uint16_t rt_app_id; /**< firmware application unique identifier.
			       Used to validate a message against the firmware
			       that receives it*/
	uint8_t flags; /**< flags */
	uint8_t msg_id; /**< It uniquely identify the message type. The
			   first __TRTL_MSG_ID_MAX_USER are free use */
	/* word 1 */
	uint16_t len; /**< message-length in 32bit words */
	uint16_t sync_id;/**< synchronous identifier (automatically set by the library) */
	/* word 2 */
	uint32_t seq; /**< sequence number (to be defined and used by the application if necessary) */
};


#define RT_VERSION_MAJ(_v) ((_v >> 16) & 0xFFFF)
#define RT_VERSION_MIN(_v) (_v & 0xFFFF)
#define RT_VERSION(_a, _b) (((_a & 0xFFFF) << 16) | (_b & 0xFFFF))

/**
 * Describe the version running on the embedded CPU
 */
struct trtl_fw_version {
	uint16_t rt_id; /**< RT application identifier */
	uint32_t rt_version; /**< RT application version*/
	uint32_t git_version; /**< git commit SHA1 of the compilation time */
};


/**
 * Maximum number of CPU core in a bitstream
 */
#define TRTL_MAX_CPU 8


/**
 * Maximum number of (H|R)MQ per CPU core
 */
#define TRTL_MAX_MQ_CHAN 8


/**
 * Maximum number of Mock Turtle components on a single computer
 */
#define TRTL_MAX_CARRIER 20


/**
 * TLV used to embed buffers within a message
 */
struct trtl_tlv {
	uint32_t type; /**< buffer type */
	size_t size; /**< buffer size in byte */
	void *buf; /**< pointer to the buffer */
};


/**
 * Maximum number of notifications ID in the driver buffer
 */
#define TRTL_CPU_NOTIFY_HISTORY_N 128

/**
 * This value represents the maximum payload size (in words) of a single
 * message. The true maximum size is set in the internal ROM, but to simplify
 * the code here we set this to the maximum addressable payload: 4096 bytes
 */
#define TRTL_MAX_PAYLOAD_SIZE (4096 / 4)


/**
 * @enum trtl_smem_modifier
 * Shared memory operation modifier. This is a list of operation modifiers
 * that you can use to access the shared memory.
 */
enum trtl_smem_modifier {
	TRTL_SMEM_TYPE_BASE = 0, /**< no operation */
	TRTL_SMEM_TYPE_ADD, /**< atomic addition */
	TRTL_SMEM_TYPE_SUB, /**< atomic subtraction */
	TRTL_SMEM_TYPE_SET, /**< atomic bit set */
	TRTL_SMEM_TYPE_CLR, /**< atomic bit clear */
	TRTL_SMEM_TYPE_FLP, /**< atomic bit flip */
	TRTL_SMEM_TYPE_TST_SET, /**< atomic test and set */
};

/**
 * Descriptor of the IO operation on Shared Memory
 */
struct trtl_smem_io {
	uint32_t addr; /**< address to access */
	uint32_t value; /**< value to write. After ioctl it will be overwritte
			   with the new value in the shared memory */
	int is_input; /**< flag to determinte data direction */
	enum trtl_smem_modifier mod; /**< the kind of operation to do */
};

/**
 * @enum trtl_ioctl_commands
 * Available ioctl() messages
 */
enum trtl_ioctl_commands {
	TRTL_SMEM_IO, /**< access to shared memory */
	TRTL_MSG_SYNC_ABORT, /**< abort sync message*/
	TRTL_HMQ_SYNC_SET, /**< Mark HMQ user context as synchronous */
};


#define TRTL_IOCTL_MAGIC 's'
#define TRTL_IOCTL_SMEM_IO _IOWR(TRTL_IOCTL_MAGIC, TRTL_SMEM_IO, \
				    struct trtl_smem_io)

/**
 * The IOCTL command to set HMQ user context to SYNC
 */
#define TRTL_IOCTL_HMQ_SYNC_SET _IOW(TRTL_IOCTL_MAGIC,        \
				     TRTL_HMQ_SYNC_SET,     \
				     unsigned int)

/**
 * The IOCTL command to abort a sync message. Usefull when the answer is
 * not coming
 */
#define TRTL_IOCTL_MSG_SYNC_ABORT _IOW(TRTL_IOCTL_MAGIC,        \
				       TRTL_MSG_SYNC_ABORT,     \
				       unsigned int)

/**
 * Messages descriptor for host
 */
struct trtl_msg {
	/* library and kernel */
	struct trtl_hmq_header hdr;
	uint32_t data[TRTL_MAX_PAYLOAD_SIZE]; /**< payload very maximum size */
};


#define TRTL_CONFIG_ROM_MQ_ENTRIES_SHIFT 16
#define TRTL_CONFIG_ROM_MQ_ENTRIES_MASK 0x00FF0000
#define TRTL_CONFIG_ROM_MQ_PAYLOAD_SHIFT 8
#define TRTL_CONFIG_ROM_MQ_PAYLOAD_MASK 0x0000FF00
#define TRTL_CONFIG_ROM_MQ_HEADER_SHIFT 0
#define TRTL_CONFIG_ROM_MQ_HEADER_MASK 0x000000FF

/**
 * Extract the number of message queue entries from the sizes value
 * in the configuration ROM
 */
#define TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(_size) (1 << ((_size & TRTL_CONFIG_ROM_MQ_ENTRIES_MASK) >> TRTL_CONFIG_ROM_MQ_ENTRIES_SHIFT))

/**
 * Extract the maximum payload size (32bit words) from the sizes value
 * in the configuration ROM
 */
#define TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(_size) (1 << ((_size & TRTL_CONFIG_ROM_MQ_PAYLOAD_MASK) >> TRTL_CONFIG_ROM_MQ_PAYLOAD_SHIFT))

/**
 * Extract the maximum header size (32bit words) from the sizes value in
 * in the configuration ROM
 */
#define TRTL_CONFIG_ROM_MQ_SIZE_HEADER(_size) (1 << ((_size & TRTL_CONFIG_ROM_MQ_HEADER_MASK) >> TRTL_CONFIG_ROM_MQ_HEADER_SHIFT))

/**
 * The synthesis configuration for a single MQ.
 * Note that there is always an input and output channel for each declaration.
 */
struct trtl_config_rom_mq {
	uint32_t sizes; /**< it contains the MQ sizes. Use the MACROs
			   to extract them */
	uint32_t endpoint_id;
};


/**
 * This signature must be present on all the configuration rom
 */
#define TRTL_CONFIG_ROM_SIGNATURE 0x5452544c


/**
 * The synthesis configuration ROM descriptor shows useful configuration
 * options during synthesis.
 */
struct trtl_config_rom {
	uint32_t signature; /**< we expect to see a known value */
	uint32_t version; /**< Mock Turtle version */
	uint32_t pad1[1];
	uint32_t clock_freq; /**< clock frequency in Hz */
	uint32_t flags; /**< miscellaneous flags */
	uint32_t app_id; /**< Application ID */
	uint32_t n_cpu; /**< number of CPU */
	uint32_t smem_size; /**< shared memory size */
	uint32_t mem_size[TRTL_MAX_CPU]; /**< memory size for each CPU */
	uint32_t n_hmq[TRTL_MAX_CPU]; /**< number of HMQ for each CPU */
	uint32_t n_rmq[TRTL_MAX_CPU]; /**< number of RMQ for each CPU */
	uint32_t pad2[96];
	struct trtl_config_rom_mq hmq[TRTL_MAX_CPU][TRTL_MAX_MQ_CHAN]; /**< HMQ config */
	struct trtl_config_rom_mq rmq[TRTL_MAX_CPU][TRTL_MAX_MQ_CHAN]; /**< RMQ config */
};


#endif
