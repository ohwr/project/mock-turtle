/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 *
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */

#ifndef __LIB_TRTL_H__
#define __LIB_TRTL_H__
/** @file libmockturtle.h */

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <endian.h>
#include <stdint.h>
#include <stdio.h>
#include <poll.h>
#include "mockturtle/mockturtle.h"

extern const unsigned int trtl_default_timeout_ms;

/**
 * @struct trtl_dev
 * An obfuscated structure type. This is done because the user should not
 * modify it but just use it as a token to access the library API.
 */
struct trtl_dev;


/**
 * Maximum string len for sysfs paths
 */
#define TRTL_SYSFS_PATH_LEN 128


/**
 * This structure mimic the pollfd structure for the Mock Turtle needs
 */
struct polltrtl {
	struct trtl_dev *trtl; /**< device token */
	unsigned int idx_cpu; /**< CPU index */
	unsigned int idx_hmq; /**< HMQ index */
	short events; /**< like in pollfd poll(2) */
	short revents; /**< like in pollfd poll(2) */
};


/**
 * Error codes for Mock-Turtle applications
 */
enum trtl_error_number {
	ETRTL_INVAL_PARSE = 83630, /**< cannot parse data from sysfs */
	ETRTL_INVAL_SLOT, /**< invalid slot */
	ETRTL_NO_IMPLEMENTATION, /**< a prototype is not implemented */
	ETRTL_HMQ_CLOSE, /**< The HMQ is closed */
	ETRTL_INVALID_MESSAGE, /**< Invalid message */
	ETRTL_HMQ_READ, /**< Error while reading messages */
	ETRTL_MSG_SYNC_FAILED_SEND, /**< Send sync message failure */
	ETRTL_MSG_SYNC_FAILED_RECV, /**< Receive sync message failure */
	ETRTL_MSG_SYNC_FAILED_RECV_TIMEOUT, /**< Receive sync message failure:
					       timeout */
	ETRTL_MSG_SYNC_FAILED_RECV_POLLERR, /**< Receive sync message failure */

	ETRTL_MSG_SYNC_FAILED_INVAL, /**< Receive sync message failure:
					  invalid */
	ETRTL_DEVICE_NAME_NOID, /**< The device name does not contain a valid ID */
	__ETRTL_MAX,
};


/**
 * @file libmockturtle.c
 */
/**
 * @defgroup dev Device
 * Set of functions to perform basic openartion on the device
 * @{
 */
extern int trtl_init();
extern void trtl_exit();
extern uint32_t trtl_count();
extern char **trtl_list();
extern void trtl_list_free(char **list);

extern struct trtl_dev *trtl_open(const char *device);
extern struct trtl_dev *trtl_open_by_id(uint32_t device_id);
extern struct trtl_dev *trtl_open_by_lun(unsigned int lun) __attribute__((deprecated));
extern void trtl_close(struct trtl_dev *trtl);
extern char *trtl_name_get(struct trtl_dev *trtl);
extern uint32_t trtl_id_get(struct trtl_dev *trtl);

const struct trtl_config_rom *trtl_config_get(struct trtl_dev *trtl);
/**@}*/

/**
 * @defgroup util Utilities
 * Utilities collection
 * @{
 */
extern void trtl_print_header(struct trtl_msg *msg);
extern void trtl_print_payload(struct trtl_msg *msg);
extern void trtl_print_message(struct trtl_msg *msg);
extern const char *trtl_strerror(int err);
/**@}*/

/**
 * @defgroup cpu CPU
 * Set of function that allow you to manage the FPGA cores
 * @{
 */
extern int trtl_cpu_load_application_raw(struct trtl_dev *trtl,
					 unsigned int index,
					 const void *code, size_t length,
					 unsigned int offset);
extern int trtl_cpu_load_application_file(struct trtl_dev *trtl,
					  unsigned int index,
					  const char *path);
extern int trtl_cpu_dump_application_raw(struct trtl_dev *trtl,
					 unsigned int index,
					 void *code, size_t length,
					 unsigned int offset);
extern int trtl_cpu_dump_application_file(struct trtl_dev *trtl,
					  unsigned int index,
					  const char *path);

extern int trtl_cpu_reset_set(struct trtl_dev *trtl, uint32_t mask);
extern int trtl_cpu_reset_get(struct trtl_dev *trtl, uint32_t *mask);
extern int trtl_cpu_enable(struct trtl_dev *trtl, unsigned int index);
extern int trtl_cpu_disable(struct trtl_dev *trtl, unsigned int index);
extern int trtl_cpu_is_enable(struct trtl_dev *trtl, unsigned int index,
			      unsigned int *enable);

/**
 * Diable and re-enable the CPU to perform a restart
 */
static inline int trtl_cpu_restart(struct trtl_dev *trtl, unsigned int index)
{
	int err;

	err = trtl_cpu_disable(trtl, index);
	if (err)
		return -1;
	return trtl_cpu_enable(trtl, index);
}
/**@}*/

/**
 * @defgroup hmq Host Message Queue
 * Functions to manage the HMQ slots: configuration and transmission
 * @{
 */

extern int trtl_hmq_fd(struct trtl_dev *trtl,
		       unsigned int idx_cpu,
		       unsigned int idx_hmq);
extern int trtl_hmq_fd_sync(struct trtl_dev *trtl,
			    unsigned int idx_cpu,
			    unsigned int idx_hmq);
extern int trtl_msg_async_send(struct trtl_dev *trtl,
			       unsigned int idx_cpu,
			       unsigned int idx_hmq,
			       struct trtl_msg *msg,
			       unsigned int n);
extern int trtl_msg_async_recv(struct trtl_dev *trtl,
			       unsigned int idx_cpu,
			       unsigned int idx_hmq,
			       struct trtl_msg *msg,
			       unsigned int n);
extern int trtl_msg_poll(struct polltrtl *trtlp,
			 unsigned int npolls, int timeout);
extern int trtl_msg_sync(struct trtl_dev *trtl,
			 unsigned int idx_cpu,
			 unsigned int idx_hmq,
			 struct trtl_msg *msg_s,
			 struct trtl_msg *msg_r,
			 int timeout);
extern int trtl_hmq_flush(struct trtl_dev *trtl,
			  unsigned int idx_cpu,
			  unsigned int idx_hmq);
/**@}*/

/**
 * @defgroup smem Shared Memory
 * Functions to access the shared memory from the host
 * @{
 */
extern int trtl_smem_read(struct trtl_dev *trtl, uint32_t addr, uint32_t *data,
			  size_t count, enum trtl_smem_modifier mod);
extern int trtl_smem_write(struct trtl_dev *trtl, uint32_t addr, uint32_t *data,
			   size_t count, enum trtl_smem_modifier mod);
/**@}*/


/**
 * @defgroup rtmsg Real Time service messages
 * Message builders for RT service messages
 * @{
 */
extern int trtl_fw_name(struct trtl_dev *trtl,
						unsigned int idx_cpu,
						unsigned int idx_hmq,
						char *name, size_t len);
extern int trtl_fw_version(struct trtl_dev *trtl,
			   unsigned int idx_cpu,
			   unsigned int idx_hmq,
			   struct trtl_fw_version *version);
extern int trtl_fw_ping_timeout(struct trtl_dev *trtl,
                                unsigned int idx_cpu,
                                unsigned int idx_hmq,
                                int timeout_ms);
extern int trtl_fw_ping(struct trtl_dev *trtl,
			unsigned int idx_cpu,
			unsigned int idx_hmq);
extern int trtl_fw_variable_set(struct trtl_dev *trtl,
				unsigned int idx_cpu,
				unsigned int idx_hmq,
				uint32_t *variables, unsigned int n_variables);
extern int trtl_fw_variable_get(struct trtl_dev *trtl,
				unsigned int idx_cpu,
				unsigned int idx_hmq,
				uint32_t *variables, unsigned int n_variables);
extern int trtl_fw_buffer_set(struct trtl_dev *trtl,
			      unsigned int idx_cpu,
			      unsigned int idx_hmq,
			      struct trtl_tlv *tlv,
			      unsigned int n_tlv);
extern int trtl_fw_buffer_get(struct trtl_dev *trtl,
			      unsigned int idx_cpu,
			      unsigned int idx_hmq,
			      struct trtl_tlv *tlv,
			      unsigned int n_tlv);
/**@}*/
#ifdef __cplusplus
};
#endif

#endif
