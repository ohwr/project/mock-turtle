# SPDX-License-Identifier: LGPL-2.1-or-later
#
# SPDX-FileCopyrightText: 2019 CERN

src := $(M)/

-include $(src)/Makefile.specific
-include $(src)/.config

include $(src)/TBuild

# Validation
ifndef OUTPUT
  $(error Missing variable OUTPUT)
else
  build_output=$(src)/$(OUTPUT)
endif

ifndef OBJS
  $(error Missing variable OBJS)
endif

# and don't touch the rest unless you know what you're doing.
DESTDIR ?=
prefix ?= /lib
firmwaredir ?= $(prefix)/firmware
PATH_COMMON_RT ?= .
PATH_COMMON_H ?= ../include
TRTL ?= ../..
TRTL_SW ?= $(TRTL)/software
TRTL_FW ?= $(TRTL_SW)/firmware
TRTL_HDL ?= $(TRTL)/hdl/rtl

GEN_CORES ?= $(TRTL)/hdl/ip_cores/general-cores

RT_GIT_VERSION = 0x$(shell git rev-parse --short=8 HEAD || echo "0") # empty if git is not there
# header file generated from the .config
AUTOCONF = $(src)/$(BUILDDIR)/include/generated/autoconf.h

CROSS_COMPILE_TARGET ?= riscv32-elf-
CFLAGS  ?= -mabi=ilp32 -march=rv32im
ASFLAGS ?= -mabi=ilp32 -march=rv32im_zicsr
CXXFLAGS += -fno-rtti -fno-exceptions
LDFLAGS += -lgcc -lc -Wl,--gc-sections
TRTL_LD_DIR ?= $(src)
LDFLAGS += -L$(TRTL_FW)/urv/
LDFLAGS += -L$(TRTL_LD_DIR)

# provide search patch for sources
vpath %.c $(TRTL_FW)
vpath %.cpp $(TRTL_FW)
vpath %.S $(TRTL_FW)
vpath %.c $(src)
vpath %.cpp $(src)
vpath %.S $(src)

BUILDDIR := $(src)/build

# auto dependency generation from
# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
DEPDIR := .d
DEPFLAGS = -MT $@ -MMD -MP -MF $(@D)/$(*F).Td
POSTCOMPILE = mv -f $(@D)/$(*F).Td $(@D)/$(DEPDIR)/$(*F).d && touch $@

CC =		$(CROSS_COMPILE_TARGET)gcc
CXX =		$(CROSS_COMPILE_TARGET)g++
LD =		$(CROSS_COMPILE_TARGET)ld
OBJDUMP =	$(CROSS_COMPILE_TARGET)objdump
OBJCOPY =	$(CROSS_COMPILE_TARGET)objcopy
SIZE =		$(CROSS_COMPILE_TARGET)size
STRIP =	$(CROSS_COMPILE_TARGET)strip

MEM_INIT_GEN = $(GEN_CORES)/tools/mem_init_gen.py

CFLAGS += -Wall -D__TRTL_FIRMWARE__ -DARCH=urv -ffunction-sections -fdata-sections
CFLAGS += -I.
CFLAGS += -I$(BUILDDIR)/include
CFLAGS += -I$(TRTL_FW)
CFLAGS += -I$(TRTL_FW)/lib
CFLAGS += -I$(TRTL_FW)/framework
CFLAGS += -I$(TRTL_SW)/include
CFLAGS += -DGIT_VERSION=$(RT_GIT_VERSION)

EXTRA2_CFLAGS += # To be set by user on make line
EXTRA_CFLAGS  += $(EXTRA2_CFLAGS)

# used for firmware by trtl-project-creator
CFLAGS += $(EXTRA_CFLAGS)

# $(shell echo $(CONFIG_*)) to support escape chars in .config
CFLAGS += $(shell echo $(CONFIG_CFLAGS_OPT))
CFLAGS += $(shell echo $(CONFIG_CFLAGS_EXTRA))


OBJDIR += urv
OBJS += urv/irq.o
OBJS += urv/crt0.o

OBJDIR += lib
OBJS += lib/vsprintf-xint.o
OBJS += lib/printf.o
OBJS += lib/mockturtle-rt-common.o

OBJDIR-$(CONFIG_MOCKTURTLE_FRAMEWORK_ENABLE) += framework
OBJS-$(CONFIG_MOCKTURTLE_FRAMEWORK_ENABLE) += framework/mockturtle-framework.o

OBJDIR += $(OBJDIR-y)
OBJS += $(OBJS-y)


OBJS_BUILD = $(addprefix $(BUILDDIR)/, $(OBJS))
OBJDIR_BUILD = $(addprefix $(BUILDDIR)/, $(OBJDIR))
OBJDIR_BUILD += $(BUILDDIR)

TRTL_LD_SCRIPT ?= $(TRTL_FW)/urv/mockturtle.ld



.PHONY: all clean cleanall

all: $(build_output).bram $(build_output).bin $(build_output).elf

WBGEN_FILES = mt_cpu_csr.wb mt_cpu_lr.wb
WBGEN_HEADERS = mockturtle_cpu_csr.h mockturtle_cpu_lr.h

# .wb files should simply be there. This is used to check against the changes
$(addprefix $(TRTL_HDL)/cpu/,$(WBGEN_FILES)): ;

# dependencies between .h and .wb files
$(TRTL_SW)/include/mockturtle/hw/mockturtle_cpu_csr.h: $(TRTL_HDL)/cpu/mt_cpu_csr.wb
$(TRTL_SW)/include/mockturtle/hw/mockturtle_cpu_lr.h: $(TRTL_HDL)/cpu/mt_cpu_lr.wb

# target to let wbgen2 to generate headers
$(addprefix $(TRTL_SW)/include/mockturtle/hw/,$(WBGEN_HEADERS)):
	make -C $(TRTL_SW) include

# create dirs for object files
$(OBJDIR_BUILD):
	mkdir -p $@
	mkdir -p $@/$(DEPDIR)

# remove "|" to rebuild all objects on the .config change
$(OBJS_BUILD): | $(AUTOCONF)
$(OBJS_BUILD): | $(OBJDIR_BUILD)


$(src)/trtl-memory.ld:
	@echo "" > /dev/stderr
	@echo "*** Linker script file \"trtl-memory.ld\" file not found" > /dev/stderr
	@echo "*** Write a \"trtl-memory.ld\" linker script with a MEMORY" > /dev/stderr
	@echo "*** command that describes the target Mock Turtle CPU layout." > /dev/stderr
	@echo "" > /dev/stderr
	@exit 1

$(build_output).elf: $(addprefix $(TRTL_SW)/include/mockturtle/hw/,$(WBGEN_HEADERS)) $(TRTL_LD_SCRIPT) $(OBJS_BUILD) $(src)/trtl-memory.ld
	${CC} $(CFLAGS) $(LDFLAGS) -o $(build_output).elf -nostartfiles $(OBJS_BUILD) -T $(TRTL_LD_SCRIPT)

$(build_output).bin: $(build_output).elf
	${OBJCOPY} --remove-section .smem -O binary $(build_output).elf $(build_output).bin
	$(SIZE) $(build_output).elf

$(build_output).bram: $(build_output).bin
ifeq (0 , $(shell test -e $(MEM_INIT_GEN); echo $$?))
	$(MEM_INIT_GEN) -i $(build_output).bin > $(build_output).bram
else
	@echo "Cannot generate \"bram\" file. Missing $(MEM_INIT_GEN)" > /dev/stderr
endif

$(BUILDDIR)/urv/emulate.o: urv/emulate.c
	${CC} $(DEPFLAGS) $(CFLAGS) -march=rv32i -c $< -o $@
	$(POSTCOMPILE)

$(BUILDDIR)/%.o: %.c
	${CC} $(DEPFLAGS) $(CFLAGS) $(LDFLAGS) -c $< -o $@
	$(POSTCOMPILE)

$(BUILDDIR)/%.o: %.cpp
	${CXX} $(DEPFLAGS) $(CFLAGS) $(CXXFLAGS) $(LDFLAGS) -c $< -o $@
	$(POSTCOMPILE)

$(BUILDDIR)/%.o: %.S
	${CC} $(DEPFLAGS) $(ASFLAGS) $(LDFLAGS) -c $< -o $@
	$(POSTCOMPILE)

clean:
	rm -f $(build_output).bram $(build_output).bin $(build_output).elf
	rm -rf $(BUILDDIR)

clean-dot-config:
	rm -f .config

cleanall: clean clean-dot-config

install:
	mkdir -m 0775 -p $(DESTDIR)$(firmwaredir)
	install -D -t $(DESTDIR)$(firmwaredir) -m 0644 $(build_output).{bin,elf,bram}

# inlude *.d files from the build directory
include $(wildcard $(patsubst %,%/.d/*.d,$(basename $(OBJDIR_BUILD))))


# following targets from Makefile.kconfig
# this one is used to generate autoconf.h file
$(AUTOCONF) silentoldconfig: .config | $(BUILDDIR)
	export KCONFIG_CONFIG=$(src)/.config; \
	$(MAKE) quiet=quiet_ KBUILD_KCONFIG=$(src)/Kconfig projtree=$(BUILDDIR) -C $(TRTL_FW) -f Makefile.kconfig silentoldconfig

scripts_basic config:
	$(MAKE) quiet=quiet_ KBUILD_KCONFIG=$(src)/Kconfig projtree=$(src) -C $(TRTL_FW) -f Makefile.kconfig $@

%config:
	$(MAKE) quiet=quiet_ KBUILD_KCONFIG=$(src)/Kconfig projtree=$(src) -C $(TRTL_FW) -f Makefile.kconfig $@

defconfig:
	$(MAKE) quiet=quiet_ KBUILD_KCONFIG=$(src)/Kconfig projtree=$(src) -C $(TRTL_FW) -f Makefile.kconfig mt_defconfig

.config: ;

# Explicit rule for .config
# needed since -include XXX triggers build for XXX
$(src)/.config: ;
