/**
 * SPDX-FileCopyrightText: 2015-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <errno.h>
#include <inttypes.h>
#include <stdint.h>
#include <mockturtle-framework.h>

#if HAS_MOCKTURTLE_FRAMEWORK_DEBUG_ENABLE == 1
int pr_frm_debug(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = pp_vprintf(fmt, args);
	va_end(args);

	delay(10000);

	return ret;
}
#endif

/**
 * Make the given message an error message
 */
void trtl_fw_message_error(struct trtl_fw_msg *msg, int err)
{
	msg->header->msg_id = TRTL_MSG_ID_ERROR;
	msg->header->len = 1;
	((uint32_t *)msg->payload)[0] = err;
}


/**
 * This is an @ref trtl_fw_action_t function type. It fills the payload
 * witht an answer for the ping message.
 */
static int rt_recv_ping(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o)
{
	if (!HAS_MOCKTURTLE_FRAMEWORK_PING_ENABLE) {
		return -EPERM;
	}

	msg_o->header->msg_id = TRTL_MSG_ID_PING;
	return 0;
}


/**
 * This is an @ref trtl_fw_action_t function type. It fills the payload with
 * application name.
 */
static int rt_name_getter(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o)
{
	if (!HAS_MOCKTURTLE_FRAMEWORK_NAME_ENABLE) {
		return -EPERM;
	}

	msg_o->header->msg_id = TRTL_MSG_ID_NAME;
	msg_o->header->len = TRTL_NAME_LEN;
	memcpy(msg_o->payload, app.name, TRTL_NAME_LEN);
	return 0;
}


/**
 * This is an @ref trtl_fw_action_t function type. It fills the payload with
 * version information.
 */
static int rt_version_getter(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o)
{
	if (!HAS_MOCKTURTLE_FRAMEWORK_VERSION_ENABLE) {
		return -EPERM;
	}

	msg_o->header->msg_id = TRTL_MSG_ID_VER;
	msg_o->header->len = sizeof(struct trtl_fw_version) / 4;
	memcpy(msg_o->payload, (void *)&app.version,
	       sizeof(struct trtl_fw_version));
	return 0;
}


/**
 * This is an @ref trtl_fw_action_t function type. Accorind the message request,
 * it get/set one of the declared buffers
 */
static int trtl_fw_buffer(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o)
{
	struct trtl_fw_buffer *buf;
	unsigned int offset = 0, index, size;
	uint32_t *din;
	uint32_t *dout = NULL;

	if (!HAS_MOCKTURTLE_FRAMEWORK_BUFFER_ENABLE) {
		return -EPERM;
	}

	/* Only synchronous */
	if (msg_i->header->msg_id == TRTL_MSG_ID_VAR_GET &&
	    !(msg_i->header->flags & TRTL_HMQ_HEADER_FLAG_SYNC))
		return -EINVAL;

	din = msg_i->payload;
        dout = msg_o->payload;
	if (msg_o) {
		msg_o->header->msg_id = TRTL_MSG_ID_BUF_GET;
		msg_o->header->len = msg_i->header->len;
	}

	while (offset < msg_i->header->len) {
		index = din[offset + 0];
		size = din[offset + 1];
		if (index >= app.n_buffers) {
			pr_error("%s: TLV %d buffer unknown %d\n\r",
				  __func__, index, index);
			return -EINVAL;
		}
		buf = &app.buffers[index];

		if (size > buf->len) {
			pr_error("%s: buffer %d len %d not correct max  %"PRId32"\n\r",
				 __func__, index, size, buf->len);
			return -EINVAL;
		}

		pr_frm_debug("%s buffer %d len %d (%ld) Addr 0x%p\n\r", __func__,
			     index, size, buf->len, buf->buf);

		switch (msg_i->header->msg_id) {
		case TRTL_MSG_ID_BUF_SET:
			memcpy(buf->buf, &din[offset + 2], size);
			/* After a set we always read back => get */
		case TRTL_MSG_ID_BUF_GET:
			if (dout) {
				dout[offset + 0] = index;
				dout[offset + 1] = size;
				memcpy(&dout[offset + 2], buf->buf, size);
			}
			break;
		default:
			return -EINVAL;
		}

		offset += (2 + (size / 4)); /* Next TLV record */
	}

	return 0;
}


/**
 * This is an @ref trtl_fw_action_t function type. Accorind the message request,
 * it get/set a number of declared variables.
 */
static int trtl_fw_variable(struct trtl_fw_msg *msg_i, struct trtl_fw_msg *msg_o)
{
	struct trtl_fw_variable_msg {
		uint32_t index;
		uint32_t value;
	} *dout = NULL, *din = NULL;

	struct trtl_fw_variable *var;
	uint32_t *mem, val;
	int i;

	if (!HAS_MOCKTURTLE_FRAMEWORK_VARIABLE_ENABLE) {
		return -EPERM;
	}

	/* we always have a pair of values  */
	if (msg_i->header->len & 0x1)
		return -EINVAL;

	/* Only synchronous */
	if (msg_i->header->msg_id == TRTL_MSG_ID_VAR_GET &&
	    !(msg_i->header->flags & TRTL_HMQ_HEADER_FLAG_SYNC))
		return -EINVAL;

	din = msg_i->payload;
	if (msg_o) {
		dout = msg_o->payload;
		msg_o->header->msg_id = TRTL_MSG_ID_VAR_GET;
		msg_o->header->len = msg_i->header->len;
	}

	/* Write all values in the proper place */
	for (i = 0; i < msg_i->header->len / 2; ++i) {
		if (din[i].index >= app.n_variables) {
			pr_error("%s: Variable index %ld unknown\n\r",
				  __func__, din[i].index);
			return -EINVAL;
		}

		var = &app.variables[din[i].index];
		mem = (uint32_t *) var->addr;

		switch (msg_i->header->msg_id) {
		case TRTL_MSG_ID_VAR_SET:
			val = ((din[i].value & var->mask) << var->offset);
			if (var->flags & TRTL_FW_VARIABLE_FLAG_FLD)
				*mem = (*mem & ~var->mask) | val;
			else
				*mem = val;
			/* After a set we always read back => get */
		case TRTL_MSG_ID_VAR_GET:
			val = (*mem >> var->offset) & var->mask;
			break;
		default:
			return -EINVAL;
		}

		pr_frm_debug("%s index %"PRIu32"d/%d | [0x%p] = 0x%08"PRIx32" -> 0x%08"PRIx32")\n\r",
			     __func__,
			     din[i].index, app.n_variables - 1,
			     mem, *mem, val);

		if (dout) {
			/* when this is a synchronous action, we have to fill
			   the output */
			dout[i].index = din[i].index;
			dout[i].value = val;
		}
	}

	return 0;
}


/**
 * List of standard actions
 */
static trtl_fw_action_t *trtl_actions_in[] = {
	[TRTL_MSG_ID_PING - __TRTL_MSG_ID_MAX_USER] = rt_recv_ping,
	[TRTL_MSG_ID_VER - __TRTL_MSG_ID_MAX_USER] = rt_version_getter,
	[TRTL_MSG_ID_VAR_SET - __TRTL_MSG_ID_MAX_USER] = trtl_fw_variable,
	[TRTL_MSG_ID_VAR_GET - __TRTL_MSG_ID_MAX_USER] = trtl_fw_variable,
	[TRTL_MSG_ID_BUF_SET - __TRTL_MSG_ID_MAX_USER] = trtl_fw_buffer,
	[TRTL_MSG_ID_BUF_GET - __TRTL_MSG_ID_MAX_USER] = trtl_fw_buffer,
	[TRTL_MSG_ID_NAME - __TRTL_MSG_ID_MAX_USER] = rt_name_getter,
};


/**
 * According to the given message ID it returns the correspondent action
 * @param[in] msg_id message id
 * @return pointer to action function
 */
static inline trtl_fw_action_t *rt_action_get(unsigned int msg_id)
{
	unsigned int idx_act = msg_id;

	if (!HAS_MOCKTURTLE_FRAMEWORK_ACTION_ENABLE) {
		return NULL;
	}

	if (idx_act < __TRTL_MSG_ID_MAX_USER) {
		/* User Actions */
		if (idx_act >= app.n_actions || !app.actions[idx_act]) {
			pr_error("Unknown action (usr) msg_id 0x%x\n\r",
				 msg_id);
			return NULL;
		}
		return app.actions[idx_act];
	} else {
		/* Mock Turtle Standard Actions */
		idx_act -= __TRTL_MSG_ID_MAX_USER;
		if (ARRAY_SIZE(trtl_actions_in) < idx_act) {
			pr_error("Unknown action msg_id 0x%x\n\r",
				 msg_id);
			return NULL;
		}
		return trtl_actions_in[idx_act];
	}
}

/**
 * Run the action associated with the given identifier
 * @param[in] type MQ type
 * @param[in] idx_mq MQ index
 * @param[out] msg the incoming message
 * @return 0 on success. Otherwise an error code
 *
 * If the action generates an error, the framework will send
 * an error message with the error code.
 */
static inline int rt_action_run(enum trtl_mq_type type,
				unsigned int idx_mq,
				struct trtl_fw_msg *msg)
{
	struct trtl_fw_msg msg_out;
	trtl_fw_action_t *action = rt_action_get(msg->header->msg_id);
	int err = 0;

	if (!HAS_MOCKTURTLE_FRAMEWORK_ACTION_ENABLE) {
		return -EPERM;
	}

	if (!action)
		return -EINVAL;

	if (!(msg->header->flags & TRTL_HMQ_HEADER_FLAG_SYNC)) {
		/* Asynchronous message, then no output */
		return action(msg, NULL);
	}

	/* Synchronous message */
	if (mq_claim(type, idx_mq) < 0) {
		return -EAGAIN;
	}
	mq_map_out_message(type, idx_mq, &msg_out);

	/* prepare the header */
	msg_out.header->rt_app_id = app.version.rt_id;
	msg_out.header->flags = TRTL_HMQ_HEADER_FLAG_ACK;
	msg_out.header->msg_id = 0;
	msg_out.header->len = 0;
	msg_out.header->sync_id = msg->header->sync_id;

	err = action(msg, &msg_out);
	if (err)
		trtl_fw_message_error(&msg_out, err);

	if (HAS_MOCKTURTLE_FRAMEWORK_DEBUG_ENABLE) {
		pr_frm_debug("Output message\n\r");
		pr_message(&msg_out);
	}

	mq_send(type, idx_mq);

	return err;
}


/**
 * Dipatch messages coming from a given message queue.
 * @param[in] type MQ type
 * @param[in] idx_mq MQ index
 * @return 0 on success. -1 on error
 *
 * If the message is not acceptable, this function will discard it.
 */
int trtl_fw_mq_action_dispatch(enum trtl_mq_type type, unsigned int idx_mq)
{
	struct trtl_fw_msg msg;
	int err = 0;

	if (!HAS_MOCKTURTLE_FRAMEWORK_ACTION_ENABLE) {
		return -EPERM;
	}

	if (type == TRTL_RMQ)
		return -EPERM; /* not supported yet */

	/* Check if we have a pending message */
	if (!(mq_poll_in(type, 1 << idx_mq)))
		return -EAGAIN;

	/* Map the message  */
	mq_map_in_message(type, idx_mq, &msg);

	if (HAS_MOCKTURTLE_FRAMEWORK_DEBUG_ENABLE) {
		pr_frm_debug("Input message\n\r");
		pr_message(&msg);
	}

	if (!(msg.header->flags & TRTL_HMQ_HEADER_FLAG_RPC)) {
		err = -EINVAL;
		goto out;
	}

	if (msg.header->rt_app_id &&
	    msg.header->rt_app_id != app.version.rt_id) {
		pr_error("Invalid app id 0x%x\n\r",
			 msg.header->rt_app_id);
		err = -EINVAL;
		goto out;
	}

	/* Run the correspondent action */
	err = rt_action_run(type, idx_mq, &msg);
	if (err)
		pr_error("Action failure err: %d\n\r", err);
out:
	if (err != -EAGAIN) {
		mq_discard(type, idx_mq);
	}
	return err;
}


/**
 * Build and send a message over MQ.
 * The vargs will be copied into the payload message.
 * @param[in] type MQ type
 * @param[in] idx_mq MQ index within the declaration
 * @param[in] msg_id message identifier
 * @param[in] n number of vargs
 */
int trtl_fw_mq_send_uint32(enum trtl_mq_type type,
			   unsigned int idx_mq,
			   uint8_t msg_id,
			   unsigned int n, ...)
{
	struct trtl_fw_msg msg;
	uint32_t sizes;
	va_list ap;
	int i;

	if (!HAS_MOCKTURTLE_FRAMEWORK_VALUE_SEND_ENABLE) {
		return -EPERM;
	}

	sizes = app.cfgrom->hmq[trtl_get_core_id()][idx_mq].sizes;
	if (n > TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes))
		return -EINVAL;

	mq_claim(type, idx_mq);

	mq_map_out_message(type, idx_mq, &msg);
	msg.header->rt_app_id = app.version.rt_id;
	msg.header->flags = 0;
	msg.header->msg_id = msg_id;
	msg.header->len = n;

	va_start(ap, n);
	for (i = 0; i < msg.header->len;++i)
		((uint32_t *)msg.payload)[i] = va_arg(ap, uint32_t);
	va_end(ap);

	mq_send(type, idx_mq);

	return 0;
}


/**
 * Build and send a message over MQ.
 * The buffer will be copied into the payload message.
 * Beware that, internally, it uses trtl_fw_mq_send().
 * @param[in] type MQ type
 * @param[in] idx_mq MQ index within the declaration
 * @param[in] msg_id message identifier
 * @param[in] n buffer size in bytes
 * @param[in] data buffer to send
 */
int trtl_fw_mq_send_buf(enum trtl_mq_type type,
			unsigned int idx_mq,
			uint8_t msg_id,
			unsigned int n,
			void *data)
{
	struct trtl_fw_msg msg;
	uint32_t sizes;

	if (!HAS_MOCKTURTLE_FRAMEWORK_BUFFER_SEND_ENABLE) {
		return -EPERM;
	}

	sizes = app.cfgrom->hmq[trtl_get_core_id()][idx_mq].sizes;
	if ((n / 4) > TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes))
		return -EINVAL;

	mq_claim(type, idx_mq);

	mq_map_out_message(type, idx_mq, &msg);
	msg.header->rt_app_id = app.version.rt_id;
	msg.header->flags = 0;
	msg.header->msg_id = msg_id;
	msg.header->len = n / 4;
	memcpy(msg.payload, data, n);

	mq_send(type, idx_mq);

	return 0;
}


/**
 * Get the current time from the internal TRTL timer
 * @param[out] seconds
 * @param[out] cycles
 */
void trtl_fw_time(uint32_t *seconds, uint32_t *cycles)
{
	*seconds = lr_readl(MT_CPU_LR_REG_TAI_SEC);
	*cycles = lr_readl(MT_CPU_LR_REG_TAI_CYCLES);
}


/**
 * Validate the user application actions
 * @param[in] app the user application
 * @return 0 on success. -EINVAL on error
 */
static inline int trtl_fw_init_action(struct trtl_fw_application *app)
{
	int i;

	/* Validate actions */
	for (i = 0; i < app->n_actions; ++i) {
		if (i > __TRTL_MSG_ID_MAX_USER) {
			pr_error("Too many actions defined. Maximum '%d'\n\r",
				  __TRTL_MSG_ID_MAX_USER);
			return -EINVAL;
		}
		if (app->actions[i] == NULL) {
			pr_error("Undefined action %d\n\r", i);
			return -EINVAL;
		}
	}
	return 0;
}


/**
 * Validate the FPGA
 * @param[in] app the user application
 * @return 0 on success. -EINVAL on error
 */
static inline int trtl_fw_init_fpga(struct trtl_fw_application *app)
{
	int i, found = 0;

	for (i = 0; i < app->fpga_id_compat_n; i++) {
		if (app->fpga_id_compat[i] == app->cfgrom->app_id) {
			found = 1;
			break;
		}
	}

	if (!found && app->fpga_id_compat_n) {
		pr_error("\tFPGA '0x%"PRIx32"' not compatible with RT app: 0x%"PRIx16" 0x%"PRIx32" (git %"PRIx32")\n\r",
			 app->cfgrom->app_id,
			 app->version.rt_id,
			 app->version.rt_version,
			 app->version.git_version);
		return -EINVAL;
	}

	pr_frm_debug("Running application '%s'\n\r", app->name);
	pr_frm_debug("\tapp id     \t'0x%"PRIx16"'\n\r",
		     app->version.rt_id);
	pr_frm_debug("\tapp version\t'%"PRId32".%"PRId32"'\n\r",
		     RT_VERSION_MAJ(app->version.rt_version),
		     RT_VERSION_MIN(app->version.rt_version));
	pr_frm_debug("\tsource id  \t'0x%"PRIx32"'\n\r",
		     app->version.git_version);

	return 0;
}


/**
 * Initialize the RMQ and the HMQ
 * @param[in] app the user application
 * @return 0 on success. -EINVAL on error
 */
static inline int trtl_fw_init_mq(struct trtl_fw_application *app)
{
	// TODO purge RMQ
	return 0;
}


/**
 * Validate the RMQ and the HMQ
 * @param[in] app the user application
 * @return 0 on success. -EINVAL on error
 */
static inline int trtl_fw_init_variables(struct trtl_fw_application *app)
{
	int i;

	pr_frm_debug("Exported Variables\n\r");
	for (i = 0; i < app->n_variables; ++i)
		pr_frm_debug("[%d] addr %p | mask 0x%"PRIx32" | off %d\n\r",
			     i,
			     app->variables[i].addr,
			     app->variables[i].mask,
			     app->variables[i].offset);

	return 0;
}


/**
 * Validate the RMQ and the HMQ
 * @param[in] app the user application
 * @return 0 on success. -EINVAL on error
 */
static inline int trtl_fw_init_buffers(struct trtl_fw_application *app)
{
	int i;

	pr_frm_debug("Exported Buffers\n\r");
	for (i = 0; i < app->n_buffers; ++i)
		pr_frm_debug("[%d] addr %p | len %"PRId32"\n\r", i,
			     app->buffers[i].buf,
			     app->buffers[i].len);

	return 0;
}


/**
 * Initialize the application and some compatibility check.
 *
 * - check bitstream FPGA ID if compatibile with the application.
 * - purge remote message queue
 *
 * @return 0 on success. -1 on error
 */
int trtl_fw_init(void)
{
	int err;

	app.cfgrom = trtl_config_rom_get();

	err = trtl_fw_init_action(&app);
	if (err)
		return err;

	err = trtl_fw_init_fpga(&app);
	if (err)
		return err;

	err = trtl_fw_init_mq(&app);
	if (err)
		return err;

	err = trtl_fw_init_buffers(&app);
	if (err)
		return err;

	err = trtl_fw_init_variables(&app);
	if (err)
		return err;

	return 0;
}


/**
 * The main function
 */
int main(void)
{
	int err = 0;


	trtl_notify(TRTL_CPU_NOTIFY_INIT);
	err = trtl_fw_init();
	if (err)
		return -1;
	if (app.init){
		err = app.init();
		if (err)
			return -1;
	}

	trtl_notify(TRTL_CPU_NOTIFY_MAIN);
	if (app.main) {
		err = app.main();
		if (err)
			return -1;

	}

	trtl_notify(TRTL_CPU_NOTIFY_EXIT);
	if (app.exit) {
		err = app.exit();
		if (err)
			return -1;
	}

	return 0;
}
