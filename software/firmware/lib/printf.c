/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN (home.cern)
 * Author: Alessandro Rubini
 *
 * Basic printf based on vprintf based on vsprintf
 *
 * (please note that the vsprintf is not public domain but GPL)
 */

/**
 * Include this code only if explicitly asked to
 */

#include <stdarg.h>

#include <mockturtle-rt.h>

#if HAS_MOCKTURTLE_LIBRARY_PRINT_ENABLE == 1

static char print_buf[CONFIG_PRINT_BUFSIZE];

int pp_vprintf(const char *fmt, va_list args)
{
	int ret;

	ret = pp_vsprintf(print_buf, fmt, args);
	puts(print_buf);
	return ret;
}

int pp_sprintf(char *s, const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = pp_vsprintf(s, fmt, args);
	va_end(args);
	return ret;
}


int pp_printf(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = pp_vprintf(fmt, args);
	va_end(args);

	return ret;
}

#if HAS_MOCKTURTLE_LIBRARY_PRINT_ERROR_ENABLE == 1
int pr_error(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = pp_vprintf(fmt, args);
	va_end(args);

	delay(10000);

	return ret;
}
#endif

#if HAS_MOCKTURTLE_LIBRARY_PRINT_DEBUG_ENABLE == 1
int pr_debug(const char *fmt, ...)
{
	va_list args;
	int ret;

	va_start(args, fmt);
	ret = pp_vprintf(fmt, args);
	va_end(args);

	delay(10000);

	return ret;
}
#endif

#endif
