..
  SPDX-License-Identifier: CC-BY-SA-4.0+

  SPDX-FileCopyrightText: 2019 CERN

==========
Change Log
==========
Format: `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_
Versioning: `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_

[4.3.0] - 2023-11-14
====================
Added
-----
- [bld] build distribution tarball with all binaries and development files (#36)

Changed
-------
- [sw/fw] use GCC toolchain v11.2 for RISC-V firmware compilation (#37)
- [bld] use common EDL CI pipelines (#35)

Fixed
-----
- [sw/py] fix calls to trtl_close() in TrtlDevice object destructor
- [sw] fix bug in sync_id when used by multiple sync users on same HMQ (#34)

[4.2.0] - 2023-06-12
====================
Added
-----
- [doc] Document RMQ interface (#28)
- [hdl] per-CPU soft-reset port (#31)

Changed
-------
- [sw] Driver no longer handles sequence codes (#24)
- [bld] pytests on real hardware now run through CI (#29)
- [bld] Software installation cleanup and migration to latest common CI

Fixed
-----
- [sw] Fix .so versioning (#25)
- [sw] Make driver compatible with 5.15+ kernels
- [hdl] RMQ TX interface flow control (#26)
- [hdl]Fix regressions in SVEC demo design (#27)

[4.1.0] - 2023-01-12
====================
Added
-----
- [hdl] support for VLANs to endpoint
- [hdl] new hmq-async-recv testbench
- [sw] support C++ for firmware
- [sw] functions to return device ID and application name
- [sw] top-level kernel driver for demos

Changed
-------
- [hdl] MT driver testbench now auto-updates after init
- [hdl] per-cpu UART is now reset when the cpu is reset
- [bld] Many improvements to build system and CI
- [sw] Wait for CPU reset before returning

Fixed
-----
- [hdl] wrong slot indexing for message queues
- [hdl] HMQ selection
- [hdl] miscalculation of payload size on RMQ RX (was 2 less)
- [sw] concurrency issue if both sync and async messages are used
- [sw] fix ctype variable type in python wrapper for trtl_cpu_is_enable
- [fw] do not discard message queue if response was not sent
- [fw] linker did not properly initialise bss segment
- [fw] proper initialisation of risc-v global pointer

[4.0.5] - 2021-07-29
====================
Fixed
-----
- [sw] support for Linux versions greater than 5.8


[4.0.4] - 2019-10-21
====================
Changed
-----
- [sw] remove git dependency when building from SRPM
- [sw] use Kbuild to build kernel module
- [sw] remove wbgen2 requirement when bailing from SRPM
Fixed
-----
- [sw] dkms is not a building requirement for RPM

[4.0.3] - 2019-10-18
====================
Changed
-------
- [sw] make file 'install' rules and environment variables to support RPMs

Added
-----
- [dist] proper .spec file to produce RPMs
- [ci] gitlab ci jobs

[4.0.2] - 2019-10-14
====================
Changed
-------
- [sw] build driver on Linux v5.0 or greater
- [sw] driver sysfs files permission are g+w
Fixed
-----
- [sw] string overflow control
- [sw] ``trtl_msg_poll`` input validation

[4.0.1] - 2019-09-13
====================
Fixed
-----
- [sw] File Descriptor closed correctly so that the same process can do
  ``trtl_open()`` and ``trtl_close()`` multiple times

[4.0.0] - 2019-05-14
====================
Changed
-------
- [gw] soft-CPU is now uRV
- [gw] each core has a set of dedicated HMQ and RMQ
- [doc] moved to sphinx and ReST
- [doc] merged HDL and software
- [doc] improved with examples
- [doc] doxygen and wbgen2 integration
- [sw] synchronous messages redesigned
- [sw] HMQ API reflects gateware changes
- [sw] firmware library and framework improvements
- [sw] re-design Python wrapper a bit
Added
-----
- [gw] uRV debug interface
- [gw] notification interrupts from firmwares
- [gw] new test benches
- [doc] Tools section
- [doc] Demo section
- [doc] Glossary section
- [sw] driver auto-configuration from configuration ROM
- [sw] GDB agent to debug firmware
- [sw] tool to generated empty projects
- [sw] tools to interact with firmware framework
- [demo] new demo section
- [test] new integration test section
Removed
-------
- [sw] Driver message filter
- [sw] LM32 support
- [sw] function to pause and start CPU execution

[3.1.0] - 2018-03-09
====================
This release brings in the gateware from wr-node-core repository, such as it
is being used in the released and deployed v1.0 version of masterFIP.
The relevant commit is 96a7859 from git://ohwr.org/white-rabbit/wr-node-core.git.
This is a "known to work" version of the gateware, still using the "node core" name.
Following releases will switch to the "mock turtle" name.

[3.0.0] - 2017-09-14
====================
The realease of a new version has been necessary because I decided to remove
the debug interface from the API. This interface has been problematic, and
its original purpose has been neglected
Added
-----
- [sw] standard TTY interface to access the soft-CPU serial console output
Removed
-------
- [sw] debugfs interface to access the soft-CPU serial console output
- [sw] removed library functions to access the debugfs console output
- [sw] the ``mockturtle-messages`` tool is not able anymore to access the
  soft-CPU serial console output
Fixed
-----
- [sw] minor fixes

[2.0.0] - 2016-06-08
====================
Changed
-------
- [sw] Project change name to Mock Turtle. this means renaming all files
  [sw] functions, data structures, enumerates and variables to a new
  convention
- [sw] port output real-time application to the real-time library
- [sw] improve kernel driver performance
Added
-----
- [test] unittest

[1.1.0] - 2015-10-20
====================
Changed
-------
- [sw] minor changes in API behaviour on error

[1.0.0] - 2015-09-17
====================
- [sw] first release White Rabbit Node Core
