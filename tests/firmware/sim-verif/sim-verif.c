/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <string.h>
#include <stdint.h>

#include "mockturtle-rt.h"
#include "mockturtle/hw/mockturtle_addresses.h"
#include "mockturtle/hw/mockturtle_queue.h"
#include "mockturtle/hw/mockturtle_cpu_lr.h"

volatile unsigned int counts[8] __attribute__((section(".smem")));

static const int cpu = 0;

static void
failed(char c)
{
  putchar (c);
  puts ("-FAILED!\n");
  counts[cpu] |= 0xff00;
  while (1)
    ;
}

static void
test_notify (void)
{
  unsigned int v;

  v = counts[cpu] + 1;
  // send notification
  lr_writel(v, MT_CPU_LR_REG_NTF_INT);

  //  Wait for answer
  mq_claim(TRTL_HMQ, 0);
  while(mq_poll_in(TRTL_HMQ, 1) == 0)
    ;

  mq_discard(TRTL_HMQ, 0);

  if (mq_poll_in(TRTL_HMQ, 1) != 0)
    failed('N');

  if (counts[cpu] != v)
    failed('P');
}

static const char msg[] = "Hello!";

static void
test_hmq(int slot)
{
  int i;
  unsigned char *p;
  unsigned *hdr;
  unsigned int v;

  v = counts[cpu];

  //  Send a message
  mq_claim(TRTL_HMQ, slot);
  p = mq_map_out_buffer(TRTL_HMQ, slot);
  for (i = 0; i < sizeof (msg); i++)
    p[i] = msg[i];
  hdr = mq_map_out_header(TRTL_HMQ, slot);
  hdr[1] = (sizeof (msg) + 3) / 4;
  mq_send(TRTL_HMQ, slot);

  //  Wait for answer
  while(mq_poll_in(TRTL_HMQ, 1 << slot) == 0)
    ;
  if (counts[cpu] != v + 1)
    failed('H');

  //  Read answer
  hdr = mq_map_in_header(TRTL_HMQ, slot);
  if (hdr[1] != (sizeof (msg) + 3) / 4)
    failed('h');
  p = mq_map_in_buffer(TRTL_HMQ, slot);
  for (i = 0; i < sizeof (msg); i++)
    if ((~p[i] & 0xff) != msg[i])
      failed('K');
  mq_discard(TRTL_HMQ, slot);

  if (mq_poll_in(TRTL_HMQ, 1 << slot) != 0)
    failed('k');
}

static void
test_rmq(int slot)
{
  int i;
  char *p;
  unsigned *hdr;

  //  Send a message
  mq_claim(TRTL_RMQ, slot);
  p = mq_map_out_buffer(TRTL_RMQ, slot);
  for (i = 0; i < sizeof (msg); i++)
    p[i] = msg[i];
  hdr = mq_map_out_header(TRTL_RMQ, slot);
  hdr[0] = 0;
  hdr[1] = (sizeof (msg) + 3) / 4;
  hdr[2] = 2;
  mq_send(TRTL_RMQ, slot);

  //  Wait for answer
  while(mq_poll_in(TRTL_RMQ, 1 << slot) == 0)
    ;

  //  Read answer
  hdr = mq_map_in_header(TRTL_RMQ, slot);
  if (hdr[1] != ((sizeof (msg) + 3) / 4) * 4)
    failed('R');
  p = mq_map_in_buffer(TRTL_RMQ, slot);
  for (i = 0; i < sizeof (msg); i++)
    if (p[i] != msg[i])
      failed('r');
  mq_discard(TRTL_RMQ, slot);

  if (mq_poll_in(TRTL_RMQ, 1 << slot) != 0)
    failed('s');
}

int
main(void)
{
  counts[cpu] = 0;

  puts("#1 console\n");

  //lr_writel(1, MT_CPU_LR_REG_NTF_INT);

  puts("#2 notify irq\n");
  for (int i = 0; i < 10; i++)
    test_notify();

  puts("#3 hmq\n");
  for (int slot = 0; slot < 8; slot++)
    test_hmq(slot);

  puts("#4 rmq\n");
  for (int slot = 0; slot < 8; slot++)
    test_rmq(slot);

  counts[cpu] = 0xf0;

  puts ("Done\n");

  return 0;
}
