/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	int i, n_word, depth, slot, cpu, hmq;
	const struct trtl_config_rom *cfgrom = trtl_config_rom_get();
	uint32_t sizes;
	struct trtl_fw_msg msg;

	pr_debug("ASYNC MESSAGES RECV\r\n");
	cpu = trtl_get_core_id();
	for (hmq = 0; hmq < cfgrom->n_hmq[cpu]; ++hmq)
		mq_purge(TRTL_HMQ, hmq);

	for (hmq = 0; hmq < cfgrom->n_hmq[cpu]; ++hmq) {
		sizes = cfgrom->hmq[cpu][hmq].sizes;
		n_word = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(sizes);
		depth = TRTL_CONFIG_ROM_MQ_SIZE_ENTRIES(sizes);
		pr_debug("\tcpu: %d, hmq: %d, size: %d, entries: %d\r\n",
				 cpu, hmq, n_word, depth);
		for (slot = 0; slot < depth; ++slot) {
			mq_claim(TRTL_HMQ, hmq);

			mq_map_out_message(TRTL_HMQ, hmq, &msg);
			memset(msg.header, 0, sizeof(*msg.header));
			memset(msg.payload, 0, n_word * 4);
			pr_debug("\t\tFilling buffer (%p) with %d 32bit numbers %d\r\n",
					 msg.payload, msg.header->len, n_word);
			msg.header->seq = slot;
			msg.header->len = n_word;
			for (i = 0; i < msg.header->len; i++) {
				volatile uint32_t *data = (uint32_t *)msg.payload;

				data[i] = i;
				pr_debug("\t\t\t(%p) val[%d] = %d\r\n", &data[i], i, data[i]);
			}
			mq_send(TRTL_HMQ, hmq);
		}
	}

	return 0;
}
