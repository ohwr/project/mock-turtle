/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
/*
 * Copyright (C)
 * Author:
 * License:
 */

#include <mockturtle-framework.h>

#define BUF1_LEN 4
#define BUF2_LEN 5

static int var1;
static int var2;
static uint32_t buf1[BUF1_LEN];
static uint32_t buf2[BUF2_LEN];

struct trtl_fw_variable variables[] = {
	[0] = {
		.addr = (void *)&var1,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
	[1] = {
		.addr = (void *)&var2,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
};

struct trtl_fw_buffer buffers[] = {
	[0] = {
		.buf = buf1,
		.len = BUF1_LEN * 4,
	},
	[1] = {
		.buf = buf2,
		.len = BUF2_LEN * 4,
	},

};


static int frm_init()
{
	int i;

	var1 = 0x55AA55AA;
	var2 = 0xFEDE0786;

	for (i = 0; i < BUF1_LEN; ++i)
		buf1[i] = i;

	for (i = 0; i < BUF2_LEN; ++i)
		buf2[i] = i * 2;

	return 0;
}


/**
 * Well, the main :)
 */
static int frm_main()
{
	while (1) {
		/* Handle all messages incoming from HMQ 0 as actions */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
	}

	return 0;
}


struct trtl_fw_application app = {
	.name = "testfrm",
	.version = {
		.rt_id = 0xFEDE,
		.rt_version = RT_VERSION(0, 1),
		.git_version = 0x12345678,
	},

	.fpga_id_compat_n = 0,

	.variables = variables,
	.n_variables = ARRAY_SIZE(variables),

	.buffers = buffers,
	.n_buffers = ARRAY_SIZE(buffers),

	.init = frm_init,
	.main = frm_main,
};
