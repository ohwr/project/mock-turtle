/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	int i, size, cpu;
	const struct trtl_config_rom *cfgrom = trtl_config_rom_get();
	const uint32_t *data = (const uint32_t *)cfgrom;
	uint32_t *payload;
	struct trtl_fw_msg msg;

	pr_debug("CONFIG ROM\r\n");

	cpu = trtl_get_core_id();
	size = TRTL_CONFIG_ROM_MQ_SIZE_PAYLOAD(cfgrom->hmq[cpu][0].sizes);

	/* Copy only the initial part - we do not need to read it all */
	mq_claim(TRTL_HMQ, 0);
	mq_map_out_message(TRTL_HMQ, 0, &msg);

	memset(msg.header, 0, sizeof(struct trtl_hmq_header));
	msg.header->msg_id = TRTL_MSG_ID_UNDEFINED;
	msg.header->len = size;

	memcpy(msg.payload, trtl_config_rom_get(), msg.header->len * 4);

	payload = msg.payload;
	for (i = 0; i < msg.header->len; i++) {
		pr_debug("%p = 0x%08"PRIx32" 0x%08"PRIx32"\r\n",
			 &data[i],
			 data[i],
			 payload[i]);
	}

	mq_send(TRTL_HMQ, 0);

	return 0;
}
