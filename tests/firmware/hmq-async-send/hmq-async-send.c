/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>


struct payload {
	uint32_t idx;
	uint32_t max;
};


int main()
{
	int cpu, hmq, count, run;
	const struct trtl_config_rom *cfgrom = trtl_config_rom_get();
	struct trtl_fw_msg msg, msg_s;
	struct payload *p;
	uint32_t status;

	pr_debug("TEST for: async send, sync\r\n");

	cpu = trtl_get_core_id();
	for (hmq = 0; hmq < cfgrom->n_hmq[cpu]; ++hmq) {
		mq_map_in_message(TRTL_HMQ, hmq, &msg);
		mq_map_out_message(TRTL_HMQ, hmq, &msg_s);
		p = msg.payload;

		for (count = 1, run = 1; run; ++count) {
			/* Wait incoming message - 1s timeout */
			status = mq_poll_in_wait(TRTL_HMQ, 1 << hmq, 1000000);
			if (!status) {
				pr_error("\tNO MESSAGE PENDING h:%d, cnt:0x%x\r\n",
					 hmq, count);
				return -1;
			}

			pr_debug("\th: %d, l:%d, {idx:0x%lx, max:0x%lx}, cnt:0x%x\r\n",
				 hmq, msg.header->len, p->idx, p->max, count);

			/* validate message */
			if (msg.header->len != 2 || p->idx != count || p->max < count) {
				pr_error("\th:%d, len:%d, {idx: %ld, max: %ld}, cnt:0x%x\r\n",
					 hmq, msg.header->len, p->idx, p->max, count);
				mq_discard(TRTL_HMQ, hmq);
				return -1;
			}

			/* stop on the last one */
			if (p->idx == p->max)
				run = 0; /* stop the loop */

			if (msg.header->flags & TRTL_HMQ_HEADER_FLAG_SYNC) {
				/* For sync answer test */
				pr_debug("SEND MESSAGES SYNC ANSWER\r\n");
				mq_claim(TRTL_HMQ, hmq);
				memcpy(msg_s.header, msg.header,
				       sizeof(struct trtl_hmq_header));
				msg_s.header->flags &= ~TRTL_HMQ_HEADER_FLAG_SYNC;
				msg_s.header->flags |= TRTL_HMQ_HEADER_FLAG_ACK;
				memcpy(msg_s.payload, msg.payload, msg.header->len * 4);
				mq_send(TRTL_HMQ, hmq);
			}

			/* goto the next one */
			mq_discard(TRTL_HMQ, hmq);
		}
	}

	pp_printf("OK\r\n");
	return 0;
}
