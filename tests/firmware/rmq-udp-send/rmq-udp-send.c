/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

#define RMQ 0
#define TEST_MESSAGE_ID 0xdeadbeef

static void send_pkt(unsigned val)
{
	struct trtl_fw_msg msg;

	// queue full? wait
	while (mq_claim(TRTL_RMQ, RMQ) < 0)
                ;

	mq_map_out_message(TRTL_RMQ, RMQ, &msg);

	unsigned *p = msg.payload;
	msg.header->len = 2;

	p[0] = TEST_MESSAGE_ID;
        p[1] = val;

	mq_send(TRTL_RMQ, RMQ);
}

static unsigned handle_rx(void)
{
        unsigned res;

	while (!mq_poll_in(TRTL_RMQ, 1 << RMQ))
                ;

	struct trtl_fw_msg tmsg;

	mq_map_in_message(TRTL_RMQ, RMQ, &tmsg);

	unsigned *p = tmsg.payload;
        res = p[1];

        pp_printf("Recv id=%x, val=%x\n", p[0], p[1]);

	mq_discard(TRTL_RMQ, RMQ);

        return res;
}


int main()
{
        unsigned v;
	int rmq = 0;
	struct trtl_ep_eth_address bind_addr;

	pp_printf("RMQ UDP EP test\n");

	// set up the RMQ Endpoint

	// destination MAC: we use broadcast
	bind_addr.dst_mac[0] = 0xff;
	bind_addr.dst_mac[1] = 0xff;
	bind_addr.dst_mac[2] = 0xff;
	bind_addr.dst_mac[3] = 0xff;
	bind_addr.dst_mac[4] = 0xff;
	bind_addr.dst_mac[5] = 0xff;

	// destination port
	bind_addr.dst_port = 12345;
	// source port
	bind_addr.src_port = 7777;
	// destination IP: 192.168.90.255 (broadcast)
	bind_addr.dst_ip = 0xC0A85AFF;
	// source IP: 192.168.90.255 (broadcast)
	bind_addr.src_ip = 0xC0A85A11;
	bind_addr.ethertype = 0x800; // IPv4
	// RX filter: we want only UDP packets with matching desination port & IP address.
	bind_addr.filter = TRTL_EP_FILTER_UDP | TRTL_EP_FILTER_DST_PORT | TRTL_EP_FILTER_DST_IP;
        bind_addr.filter |= TRTL_EP_FILTER_ENABLE;

	// configure outgoing channel
	rmq_bind_out(rmq, TRTL_EP_ETH, &bind_addr);
	// configure incoming channel
	rmq_bind_in(rmq, TRTL_EP_ETH, &bind_addr);

        /* Send twice */
        send_pkt(0x123);
        v = handle_rx();
        if (~v != 0x123)
                goto error;
        send_pkt(v);
        v = handle_rx();
        if (v != 0x123)
                goto error;

        /* Test send with a vlan tag */
        bind_addr.vlan_id = 0x05;
        bind_addr.filter |= TRTL_EP_FILTER_VLAN;
	rmq_bind_out(rmq, TRTL_EP_ETH, &bind_addr);

        /* This packet should be filtered out */
        send_pkt(0x201);

        /* Accept vlan or not */
        bind_addr.filter |= TRTL_EP_FILTER_VLAN_DIS;
        rmq_bind_in(rmq, TRTL_EP_ETH, &bind_addr);

        /* This packet should be accepted */
        send_pkt(0x208);
        v = handle_rx();
        if (~v != 0x208)
                goto error;

        /* Disable tx vlan */
        bind_addr.filter &= ~TRTL_EP_FILTER_VLAN;
	rmq_bind_out(rmq, TRTL_EP_ETH, &bind_addr);

        /* This packet should be accepted */
        send_pkt(0x203);
        v = handle_rx();
        if (~v != 0x203)
                goto error;

        /* Add vlan on both rx and tx.  */
        bind_addr.filter |= TRTL_EP_FILTER_VLAN;
	rmq_bind_out(rmq, TRTL_EP_ETH, &bind_addr);
        rmq_bind_in(rmq, TRTL_EP_ETH, &bind_addr);

        send_pkt(0x206);
        v = handle_rx();
        if (~v != 0x206)
                goto error;

        /* Notify end  */
        lr_writel(v, MT_CPU_LR_REG_NTF_INT);
        return 0;

error:
        pp_printf("Unexpected result - failure\n");
	return 0;
}
