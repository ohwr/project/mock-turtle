/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * SPDX-FileCopyrightText: 2019 CERN
 */
#include <mockturtle-rt.h>

int main()
{
	int cpu, hmq;
	const struct trtl_config_rom *cfgrom = trtl_config_rom_get();
	struct trtl_fw_msg msg, msg_s;
	struct payload *p;
	uint32_t status;

	pr_debug("TEST for: async send, sync\r\n");

	cpu = trtl_get_core_id();
	while (1) {
		for (hmq = 0; hmq < cfgrom->n_hmq[cpu]; ++hmq) {
			mq_map_in_message(TRTL_HMQ, hmq, &msg);
			mq_map_out_message(TRTL_HMQ, hmq, &msg_s);

			status = mq_poll_in_wait(TRTL_HMQ, 1 << hmq, 1);
			if (!status)
				continue;

			if (!(msg.header->flags & TRTL_HMQ_HEADER_FLAG_SYNC))
				continue;

			/* For sync answer test */
			pr_debug("SEND MESSAGES SYNC ANSWER\r\n");
			mq_claim(TRTL_HMQ, hmq);
			memcpy(msg_s.header, msg.header,
			       sizeof(struct trtl_hmq_header));
			msg_s.header->flags &= ~TRTL_HMQ_HEADER_FLAG_SYNC;
			msg_s.header->flags |= TRTL_HMQ_HEADER_FLAG_ACK;
			memcpy(msg_s.payload, msg.payload, msg.header->len * 4);
			mq_send(TRTL_HMQ, hmq);
			mq_discard(TRTL_HMQ, hmq);
		}
	}

	pp_printf("OK\r\n");
	return 0;
}
