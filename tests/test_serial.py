"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2019 CERN
"""

import os
import pytest
import serial
import stat
import time

@pytest.fixture
def firmware_file_serial(fw_locator):
    return fw_locator.get_path("firmware/serial/fw-serial.bin")


def firmware_output():
    return ["this is a message\r\n",
                ]

    # FIXME test different print level
    # return ["this is a message\r\n",
    #         "this is a message\r\nand we use it for testing\r\n",
    #         "this is a message\r\nthat goes through the serial interface\n\rand we use it for testing\r\n"]


class TestSerial(object):
    def test_exist(self, trtl_cpu):
        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        assert os.path.exists(filename) == True

    def test_chardevice(self, trtl_cpu):
        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        st = os.stat(filename)
        assert stat.S_ISCHR(st.st_mode) == True

    def test_permission(self, trtl_cpu):
        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        st = os.stat(filename)
        assert st.st_mode & stat.S_IRUSR != 0

    @pytest.mark.parametrize("msg_ref", enumerate(firmware_output()))
    def test_print(self, trtl_cpu, msg_ref, firmware_file_serial):
        trtl_cpu.disable()
        trtl_cpu.load_application_file(firmware_file_serial)

        filename = "/dev/ttytrtl-{:04x}-{:d}".format(trtl_cpu.trtl_dev.device_id,
                                                     trtl_cpu.idx_cpu)
        with serial.Serial(filename) as ser:
            trtl_cpu.enable()
            time.sleep(1)  # wait for all the characters

            nbyte = len(msg_ref[1])
            msg = ser.read(nbyte).decode()

            assert msg_ref[1] == msg
