/*
 * Copyright (c) 2016-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <mockturtle-framework.h>

static uint32_t period_c;
static unsigned int period = 1000000;

#define DG_BUF_SIZE (32)
static uint32_t dg_data[DG_BUF_SIZE];


struct dg_conf {
	uint32_t gain;
	uint32_t offset;
};
static struct dg_conf dg_conf;

static uint32_t dg_last_sample_idx;
static uint32_t dg_last_sample = 1;

enum dg_variable {
	DG_PERIOD_UPDATE = 0,
};

static struct trtl_fw_variable variables[] = {
	[DG_PERIOD_UPDATE] = {
		.addr = (void *)&period,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
};

enum dg_structures {
	DG_DATA = 0,
	DG_CONF,
};

struct trtl_fw_buffer buffers[] = {
	[DG_DATA] = {
		.buf = dg_data,
		.len = sizeof(dg_data),
	},
	[DG_CONF] = {
		.buf = &dg_conf,
		.len = sizeof(struct dg_conf)
	},
};


static void generate_sample(void)
{
	if (dg_last_sample == 0)
		dg_last_sample = 1;

	dg_last_sample++;
	if (dg_last_sample * dg_conf.gain < dg_last_sample)
		dg_last_sample = 1;
	dg_last_sample = (dg_last_sample * dg_conf.gain) + dg_conf.offset;


	pr_debug("%s:%d [%"PRIu32"d/%d] = %"PRIu32"u\n\r", __func__, __LINE__,
		 dg_last_sample_idx + 1, DG_BUF_SIZE, dg_last_sample);

	dg_data[dg_last_sample_idx] = dg_last_sample;

	dg_last_sample_idx++;
	dg_last_sample_idx &= (DG_BUF_SIZE - 1);
}

static void dg_update(void)
{
	if ((--period_c) == 0) {
		period_c = period;
		generate_sample();
	}
}


/**
 * Firmware initialization
 */
static int dg_init(void)
{
	period_c = period;
	dg_last_sample_idx = 0;
	dg_last_sample = 1;
	dg_conf.offset = 0;
	dg_conf.gain = 1;

	return 0;
}


/**
 * Well, the main :)
 */
static int dg_main()
{
	while (1) {
		/* Handle all messages incoming from HMQ 0 as actions */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);

		dg_update();
	}

	return 0;
}


struct trtl_fw_application app = {
	.name = "data-gen",
	.version = {
		.rt_id = CONFIG_RT_APPLICATION_ID,
		.rt_version = RT_VERSION(0, 1),
		.git_version = GIT_VERSION
	},

	.variables = variables,
	.n_variables = ARRAY_SIZE(variables),

	.buffers = buffers,
	.n_buffers = ARRAY_SIZE(buffers),

	.init = dg_init,
	.main = dg_main,
};
