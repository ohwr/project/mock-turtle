/*
 * Copyright (c) 2016-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <mockturtle-framework.h>

static unsigned int iteration;
static uint32_t period_c;
static unsigned int period = 1000000;
static unsigned int alarm_enable;
static uint32_t alarm_iter;

enum ac_variable {
	AC_TIME = 0,
	AC_PERIOD_UPDATE,
	AC_ALARM_EN,
	AC_ALARM_ITER,
};

static struct trtl_fw_variable variables[] = {
	[AC_TIME] = {
		.addr = (void *)&iteration,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
	[AC_PERIOD_UPDATE] = {
		.addr = (void *)&period,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
	[AC_ALARM_EN] = {
		.addr = (void *)&alarm_enable,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
	[AC_ALARM_ITER] = {
		.addr = (void *)&alarm_iter,
		.mask = 0xFFFFFFFF,
		.offset = 0,
		.flags = 0,
	},
};



static void ac_update(void)
{
	uint32_t sec, cyc;

	trtl_fw_time(&sec, &cyc);

	if ((--period_c) == 0) {
		period_c = period;
		trtl_fw_mq_send_uint32(TRTL_HMQ, 0, 0x12, 1,
				       iteration);
		pr_debug("Iteration %d\n\r", iteration);
	}

	if (alarm_enable) {
		if (alarm_iter < iteration) {
			trtl_fw_mq_send_uint32(TRTL_HMQ, 0, 0x34, 2,
					       iteration, alarm_iter);
			alarm_enable = 0;
			alarm_iter = 0;
		}
	}

	iteration++;
}


/**
 * Firmware initialization
 */
static int ac_init(void)
{
	period_c = period;

	return 0;
}


/**
 * Well, the main :)
 */
static int ac_main(void)
{
	while (1) {
		/* Handle all messages incoming from HMQ 0 as actions */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);

		ac_update();
	}

	return 0;
}


struct trtl_fw_application app = {
	.name = "alarm-clk",
	.version = {
		.rt_id = CONFIG_RT_APPLICATION_ID,
		.rt_version = RT_VERSION(0, 1),
		.git_version = GIT_VERSION
	},

	.variables = variables,
	.n_variables = ARRAY_SIZE(variables),

	.init = ac_init,
	.main = ac_main,
};
