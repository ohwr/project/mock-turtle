/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include <string.h>
#include <mockturtle-rt.h>
#include <fw-spec-common.h>
#include <mockturtle-framework.h>

struct trtl_fw_buffer spec_buffers[] = {
	[SPEC_BUF_CFG] = {
		.buf = &cfg,
		.len = sizeof(struct spec_cfg),
	}
};


static int spec_init()
{
	pp_printf("Booting %s ...\n", app.name);

	return 0;
}


/**
 * Well, the main :)
 */
static int spec_main()
{
	pp_printf("Running %s ...\n", app.name);
	while (1) {
		/* Handle all messages incoming from HMQ 0 as actions */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
	}

	return 0;
}

static int spec_exit()
{
	pp_printf("Closing %s ...\n", app.name);

	return 0;
}

struct trtl_fw_application app = {
	.name = "controller",
	.version = {
		.rt_id = RT_APPLICATION_ID,
		.rt_version = RT_VERSION(1, 0),
		.git_version = GIT_VERSION
	},

	.buffers = spec_buffers,
	.n_buffers = ARRAY_SIZE(spec_buffers),

	.init = spec_init,
	.main = spec_main,
	.exit = spec_exit,
};
