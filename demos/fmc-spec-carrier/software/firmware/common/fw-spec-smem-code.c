/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "mockturtle-rt.h"
#include "fw-spec-common.h"

SMEM volatile struct spec_cfg cfg;
#ifndef SIMULATION
SMEM volatile int autospec_led_period = 0x1FFFF;
SMEM volatile int autospec_print_period = 0xFFFFF;
#else
SMEM volatile int autospec_led_period = 0x1;
SMEM volatile int autospec_print_period = 0x7;
#endif
