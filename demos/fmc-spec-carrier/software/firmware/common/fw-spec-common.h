/*
 * Copyright (c) 2016-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#ifndef __FW_SVEC_COMMON_H__
#define __FW_SVEC_COMMON_H__
#include <mockturtle-rt.h>
#include <spec-common.h>

extern volatile struct spec_cfg cfg;
extern volatile int autospec_led_period;
extern volatile int autospec_print_period;

#endif
