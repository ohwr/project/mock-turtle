/*
 * Copyright (c) 2018-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */


#ifndef __LIBSPEC_H__
#define __LIBSPEC_H__
/** @file libspec.h */

#include <spec-common.h>

struct spec_node;

enum spec_error_list {
	__ESPEC_MIN_ERROR_NUMBER = 3200,
	ESPEC_INVALID_CONFIG = __ESPEC_MIN_ERROR_NUMBER,
	/*
	 * Add new messages here.
	 * The first one must be equal to __ESPEC_MIN_ERROR_NUMBER
	 */
	__ESPEC_MAX_ERROR_NUMBER,
};


/**
 * @defgroup util Generic Functions
 * Generic funcions to handle fmc-spec-carrier devices
 * @{
 */
extern const char *spec_btn_to_name(enum spec_btn_mode mode);
extern const char *spec_strerror(unsigned int error);
extern int spec_init();
extern void spec_exit();
extern struct spec_node *spec_open_by_id(uint32_t device_id);
extern void spec_close(struct spec_node *dev);
extern struct trtl_dev *spec_get_trtl_dev(struct spec_node *dev);
extern int spec_version(struct spec_node *dev, struct trtl_fw_version *version);
extern int spec_configuration_get(struct spec_node *dev, struct spec_cfg *cfg);
extern int spec_configuration_set(struct spec_node *dev, struct spec_cfg *cfg);
/**@}*/

#endif
