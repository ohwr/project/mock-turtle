/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN
 */

#include <mockturtle-framework.h>

static int frm_init()
{
	pp_printf("Hello world!\r\n");

	return 0;
}

/**
 * Well, the main :)
 */
static int frm_main()
{
	while (1) {
		/*
		 * Handle all messages incoming from slot 0
		 * as actions
		 */
		trtl_fw_mq_action_dispatch(TRTL_HMQ, 0);
	}

	return 0;
}


struct trtl_fw_application app = {
	.name = "hellofrm",
	.version = {
		.rt_id = CONFIG_RT_APPLICATION_ID,
		.rt_version = RT_VERSION(0, 1),
		.git_version = GIT_VERSION,
	},

	.init = frm_init,
	.main = frm_main,
};
