# SPDX-License-Identifier: LGPL-2.1-or-later
#
# SPDX-FileCopyrightText: 2019 CERN

# If it exists includes Makefile.specific. In this Makefile, you should put
# specific Makefile code that you want to run before this. For example,
# build a particular environment.
-include Makefile.specific

TRTL ?= ../../../..
TRTL_SW = $(TRTL)/software

# include parent_common.mk for buildsystem's defines
REPO_PARENT ?= $(TRTL)/..
-include $(REPO_PARENT)/parent_common.mk

DESTDIR ?= /usr/local

CFLAGS += -Wall -ggdb
CFLAGS += -I. -I../include -I../lib
CFLAGS += -I$(TRTL_SW)/include -I$(TRTL_SW)/lib
CFLAGS += $(EXTRACFLAGS)
LDFLAGS += -L../lib -L$(TRTL_SW)/lib
LDLIBS += -Wl,-Bstatic -lsvec -lmockturtle
LDLIBS += -Wl,-Bdynamic

PROGS := mockturtle-svec

all: $(PROGS)

install:
	install -d $(DESTDIR)/bin
	install -D $(PROGS) $(DESTDIR)/bin

mockturtle-svec: $(TRTL_SW)/lib/libmockturtle.a

$(TRTL_SW)/lib/libmockturtle.a:
	$(MAKE) -C $(@D)

# TODO add rules to compile your programs


# make nothing for modules_install, but avoid errors
modules_install:

clean:
	rm -f $(PROGS) *.o *~

cleanall: clean

.PHONY: all clean cleanall
