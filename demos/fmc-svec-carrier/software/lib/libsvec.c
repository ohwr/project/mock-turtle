/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

/*
 * This is just a SVEC, the code is not optimized
 */
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <mockturtle/libmockturtle.h>
#include <libsvec-internal.h>

const char *svec_errors[] = {
	"Received an invalid answer from white-rabbit-node-code CPU",
	"Real-Time application does not acknowledge",
};


/**
 * Return a string messages corresponding to a given error code. If
 * it is not a libwrtd error code, it will run trtl_strerror()
 * @param[in] err error code
 * @return a message error
 */
const char *svec_strerror(unsigned int err)
{
	if (err < ESVEC_INVALID_ANSWER_ACK || err >= __ESVEC_MAX_ERROR_NUMBER)
		return trtl_strerror(err);

	return svec_errors[err - ESVEC_INVALID_ANSWER_ACK];
}


/**
 * Initialize the SVEC library. It must be called before doing
 * anything else.
 * This library is based on the libmockturtle, so internally, this function also
 * run svec_init() in order to initialize the TRTL library.
 * @return 0 on success, otherwise -1 and errno is appropriately set
 */
int svec_init()
{
	int err;

	err = trtl_init();
	if (err)
		return err;

	return 0;
}


/**
 * Release the resources allocated by svec_init(). It must be called when
 * you stop to use this library. Then, you cannot use functions from this
 * library.
 */
void svec_exit()
{
	trtl_exit();
}


/**
 * Open a WRTD node device using ID ID
 * @param[in] device_id ID device identificator
 * @return It returns an anonymous svec_node structure on success.
 *         On error, NULL is returned, and errno is set appropriately.
 */
struct svec_node *svec_open_by_id(uint32_t device_id)
{
	struct svec_desc *svec;

	svec = malloc(sizeof(struct svec_desc));
	if (!svec)
		return NULL;

	svec->trtl = trtl_open_by_id(device_id);
	if (!svec->trtl)
		goto out;

	svec->dev_id = device_id;
	return (struct svec_node *)svec;

out:
	free(svec);
	return NULL;
}


/**
 * Close a SVEC device opened with one of the following function:
 * svec_open_by_id()
 * @param[in] dev device token
 */
void svec_close(struct svec_node *dev)
{
	struct svec_desc *svec = (struct svec_desc *)dev;

	trtl_close(svec->trtl);
	free(svec);
	dev = NULL;
}


/**
 * Return the TRTL token in order to allows users to run
 * functions from the TRTL library
 * @param[in] dev device token
 * @return the TRTL token
 */
struct trtl_dev *svec_get_trtl_dev(struct svec_node *dev)
{
	struct svec_desc *svec = (struct svec_desc *)dev;

	return (struct trtl_dev *)svec->trtl;
}

int svec_lemo_dir_set(struct svec_node *dev, uint32_t value)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	uint32_t fields[] = {SVEC_VAR_LEMO_DIR, value};

	return trtl_fw_variable_set(svec->trtl,
				    SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				    fields, 1);
}

int svec_lemo_set(struct svec_node *dev, uint32_t value)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	uint32_t fields[] = {SVEC_VAR_LEMO_SET, value,
			     SVEC_VAR_LEMO_CLR, ~value};

	return trtl_fw_variable_set(svec->trtl,
				    SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				    fields, 2);
}


/**
 * Convert the given LEDs value with color codification
 */
static uint32_t svec_apply_color(uint32_t value, enum svec_color color)
{
	uint32_t val = 0;
	int i;

	for (i = 0; i < PIN_LED_COUNT; ++i) {
		if (!((value >> i) & 0x1))
			continue;
		switch (color) {
		case SVEC_GREEN:
		case SVEC_RED:
			val |= (0x1 << (color + (i * 2)));
			break;
		case SVEC_ORANGE:
			val |= (0x3 << ((i * 2)));
			break;
		}
	}

	return val;
}


/**
 * Set LED's register
 */
int svec_led_set(struct svec_node *dev, uint32_t value, enum svec_color color)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	uint32_t real_value = svec_apply_color(value, color);
	uint32_t fields[] = {SVEC_VAR_LED_SET, real_value,
			     SVEC_VAR_LED_CLR, ~real_value};

	return trtl_fw_variable_set(svec->trtl,
				    SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				    fields, 2);
}


/**
 * Get the status of the SVEC program
 */
int svec_status_get(struct svec_node *dev, struct svec_status *status)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	uint32_t fields[] = {SVEC_VAR_LEMO_STA, 0,
			     SVEC_VAR_LED_STA, 0,
			     SVEC_VAR_LEMO_DIR, 0};
	int err;

	err = trtl_fw_variable_get(svec->trtl,
				   SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				   fields, 3);
	if (err)
		return err;

	status->lemo = fields[1];
	status->led = fields[3];
	status->lemo_dir = fields[5];

	return 0;
}

int svec_run_autosvec(struct svec_node *dev, uint32_t run)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	uint32_t fields[] = {SVEC_VAR_AUTO, run};

	return trtl_fw_variable_set(svec->trtl,
				    SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				    fields, 1);
}

int svec_version(struct svec_node *dev, struct trtl_fw_version *version)
{
	struct svec_desc *svec = (struct svec_desc *)dev;

	return trtl_fw_version(svec->trtl,
			       SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
			       version);
}

int svec_test_struct_get(struct svec_node *dev, struct svec_structure *test)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	struct trtl_tlv tlv = {
		.type = SVEC_BUF_TEST,
		.size = sizeof(struct svec_structure),
		.buf = test,
	};

	return trtl_fw_buffer_get(svec->trtl,
				  SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				  &tlv, 1);
}

int svec_test_struct_set(struct svec_node *dev, struct svec_structure *test)
{
	struct svec_desc *svec = (struct svec_desc *)dev;
	struct trtl_tlv tlv = {
		.type = SVEC_BUF_TEST,
		.size = sizeof(struct svec_structure),
		.buf = test,
	};

	return trtl_fw_buffer_set(svec->trtl,
				  SVEC_CPU_MANUAL, SVEC_CPU_MANUAL_HMQ,
				  &tlv, 1);
}
