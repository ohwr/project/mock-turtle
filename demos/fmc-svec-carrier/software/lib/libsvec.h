/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */


#ifndef __LIBSVEC_H__
#define __LIBSVEC_H__
/** @file libsvec.h */

#include <svec-common.h>

struct svec_node;

struct svec_status {
	uint32_t led;
	uint32_t lemo;
	uint32_t lemo_dir;
	uint32_t autosvec;
};

enum svec_error_list {
	__ESVEC_MIN_ERROR_NUMBER = 3200,
	ESVEC_INVALID_ANSWER_ACK = __ESVEC_MIN_ERROR_NUMBER,
	ESVEC_ANSWER_NACK,
	/*
	 * Add new messages here.
	 * The first one must be equal to __ESVEC_MIN_ERROR_NUMBER
	 */
	__ESVEC_MAX_ERROR_NUMBER,
};

enum svec_color {
	SVEC_GREEN = 0,
	SVEC_RED,
	SVEC_ORANGE,
};

/**
 * @defgroup util Generic Functions
 * Generic funcions to handle fmc-svec-carrier devices
 * @{
 */
extern const char *svec_strerror(unsigned int error);
extern int svec_init();
extern void svec_exit();
extern struct svec_node *svec_open_by_id(uint32_t device_id);
extern void svec_close(struct svec_node *dev);
extern struct trtl_dev *svec_get_trtl_dev(struct svec_node *dev);
extern int svec_led_set(struct svec_node *dev, uint32_t value,
			enum svec_color color);
extern int svec_lemo_set(struct svec_node *dev, uint32_t value);
extern int svec_lemo_dir_set(struct svec_node *dev, uint32_t value);
extern int svec_status_get(struct svec_node *dev, struct svec_status *status);
extern int svec_run_autosvec(struct svec_node *dev, uint32_t run);
extern int svec_version(struct svec_node *dev, struct trtl_fw_version *version);
extern int svec_test_struct_set(struct svec_node *dev,
				struct svec_structure *test);
extern int svec_test_struct_get(struct svec_node *dev,
				struct svec_structure *test);
/**@}*/

#endif
