/*
 * Copyright (c) 2014-2019 CERN (home.cern)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */


#ifndef __LIBSVEC_INTERNAL__H__
#define __LIBSVEC_INTERNAL__H__

#include <stdlib.h>
#include <libsvec.h>

#define SVEC_CPU_AUTO 0
#define SVEC_CPU_MANUAL 1
#define SVEC_CPU_MANUAL_HMQ 0

/**
 * Description of a fmc-svec-carrier device
 */
struct svec_desc {
	struct trtl_dev *trtl; /**< TRTL device associated */
	uint32_t dev_id; /**< fmc device id */
	uint32_t app_id; /**< Application id */
	uint32_t n_cpu; /**< Number of CPUs */
};

#endif
