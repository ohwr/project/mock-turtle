/*
 * SPDX-License-Identifier: LGPL-2.1-or-later
 * SPDX-FileCopyrightText: 2019 CERN
 */

#include <mockturtle-rt.h>

int main()
{
	pp_printf("Hello World!\r\n");
	return 0;
}
