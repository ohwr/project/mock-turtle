..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _tools:mockturtle-ping:

================
Mock Turtle Ping
================

The purpose of the Mock Turtle Ping application (*mockturtle-ping*) is
to be able to verify that a firmware is running. In addition, the
mockturtle-ping application provides information about the firmware
version running on a Mock Turtle soft-CPU. The tool only works with
firmware developed using :ref:`sw:fw:frm`.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
