..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _tools:mockturtle-smem:

=========================
Mock Turtle Shared Memory
=========================

The Mock Turtle Shared Memory application (*mockturtle-smem*) provides
access to the Mock Turtle shared memory. The user can read/write any location
of the shared memory using different access modes.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
