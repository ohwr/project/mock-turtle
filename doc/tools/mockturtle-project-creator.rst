..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`tools:mockturtle-project-creator`:

============================
Mock Turtle Project Creator
============================

The process of setting up a new Mock Turtle project can takes hours the
first time if you do not know already what it is expected from the user.
Fortunately, this process can be automated and there is not much
knowledge in it.

The Mock Turtle Project Creator (*mockturtle-project-creator*) is a
Python script that creates a basic Mock Turtle project. This project
should be used to start the development of your project.

The generated Mock Turtle project is based on a template and it
includes:

-  a project library on top of :ref:`sw:lnx:library`
   for the development of software support layer on Linux host;

-  a basic firmware based on :ref:`sw:fw:frm`;

-  Makefiles to compile the project

Optionally, the mockturtle-project-creator can generate a git repository
with some initial commits. This will give the possibility to rollback to
the skeleton whenever you want.

The tools will also create a basic ``trtl-memory.ld`` linker script that
you should modify to reflect your target CPU.
