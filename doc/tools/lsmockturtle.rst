..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _tools:lsmockturtle:

=================
Mock Turtle List
=================

The Mock Turtle List application (*lsmockturtle*) shows the list of
available Mock Turtle devices. Optionally it shows the configuration ROM
content of each device.
