..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`tools:mockturtle-firmware-loader`:

===========================
Mock Turtle Firmware Loader
===========================

The Mock Turtle Loader application (*mockturtle-firmware-loader*) allows
the user to load a firmware in a soft-CPU. It gives also the possibility
to dump the RAM content from a soft-CPU.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
