..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`tools:mockturtle-messages`:

====================
Mock Turtle Messages
====================

The Mock Turtle Messages application (*mockturtle-messages*) can be used
to sniff the traffic over a HMQ or to access the serial console of a
soft-CPU.

You can get the list of available Mock Turtle devices that you can access
with the command ``lsmockturtle``.
