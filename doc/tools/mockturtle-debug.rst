..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: none

.. _`tools:mockturtle-debug`:

====================
Mock Turtle Debugger
====================

The Mock Turtle Debugger agent (*mockturtle-debug.py*) is
used to debug a CPU with the cross gdb (*riscv32-elf-gdb*).

The debugger agent is a standalone gdb server that implements the
remote protocol.  It is standalone because it doesn't need any linux
device driver and relies of the Universal Access Library (UAL) to do
hardware accesses (See https://gitlab.cern.ch/cohtdrivers/ual).

You need to start the debug agent on the host that drives the Mock Turtle.

The debug agent will create a TCP socket and listen on it on port 3000 for
a connection from gdb.  It displays this message before listening::

  Waiting for connection on port 3000

At that point you can start the cross debugger on the host where you
have compiled your program::

  riscv32-elf-gdb ./myprog.elf

Then you have to connect to the debug agent (replace `address` by the name of
the target machine which runs the debug agent)::

  (gdb) target remote address:3000

It is possible to load the program directly, and you should load
before restarting a program to be sure about the state of it::

  (gdb) load

The execution is started with the `continue` command::

  (gdb) c

The usual gdb commands are available: you can set a breakpoint,
display the backtrace, inspect registers, display a variable, do a
single step, execute until the next line...  Refer to the gdb manual
or any gdb tutorial.
