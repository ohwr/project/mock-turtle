..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`sw:fw`:

======================
Firmware Development
======================

This section explains how to write firmware using the Mock Turtle API.

The Mock Turtle offers 2 API for the firmware development:
:ref:`sw:fw:lib` and :ref:`sw:fw:frm`.

.. graphviz::
   :align: center
   :caption: Mock Turtle Firmware Interfaces.

   digraph layers {
      concentrate=true;
      node [shape=rectangle, style=filled, penwidth=2];
      edge [dir=both]
      mt [shape=record, label="<f> framework | <l> library", fillcolor=lightblue, color=blue];
      usr [label="user firmware", fillcolor=lightgrey, color=gray];

      mt:f -> usr;
      mt:l -> usr;
   }

It is strongly recommended to use the library because it offers a set of
macros and functions that simplifies the access to Mock Turtle
resources and to external gateware cores. This will help mainly in
firmware development.

It is recommended to use the framework because it guides you in the
development by keeping you focused on your core logic without the
need to deal with Mock Turtle architecture details.

The framework usage, rather than precluding the user from using library
functions, is complementary to the library.

Of course, this framework provides more features and features cost space
and computation time. If you need more space (and you can't allocate more
memory) or you need much better performance: don't use this framework.

All the Mock Turtle firmware source code can be found in the directory
``/path/to/mockturtle/software/firmware/``.

Mock Turtle has a generic building system which can be used to produce
Mock Turtle firmware applications. What you have to do is to prepare a
file named ``TBuild``  next to your firmware source files. In this file
you have to specify what to build, for example::

    # Mandatory
    OBJS = source1.o
    OBJS += source2.o
    OBJDIR += some/local/directory
    OUTPUT = firmware-name

    # Optional (prefer default when possible)
    EXTRA_CFLAGS :=
    TRTL_LD_SCRIPT := myfirmware.ld
    TRTL_LD_DIR := path/to/linker-script/directory

Here the list of supported TBuild variables

OBJS
    (Mandatory) List of object files to generate from sources with
    the same name. The default is an empty variable, this means that it
    will not compile any source file.

OUTPUT
    (Mandatory) Final binary name (the firmware).

EXTRA_CFLAGS
    (Optional) Additional compiler options.

TRTL_LD_SCRIPT
    (Optional) Local path to the linker script.
    The default is the standard Mock Turtle linker script.

TRTL_LD_DIR
    (Optional) Path to the directory containing the ``trtl-memory.ld`` file.
    By default this is set to the source's directory

Another requirement for a successful build is a linker script file containing
the ``MEMORY`` command. This linker script file must be named
``trtl-memory.ld`` and its content should look like this::

    MEMORY
    {
    ram : ORIGIN = 0x00000000, LENGTH = 32768 - 2048
    stack : ORIGIN = 32768 - 2048, LENGTH = 2048
    smem : ORIGIN = 0x40200000, LENGTH = 65536
    }

Unless you are modifying the Mock Turtle core itself, the following values are
fixed: ``ORIGIN = 0x00000000`` for the *ram*, ``ORIGIN = 0x40200000`` for
the *smem*. The ``LENGTH`` value for *ram* depends on the CPU memory size on
which the firmware will run; the ``LENGTH`` value for *smem* depends on the
Mock Turtle shared memory size; both these values depend on the FPGA synthesis
configuration of the target Mock Turtle instance.

.. note::
   It is possible to add more linker script commands to ``trtl-memory.ld`` but
   then the behavior is undefined. If you need more linker script commands,
   please write your own linker script file and pass it to the build system
   by using ``TRTL_LD_SCRIPT``.

You can build such a firmware application by calling ``make`` from the
application directory (where the ``TBuild`` file is) like this::

  make -C <path-to-mockturtle-project>/software/firmware M=$PWD

Or alternatively, you can copy the following lines in a Makefile::

  TRTL_FW = $(TRTL)/software/firmware

  all:

  # Redirect all rules to MockTurtle
  %:
    $(MAKE) -C $(TRTL_FW) M=$(shell /bin/pwd) $@

Then, you will compile your application with the following command
from the application directory (where the ``TBuild`` file is)::

  make TRTL=<path-to-mockturtle-project>

Memory resources on Mock Turtle are very limited and the full framework
may take more space than needed. For this reason Mock Turtle has
*Kconfig* support which allows you to interactivly enable/disable both
library and framework. You should create a local ``Kconfig`` file in
your firmware directory; in this file you must include the generic
one from Mock Turtle.::

   mainmenu "Project Firmware Name"

   comment "Project specific configuration"

   # INCLUDE GENERIC Kconfig
   source "Kconfig.mt"

Configuration options are not documented here. For more details
use the help messages from Kconfig: run ``make menuconfig``
from your firmware directory.

Mock Turtle is using the *RISC-V* ISA, which means that your code must be
compiled for this instruction set. Mock Turtle uses the environment variable
``CROSS_COMPILE_TARGET`` to provide the path to the cross-compilation
toolchain. By default, Mock Turtle expects the cross-compilation toolchain
to be installed on your system and visible in ``PATH``. If this is not
the case you have to overwrite this variable.::

    export CROSS_COMPILE_TARGET=/path/to/toolchain/bin/riscv32-elf-

At this point you can call ``make(1)`` to build your firmware.

.. note:: If you do not know how to get the cross-compilation toolchain
   or you need to build your own, please have a look at the
   `soft-cpu toolchain`_ project on the `OHWR`_.

.. _`OHWR`: https://www.ohwr.org/
.. _`soft-cpu toolchain`: https://www.ohwr.org/project/soft-cpu-toolchains/wiki/wiki


.. toctree::
   :maxdepth: 2
   :caption: Contents

   firmware-library
   firmware-framework
