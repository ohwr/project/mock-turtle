..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: none

.. _`sw:lnx:library`:

==============================
The Mock Turtle Linux Library
==============================

The Mock Turtle Library for host system development handles all the Mock
Turtle features and it makes them available to the user. The aim of the
Mock Turtle library is to export all the Mock Turtle driver features to
user-space programs in a more user-friendly way without paying much in
terms of the flexibility that the driver offers.

The library layer covers all the driver features; for this reason, the
user should only use the library or the Python module. The user can
still access the Mock Turtle driver directly but it is strongly
discouraged.

Installation
============

Requirements
------------

The Mock Turtle library depends on:

- the standard C library and on;

- the Mock Turtle driver;

Compile And Install
-------------------

The Mock Turtle library can be installed in your environment by running::

        cd /path/to/mockturtle/software/lib
        make install

This will install both the static library and the shared object library.

When using the shared object library, you can skip the installation and
use the environment variable ``LD_LIBRARY_PATH`` to make the library
visible to your programs/libraries::

      $ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/mockturtle/software/lib

In order to include this library in your project you must include in your
source code:::

    #include <mockturtle/libmockturtle.h>

While in the compilation command you have to provide the following options
(e.g. GCC):::

    CC = /path/to/your/compiler
    CFLAGS += -I/path/to/mockturtle/software/lib
    CFLAGS += -I/path/to/mockturtle/software/include
    CFLAGS += -L/path/to/mockturtle/software/lib
    LDLIBS += -lmockturtle
    $(CC) $(CFLAGS) $(LDLIBS)

The example above assumes that you are compiling from the Mock Turtle
git repository or a copy of it; if on your environment libraries and header
files are in different locations, please adjust the above example accordingly.

Initialization
==============

The library initialization is done with the function :c:func:`trtl_init()`.
You must initialize the library before calling any library function.

.. doxygenfunction:: trtl_init

Once you are completely done with Mock Turtle you should properly
close the library by calling :c:func:`trtl_exit()`

.. doxygenfunction:: trtl_exit

Open And Close Devices
======================

In order to be able to handle a Mock Turtle device you must open it
with one of the following functions :c:func:`trtl_open`,
:c:func:`trtl_open_by_id` or :c:func:`trtl_open_by_lun`. All these
functions return a device token which is required by most Mock Turtle
functions. When you do not want to use anymore the device, you should
close it with :c:func:`trtl_close`.

.. doxygenfunction:: trtl_open

.. doxygenfunction:: trtl_open_by_id

.. doxygenfunction:: trtl_open_by_lun

.. doxygenfunction:: trtl_close

.. doxygenstruct:: trtl_dev

.. _`sw:lnx:lib:cpu`:

Mock Turtle Cores Management
============================

Library support for cores' management is limited to the firmware loading
(and dumping) and enabling/disabling of the cores.

The typical use of these functions is to load an executable file into
the soft-CPU. The following listing shows an example

.. highlight:: c
.. code:: c

   void progr_cpu(struct trtl_dev *trtl, unsigned int cpu_idx, char *file_name)
   {
       trtl_cpu_load_application_file(trtl, cpu_idx, file_name);
       trtl_cpu_enable(trtl, cpu_idx);
   }

.. highlight:: none

.. doxygenfunction:: trtl_cpu_load_application_raw

.. doxygenfunction:: trtl_cpu_dump_application_raw

.. doxygenfunction:: trtl_cpu_load_application_file

.. doxygenfunction:: trtl_cpu_dump_application_file

.. doxygenfunction:: trtl_cpu_enable

.. doxygenfunction:: trtl_cpu_disable

.. doxygenfunction:: trtl_cpu_is_enable

.. doxygenfunction:: trtl_cpu_reset_set

.. doxygenfunction:: trtl_cpu_reset_get

.. _`sw:lnx:lib:hmq`:

Host Message Queue and Messages
===============================

This library has a set of functions to handle HMQs and to send/receveice
messages to/from them.

Whenever you need to remove all the messages from the HMQ you can use
the function :c:func:`trtl_hmq_flush()`. The host system does not have
access to the RMQ. If what you want to achieve is a complete flush of
all the message queues (host and remote) you should do it on firmware
side so that the complete flush happens synchronously.

.. doxygenfunction:: trtl_hmq_flush

Furthermore, there are the functions to exchange messages with firmware
running on Mock Turtle. This API offers a set of functions
to allow users to send/receive synchronous/asynchronous messages.
This library does not have any knowledge about the message content.
It processes the header but the payload is transfered as is.
Any processing of the payload is left to the user. This is the rule
for most messages, but Mock Turtle also offers a set of special messages
which are completely handled internally by Mock Turtle, including their payload.

.. doxygenstruct:: polltrtl
   :members:

.. doxygenfunction:: trtl_msg_poll

.. doxygenfunction:: trtl_msg_sync

.. doxygenfunction:: trtl_msg_async_send

.. doxygenfunction:: trtl_msg_async_recv

Mock Turtle offers a set of special messages which can be used in
combination with the firmware framework to ease the development.
The idea behind these special messages is to offer an API for the most
common operations that you will perform with Mock Turtle. Of course,
you are always free to use the basic message exchange mechanism and
build on top of them your high level API.

.. doxygenfunction:: trtl_fw_ping

.. doxygenfunction:: trtl_fw_name

.. doxygenfunction:: trtl_fw_version

.. doxygenfunction:: trtl_fw_variable_set

.. doxygenfunction:: trtl_fw_variable_get

.. doxygenfunction:: trtl_fw_buffer_set

.. doxygenfunction:: trtl_fw_buffer_get


.. _`sw:lnx:lib:shm`:

Shared Memory
=============

The Mock Turtle shared memory is accessible also from the host.

Even though In some cases it is necessary to access the shared memory from the host,
this is not really encouraged because it may affect
the Mock Turtle determinism. This API is limited to the basic functions
to read and write: :c:func:`trtl_smem_write()`, :c:func:`trtl_smem_write()`.

.. doxygenfunction:: trtl_smem_read

.. doxygenfunction:: trtl_smem_write

.. doxygenenum:: trtl_smem_modifier

Utilities
=========

This library offers a set of handlers.

.. doxygenfunction:: trtl_strerror

.. doxygenfunction:: trtl_count

.. doxygenfunction:: trtl_list

.. doxygenfunction:: trtl_list_free

.. doxygenfunction:: trtl_name_get

.. doxygenfunction:: trtl_print_header

.. doxygenfunction:: trtl_print_payload

.. doxygenfunction:: trtl_print_message

.. doxygenfunction:: trtl_hmq_fd

.. doxygenenum:: trtl_error_number
