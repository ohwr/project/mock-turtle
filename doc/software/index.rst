..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: none

==============
The Software
==============

This chapter explains the Mock Turtle software architecture as well as
the necessary steps to develop software layers on top of the Mock Turtle
ones.

The following discussion assumes that the reader already has a general
understanding of :ref:`arch`

The Mock Turtle software stack consists of two main development domains:
:ref:`sw:fw` and :ref:`sw:lnx`
(libraries or applications).

The main objectives of the Mock Turtle software stack are:

-  to allow managing of the Mock Turtle cores from the host

-  to allow firmware to access Mock Turtle resources

-  to provide a communication infrastructure between firmware and host

-  to provide a communication infrastructure between remote nodes

:ref:`sw:fw` is necessary to make any Mock Turtle based system to work. In
the :ref:`sw:fw` section you will learn how to write a firmware using
the provided Mock Turtle API.

On the other hand, :ref:`sw:lnx` on the
host depends on your needs. If you need a customized control/monitor
infrastructure for firmware, then it is recommended to develop
your software support layer(s) on top of the Mock Turtle ones.

Keep in mind that :ref:`tools` can be used for basic control/monitor
operations. This means that for basic requirements you can directly use
the tools without developing any support layer.

We strongly recommend you to start the develpment of a new Mock Turtle
project by using the :ref:`tools:mockturtle-project-creator`.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   protocol
   index-lnx
   index-fw
