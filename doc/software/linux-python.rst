..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`sw:lnx:python`:

===============================
The Mock Turtle Python Support
===============================

The Mock Turtle Python Module (*PyMockTurtle*) is a Python module that
wraps the Mock Turtle library described in :ref:`sw:lnx:library`.

Most of the features that the Linux library provides are as well
provided by the PyMockTurtle module.

Installation
============

Requirements
------------

PyMockTurtle depends on:

- Python 3.x;

- Python ctype

- Mock Turtle Linux library ``libmockturtle.so`` installed;

- Mock Turtle Linux driver loaded;

.. note::
   The module has been tested with Python 3.5. In principle it should work
   as well on any 3.x version. Compatibility with Python 2.7 has not been
   verified. Open an issue if you find Python version incompatibilities.

Install
-------

You can use the Makefile to install PyMockTurtle module::

    cd /path/to/mockturtle/software/lib/PyMockTurtle
    make install

Alternatively, you can use the *distutil* script that takes care of the
module installation in your Python environment::

    cd /path/to/mockturtle/software/lib/PyMockTurtle
    python setup.py install

On a successful installation you should be able to import PyMockTurtle::

    import PyMockTurtle

The installation is not mandatory. What is really important is that
both the shared object library and the Python module are visible to
the Python interpreter. You can use the environment variables
``PYTHONPATH`` and ``LD_LIBRARY_PATH`` to make them visible::

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/mock-turtle-sw/lib
    export PYTHONPATH=$PYTHONPATH:/path/to/mock-turtle-sw/lib/PyMockTurtle

    python3
    >>> import PyMockTurtle

Distribution
------------

If you want to create a package for the distribution of this module, you
can use the ``sdist`` command::

    cd /path/to/mock-turtle-sw/lib/PyMockTurtle
    python setup.py sdist

This will create the dist directory in which you will find an archive
corresponding to the version declared in the setup.py script.


.. _`sw:lnx:py:use`:

PyMockTurtle Basic Usage
============================

The usage of this module is quite straight forward. The first thing
you have to to do is to create an instance for
:py:class:`PyMockTurtle.TrtlDevice`.
The instantiation process will autoconfigure the new object using the
information from the configuration ROM. This means that all the cores
(:py:class:`PyMockTurtle.TrtlCpu`) and the respective host message queues
(:py:class:`PyMockTurtle.TrtlHmq`) will be instantiated automaticaly::

  import PyMockTurtle

  trtl = PyMockTurtle.TrtlDevice(0x1)

At this point it should be enough to have a look at :ref:`sw:lnx:py:api`
to start using the object.

.. _`sw:lnx:py:api`:

The PyMockTurtle API
====================

Here you can find the complete *PyMockTurtle* API. PyMockTurtle
exports a set of objects used to handle Mock Turtle components.
Then, it exports a set of *ctype* data structures used to exchange
information with the Mock Turtle layers.

.. note::
  Since this Python module is nothing more than a wrapper on top of
  a C library, we suggest you to have a look at :ref:`sw:lnx:library`
  for a better understanding of this API

PyMockTurtle Objects
--------------------

.. autoclass:: PyMockTurtle.TrtlDevice
   :members:

.. autoclass:: PyMockTurtle.TrtlCpu
   :members:

.. autoclass:: PyMockTurtle.TrtlHmq
   :members:

.. autoclass:: PyMockTurtle.TrtlSmem
   :members:

PyMockTurtle Data Structures
----------------------------

.. autoclass:: PyMockTurtle.TrtlFirmwareVersion
   :members:

.. autoclass:: PyMockTurtle.TrtlMessage
   :members:

.. autoclass:: PyMockTurtle.TrtlHmqHeader
   :members:

.. autoclass:: PyMockTurtle.TrtlConfig
   :members:

.. autoclass:: PyMockTurtle.TrtlConfigMq
   :members:
