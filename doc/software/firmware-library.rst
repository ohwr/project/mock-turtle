..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: c

.. _`sw:fw:lib`:

====================================
The Mock Turtle Firmware Library
====================================

The Mock Turtle firmware library offers a set of macros and functions
for the basic interaction with Mock Turtle resources.
This API is available by including ``mockturtle-rt.h`` in your source file.::

      #include <mockturtle-rt.h>

We strongly recommend users to use this library to develop any Mock
Turtle firmware.

.. warning::

   Any firmware developed without this library will not receive any kind
   of support.

.. _`sw:fw:lib:mem`:

Read And Write Memory Locations
===============================

This firmware library offers a set of functions to read/write memory locations.

You can access local registers with :c:func:`lr_readl()` and
:c:func:`lr_writel()`.

You can access device peripherals with :c:func:`dp_readl()` and
:c:func:`dp_writel()`::

      #include <mockturtle-rt.h>

      /*
       * List of device peripheral cores offsets from the point of view
       * of the soft-cpu
       */
      #define DP_CORE_1 0x1000

      int main ()
      {
        int val;

        /* Read a register from the device peripheral 1 */
        dp_writel(0xBADCOFFE, DP_CORE_1 +0x4);
        val = dp_readl(DP_CORE_1 + 0x4);

        /* Read registers from the local soft-core */
        lr_writel(0xDEADBEEF, 0x4);
        val = lr_readl(0x4);
      }

.. doxygenfunction:: dp_readl

.. doxygenfunction:: dp_writel

.. doxygenfunction:: lr_readl

.. doxygenfunction:: lr_writel

Each Mock Turtle core has a group of GPIO lines which can be used to access
external signals. You can handle the GPIO with the functions
:c:func:`gpio_set()`, :c:func:`gpio_clear()`, :c:func:`gpio_status()`.::

    #include <mockturtle-rt.h>

    int main ()
    {
      int val;

      gpio_set(11); /* set BIT(11) to 1 */
      val = gpio_status(11);
      gpio_clear(11); /* set BIT(11) to 0 */
      val = gpio_status(11);
    }

.. doxygenfunction:: gpio_set

.. doxygenfunction:: gpio_clear

.. doxygenfunction:: gpio_status

All these functions are based on the generic :c:func:`readl()` and
:c:func:`writel()`.::

    #include <mockturtle-rt.h>

    int main ()
    {
      /* set GPIO 11 to 1 - using generic write */
      writel((1 << 11), TRTL_ADDR_LR(MT_CPU_LR_REG_GPIO_SET));
      /* set GPIO 11 to 1 */
      gpio_set(11);
    }

.. note::

   In order to keep your code clean and future proof, do not use generic
   functions when a specific one is available. The example above makes it
   evident.

.. doxygenfunction:: readl

.. doxygenfunction:: writel

.. _`sw:fw:lib:mq`:

Message Queues
================

Mock Turtle cores' main communication mechanism is the message queues.
The API is almost identical for both remote and host because most of these
functions have the *message queue type* argument to distinguish them.

You can handle the message queue with the commands: *claim*, *send*,
*discard*, *purge*. For each of these commands there is a function that
you can call to execute that command: :c:func:`mq_claim`, :c:func:`mq_send`,
:c:func:`mq_discard`, :c:func:`mq_purge`. Apart from performing active
actions on the message queue, sometimes we are interested only in their
status, expecialy when we want to know if the message queue input channel is
*not empty* (it means that there is something to read) or the output channel
is *not full* (it means that there space for writing). You can check the queue
status with :c:func:`mq_poll_in` and :c:func:`mq_poll_out`.

The API usage is different for input and for output.

The typical procedure to send (output) messages is the following.

#. *poll* the mq to see if there is at least an empty entry;

#. *claim* a mq in order to get exclusive access to it;

#. *map* the claimed mq slot in order to get the buffer where to write. Keep
   in mind that the memory is not initialized, so you probably need to set
   correctly all header fields;

#. *write* your message;

#. *send* the data, which will also release the mq slot;

Here is an example of how to send a message::

    #define HMQ_NUM 0
    struct trtl_fw_msg msg;
    uint32_t status;

    while ((mq_poll_out(TRTL_HMQ, 1 << HMQ_NUM)) == 0)
        ; /* wait until queue not full */

    mq_claim(TRTL_HMQ, HMQ_NUM);
    mq_map_out_message(TRTL_HMQ, HMQ_NUM, &msg);

    /* Play with the message queue entry */

    mq_send(TRTL_HMQ, HMQ_NUM);


The typical procedure to receive (input) messages is the following.

#. *poll* the mq to see if there is available data on a slot;

#. *map* the mq slot in order to get the slot buffer;

#. *read* your data from the mapped buffer;

#. *discard* the slot, which will erase the data and point to the next
   one;

Here is an example on how to receive a message::

    #define HMQ_NUM 0
    struct trtl_fw_msg msg;
    uint32_t status;

    while ((mq_poll_in(TRTL_HMQ, 1 << HMQ_NUM)) == 0)
        ; /* wait until queue not empty */

    mq_map_in_message(TRTL_HMQ, HMQ_NUM, &msg);

    /* Play with the message queue entry */

    mq_discard(TRTL_HMQ, HMQ_NUM);

The library does not perform any validation on the data you write in
the message. Any kind of overflow control is up to the user.

The size of the payload can be retrieved from the configuration rom using
:c:func:`trtl_config_rom_get`.

On the host you can read the messages using the tool :ref:`tools:mockturtle-messages`

.. doxygenenum:: trtl_mq_type

.. doxygenfunction:: mq_poll_in

.. doxygenfunction:: mq_poll_out

.. doxygenfunction:: mq_claim

.. doxygenfunction:: mq_send

.. doxygenfunction:: mq_purge

.. doxygenfunction:: mq_discard

.. doxygenstruct:: trtl_fw_msg
   :members:

.. doxygenfunction:: mq_map_out_message

.. doxygenfunction:: mq_map_in_message

These functions are enough to send and receive messages with both HMQ and RMQ.

The remaining functions listed below are actually used to implement
the ones above.

.. note::
   In principle, you should never use the lower level API. These functions
   are used to provide services for the higher level API

.. doxygenfunction:: trtl_mq_base_address

.. doxygenfunction:: mq_map_out_header

.. doxygenfunction:: mq_map_out_buffer

.. doxygenfunction:: mq_map_in_header

.. doxygenfunction:: mq_map_in_buffer


.. _`sw:fw:lib:shm`:

Shared Memory
=============

This is a collection of functions and macros whose purpose is:

- to read/write the Mock Turtle shared memory

- to perform atomic operations on the shared memory

In order to declare a variable in shared memory, instead of the local
soft-core RAM, you have to add ``SMEM`` before your variable declaration::

      #include <mockturtle-rt.h>

      SMEM int my_variable = 100;

Then you can use a shared memory variable as a normal variable::

      #include <mockturtle-rt.h>

      SMEM int my_variable = 100;

      int main ()
      {
        int b = 5;

        my_variable += 10; /* Not atomic operation */
        b = my_variable;
      }

The shared memory provides a set of atomic operations, to avoid race conditions
while different cores are writing. There is a dedicated API for such operations.

Here is an example that uses all of the available operations::

      #include <mockturtle-rt.h>

      SMEM int my_variable = 100;

      int main ()
      {
        int b = 5, t;

        smem_atomic_add(&my_variable, 10);
        smem_atomic_sub(&my_variable, 10);
        smem_atomic_or(&my_variable, 0xF0);
        smem_atomic_and_not(&my_variable, 0xF0);
        smem_atomic_xor(&my_variable, 0xF0);
        t = smem_atomic_test_and_set(&my_variable);
        b = my_variable;
      }

.. doxygenfunction:: smem_atomic_add

.. doxygenfunction:: smem_atomic_sub

.. doxygenfunction:: smem_atomic_or

.. doxygenfunction:: smem_atomic_and_not

.. doxygenfunction:: smem_atomic_xor

.. doxygenfunction:: smem_atomic_test_and_set

.. _`sw:fw:lib:uart`:

Serial Interface
=================

You can user the serial interface to print formatted string messages.

.. note::
   Even if it is potentially possible to use the serial interface to
   exchange binary data, this is not supported. The only supported use of
   the *Serial Interface* is to send strings to the host system.

This API is based on :c:func:`pp_printf` and its different flavors:
:c:func:`pr_error`, :c:func:`pr_debug`.

Here is an example on how to print over the serial interface::

      #include <mockturtle-rt.h>

      int main ()
      {
        pp_printf("Hello World\n");
        pr_debug("%s:%d something here\n", __func__, __LINE__);
        pr_error("%s:%d Error Message\n", __func__, __LINE__);
      }

On the host side you can read the serial messages using any tool that
can read a TTY interface (e.g. ``cat``, ``minicom``).

Since the standard ``printf`` function is heavy, Mock Turtle offers
a light implementation named :c:func:`pp_printf`. This function relays on
function :c:func:`puts()` to send the strings over the serial interface.

.. doxygenfunction:: pp_printf

.. doxygenfunction:: pr_debug

.. doxygenfunction:: pr_error

.. doxygenfunction:: pr_message

.. doxygenfunction:: putchar

.. doxygenfunction:: puts


Host Notification
=================

Mock Turtle has a mechanism that allows firmware to send arbitrary interrupts
to the host system. This mechanism is used by Mock Turtle software to deliver
special notifications; but this mechanism can be used as well by the user to
deliver custom notifications to their support layer.

The Mock Turtle notifications are enumerated
by :c:type:`trtl_cpu_notification`. The user must start their enumeration
after the value *__TRTL_CPU_NOTIFY_MAX*.

.. doxygenenum:: trtl_cpu_notification

Mock Turtle will deliver a notification to the host when the firmware calls
:c:func:`trtl_notify()` or :c:func:`trtl_notify_user()`. The latter is
suggested for users' notifications.

.. doxygenfunction:: trtl_notify

.. doxygenfunction:: trtl_notify_user

Miscellaneous
==============

The following is a list of miscellaneous, helper functions.

.. doxygenfunction:: delay

.. doxygenfunction:: trtl_config_rom_get

.. doxygenfunction:: trtl_get_core_id
