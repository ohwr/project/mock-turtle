..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. highlight:: none

.. _`sw:lnx:drv`:

=========================
The Linux Device Driver
=========================

The Mock Turtle device driver is a software component that exposes the
Mock Turtle gateware core to the host. Any interaction with the Mock
Turtle gateware core passes trought the device driver. This implies that
if the driver does not support a Mock Turtle feature, neither the other
layers (the library or the Python module) will do.

Requirements
==============

The Mock Turtle device driver has been developed and tested on Linux
3.6, 3.10 and 4.14. Other Linux versions might work as well but it is
not guaranteed.

The FPGA address space must be visible on the host system. This requires
a driver for the FPGA carrier that exports the FPGA address space to the
host.

Compile And Install
====================

The compile and install the Mock Turtle device driver simply execute ``make``::

      $ cd /path/to/mockturtle/software/kernel
      $ export KERNELSRC=/path/to/linux/sources
      $ make
      $ make install

Load Driver
=============

The Mock Turtle device driver module needs to be loaded in order for it to be
used::

      $ cd /path/to/mockturtle/software/kernel
      $ sudo insmod mock-turtle.ko

The following table lists the module parameters that can be used to customize
the driver instance.

.. list-table::
   :header-rows: 1
   :widths: 25 25 50

   * - Name
     - Default Value
     - Description
   * - hmq_buf_max_msg
     - 16
     - Maximum number of messages stored by the driver for each direction


Load Gateware
================

A Mock Turtle instance must already exist on the FPGA in order to be
able to drive it. Loading the gateware bitstream depends on the FPGA
carrier in use.  Therefore, the bitstream must be loaded to the FPGA
**before** adding a Mock Turtle device instance in the Linux kernel. If
not, the host will crash because the device driver will try to access
something that does not exist yet.

.. _integration:software:host:linux:platform:

Load Device
=============

The Mock Turtle device driver is based on the platform Linux subsystem
[1]_ . This means that you need a mechanism to load a platform device
that describes a Mock Turtle device. Typically, this mechanism involves
the development of a Linux module or a Device Tree Structure.

This driver handles all platform_device instances whose name is one of
the following: "mock-turtle", "mockturtle".

The Mock Turtle device driver expects five ``resources`` from the platform
device.

- memory address: The gateware core base address within the virtual address
  space.

- hmq IRQ input: The Linux IRQ number to use for the hmq input.

- hmq IRQ output: The Linux IRQ number to use for the hmq output.

- console IRQ: The Linux IRQ number to use for the serial interface.

- notification IRQ: The Linux IRQ number to use for Mock Turtle cores
  notifications.

Permissions
-----------

Mock Turtle does not uses any special group or user on the host system;
all driver's files are owned by ``root:root``. Mock Turtle applications may
want to change this and assign special users, groups and permission. We suggest
to do this using ``udev(7)`` rules. To filter out a special application in
udev rules you should use the sysfs attribute ``application_id``. For example
the following rule matches the Mock Turtle device with application ID
``0xbadc0ffe``::

  SUBSYSTEM=="mockturtle", KERNEL=="trtl-[0-9a-zA-Z][0-9a-zA-Z][0-9a-zA-Z][0-9a-zA-Z]", ATTR{application_id}="0xbadc0ffe"

Interfaces
==========

.. _`sw:drv:dev`:

Mock Turtle Device
------------------

The Mock Turtle driver exports a *char device* for each Mock Turtle.
In */dev/mockturtle* you will have devices named *trtl-%04x*
(trtl-<device-id>). This exports a set of ``ioctl(2)`` commands:

.. doxygendefine:: TRTL_IOCTL_SMEM_IO
   :outline:

You can find the *sysfs* attributes for each instance of Mock Turtle at::

  /sys/class/mockturtle/trtl-%04x/

.. list-table::
   :align: center
   :header-rows: 1
   :widths: 25 25 55

   * - Name
     - Direction
     - Description
   * - config-rom
     - RO
     - Binary data containing all the synthesis configuration
   * - reset_mask
     - RW
     - Set or clear the soft-CPUs reset. It is a bit-mask where each bit
       correspond to a soft-CPU (e.g. bit 0 -> soft-CPU 0)

.. _`sw:drv:core`:

Mock Turtle Cores
-----------------

The Mock Turtle driver exports a *char device* for each Mock Turtle
core.  All core instances will appear as children of a
:ref:`sw:drv:dev`; in */dev/mockturtle* you will have devices named
*trtl-%04x-%02d``* (trtl-<device-id>-<cpu-index>). The main purpose of
this interface is to program firmware into cores, or, dump the firmware
that is already loaded.

These devices are
bidirectional, so you can: ``write(2)`` to program a firmware, ``read(2)``
to dump a firmware; ``lseek(2)`` to move to different memory locations.

From the command line you can load a new program using ``dd(1)`` or similar
tools.::

    dd if=firmware.bin of=/dev/mockturtle/trtl-0001-00

The same command can also be used to dump the memory contents::

    dd if=/dev/mockturtle/trtl-0001-00 of=firmwaredump.bin

In both cases (loading and dumping) the driver automatically puts the core
in *reset* state.
This means that after your operation you need to *unreset* the
core in order to make it running again::

    echo 0 > /sys/class/mockturtle/trtl-0001-00/reset

.. _`sw:drv:serial`:

Mock Turtle uses the standard TTY layer from the Linux kernel. Each
core has a dedicated serial interface which is used for communications
from soft-CPU to host.

Linux TTY devices appear in the */dev* directory and they are named
*ttytrtl-%04x-%d*.

Since it is a standard TTY interface you can use the tool you like to read it.
For example::

    minicom -D /dev/ttytrtl-0001-00
    cat /dev/ttytrtl-0001-00

.. note::

   The driver does not perform any data processing on the console. In other
   words whatever the firmware application writes is replicated on this
   interface.

You can find the *sysfs* attributes for each core at::

  /sys/class/mockturtle/trtl-%04x/trtl-%04x-%02d-%02d

.. list-table::
   :align: center
   :header-rows: 1
   :widths: 25 25 55

   * - Name
     - Direction
     - Description
   * - reset
     - RW
     - It asserts (1) or de-asserts (0) the soft-CPU reset line
   * - last_notification
     - RO
     - It shows the last notification ID received
   * - notification_history
     - RO
     - It shows le last 128 notification IDs received

Host Message Queue
--------------------

The Mock Turtle driver exports a *char device* for each Mock Turtle HMQ.
All HMQ instances will appear as childred of a :ref:`sw:drv:core`; in
*/dev/mockturtle/* you will have devices named ``trtl-%04x-%02d-%02d``
(trtl-<device-id>-<cpu-index>-<hmq-index>). The main purpose of this
interface is to exchange messages.

These devices are bidirectional,
so you can: ``write(2)`` to send messages; ``read(2)`` to receive messages;
``poll(2)`` or ``select(2)`` to wait for the interface to become accessible.

This *char device* provides a set of ``ioctl(2)`` commands:

.. doxygendefine:: TRTL_IOCTL_HMQ_SYNC_SET

.. doxygendefine:: TRTL_IOCTL_MSG_SYNC_ABORT

You can find the HMQ *sysfs* attributes at::

  /sys/class/mockturtle/trtl-%04x/trtl-%04x-%02d-%02d

.. list-table::
   :align: center
   :header-rows: 1
   :widths: 25 25 55

   * - Name
     - Direction
     - Description
   * - empty
     - RO
     - It shows the input and output empty status:
       1 when the HMQ channel buffer is empty, 0 otherwise (hardware)
   * - full
     - RO
     - It shows the input and output full status:
       1 when the HMQ channel buffer is full, 0 otherwise (hardware)
   * - occupied
     - RO
     - The number of entries in the queue for input and output
   * - discard_all
     - WO
     - When written it flushes the HMQ from all pending messages
   * - statistics/message_received
     - RO
     - Total number of messages received through the HMQ channel
   * - statistics/message_sent
     - RO
     - Total number of messages sent through the HMQ channel

Debugging Interface
-------------------

The driver exports on debugfs a file in YAML format which contains
internal information about the driver: variable values, register
values. This file is named as the Mock Turtle instance that it
represents ("trtl-%04x")::

    mount -t debugfs none /sys/kernel/debug
    cat /sys/kernel/debug/trtl-0001/info

This is typically used by driver developers for debugging purposes.

.. warning:: The contents of the YAML file are not stable and may
   change at any time. Do not consider this as a stable interface.

Then, there is a debugfs file for each Mock Turtle instance that can be
used to access the CPU debug registers. These files are named using the
following format "trtl-%0x4x-dbg". These files can be accessed only with
``mmap(2)`` and typically the user does not need to use it directly, instead
the user should use the :ref:`Mock Turtle GDB Server <tools:mockturtle-gdbserver>`.

.. [1] https://www.kernel.org/doc/Documentation/driver-model/platform.txt
