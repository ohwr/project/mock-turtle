..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _`demo:svec`:

====================
The *FMC SVEC* Demo
====================

The *FMC SVEC* demo is a complete demo that uses hardware features from
the `FMC SVEC carrier`_. This demo offers an example of all layers, so it is
a good starting point to understand how to create a complete Mock Turtle
application. Apart from the board itself, no other hardware is necessary to
run the demo.

The main aim of this demo is to handle the SVEC LEDs and LEMOs. The LEDs can be
turned *on* (*red*, *green*, *orange*) and *off*. The LEMOs can be set to
*input* or *output*; when output they can be set to *high* or *low* voltage;
when input it is possible to read their status (*high* or *low*).

HDL Code
==========

The top-level VHDL entity of the demo can be found under
*hdl/top/svec_mt_demo/svec_mt_demo.vhd*, while an `Hdlmake`_ project file
(able to produce an FPGA bitstream of the demo) is available under
*hdl/syn/svec_mt_demo/Manifest.py*.

The SVEC demo defines the following :ref:`hdl:cfg`::

  constant c_MT_CONFIG : t_mt_config := (
    app_id     => x"d330d330",
    cpu_count  => 2,
    cpu_config => (others =>
		   (memsize => 8192,
		    hmq_config => (2, (0      => (7, 3, 2, x"0000_0000"),
				       1      => (5, 4, 3, x"0000_0000"),
				       others => (c_DUMMY_MT_MQUEUE_SLOT))),
		    rmq_config => (1, (0      => (7, 2, 2, x"0000_0000"),
				       others => (c_DUMMY_MT_MQUEUE_SLOT))))),
    shared_mem_size => 2048);

The above configuration instantiates two soft CPUs, each with two host
message queues (of different sizes) and one remote message queue.

The SVEC demo :ref:`hdl:inst` is done using the following VHDL code::

  U_Mock_Turtle : mock_turtle_core
    generic map (
      g_CONFIG            => c_MT_CONFIG,
      g_WITH_WHITE_RABBIT => FALSE)
    port map (
      clk_i           => clk_sys,
      rst_n_i         => rst_n_sys,
      dp_master_o     => dp_wb_out,
      dp_master_i     => dp_wb_in,
      host_slave_i    => cnx_master_out(c_SLAVE_MT),
      host_slave_o    => cnx_master_in(c_SLAVE_MT),
      rmq_src_o       => rmq_ds_o,
      rmq_src_i       => rmq_ds_i,
      rmq_snk_o       => rmq_us_o,
      rmq_snk_i       => rmq_us_i,
      hmq_in_irq_o    => mt_hmq_in_irq,
      hmq_out_irq_o   => mt_hmq_out_irq,
      notify_irq_o    => mt_notify_irq,
      console_irq_o   => mt_console_irq);

All unconnected inputs will get their default values.

The Wishbone host interface is attached to a Wishbone crossbar, and from
there to the VME64x host interface. All interrupt lines are driven into a
Vectored Interrupt Controller (VIC). The remote message queue interfaces
are simply forming a loopback (for testing).

For each one of the two configured soft CPUs, their respective DP interface
is attached to a 24-bit Wishbone GPIO peripheral. The outputs from the two
GPIO peripherals are logically OR'ed, while their are copies of the same
signals. The mapping of these 24 signals is the following:

+ GPIO0 to GPIO3: 4 LEMO I/O connectors on the front panel of the SVEC
+ GPIO4 to GPIO7: not used
+ GPIO8 to GPIO23: control the 8 bi-color LEDs on the front panel of the SVEC

All mentioned peripherals (WB crossbar, VIC, WB GPIO) are available as part of `OHWR
general-cores`_. This demo also uses the `VME64x core`_ to provide the host interface.

Software
=========

This demo uses two firmware programs. One is named *autosvec* (fw-01), the other
*manualsvec* (fw-02).

The *autosvec* firmware runs autonomously without any communication with the host
system or a remote node and for this reason it is the simplest one.
It does not use :ref:`sw:fw:frm` but only :ref:`sw:fw:lib`. This firmware does
the following things:

- it turns *on* and *off* all the LEDs one after the other;
- it reproduces on LEMO connector 2 whatever state is on LEMO connector 1
- it generates square signals on LEMO connectors 3
- it generates square signals on LEMO connectors 4
- it periodically prints messages on the console with the GPIO status (LEDs
  and LEMOs)

.. highlight:: c
.. literalinclude:: ../../demos/fmc-svec-carrier/software/firmware/fw-01/fw-svec.c

The *manualsvec* firmware offers a manual control of all LEDs and LEMOs.
It does use :ref:`sw:fw:frm`. This firmware does the following things:

- it exports as :ref:`variables <sw:fw:frm:var>` the device peripheral registers
  to configure LEDs and LEMOs
- it exports a local :ref:`buffer <sw:fw:frm:buf>` where the user can read and write
  (it is not used)
- it exports a local :ref:`variable <sw:fw:frm:var>` that can be used to
  stop/start an *autosvec* firmware running on a different core.

.. note::
   Remember that the SVEC connects LEMO 3 and LEMO 4 to the same GPIO port.
   This means that they must have the same direction (both input, or both
   output)

.. highlight:: c
.. literalinclude:: ../../demos/fmc-svec-carrier/software/firmware/fw-02/fw-svec.c

This firmware also provides a support layer to the host side. This is not really
necessary because you can always use the generic Mock Turtle tools
to :ref:`read/write variables <tools:mockturtle-variable>` and
to :ref:`read/write buffers <tools:mockturtle-buffer>`; but for the sake of
making this demo as complete as possible we added a host support layer which is
made of a C library and a C program. Apart from the standard operations to open
and close a device, the library exports an API to handle the status of the LEDs and LEMOs
and to set/get a dummy data structure. This library is mainly a
wrapper around the Mock Turtle one.

.. highlight:: c
.. literalinclude:: ../../demos/fmc-svec-carrier/software/lib/libsvec.c

Last but not least, there is the host program. This program is a command line tool that uses the
svec library described above to handle the SVEC board. Again, it gives users
the possibility to play with the status of the LEDs and LEMOs.

.. highlight:: c
.. literalinclude:: ../../demos/fmc-svec-carrier/software/tools/mockturtle-svec.c


.. _`FMC SVEC carrier`: https://www.ohwr.org/project/svec/
.. _Hdlmake: https://www.ohwr.org/project/hdl-make/wiki
.. _VME64x core: https://www.ohwr.org/project/vme64x-core/wiki
.. _OHWR general-cores: https://www.ohwr.org/project/general-cores/wiki
