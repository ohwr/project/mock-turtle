..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

.. _arch:

===============================
The Mock Turtle Architecture
===============================

The Mock Turtle framework offers a complete and integrated stack from
the HDL core to the software application.

The following figure shows an overview of the Mock Turtle architecture.

.. figure:: img/mock-turtle-overview.svg
   :align: center

   Mock Turtle Architecture Overview.

The blue and orange blocks are Mock Turtle components (respectively
software and gateware cores).  Grey blocks are external components
(gateware cores or software) developed by the user. Purple represents
any communication to the external world over the network.

This chapter tries to provide an overview of what Mock Turtle offers and
what is important to know when designing a Mock Turtle application.

For more information, please read the dedicated chapters for the
different parts.


Mock Turtle Core
==================

Mock Turtle can have one or more cores. A single core is made of the
following components: soft-CPU, Serial console and message queues.

Soft-CPU
---------

Mock Turtle uses the `uRV processor`_, a `RISC-V`_ ISA implementation. This is
just a CPU without any sort of integrated peripheral. This is where the firmware
runs. Any kind of bus controller, or device must be connected externally as a
:ref:`device peripheral<arch:dp>` and driven from the firmware.

The memory size for code and data is :ref:`configurable at synthesis time <hdl:cfg>`.

.. _uRV processor: https://www.ohwr.org/project/urv-core
.. _`RISC-V`: https://riscv.org/

For more information on how to handle cores from software, please read:

- :ref:`Linux Library - Cores Management<sw:lnx:lib:cpu>`

Serial Console
--------------

Each core has a serial console connected to the host system. This link is
unidirectional from core to host. Whenever there is a pending character in
a serial buffer, Mock Turtle raises an interrupt to the host.

.. graphviz::
   :align: center
   :caption: Example of Mock Turtle Serial Connection.

   digraph layers {
      concentrate=true;
      node [shape=rectangle, style=filled, penwidth=2, fillcolor="#e3b68b", color="#ee6103"];

      host [label="Host", fillcolor=lightblue, color=blue];
      fw1 [label="Firmware 1", fillcolor=lightblue, color=blue]
      fw2 [label="Firmware 2", fillcolor=lightblue, color=blue]
      fw3 [label="Firmware 3", fillcolor=lightblue, color=blue]

      fw1 -> "Serial 1" -> host;
      fw2 -> "Serial 2" -> host;
      fw3 -> "Serial 3" -> host;
   }

This is used to send string messages from the running firmware to the
host system.

For more information on how to access the serial console from the
firmware and how to read it on host side, please read:

- :ref:`Firmware Library - Serial Interface<sw:fw:lib:uart>`
- :ref:`Linux Driver - Read Serial Console<sw:drv:serial>`

Message Queue
----------------

Firmware running on Mock Turtle can communicate with external agents using
*message queues*; as the name suggests, these are message queues with
FIFO priority. Each soft-CPU has two sets of private message queues:
one set is for host communication (*host message queue*), the other one is
for external communication (*remote message queue*). All message queues
are bidirectional (one queue per direction).

A message queue entry is split in two fixed size buffers: header and
payload.  The header buffer should be used to store a protocol header,
while the payload buffer should be used to store the message content to
be exchanged.

.. figure:: img/mock-turtle-mq.svg
   :align: center
   :scale: 70%

   Mock Turtle Message Queue Overview.

.. note::
   Header and payload are conventions, nothing prevents users from using them
   otherwise. But remember that this convention is used in the Mock Turtle
   software APIs as described above.

All Message queue dimensions are fixed and
:ref:`configured at synthesis time <hdl:cfg>`. These dimensions apply to both
input and output queues:

   - maximum entries number
   - maximum header size
   - maximum payload size

Host message queues are connected to the host system. The host receives
an interrupt whenever an input queue contains at least one message;
while for output it receives an interrupt when a queue has at least one
free entry.

Remote message queues are not handled by the host system. They must be
connected to an *end-point* that provides a connection to the external
world.  Their task is to pack and unpack messages according to the type
of network they are connected to. The end-point implementation is
application specific and outside the scope of Mock Turtle. Mock Turtle
offers a set of generic end-points that you can use.

For more information on how to access message queues from the firmware,
please read:

- :ref:`Firmware Library - Message Queue<sw:fw:lib:mq>`

For more information on how to access message queues from user space,
please read:

- :ref:`Linux Library - Message Queue<sw:lnx:lib:hmq>`

Shared Memory
================

Mock Turtle offers a *shared memory* block, accessible from the host
system as well as from soft-CPU cores. This can be used to share data
among all actors.  Since access to the shared memory is serialized, an
intensive use of it can affect the determinism.

.. graphviz::
   :align: center
   :caption: Mock Turtle Shared Memory.

   graph layers {
      concentrate=true;
      node [shape=rectangle, style=filled, penwidth=2, fillcolor=lightblue, color=blue];
      edge [dir=both]

      shm [label="Shared Memory", fillcolor="#e3b68b", color="#ee6103"];

      "Host" -- shm;
      shm -- "Firmware 1";
      shm -- "Firmware 2";
      shm -- "Firmware 3";
   }


The shared memory size can be :ref:`configured at synthesis time <hdl:cfg>`
but it cannot exceed 64KiB.  Its address space is mirrored into multiple
address ranges (contiguous), each responsible for a single atomic
operation.

For more information on how to access the shared memory from the firmware, please read:

- :ref:`Firmware Library - Shared Memory<sw:fw:lib:shm>`

For more information on how to access the shared memory from user space, please read:

- :ref:`Linux Library - Shared Memory<sw:lnx:lib:shm>`

.. _`arch:dp`:

Device Peripherals
==================

Device peripherals are external components connected to a Mock Turtle
core over a Wishbone interface.  What they do, how many they are and how
they are connected is application specific: Mock Turtle just offers
connections to cores.

Once a device peripheral is connected to a Mock Turtle core, firmware
running on that core can access the peripheral by performing
reads/writes over Wishbone.

Note that device peripherals are not directly accessible from user
space. Only the firmware can access them.

.. graphviz::
   :align: center
   :caption: Example of Mock Turtle Device Peripheral Connection.

   graph layers {
      concentrate=true;
      node [shape=rectangle, style=filled, penwidth=2, fillcolor=lightgray, color=gray];

      c1 [label="soft-CPU 1", fillcolor="#e3b68b", color="#ee6103"];
      c2 [label="soft-CPU 2", fillcolor="#e3b68b", color="#ee6103"];

     subgraph core2 {
          c2 -- "Custom Logic";
     }
     subgraph core1 {
          graph [nodesep=1];
          c1 -- crossbar -- "I2C master";
          c1 -- crossbar -- "SPI master";
     }

     {rank=same; c1; c2}
     {rank=same; "Custom Logic"; "I2C master"; "SPI master"}
   }

For more information about how to access device peripherals from the
firmware, please read:

- :ref:`Firmware Library - Memory Location<sw:fw:lib:mem>`
