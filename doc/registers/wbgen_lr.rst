..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

:orphan:

.. _wbgen_lr:

Local Registers (LR)
====================

This is a list of all the local registers, which are accessible from the
soft-CPUs. Each soft CPU has its own copy of the LR.

.. raw:: html
  :file: wbgen/mt_cpu_lr.html

.. raw:: latex
  :file: wbgen/mt_cpu_lr.tex
