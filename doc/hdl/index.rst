..
  SPDX-License-Identifier: CC-BY-SA-4.0+
  SPDX-FileCopyrightText: 2019 CERN

===============
The HDL Core
===============

.. contents::

The VHDL top-level entity of Mock Turtle (MT), to be included in any HDL design that uses it, can be
found under *hdl/rtl/mock_turtle_core.vhd*.

Complete examples of MT instantation are available in the :ref:`demo projects<demo>`.

HDL Dependencies
================

MT depends heavily on `OHWR general-cores`_. Furthermore, if `White Rabbit support`_ is enabled, MT
will also require the `White Rabbit PTP Core`_. Last but not least, each one of the soft-CPUs inside
the MT is an instance of a `uRV processor`_.

.. highlight:: vhdl

As an absolute minimum, for `Mock Turtle Configuration`_ and `Mock Turtle Instantiation`_, users
will need to use the following packages in their top-level design file::

  -- from General Cores
  use work.gencores_pkg.all;
  use work.wishbone_pkg.all;
  -- Local MT packages
  use work.mock_turtle_pkg.all;
  use work.mt_mqueue_pkg.all;

Users will probably add other application-specific packages to the above list.

For :ref:`demo projects<demo>`, where HDL is involved, dependencies are
handled by `Hdlmake`_, through the use of `Git submodules`_, located
under *hdl/ip_cores/*. To view a list of currently used submodules, along
with their versions (Git SHA hashes), issue from anywhere in the MT folder
structure the command: ``git submodule``.

.. highlight:: none

The output of this command should look something like this::

 f692bc83f42ffd54496f9e0da571c6f9dfcf2335 hdl/ip_cores/ddr3-sp6-core (v2.0.1)
 b84c76be1eee1e987a0ff9947b5627d58e935f3f hdl/ip_cores/general-cores (Master)
 d265dc5d0cbfe84bd5d310982e1a319a26bb53b2 hdl/ip_cores/gn4124-core (v3.1.1)
 134759b20e8fa5241d3a3424393c6fbdfb66c6df hdl/ip_cores/urv-core (v0.9-32-g134759b)
 a0ca042e1f3c19a81c3594477c0c811ac761aaa4 hdl/ip_cores/vme64x-core (v2.2)
 63d6e85c292e57360fe106acfd08de66d3c0acb7 hdl/ip_cores/wr-cores (wrpc-proposed-master)
 c91efa5e378b0b65a0f4fff9729079007d99ba57 hdl/ip_cores/svec (v3.0.0)
 affb718e16fd0336f262441bf9f7ae7e570d55c6 hdl/ip_cores/spec (v3.0.0)
 65e80de29a85668bdf2f60539a413fe2889b5141 hdl/ip_cores/urv-core (urv-v1.1)


.. important::
   Even if you do not intend to follow the same approach in your project, you
   should use the ``git submodule`` command above to get a list of
   working/compatible versions for the various HDL dependencies.

.. _`hdl:cfg`:

Mock Turtle Configuration
=========================

As seen in `Mock Turtle Instantiation`_, an MT instance expects a VHDL record with the complete
configuration of the MT. Configurable aspects of the MT include:

- An ID for the instance/application
- Number of soft CPU cores
- Size of shared and per-CPU memories
- Number and dimensions of host and remote message queues per CPU

Details about the meaning of the fields of this VHDL record can also be found in :ref:`arch`.

All MT configuration values are also accessible at run-time from the configuration ROM of the MT.

VHDL Configuration Record
-------------------------

.. highlight:: vhdl

This VHDL record is of type **t_mt_config**, and it is defined in the **mock_turtle_pkg**::

  type t_mt_config is record
    app_id          : std_logic_vector(31 downto 0);
    cpu_count       : natural range 1 to 8;
    cpu_config      : t_mt_cpu_config_array;
    -- shared memory size, in words
    shared_mem_size : integer range 256 to 65536;
  end record t_mt_config;

As described in the comments, shared memory size is in 4-byte words. The *cpu_config* field contains
information about each CPU core, is of type **t_mt_cpu_config_array** and is defined as::

  subtype t_maxcpu_range is natural range 0 to 7;

  type t_mt_cpu_config_array is array(t_maxcpu_range) of t_mt_cpu_config;

It is essentialy an array of 8 **t_mt_cpu_config**, which in turn is defined as::

  type t_mt_cpu_config is record
    -- CPU memory sizes, in words
    memsize    : natural;
    -- Per CPU message queue config.
    hmq_config : t_mt_mqueue_config;
    rmq_config : t_mt_mqueue_config;
  end record t_mt_cpu_config;

Per-CPU memory size is in 4-byte words. Yet another VHDL record, of type **t_mt_mqueue_config**
(defined in **mt_mqueue_pkg**), is used for the configuration of the host and remote message queues
per CPU::

  type t_mt_mqueue_config is record
    --  IN and OUT slots are always peered.
    slot_count  : integer;
    slot_config : t_mt_mqueue_slot_config_array;
  end record t_mt_mqueue_config;

In this context, a *slot* is a bidirectional queue. Therefore, *slot_count* is the number of queues
for that CPU. *slot_config* is of type **t_mt_mqueue_slot_config_array**, which is defined as::

  subtype t_maxslot_range is natural range 0 to 7;

  type t_mt_mqueue_slot_config_array is
    array(t_maxslot_range) of t_mt_mqueue_slot_config;

It is essentialy an array of 8 **t_mt_mqueue_slot_config**, which in turn is defined as::

  type t_mt_mqueue_slot_config is record
    entries_bits : natural;
    width_bits   : natural;
    header_bits  : positive;
    endpoint_id  : std_logic_vector(31 downto 0);
  end record t_mt_mqueue_slot_config;

The meaning of these fields is explained in :ref:`arch`. Do note that *entries_bits*, *width_bits*
and *header_bits* express number of bits; if *entries_bits* is set to 7, there will be 2\ :sup:`7` =
128 entries, if *width_bits* is set to 2 the width of each entry will be 2\ :sup:`2` = 4, and so on.

Default Configuration
---------------------

If a **t_mt_config** is not provided, a default one will be used::

  constant c_DEFAULT_MT_CONFIG : t_mt_config :=
  (
    app_id          => x"115790de",
    cpu_count       => 2,
    cpu_config      => (others => (8192,
			      c_MT_DEFAULT_MQUEUE_CONFIG,
			      c_MT_DEFAULT_MQUEUE_CONFIG)),
    shared_mem_size => 2048
    );

The default configuration instantiates two soft CPUs, each with 32KiB of memory, plus 8KiB of shared
memory. Each CPU will have default host and remote message queue configurations defined as::

  constant c_MT_DEFAULT_MQUEUE_CONFIG : t_mt_mqueue_config :=
    (1, ((7, 3, 2, x"0000_0000"), others => (c_DUMMY_MT_MQUEUE_SLOT)));

The default message queue configuration declares one queue, with 7 *entries_bits*, 3 *width_bits*, 2
*header_bits* and an *endpoint_id* of 0. All other queues will be disabled, as defined in::

  constant c_DUMMY_MT_MQUEUE_CONFIG : t_mt_mqueue_config := (
    slot_count  => 0,
    slot_config => (others => (c_DUMMY_MT_MQUEUE_SLOT)));

  constant c_DUMMY_MT_MQUEUE_SLOT : t_mt_mqueue_slot_config :=
    (0, 0, 1, x"0000_0000");

.. hint::
   Users are advised to also make use of the above *dummy* constants when describing disabled
   queues in their MT configurations.

See also the :ref:`demo projects<demo>` for actual examples of MT configuration.

.. _`hdl:inst`:

Mock Turtle Instantiation
=========================

.. highlight:: vhdl

A VHDL component declaration for the MT top-level entity is included in the **mock_turtle_pkg**::

  component mock_turtle_core is
    generic (
      g_CONFIG            : t_mt_config := c_DEFAULT_MT_CONFIG;
      g_SYSTEM_CLOCK_FREQ : integer     := 62500000;
      g_CPU0_IRAM_INITF   : string      := "none";
      g_CPU1_IRAM_INITF   : string      := "none";
      g_CPU2_IRAM_INITF   : string      := "none";
      g_CPU3_IRAM_INITF   : string      := "none";
      g_CPU4_IRAM_INITF   : string      := "none";
      g_CPU5_IRAM_INITF   : string      := "none";
      g_CPU6_IRAM_INITF   : string      := "none";
      g_CPU7_IRAM_INITF   : string      := "none";
      g_WITH_WHITE_RABBIT : boolean     := FALSE);
    port (
      clk_i           : in  std_logic;
      rst_n_i         : in  std_logic;
      cpu_ext_rst_n_i : in  std_logic_vector(g_CONFIG.cpu_count-1 downto 0) := (others=>'1');
      sp_master_o     : out t_wishbone_master_out;
      sp_master_i     : in  t_wishbone_master_in := c_DUMMY_WB_MASTER_IN;
      dp_master_o     : out t_wishbone_master_out_array(0 to g_CONFIG.cpu_count-1);
      dp_master_i     : in  t_wishbone_master_in_array(0 to g_CONFIG.cpu_count-1) := (others => c_DUMMY_WB_MASTER_IN);
      host_slave_i    : in  t_wishbone_slave_in;
      host_slave_o    : out t_wishbone_slave_out;
      rmq_endpoint_o  : out t_mt_rmq_endpoint_iface_out;
      rmq_endpoint_i  : in  t_mt_rmq_endpoint_iface_in := c_MT_RMQ_ENDPOINT_IFACE_IN_DEFAULT_VALUE;
      clk_ref_i       : in  std_logic := '0';
      tm_i            : in  t_mt_timing_if := c_DUMMY_MT_TIMING;
      gpio_o          : out std_logic_vector(31 downto 0);
      gpio_i          : in  std_logic_vector(31 downto 0) := (others => '0');
      hmq_in_irq_o    : out std_logic;
      hmq_out_irq_o   : out std_logic;
      notify_irq_o    : out std_logic;
      console_irq_o   : out std_logic);
  end component mock_turtle_core;

All generics and all input ports (except **clk_i** and **rst_n_i**) have default values if left
unconnected.

Generics
--------

g_CONFIG
  VHDL record of type **t_mt_config**, as described in `Mock Turtle Configuration`_.

g_SYSTEM_CLOCK_FREQ
  Frequency of system clock (provided on the *clk_i* input port). This is used internally for
  keeping track of time when `White Rabbit Support`_ is not enabled and it is also used by
  software to calculate delays.

g_CPUx_IRAM_INITF
  Memory initialization file for CPUx, to be included in the FPGA bitstream, if any.

g_WITH_WHITE_RABBIT
  Controls enabling of `White Rabbit Support`_.


Ports
-----

clk_i
  is the system clock.

rst_n_i
  is an active-low reset input, which is expected to be synchronous to the system clock.

sp_master_i, sp_master_o
  they form the *Shared Peripheral (SP)* Wishbone bus. Wishbone peripherals attached to these ports
  will be accessible by all MT soft-CPUs.

dp_master_i, dp_master_o
  they form the *Dedicated Peripheral (DP)* Wishbone buses. There is one DP Wishbone bus per soft
  CPU configured. Wishbone peripherals attached to these ports will be accessible only by their
  respective soft-CPU.

host_slave_i, host_slave_o
  they provide a Wishbone slave interface, to be attached to a controlling *host*, typically through
  some sort of bridge (eg. PCI to Wishbone). This interface provides access to the following
  Wishbone peripherals inside the MT:

  - Control/Status registers
  - Shared memory
  - per-CPU host message queues
  - Configuration ROM

  .. important::
    When mapping the MT address area to the host, users should keep in mind that the
    whole MT address space is 128KiB (0x20000).

**t_wishbone_master_out**, **t_wishbone_master_in** and their respective arrays, as well as
**t_wishbone_slave_out** and **t_wishbone_slave_in** are VHDL record types declared in the
***wishbone_pkg** of `OHWR general-cores`_, along with their default/dummy constants::

  type t_wishbone_master_out is record
    cyc : std_logic;
    stb : std_logic;
    adr : t_wishbone_address;
    sel : t_wishbone_byte_select;
    we  : std_logic;
    dat : t_wishbone_data;
  end record t_wishbone_master_out;

  subtype t_wishbone_slave_in is t_wishbone_master_out;

  type t_wishbone_slave_out is record
    ack   : std_logic;
    err   : std_logic;
    rty   : std_logic;
    stall : std_logic;
    dat   : t_wishbone_data;
  end record t_wishbone_slave_out;

  subtype t_wishbone_master_in is t_wishbone_slave_out;

  constant c_DUMMY_WB_ADDR : std_logic_vector(c_WISHBONE_ADDRESS_WIDTH-1 downto 0) :=
    (others => 'X');
  constant c_DUMMY_WB_DATA : std_logic_vector(c_WISHBONE_DATA_WIDTH-1 downto 0) :=
    (others => 'X');
  constant c_DUMMY_WB_SEL : std_logic_vector(c_WISHBONE_DATA_WIDTH/8-1 downto 0) :=
    (others => 'X');
  constant c_DUMMY_WB_SLAVE_IN   : t_wishbone_slave_in :=
    ('0', '0', c_DUMMY_WB_ADDR, c_DUMMY_WB_SEL, 'X', c_DUMMY_WB_DATA);
  constant c_DUMMY_WB_MASTER_OUT : t_wishbone_master_out := c_DUMMY_WB_SLAVE_IN;
  constant c_DUMMY_WB_SLAVE_OUT  : t_wishbone_slave_out :=
    ('1', '0', '0', '0', c_DUMMY_WB_DATA);
  constant c_DUMMY_WB_MASTER_IN  : t_wishbone_master_in := c_DUMMY_WB_SLAVE_OUT;

rmq_endpoint_o, rmq_endpoint_i
  These ports provide the bidirectional interface from each of the configured soft CPUs to their
  repspective end-points.

**t_mt_rmq_endpoint_iface_out** and **t_mt_rmq_endpoint_iface_in** are VHDL record types defined in
the **mock_turtle_pkg**::

  type t_mt_rmq_endpoint_iface_out is record
    src_out        : t_mt_stream_source_out_array2d;
    snk_out        : t_mt_stream_sink_out_array2d;
    src_config_out : t_mt_stream_config_out_array2d;
    snk_config_out : t_mt_stream_config_out_array2d;
  end record;

  type t_mt_rmq_endpoint_iface_in is record
    src_in        : t_mt_stream_source_in_array2d;
    snk_in        : t_mt_stream_sink_in_array2d;
    src_config_in : t_mt_stream_config_in_array2d;
    snk_config_in : t_mt_stream_config_in_array2d;
  end record;

*src_out*, *src_in*, *snk_out* and *snk_in* are used to transfer data to/from the end-point, while
the *config* signals are used to configure the end-points (e.g. to set the network destination for
the data).

**t_mt_stream_source_out_array2d**, **t_mt_stream_source_in_array2d**,
**t_mt_stream_sink_out_array2d**, **t_mt_stream_sink_in_array2d**,
**t_mt_stream_config_out_array2d** and **t_mt_stream_config_in_array2d** are two-dimensional arrays of
VHDL records, defined in **mock_turtle_pkg** (for the first array dimension) and **mt_mqueue_pkg**
(for the second array dimension and for the records themselves)::

  subtype t_maxcpu_range is natural range 0 to 7;
  subtype t_maxslot_range is natural range 0 to 7;

  -- defined in mock_turtle_pkg
  type t_mt_stream_sink_in_array2d is
    array(t_maxcpu_range) of t_mt_stream_sink_in_array(t_maxslot_range);
  type t_mt_stream_sink_out_array2d is
    array(t_maxcpu_range) of t_mt_stream_sink_out_array(t_maxslot_range);

  subtype t_mt_stream_source_in_array2d is t_mt_stream_sink_out_array2d;
  subtype t_mt_stream_source_out_array2d is t_mt_stream_sink_in_array2d;

  type t_mt_stream_config_in_array2d is
    array(t_maxcpu_range) of t_mt_stream_config_in_array(t_maxslot_range);
  type t_mt_stream_config_out_array2d is
    array(t_maxcpu_range) of t_mt_stream_config_out_array(t_maxslot_range);

  -- defined in mt_mqueue_pkg
  type t_mt_stream_sink_in_array is array(integer range<>) of t_mt_stream_sink_in;
  type t_mt_stream_sink_out_array is array(integer range<>) of t_mt_stream_sink_out;

  subtype t_mt_stream_source_in_array is t_mt_stream_sink_out_array;
  subtype t_mt_stream_source_out_array is t_mt_stream_sink_in_array;

  type t_mt_stream_sink_in is record
    data  : std_logic_vector(31 downto 0);
    hdr   : std_logic;
    valid : std_logic;
    last  : std_logic;
    error : std_logic;
  end record t_mt_stream_sink_in;

  type t_mt_stream_sink_out is record
    ready     : std_logic;
    pkt_ready : std_logic;
  end record t_mt_stream_sink_out;

  type t_mt_stream_config_out is record
    adr : std_logic_vector(10 downto 0);
    dat : std_logic_vector(31 downto 0);
    we  : std_logic;
  end record t_mt_stream_config_out;

  type t_mt_stream_config_in is record
    dat : std_logic_vector(31 downto 0);
  end record t_mt_stream_config_in;

  subtype t_mt_stream_source_in is t_mt_stream_sink_out;
  subtype t_mt_stream_source_out is t_mt_stream_sink_in;

For an example of the RMQ protocol and how the signals should be in order to write and read a frame
in the RMQ, have a look at the waveforms presented in `Remote Message Queues`_ section.

For an example of an end-point, please have a look at the provided Ethernet end-point, available
under *hdl/rtl/endpoint/mt_ep_ethernet_single.vhd*.

clk_ref_i
  When `White Rabbit Support`_ is enabled (via the **g_WITH_WHITE_RABBIT** generic), the White
  Rabbit 125MHz reference clock should be connected here.

tm_i
  All other timing signals from the `White Rabbit PTP Core`_ go to the **tm_i** input.

**t_mt_timing_if** is a VHDL record type, defined in **mock_turtle_pkg**, together with its
default/dummy constant as::

  type t_mt_timing_if is record
    link_up    : std_logic;
    dac_value  : std_logic_vector(23 downto 0);
    dac_wr     : std_logic;
    time_valid : std_logic;
    tai        : std_logic_vector(39 downto 0);
    cycles     : std_logic_vector(27 downto 0);
    aux_locked : std_logic_vector(7 downto 0);
  end record t_mt_timing_if;

  constant c_DUMMY_MT_TIMING : t_mt_timing_if := (
    link_up    => '0',
    dac_value  => (others => '0'),
    dac_wr     => '0',
    time_valid => '0',
    tai        => (others => '0'),
    cycles     => (others => '0'),
    aux_locked => (others => '0'));

For an explanation of these fields, please refer to the `White Rabbit PTP core`_ manual.

gpio_o, gpio_i
  These are the 32-bit inputs and outputs to the GPIO registers of each configured MT soft
  CPU. Inputs are delivered to all soft-CPUs while each bit of the output is a logic-OR of the
  respective GPIO output bit of each CPU.

hmq_in_irq_o
  Interrupt output to signal that one of the incoming host message queues is not empty.

hmq_out_irq_o
  Interrupt output to signal that one of the outgoing host message queues is not full.

notify_irq_o
  Interrupt output to signal that one of the soft-CPUs needs to notify the host.

console_irq_o
  Interrupt output to signal that one of the soft CPUs has pending data in its console output.

See also the :ref:`demo projects<demo>` for actual examples of MT instantiation.

Remote Message Queues
=====================

Remote Message Queues (RMQ) are used to allow MockTurtle CPUs to communicate
with the external world (e.g. over a communication link). In order to use them,
you have to connect the ``rmq_endpoint`` signals when you instantiate the
MockTurtle core. An example connection with 2 CPUs and each CPU having one slot
is shown below::

  p_multiple_rmq_assign : process (rmq_endpoint_out, rmq_snk_out, rmq_src_out, index)
  begin
    case index is
      when x"00" =>
        rmq_src_in <= rmq_endpoint_out.snk_out(0)(0);
        rmq_endpoint_in.snk_in(0)(0) <= rmq_src_out;
        rmq_endpoint_in.src_in(0)(0) <= rmq_snk_out(0)(0);

      when x"01" =>
        rmq_src_in <= rmq_endpoint_out.snk_out(1)(0);
        rmq_endpoint_in.snk_in(1)(0) <= rmq_src_out;
        rmq_endpoint_in.src_in(1)(0) <= rmq_snk_out(1)(0);

      when others =>
        rmq_src_in <= rmq_endpoint_out.snk_out(0)(0);
        rmq_endpoint_in.snk_in(0)(0) <= rmq_src_out;
        rmq_endpoint_in.src_in(0)(0) <= rmq_snk_out(0)(0);
    end case;
  end process p_multiple_rmq_assign;

  -- RMQ signals coming from MT
  rmq_snk_in(0)(0) <= rmq_endpoint_out.src_out(0)(0);
  rmq_snk_in(1)(0) <= rmq_endpoint_out.src_out(1)(0); 

.. figure:: ../img/write-to-rmq.png
   :name: fig-rmq-wr
   :align: center

   Write to RMQ.

:numref:`Figure %s <fig-rmq-wr>` shows how to send data to a MockTurtle CPU
through an RMQ, from the point of view of the remote device attached to the RMQ.
The `valid` flag should be asserted when either the `hdr` or the `dat` contains
valid data. The `last` flag is used to indicate the end of a frame. In case of
an error, you should assert the `error` flag (while maintaining the `valid` flag
asserted) to discard the full frame. The MockTurtle CPU will use `ready` to
indicate whether it can accept new data (`pkt_ready` is currently not used).

.. important:: Even though the `hdr` and `dat` fields in :numref:`fig-rmq-wr`
          are 32 bits, only the last 16 bits are valid, the upper 16 bits will
          always contain zeros.

Similar behavior can be seen also in the read from RMQ case as it is shown in
the below figure:

.. figure:: ../img/read-from-rmq.png
   :name: fig-rmq-rd
   :align: center

   Read from RMQ.

:numref:`Figure %s <fig-rmq-rd>` shows how to receive data from a MockTurtle CPU
through an RMQ, again from the point of view of the remote device attached to
the RMQ. The protocol is mostly the same as in the previous case. The only two
differences are that:

a) The `hdr` is always only one clock cycle long.
b) All 32 bits in the `hdr` field are used. If the firmware running in the
   MockTurtle CPU needs to send a longer header, the remaining bytes should be
   placed in the payload. Note that `dat` still only uses the lower 16 bits, as
   in the previous case.

White Rabbit Support
====================

Support for White Rabbit (WR) is controlled via the *g_WITH_WHITE_RABBIT* generic (see `Generics`_).

When WR is enabled, MT expects a reference clock on input port *clk_ref_i* and a *t_mt_timing_if*
record on input port *tm_i* (see `Ports`_).

Once WR is enabled and the WR link is up and running, each CPU core will have access to WR time via
the *TAI cycles* and *TAI seconds* registers in :ref:`wbgen_lr`.

.. _`hdl:sim`:

Simulation Testbenches
======================

.. highlight:: none

Several simulation testbenches are available under *hdl/testbench*. They all make use of the Mock
Turtle SystemVerilog simulation environment, which can be found under *hdl/testbench/include*.

All available testbenches are built using `Hdlmake`_ and Modelsim/Questa.

.. note::
   Building and running of the testbenches has been verified with Modelsim 10.2a and Questa 10.5c.

.. important:: Due to bugs present in `Hdlmake`_ release v3.0, it is necessary to use the *develop*
   branch of hdlmake, commit db4e1ab (or later).

Once the necessary tools are installed, building a particular testbench is simply a question of
running::

  $ cd <testbench_folder>
  $ hdlmake
  $ make

This will compile all the sources.

Running the testbench is simply done with::

  $ cd <testbench_folder>
  $ vsim -c -do run_ci.do

Alternatively, *vsim* can be launched interactively. In that case, users can launch the *run.do* file::

  simulator_prompt> do run.do

This will run the simulation and log *all* signals. When the execution is done, users can inspect
the signals, store them for future reference, display them as waveforms, etc.

mock_turtle_core
----------------

The mock_turtle_core testbench uses the top-level module of the MT as the Device Under Test
(DUT). It loads and executes a dedicated simulation verification program on the first CPU.

This program tests the following subsystems of the MT:

+ UART messages
+ Notification interrupts
+ Host message queues
+ Remote message queues

The expected output from the simulation is::

  App ID: 0x115790de
  Core count: 2
  UART MSG from core 0: #1 console
  UART MSG from core 0: #2 notify irq
  UART MSG from core 0: #3 hmq
  UART MSG from core 0: #4 rmq
  UART MSG from core 0: Done
  Simulation PASSED

.. note::
   The mock_turtle_core testbench expects an already compiled software binary under
   *tests/firmware/sim-verif*. Please compile the software prior to running the simulation.

mt_eth_ep
---------

The mt_eth_ep testbench uses the top-level module of the MT as the Device Under Test (DUT) and it
attaches an *mt_ep_ethernet_single* end-point to it. It loads and executes a dedicated simulation
verification program on the first CPU.

This program tests in particular the remote message queues and the mechanism to configure and control
end-points.

The expected output from the simulation is::

  App ID: 0x115790de
  Core count: 2
  UART MSG from core 0: RMQ UDP EP test
  802.1 DST [ff:ff:ff:ff:ff:ff] SRC: [00:00:00:00:00:00] Type = 0x0800 size = 50 F:(  )
  +000:  45 00 00 24 00 00 40 00-3c 11 83 ca c0 a8 5a 11
  +010:  c0 a8 5a ff 1e 61 30 39-00 10 00 00 de ad be ef
  +020:  00 00 01 23
  UART MSG from core 0: Recv id=21524110, val=fffffedc
  UART MSG from core 0: rx(1): fffffedc
  802.1 DST [ff:ff:ff:ff:ff:ff] SRC: [00:00:00:00:00:00] Type = 0x0800 size = 50 F:(  )
  +000:  45 00 00 24 00 00 40 00-3c 11 83 ca c0 a8 5a 11
  +010:  c0 a8 5a ff 1e 61 30 39-00 10 00 00 de ad be ef
  +020:  ff ff fe dc
  UART MSG from core 0: Recv id=21524110, val=123
  UART MSG from core 0: rx(2): 123
  Simulation PASSED

.. note::
   The mt_eth_ep testbench expects an already compiled software binary under
   *tests/firmware/rmq-udp-send*. Please compile the software prior to running the simulation.

spec_mt_demo
------------

The spec_mt_demo testbench uses the top-level module of the :ref:`demo:spec` as the Device Under
Test (DUT). It loads and executes a simple "hello world" demo program on the first CPU.

The purpose of this testbench is mainly to verify that the :ref:`demo:spec` HDL design is
working. To this end, the testbench simply instantiates the top-level and waits for the
firmware to print the "hello world" message on the serial console.

The expected output from the simulation is::

  App ID: 0xd331d331
  Core count: 2
  UART MSG from core 0: Hello World!
  UART MSG from core 0:

.. note::
   The spec_mt_demo testbench expects an already compiled software binary under
   *demos/hello_world*. Please compile the software prior to running the simulation.

svec_mt_demo
------------

The svec_mt_demo testbench uses the top-level module of the :ref:`demo:svec` as the Device Under
Test (DUT). It loads and executes a simple "hello world" demo program on the first CPU.

The purpose of this testbench is mainly to verify that the :ref:`demo:svec` HDL design is
working. To this end, the testbench simply instantiates the top-level and waits for the
firmware to print the "hello world" message on the serial console.

The expected output from the simulation is::

  App ID: 0xd330d330
  Core count: 2
  UART MSG from core 0: Hello World!
  UART MSG from core 0:

.. note::
   The svec_mt_demo testbench expects an already compiled software binary under
   *demos/hello_world*. Please compile the software prior to running the simulation.

.. _OHWR general-cores: https://www.ohwr.org/project/general-cores/wiki
.. _White Rabbit PTP Core: https://www.ohwr.org/project/wr-cores/wiki/Wrpc_core/wiki
.. _Hdlmake: https://www.ohwr.org/project/hdl-make/wiki
.. _uRV processor: https://www.ohwr.org/project/urv-core/wiki
.. _Git submodules: https://git-scm.com/book/en/Git-Tools-Submodules

.. highlight:: none
