# SPDX-License-Identifier: LGPL-2.1-or-later
#
# SPDX-FileCopyrightText: 2019 CERN

Summary: 	Mock Turtle
Name: 		mockturtle
Version: 	%{?_build_version}
License: 	LGPL-2.1-or-later
Release: 	1%{?dist}
URL: 		https://www.ohwr.org/projects/mock-turtle/

BuildRequires: make, gcc, git
#BuildRequires: wbgen2

Requires:	%{name}-lib
Requires:	%{name}-tools
Requires:	dkms-%{name}

Source0: %{name}-%{version}.tar.gz
Source1: CHANGELOG

%description
This package is a meta package that install the Mock Turtle distribution

%prep
%autosetup -n %{name}-%{version}

%build
make -C software/lib
make -C software/tools

%install
make -C software/include PREFIX=%{buildroot}/ install
make -C software/lib PREFIX=%{buildroot}/ install
make -C software/tools PREFIX=%{buildroot}/ install
make -C software/kernel PREFIX=%{buildroot}/ -f dkms.mk dkms_install

%changelog
%include %{SOURCE1}

#
# tools
#

%package tools
Summary: Mock Turtle tools
#Requires:

%description tools
This package contains the Mock Turtle tools

%files tools
%license LICENSES/LGPL-2.1-or-later.txt
/usr/local/bin/mockturtle-count
/usr/local/bin/lsmockturtle
/usr/local/bin/mockturtle-loader
/usr/local/bin/mockturtle-messages
/usr/local/bin/mockturtle-cpu-restart
/usr/local/bin/mockturtle-smem
/usr/local/bin/mockturtle-ping
/usr/local/bin/mockturtle-variable
/usr/local/bin/mockturtle-buffer
/usr/local/bin/mockturtle-gdbserver

#
# dkms
#

%package -n dkms-%{name}
Summary:	Mock Turtle DKMS module
Requires:	dkms

%description -n dkms-%{name}
The dkms-mockturtle kernel module.

%post -n dkms-%{name}
dkms add -m %{name} -v %{version} --rpm_safe_upgrade
dkms build -m %{name} -v %{version} --rpm_safe_upgrade
dkms install -m %{name} -v %{version} --rpm_safe_upgrade

%preun -n dkms-%{name}
dkms remove -m %{name} -v %{version} --rpm_safe_upgrade --all ||:

%files -n dkms-%{name}
%license LICENSES/GPL-2.0.txt
/usr/src/%{name}-%{version}/*

#
# lib
#

%package lib
Summary: Mock Turtle runtime library

%description lib
The mockturtle-libs package contains

%files lib
%license LICENSES/LGPL-2.1-or-later.txt
/usr/local/lib/libmockturtle.so.*

#
# libstatic
#

%package libstatic
Summary: Mock Turtle static library

%description libstatic
The mockturtle-libs package contains

%files libstatic
%license LICENSES/LGPL-2.1-or-later.txt
/usr/local/lib/libmockturtle.a

#
# devel
#

%package devel
Summary: Libraries and header files for Mock Turtle development
Group: Development/Libraries
Requires: %{name}-lib = %{version}-%{release}

%description devel
The mockturtle-devel package contains the header files and mockturtle library
necessary for developing programs using mockturtle.

%files devel
%license LICENSES/LGPL-2.1-or-later.txt
%license LICENSES/LGPL-2.1-or-later.txt
%license LICENSES/BSD-3-Clause.txt
/usr/local/lib/libmockturtle.so
/usr/local/include/mockturtle/libmockturtle.h
/usr/local/include/mockturtle/mockturtle.h
/usr/local/include/mockturtle/hw/mockturtle_addresses.h
/usr/local/include/mockturtle/hw/mockturtle_cpu_csr.h
/usr/local/include/mockturtle/hw/mockturtle_cpu_lr.h
/usr/local/include/mockturtle/hw/mockturtle_endpoint.h
/usr/local/include/mockturtle/hw/mockturtle_queue.h
